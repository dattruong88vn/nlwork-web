import React, { useState, useEffect } from "react";
import MwPrivateRoute from "app/components/routes/MwPrivateRoute";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import MainLayout from "app/modules/layouts/MainLayout";
import {
  privateRoutes,
  publicRoutes,
  publicPaths,
  privatePaths,
} from "app/config/router/routes";
import SlashPage from "app/modules/splash";
import ErrorPage from "app/modules/error";
import OfflineNotice from "app/components/offlineNotice";

function App() {
  const [isOffline, setIsOffline] = useState(false);

  useEffect(() => {
    window.addEventListener("offline", () => {
      setIsOffline(true);
      sessionStorage.setItem("live_status", "offline");
    });
    window.addEventListener("online", () => {
      setIsOffline(false);
      sessionStorage.setItem("live_status", "online");
    });
  });

  return (
    <>
      <OfflineNotice isOffline={isOffline} />

      <Router>
        <Switch>
          <Route exact path={privatePaths}>
            <MainLayout>
              <Switch>
                {privateRoutes.map(
                  (route) => route.dashLayout && <MwPrivateRoute {...route} />
                )}
              </Switch>
            </MainLayout>
          </Route>

          <Route exact path={["/"]}>
            <MwPrivateRoute component={SlashPage} path="/" exact />
          </Route>

          <Route exact path={publicPaths}>
            {publicRoutes.map((route) => (
              <Route {...route} />
            ))}
          </Route>

          <Route path="*">
            <Switch>
              <Route component={ErrorPage} />
            </Switch>
          </Route>
        </Switch>
      </Router>
    </>
  );
}

export default App;
