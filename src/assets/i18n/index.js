import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";

import commonUs from "./us/common";
import commonVn from "./vn/common";
import loginPageVn from "./vn/loginpage";
import loginPageUs from "./us/loginpage";
import pageTitleUs from "./us/pageTitle";
import pageTitleVn from "./vn/pageTitle";
import sidebarVn from "./vn/sidebar";
import sidebarUs from "./us/sidebar";
import headerUs from "./us/header";
import headerVn from "./vn/header";
import notificationUs from "./us/notification";
import notificationVn from "./vn/notification";

i18n.use(LanguageDetector).init({
  resources: {
    "en-US": {
      common: commonUs,
      loginPage: loginPageUs,
      pageTitle: pageTitleUs,
      sidebar: sidebarUs,
      header: headerUs,
      notification: notificationUs,
    },
    "vi-VN": {
      common: commonVn,
      loginPage: loginPageVn,
      pageTitle: pageTitleVn,
      sidebar: sidebarVn,
      header: headerVn,
      notification: notificationVn,
    },
  },
  fallbackLng: "vi-VN",
  debug: true,
  ns: ["common", "loginPage", "pageTitle", "sidebar", "header"],
  defaultNS: "common",

  keySeparator: false,
  interpolation: {
    escapeValue: false,
    formatSeparator: ",",
  },

  react: {
    wait: true,
  },
});

export default i18n;
