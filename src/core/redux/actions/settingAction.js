export const UPDATE_NOTIFY_REQUESTING = "UPDATE_NOTIFY_REQUESTING";
export const UPDATE_NOTIFY_SUCCESS = "UPDATE_NOTIFY_SUCCESS";
export const UPDATE_NOTIFY_ERROR = "UPDATE_NOTIFY_ERROR";

export const UPDATE_LANGUAGE_REQUESTING = "UPDATE_LANGUAGE_REQUESTING";
export const UPDATE_LANGUAGE_SUCCESS = "UPDATE_LANGUAGE_SUCCESS";
export const UPDATE_LANGUAGE_ERROR = "UPDATE_LANGUAGE_ERROR";

export const UPDATE_TIMEZONE_REQUESTING = "UPDATE_TIMEZONE_REQUESTING";
export const UPDATE_TIMEZONE_SUCCESS = "UPDATE_TIMEZONE_SUCCESS";
export const UPDATE_TIMEZONE_ERROR = "UPDATE_TIMEZONE_ERROR";

export const updateNotifyRequest = (payload) => {
  return { type: UPDATE_NOTIFY_REQUESTING, payload };
};

export const updateLanguageRequest = (payload) => {
  return { type: UPDATE_LANGUAGE_REQUESTING, payload };
};

export const updateTimezoneRequest = (payload) => {
  return { type: UPDATE_TIMEZONE_REQUESTING, payload };
};
