export const LOGIN_USER_REQUESTING = "LOGIN_USER_REQUESTING";
export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";
export const LOGIN_USER_ERROR = "LOGIN_USER_ERROR";

export const SEND_EMAIL_REQUESTING = "SEND_EMAIL_REQUESTING";
export const SEND_EMAIL_SUCCESS = "SEND_EMAIL_SUCCESS";
export const SEND_EMAIL_ERROR = "SEND_EMAIL_ERROR";

export const userLoginRequest = (payload) => {
  return { type: LOGIN_USER_REQUESTING, payload };
};

export const userResendRequest = (payload) => {
  return { type: SEND_EMAIL_REQUESTING, payload };
};
