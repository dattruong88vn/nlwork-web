export const TASK_CREATE_REQUESTING = "TASK_CREATE_REQUESTING";
export const TASK_CREATE_SUCCESS = "TASK_CREATE_SUCCESS";
export const TASK_CREATE_ERROR = "TASK_CREATE_ERROR";

export const TASK_QUICK_ADD = "TASK_QUICK_ADD";
export const TASK_QUICK_REMOVE = "TASK_QUICK_REMOVE";

export const taskCreateRequest = (payload) => {
  return { type: TASK_CREATE_REQUESTING, payload };
};

export const taskCreateSuccess = (payload) => {
  return { type: TASK_CREATE_SUCCESS, payload };
};

export const taskQuickAdd = (payload) => {
  return { type: TASK_QUICK_ADD, payload };
};

export const taskQuickRemove = (payload) => {
  return { type: TASK_QUICK_REMOVE, payload };
};
