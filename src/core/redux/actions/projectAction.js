export const PROJECT_FAVORITES_REQUESTING = "PROJECT_FAVORITES_REQUESTING";
export const PROJECT_FAVORITES_SUCCESS = "PROJECT_FAVORITES_SUCCESS";
export const PROJECT_FAVORITES_ERROR = "PROJECT_FAVORITES_ERROR";

export const PROJECT_ADD_FEATURED_SUCCESS = "PROJECT_ADD_FEATURE";
export const PROJECT_ADD_FEATURED_REQUESTING = "PROJECT_ADD_FEATURE";

export const projectFavoriteRequest = (payload) => {
  return { type: PROJECT_FAVORITES_REQUESTING, payload };
};

export const projectFeaturedAdd = (payload) => {
  return { type: PROJECT_ADD_FEATURED_REQUESTING, payload };
};
