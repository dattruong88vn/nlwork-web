export const FETCH_USER_INFO_REQUESTING = "FETCH_USER_INFO_REQUESTING";
export const FETCH_USER_INFO_SUCCESS = "FETCH_USER_INFO_SUCCESS";
export const FETCH_USER_INFO_ERROR = "FETCH_USER_INFO_ERROR";

export const UPDATE_AVATAR_REQUESTING = "UPDATE_AVATAR_REQUESTING";
export const UPDATE_AVATAR_SUCCESS = "UPDATE_AVATAR_SUCCESS";
export const UPDATE_AVATAR_ERROR = "UPDATE_AVATAR_ERROR";

export const UPDATE_INFO_REQUESTING = "UPDATE_INFO_REQUESTING";
export const UPDATE_INFO_SUCCESS = "UPDATE_INFO_SUCCESS";
export const UPDATE_INFO_ERROR = "UPDATE_INFO_ERROR";

export const CHANGE_PASSWORD_REQUESTING = "CHANGE_PASSWORD_REQUESTING";
export const CHANGE_PASSWORD_SUCCESS = "CHANGE_PASSWORD_SUCCESS";
export const CHANGE_PASSWORD_ERROR = "CHANGE_PASSWORD_ERROR";

export const UPDATE_USER_STORED = "UPDATE_USER_STORED";

export const CHANGE_AVATAR = "CHANGE_AVATAR";

export const updateAvatarRequest = (payload) => {
  return { type: UPDATE_AVATAR_REQUESTING, payload };
};

export const updateInfoRequest = (payload) => {
  return { type: UPDATE_INFO_REQUESTING, payload };
};

export const fetchUserRequest = (payload) => {
  return { type: FETCH_USER_INFO_REQUESTING, payload };
};

export const changeUserPwdRequest = (payload) => {
  return { type: CHANGE_PASSWORD_REQUESTING, payload };
};

export const updateUserInfoStored = (payload) => {
  return { type: UPDATE_USER_STORED, payload };
};
