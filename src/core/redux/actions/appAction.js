export const SET_APP_LOADING = "SET_APP_LOADING";
export const FETCH_APP_DATA = "FETCH_APP_DATA";

export function loadingAction(payload) {
  return { type: SET_APP_LOADING, payload };
}

export function fetchAppData(payload) {
  return { type: FETCH_APP_DATA, payload };
}
