export const EDIT_TASK_GROUP_REQUESTING = "EDIT_TASK_GROUP_REQUESTING";
export const EDIT_TASK_GROUP_SUCCESS = "EDIT_TASK_GROUP_SUCCESS";
export const EDIT_TASK_GROUP_ERROR = "EDIT_TASK_GROUP_ERROR";

export const DELETE_TASK_GROUP_REQUESTING = "DELETE_TASK_GROUP_REQUESTING";
export const DELETE_TASK_GROUP_SUCCESS = "DELETE_TASK_GROUP_SUCCESS";
export const DELETE_TASK_GROUP_ERROR = "DELETE_TASK_GROUP_ERROR";

export const editTaskGroupRequest = (payload) => {
  return { type: EDIT_TASK_GROUP_REQUESTING, payload };
};

export const deleteTaskGroupRequest = (payload) => {
  return { type: DELETE_TASK_GROUP_REQUESTING, payload };
};
