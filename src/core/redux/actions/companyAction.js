export const COMPANY_SETTING_REQUESTING = "COMPANY_SETTING_REQUESTING";
export const COMPANY_SETTING_SUCCESS = "COMPANY_SETTING_SUCCESS";
export const COMPANY_SETTING_ERROR = "COMPANY_SETTING_ERROR";

export const COMPANY_MEMBERS_REQUESTING = "COMPANY_MEMBERS_REQUESTING";
export const COMPANY_MEMBERS_SUCCESS = "COMPANY_MEMBERS_SUCCESS";
export const COMPANY_MEMBERS_ERROR = "COMPANY_MEMBERS_ERROR";

export const COMPANY_ADD_PROJECT = "COMPANY_ADD_PROJECT";

export function companySettingRequest(payload) {
  return { type: COMPANY_SETTING_REQUESTING, payload };
}

export function companyMemberRequest() {
  return { type: COMPANY_MEMBERS_REQUESTING };
}

export function companyAddProject(payload) {
  return { type: COMPANY_ADD_PROJECT, payload };
}
