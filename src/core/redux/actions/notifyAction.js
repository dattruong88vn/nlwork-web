export const LIST_NOTIFY_REQUESTING = "LIST_NOTIFY_REQUESTING";
export const LIST_NOTIFY_SUCCESS = "LIST_NOTIFY_SUCCESS";
export const LIST_NOTIFY_ERROR = "LIST_NOTIFY_ERROR";

export const MARK_READ_REQUESTING = "MARK_READ_REQUESTING";
export const MARK_READ_SUCCESS = "MARK_READ_SUCCESS";
export const MARK_READ_ERROR = "MARK_READ_ERROR";

export const MARK_ALL_READ_REQUESTING = "MARK_ALL_READ_REQUESTING";
export const MARK_ALL_READ_SUCCESS = "MARK_ALL_READ_SUCCESS";
export const MARK_ALL_READ_ERROR = "MARK_ALL_READ_ERROR";

export const DELETE_NOTIFY_REQUESTING = "DELETE_NOTIFY_REQUESTING";
export const DELETE_NOTIFY_SUCCESS = "DELETE_NOTIFY_SUCCESS";
export const DELETE_NOTIFY_ERROR = "DELETE_NOTIFY_ERROR";

export const NOTIFY_UNREAD_REQUESTING = "NOTIFY_UNREAD_REQUESTING";
export const NOTIFY_UNREAD_SUCCESS = "NOTIFY_UNREAD_SUCCESS";
export const NOTIFY_UNREAD_ERROR = "NOTIFY_UNREAD_ERROR";

export const LIST_NOTIFY_SCROLL_REQUESTING = "LIST_NOTIFY_SCROLL_REQUESTING";
export const LIST_NOTIFY_SCROLL_SUCCESS = "LIST_NOTIFY_SCROLL_SUCCESS";
export const LIST_NOTIFY_SCROLL_ERROR = "LIST_NOTIFY_SCROLL_ERROR";

export const SET_NOTIFICATION_UNREAD = "SET_NOTIFICATION_UNREAD";
export const SET_NOTIFICATION_LIST = "SET_NOTIFICATION_LIST";

export function listNotifyRequest(payload) {
  return { type: LIST_NOTIFY_REQUESTING, payload };
}

export function markNotifyRequest(payload) {
  return {
    type: MARK_READ_REQUESTING,
    payload,
  };
}

export function markAllNotifyRequest() {
  return {
    type: MARK_ALL_READ_REQUESTING,
  };
}

export function deleteNotifyRequest(payload) {
  return {
    type: DELETE_NOTIFY_REQUESTING,
    payload,
  };
}

export function notifyUnreadRequest(payload) {
  return {
    type: NOTIFY_UNREAD_REQUESTING,
  };
}

export function listNotifyScrollRequest(payload) {
  return {
    type: LIST_NOTIFY_SCROLL_REQUESTING,
    payload,
  };
}

export function setNotificationUnread(payload) {
  return { type: SET_NOTIFICATION_UNREAD, payload };
}

export function setNotificationList(payload) {
  return { type: SET_NOTIFICATION_LIST, payload };
}
