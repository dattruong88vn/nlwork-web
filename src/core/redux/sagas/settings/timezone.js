import { call, put } from "redux-saga/effects";
import { putFormData } from "core/utils/APIUtils";
import { CODE_SUCCESS, UPDATE_USER_SETTING } from "app/const/Api";

import {
  UPDATE_TIMEZONE_SUCCESS,
  UPDATE_TIMEZONE_ERROR,
} from "core/redux/actions/settingAction";

export function* timezoneUpdate({ payload }) {
  const response = yield call(putFormData, UPDATE_USER_SETTING, payload, {});
  if (response.meta.code === CODE_SUCCESS) {
    yield put({ type: UPDATE_TIMEZONE_SUCCESS, payload: response.data });
  } else {
    yield put({
      type: UPDATE_TIMEZONE_ERROR,
      payload: response.meta.message,
    });
  }
}
