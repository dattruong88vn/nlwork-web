import { takeLatest } from "redux-saga/effects";
import {
  UPDATE_NOTIFY_REQUESTING,
  UPDATE_LANGUAGE_REQUESTING,
  UPDATE_TIMEZONE_REQUESTING,
} from "../../actions/settingAction";

import { notifyUpdate } from "./notification";
import { languageUpdate } from "./language";
import { timezoneUpdate } from "./timezone";

export function* settingWatcher() {
  yield takeLatest(UPDATE_NOTIFY_REQUESTING, notifyUpdate);
  yield takeLatest(UPDATE_LANGUAGE_REQUESTING, languageUpdate);
  yield takeLatest(UPDATE_TIMEZONE_REQUESTING, timezoneUpdate);
}
