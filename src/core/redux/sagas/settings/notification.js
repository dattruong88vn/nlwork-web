import { call, put } from "redux-saga/effects";
import { putFormData } from "core/utils/APIUtils";
import { CODE_SUCCESS, NOTIFY_UPDATE } from "app/const/Api";

import {
  UPDATE_NOTIFY_SUCCESS,
  UPDATE_NOTIFY_ERROR,
} from "core/redux/actions/settingAction";

export function* notifyUpdate({ payload }) {
  const response = yield call(putFormData, NOTIFY_UPDATE, payload, {});
  if (response.meta.code === CODE_SUCCESS) {
    yield put({ type: UPDATE_NOTIFY_SUCCESS, payload: response.data });
  } else {
    yield put({
      type: UPDATE_NOTIFY_ERROR,
      payload: response.meta.message,
    });
  }
}
