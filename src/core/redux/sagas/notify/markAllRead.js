import { call, put } from "redux-saga/effects";
import { putWithToken } from "core/utils/APIUtils";
import { NOTIFICATION_MARK_ALL_READ, CODE_SUCCESS } from "app/const/Api";

import {
  MARK_ALL_READ_SUCCESS,
  MARK_ALL_READ_ERROR,
} from "core/redux/actions/notifyAction";

export function* markAllNotifyRead() {
  const response = yield call(putWithToken, NOTIFICATION_MARK_ALL_READ);
  if (response.meta.code === CODE_SUCCESS) {
    yield put({
      type: MARK_ALL_READ_SUCCESS,
      payload: response.data,
    });
  } else {
    yield put({
      type: MARK_ALL_READ_ERROR,
      payload: response.meta.message,
    });
  }
}
