import { takeLatest } from "redux-saga/effects";
import {
  LIST_NOTIFY_REQUESTING,
  MARK_ALL_READ_REQUESTING,
  DELETE_NOTIFY_REQUESTING,
  MARK_READ_REQUESTING,
  NOTIFY_UNREAD_REQUESTING,
  LIST_NOTIFY_SCROLL_REQUESTING,
} from "../../actions/notifyAction";

import { listNotifyFetch } from "./listNotify";
import { markNotifyRead } from "./markRead";
import { markAllNotifyRead } from "./markAllRead";
import { deleteNotify } from "./delete";
import { unreadFetch } from "./unread";
import { listNotifyScrollFetch } from "./listScrollMore";

export function* notifyWatcher() {
  yield takeLatest(LIST_NOTIFY_REQUESTING, listNotifyFetch);
  yield takeLatest(MARK_READ_REQUESTING, markNotifyRead);
  yield takeLatest(MARK_ALL_READ_REQUESTING, markAllNotifyRead);
  yield takeLatest(DELETE_NOTIFY_REQUESTING, deleteNotify);
  yield takeLatest(NOTIFY_UNREAD_REQUESTING, unreadFetch);
  yield takeLatest(LIST_NOTIFY_SCROLL_REQUESTING, listNotifyScrollFetch);
}
