import { call, put } from "redux-saga/effects";
import { fetchWithToken } from "core/utils/APIUtils";
import { NOTIFICATION_LIST, CODE_SUCCESS } from "app/const/Api";
import { LIMIT_LIST_NOTIFY } from "app/const/App";

import {
  LIST_NOTIFY_SUCCESS,
  LIST_NOTIFY_ERROR,
} from "core/redux/actions/notifyAction";

export function* listNotifyFetch({ payload }) {
  const response = yield call(
    fetchWithToken,
    NOTIFICATION_LIST,
    { ...payload, limit: LIMIT_LIST_NOTIFY },
    {}
  );
  if (response.meta.code === CODE_SUCCESS) {
    yield put({ type: LIST_NOTIFY_SUCCESS, payload: response.data });
  } else {
    yield put({
      type: LIST_NOTIFY_ERROR,
      payload: response.meta.message,
    });
  }
}
