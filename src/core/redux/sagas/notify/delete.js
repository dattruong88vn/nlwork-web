import { call } from "redux-saga/effects";
import { deleteWithToken } from "core/utils/APIUtils";
import { NOTIFICATION_DELETE } from "app/const/Api";

export function* deleteNotify({ payload }) {
  yield call(deleteWithToken, NOTIFICATION_DELETE, payload, {});
}
