import { call } from "redux-saga/effects";
import { putWithToken } from "core/utils/APIUtils";
import { NOTIFICATION_MARK_READ } from "app/const/Api";

export function* markNotifyRead({ payload }) {
  yield call(putWithToken, NOTIFICATION_MARK_READ, payload, {});
}
