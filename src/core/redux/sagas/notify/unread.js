import { call, put } from "redux-saga/effects";
import { fetchWithToken } from "core/utils/APIUtils";
import { NOTIFICATION_UNREAD, CODE_SUCCESS } from "app/const/Api";

import {
  NOTIFY_UNREAD_SUCCESS,
  NOTIFY_UNREAD_ERROR,
} from "core/redux/actions/notifyAction";

export function* unreadFetch() {
  const response = yield call(fetchWithToken, NOTIFICATION_UNREAD);
  if (response.meta.code === CODE_SUCCESS) {
    yield put({
      type: NOTIFY_UNREAD_SUCCESS,
      payload: response.data.total,
    });
  } else {
    yield put({
      type: NOTIFY_UNREAD_ERROR,
      payload: response.meta.message,
    });
  }
}
