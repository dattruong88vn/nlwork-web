import { call, put } from "redux-saga/effects";
import { fetchWithToken } from "core/utils/APIUtils";
import { COMPANY_MEMBERS, CODE_SUCCESS } from "app/const/Api";

import {
  COMPANY_MEMBERS_SUCCESS,
  COMPANY_MEMBERS_ERROR,
} from "core/redux/actions/companyAction";

export function* companyMemberFetch({ payload }) {
  const response = yield call(fetchWithToken, COMPANY_MEMBERS, payload, {});
  if (response.meta.code === CODE_SUCCESS) {
    yield put({ type: COMPANY_MEMBERS_SUCCESS, payload: response.data });
  } else {
    yield put({
      type: COMPANY_MEMBERS_ERROR,
      payload: response.meta.message,
    });
  }
}
