import { takeLatest } from "redux-saga/effects";
import {
  COMPANY_SETTING_REQUESTING,
  COMPANY_MEMBERS_REQUESTING,
} from "../../actions/companyAction";

import { companySettingFetch } from "./settings";
import { companyMemberFetch } from "./members";

export function* companyWatcher() {
  yield takeLatest(COMPANY_SETTING_REQUESTING, companySettingFetch);
  yield takeLatest(COMPANY_MEMBERS_REQUESTING, companyMemberFetch);
}
