import { call, put } from "redux-saga/effects";
import { fetchWithToken } from "core/utils/APIUtils";
import { COMPANY_SETTING, CODE_SUCCESS } from "app/const/Api";

import {
  COMPANY_SETTING_SUCCESS,
  COMPANY_SETTING_ERROR,
} from "core/redux/actions/companyAction";

export function* companySettingFetch({ payload }) {
  const response = yield call(fetchWithToken, COMPANY_SETTING, payload, {});
  if (response.meta.code === CODE_SUCCESS) {
    yield put({ type: COMPANY_SETTING_SUCCESS, payload: response.data });
  } else {
    yield put({
      type: COMPANY_SETTING_ERROR,
      payload: response.meta.message,
    });
  }
}
