import { call, put } from "redux-saga/effects";
import { fetchSaga } from "core/utils/APIUtils";
import { RESEND_ACTIVATE, CODE_SUCCESS } from "app/const/Api";

import {
  SEND_EMAIL_SUCCESS,
  SEND_EMAIL_ERROR,
} from "core/redux/actions/authAction";

export function* userResendMail({ payload }) {
  const response = yield call(fetchSaga, RESEND_ACTIVATE, payload);
  yield put({ type: SEND_EMAIL_SUCCESS });
  if (response.meta.code === CODE_SUCCESS) {
    yield put({ type: SEND_EMAIL_SUCCESS, payload: response.data });
  } else {
    yield put({ type: SEND_EMAIL_ERROR, payload: response.meta.message });
  }
}
