import { call, put } from "redux-saga/effects";
import { postWithToken } from "core/utils/APIUtils";
import { KEY_TOKEN } from "app/const/App";
import { LOGIN, CODE_SUCCESS } from "app/const/Api";
import { fetchAllData } from "core/redux/sagas/app";

import { LOGIN_USER_ERROR } from "core/redux/actions/authAction";

export function* userLogin({ payload }) {
  const response = yield call(postWithToken, LOGIN, payload);
  if (response.meta.code === CODE_SUCCESS) {
    localStorage.setItem("token", response.data.access_token);
    yield fetchAllData(true);
  } else {
    yield put({
      type: LOGIN_USER_ERROR,
      payload: {
        email: response.meta.message,
        password: response.meta.message,
      },
    });
    localStorage.removeItem(KEY_TOKEN);
  }
}
