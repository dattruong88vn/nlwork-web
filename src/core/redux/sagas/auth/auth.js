import { takeLatest } from "redux-saga/effects";
import {
  LOGIN_USER_REQUESTING,
  SEND_EMAIL_REQUESTING,
} from "../../actions/authAction";

import { userLogin } from "./login";
import { userResendMail } from "./resend";

export function* authWatcher() {
  yield takeLatest(LOGIN_USER_REQUESTING, userLogin);
  yield takeLatest(SEND_EMAIL_REQUESTING, userResendMail);
}
