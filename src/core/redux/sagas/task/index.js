import { takeLatest } from "redux-saga/effects";

import { TASK_CREATE_REQUESTING } from "core/redux/actions/taskAction";

import { taskCreate } from "./create";

export function* taskWatcher() {
  yield takeLatest(TASK_CREATE_REQUESTING, taskCreate);
}
