import { call, put } from "redux-saga/effects";
import { postFormData } from "core/utils/APIUtils";
import { CODE_SUCCESS, CREATE_TASK } from "app/const/Api";

import {
  TASK_CREATE_SUCCESS,
  TASK_CREATE_ERROR,
} from "core/redux/actions/taskAction";

export function* taskCreate({ payload }) {
  const response = yield call(postFormData, CREATE_TASK, payload);
  if (response.meta.code === CODE_SUCCESS) {
    yield put({ type: TASK_CREATE_SUCCESS, payload: response.data });
  } else {
    yield put({
      type: TASK_CREATE_ERROR,
      payload: response.meta.message,
    });
  }
}
