import { all } from "redux-saga/effects";
import { authWatcher } from "./auth/auth";
import { projectWatcher } from "./project";
import { notifyWatcher } from "./notify";
import { companyWatcher } from "./company";
import { userWatcher } from "./user";
import { settingWatcher } from "./settings";
import { taskWatcher } from "./task";
import { taskGroupWatcher } from "./taskGroup";
import { appWatcher } from "./app";

export default function* rootSaga() {
  yield all([
    appWatcher(),
    authWatcher(),
    projectWatcher(),
    notifyWatcher(),
    companyWatcher(),
    userWatcher(),
    settingWatcher(),
    taskWatcher(),
    taskGroupWatcher(),
  ]);
}
