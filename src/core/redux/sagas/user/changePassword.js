import { call, put } from "redux-saga/effects";
import { putWithToken } from "core/utils/APIUtils";
import { CODE_SUCCESS, USER_CHANGE_PASSWORD } from "app/const/Api";

import {
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_ERROR,
} from "core/redux/actions/userAction";

export function* changeUserPassword({ payload }) {
  const response = yield call(putWithToken, USER_CHANGE_PASSWORD, payload, {});
  if (response.meta.code === CODE_SUCCESS) {
    yield put({ type: CHANGE_PASSWORD_SUCCESS, payload: response.data });
  } else {
    yield put({
      type: CHANGE_PASSWORD_ERROR,
      payload: response.meta.message,
    });
  }
}
