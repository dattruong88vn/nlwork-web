import { takeLatest } from "redux-saga/effects";
import {
  UPDATE_AVATAR_REQUESTING,
  UPDATE_INFO_REQUESTING,
  FETCH_USER_INFO_REQUESTING,
  CHANGE_PASSWORD_REQUESTING,
} from "../../actions/userAction";

import { updateAvatar } from "./updateAvatar";
import { updateUserInfo } from "./updateInfo";
import { fetchUserInfo } from "./retrieve";
import { changeUserPassword } from "./changePassword";

export function* userWatcher() {
  yield takeLatest(UPDATE_AVATAR_REQUESTING, updateAvatar);
  yield takeLatest(UPDATE_INFO_REQUESTING, updateUserInfo);
  yield takeLatest(FETCH_USER_INFO_REQUESTING, fetchUserInfo);
  yield takeLatest(CHANGE_PASSWORD_REQUESTING, changeUserPassword);
}
