import { call, put } from "redux-saga/effects";
import { putWithToken } from "core/utils/APIUtils";
import { CODE_SUCCESS, UPDATE_INFORMATION } from "app/const/Api";

import {
  UPDATE_INFO_SUCCESS,
  UPDATE_INFO_ERROR,
} from "core/redux/actions/userAction";

export function* updateUserInfo({ payload }) {
  const response = yield call(putWithToken, UPDATE_INFORMATION, payload, {});
  if (response.meta.code === CODE_SUCCESS) {
    yield put({ type: UPDATE_INFO_SUCCESS, payload: response.data });
  } else {
    yield put({
      type: UPDATE_INFO_ERROR,
      payload: response.meta.message,
    });
  }
}
