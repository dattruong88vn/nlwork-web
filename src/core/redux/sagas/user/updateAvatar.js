import { call, put } from "redux-saga/effects";
import { putFormData } from "core/utils/APIUtils";
import { UPDATE_AVATAR, CODE_SUCCESS } from "app/const/Api";

import {
  UPDATE_AVATAR_SUCCESS,
  UPDATE_AVATAR_ERROR,
  CHANGE_AVATAR,
} from "core/redux/actions/userAction";

export function* updateAvatar({ payload }) {
  const response = yield call(putFormData, UPDATE_AVATAR, payload, {});
  if (response.meta.code === CODE_SUCCESS) {
    yield put({ type: UPDATE_AVATAR_SUCCESS, payload: response.data });
    yield put({ type: CHANGE_AVATAR, payload: response.data.link });
  } else {
    yield put({
      type: UPDATE_AVATAR_ERROR,
      payload: response.meta.message,
    });
  }
}
