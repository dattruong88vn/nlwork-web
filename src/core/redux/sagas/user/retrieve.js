import { call, put } from "redux-saga/effects";
import { fetchWithToken } from "core/utils/APIUtils";
import { CODE_SUCCESS, FETCH_INFORMATION } from "app/const/Api";

import {
  FETCH_USER_INFO_SUCCESS,
  FETCH_USER_INFO_ERROR,
} from "core/redux/actions/userAction";

export function* fetchUserInfo({ payload }) {
  const response = yield call(fetchWithToken, FETCH_INFORMATION, payload, {});
  if (response.meta.code === CODE_SUCCESS) {
    yield put({ type: FETCH_USER_INFO_SUCCESS, payload: response.data });
  } else {
    yield put({
      type: FETCH_USER_INFO_ERROR,
      payload: response.meta.message,
    });
  }
}
