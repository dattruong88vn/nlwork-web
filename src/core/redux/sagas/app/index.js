import { call, all, put, takeLatest } from "redux-saga/effects";
import { fetchWithToken } from "core/utils/APIUtils";
import {
  COMPANY_SETTING,
  FETCH_INFORMATION,
  NOTIFICATION_LIST,
  COMPANY_MEMBERS,
  NOTIFICATION_UNREAD,
  PROJECT_FEATURED,
} from "app/const/Api";

import { LIMIT_LIST_NOTIFY } from "app/const/App";

import { FETCH_USER_INFO_SUCCESS } from "core/redux/actions/userAction";
import { PROJECT_FAVORITES_SUCCESS } from "core/redux/actions/projectAction";

import {
  COMPANY_MEMBERS_SUCCESS,
  COMPANY_SETTING_SUCCESS,
} from "core/redux/actions/companyAction";

import {
  LIST_NOTIFY_SUCCESS,
  NOTIFY_UNREAD_SUCCESS,
} from "core/redux/actions/notifyAction";
import { SET_APP_LOADING, FETCH_APP_DATA } from "core/redux/actions/appAction";
import { LOGIN_USER_SUCCESS } from "core/redux/actions/authAction";

export function* appWatcher() {
  yield takeLatest(FETCH_APP_DATA, fetchAllData);
}

// Declare action will call here
const actions = [
  {
    description: "Fetch user information",
    method: fetchWithToken,
    url: FETCH_INFORMATION,
    params: { fields: "project,company,setting,setting_notification" },
  },
  {
    description: "Fetch company setting",
    method: fetchWithToken,
    url: COMPANY_SETTING,
    params: { fields: "project,company,setting,list_project" },
  },
  {
    description: "Fetch notification list",
    method: fetchWithToken,
    url: NOTIFICATION_LIST,
    params: { limit: LIMIT_LIST_NOTIFY },
  },
  {
    description: "Fetch list company member",
    method: fetchWithToken,
    url: COMPANY_MEMBERS,
    params: {},
  },
  {
    description: "Fetch total notification unread",
    method: fetchWithToken,
    url: NOTIFICATION_UNREAD,
    params: {},
  },
  {
    description: "Fetch list featured project ",
    method: fetchWithToken,
    url: PROJECT_FEATURED,
    params: { limit: 5 },
  },
];

// Function to map all action to array
const appCallAction = (actions = []) => {
  return actions.map((action) => {
    const { method, url, params = {}, opts } = action;
    return call(method, url, params, opts);
  });
};

/**
 * Fetch all data can reusable in one times fetch
 */
export function* fetchAllData(isFirstLogin) {
  // need to declare name with exactly position
  const [users, company, notify, members, unread, project] = yield all(
    appCallAction(actions)
  );

  yield all([
    put({ type: FETCH_USER_INFO_SUCCESS, payload: users.data }),
    put({ type: COMPANY_SETTING_SUCCESS, payload: company.data }),
    put({ type: LIST_NOTIFY_SUCCESS, payload: notify.data }),
    put({ type: COMPANY_MEMBERS_SUCCESS, payload: members.data }),
    put({
      type: NOTIFY_UNREAD_SUCCESS,
      payload: unread.data.total,
    }),
    put({ type: PROJECT_FAVORITES_SUCCESS, payload: project.data }),
  ]);

  if (!isFirstLogin) {
    yield put({ type: SET_APP_LOADING, payload: false });
  } else {
    yield put({ type: SET_APP_LOADING, payload: false });
    yield put({ type: LOGIN_USER_SUCCESS, payload: {} });
  }
}
