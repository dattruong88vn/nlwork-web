import { call, put } from "redux-saga/effects";
import { putFormData } from "core/utils/APIUtils";
import { CODE_SUCCESS, TASK_GROUP_UPDATE } from "app/const/Api";

import {
  EDIT_TASK_GROUP_ERROR,
  EDIT_TASK_GROUP_SUCCESS,
} from "core/redux/actions/taskGroupAction";

export function* editTaskGroup({ payload }) {
  const response = yield call(putFormData, TASK_GROUP_UPDATE, payload);

  if (response.meta.code === CODE_SUCCESS) {
    yield put({ type: EDIT_TASK_GROUP_SUCCESS, payload: response.data });
  } else {
    yield put({
      type: EDIT_TASK_GROUP_ERROR,
      payload: response.meta.message,
    });
  }
}
