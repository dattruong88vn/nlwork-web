import { call, put } from "redux-saga/effects";
import { deleteWithToken } from "core/utils/APIUtils";
import { CODE_SUCCESS, TASK_GROUP_DELETE } from "app/const/Api";

import {
  DELETE_TASK_GROUP_SUCCESS,
  DELETE_TASK_GROUP_ERROR,
} from "core/redux/actions/taskGroupAction";

export function* deleteTaskGroup({ payload }) {
  const response = yield call(deleteWithToken, TASK_GROUP_DELETE, payload);

  if (response.meta.code === CODE_SUCCESS) {
    yield put({ type: DELETE_TASK_GROUP_SUCCESS, payload: response.data });
  } else {
    yield put({
      type: DELETE_TASK_GROUP_ERROR,
      payload: response.meta.message,
    });
  }
}
