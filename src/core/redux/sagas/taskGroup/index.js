import { takeLatest } from "redux-saga/effects";
import {
  DELETE_TASK_GROUP_REQUESTING,
  EDIT_TASK_GROUP_REQUESTING,
} from "../../actions/taskGroupAction";

import { deleteTaskGroup } from "./delete";
import { editTaskGroup } from "./edit";

export function* taskGroupWatcher() {
  yield takeLatest(EDIT_TASK_GROUP_REQUESTING, editTaskGroup);
  yield takeLatest(DELETE_TASK_GROUP_REQUESTING, deleteTaskGroup);
}
