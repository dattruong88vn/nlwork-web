import { call, put } from "redux-saga/effects";
import { fetchWithToken } from "core/utils/APIUtils";
import { PROJECT_LIST, CODE_SUCCESS } from "app/const/Api";

import {
  PROJECT_FAVORITES_SUCCESS,
  PROJECT_FAVORITES_ERROR,
} from "core/redux/actions/projectAction";

export function* prjFavoriteFetch({ payload }) {
  const response = yield call(fetchWithToken, PROJECT_LIST, payload, {});
  if (response.meta.code === CODE_SUCCESS) {
    yield put({ type: PROJECT_FAVORITES_SUCCESS, payload: response.data });
  } else {
    yield put({
      type: PROJECT_FAVORITES_ERROR,
      payload: response.meta.message,
    });
  }
}
