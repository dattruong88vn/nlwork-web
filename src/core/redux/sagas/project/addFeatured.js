import { put } from "redux-saga/effects";

import { PROJECT_ADD_FEATURED_SUCCESS } from "core/redux/actions/projectAction";

export function prjAddFeatured({ payload }) {
  put({
    type: PROJECT_ADD_FEATURED_SUCCESS,
    payload: { pin: payload.pin, data: payload.data },
  });
}
