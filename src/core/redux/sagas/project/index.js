import { takeLatest } from "redux-saga/effects";
import {
  PROJECT_ADD_FEATURED_REQUESTING,
  PROJECT_FAVORITES_REQUESTING,
} from "../../actions/projectAction";

import { prjFavoriteFetch } from "./favorites";
import { prjAddFeatured } from "./addFeatured";

export function* projectWatcher() {
  yield takeLatest(PROJECT_FAVORITES_REQUESTING, prjFavoriteFetch);
  yield takeLatest(PROJECT_ADD_FEATURED_REQUESTING, prjAddFeatured);
}
