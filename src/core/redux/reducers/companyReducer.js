import {
  COMPANY_SETTING_REQUESTING,
  COMPANY_SETTING_SUCCESS,
  COMPANY_SETTING_ERROR,
  COMPANY_MEMBERS_REQUESTING,
  COMPANY_MEMBERS_SUCCESS,
  COMPANY_MEMBERS_ERROR,
  COMPANY_ADD_PROJECT,
} from "../actions/companyAction";

const initialState = {
  loading: false,
};

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case COMPANY_SETTING_REQUESTING:
      return { ...state, loading: true };
    case COMPANY_SETTING_SUCCESS:
      return { ...state, loading: false, settings: action.payload };
    case COMPANY_SETTING_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case COMPANY_MEMBERS_REQUESTING:
      return { ...state, loading: true };
    case COMPANY_MEMBERS_SUCCESS:
      return { ...state, loading: false, members: action.payload };
    case COMPANY_MEMBERS_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case COMPANY_ADD_PROJECT:
      return {
        ...state,
        settings: {
          ...state.settings,
          list_project: [...state.settings.list_project, action.payload],
        },
      };
    default:
      return state;
  }
};

export default appReducer;
