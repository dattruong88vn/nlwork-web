import {
  EDIT_TASK_GROUP_REQUESTING,
  EDIT_TASK_GROUP_SUCCESS,
  EDIT_TASK_GROUP_ERROR,
  DELETE_TASK_GROUP_REQUESTING,
  DELETE_TASK_GROUP_SUCCESS,
  DELETE_TASK_GROUP_ERROR,
} from "../actions/taskGroupAction";

const initialState = {
  loading: false,
  loadingDelete: false,
};

const taskReducer = (state = initialState, action) => {
  switch (action.type) {
    case EDIT_TASK_GROUP_REQUESTING:
      return { ...state, loading: true };
    case EDIT_TASK_GROUP_SUCCESS:
      return { ...state, loading: false };
    case EDIT_TASK_GROUP_ERROR:
      return { ...state, loading: false, error: action.payload };
    case DELETE_TASK_GROUP_REQUESTING:
      return { ...state, loadingDelete: true };
    case DELETE_TASK_GROUP_SUCCESS:
      return { ...state, loadingDelete: false, idDeleted: action.payload.id };
    case DELETE_TASK_GROUP_ERROR:
      return { ...state, loadingDelete: false, error: action.payload };
    default:
      return state;
  }
};

export default taskReducer;
