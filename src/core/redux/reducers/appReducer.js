import { SET_APP_LOADING } from "../actions/appAction";

const initialState = {
  loading: true,
};

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_APP_LOADING:
      return { ...state, loading: action.payload };
    default:
      return state;
  }
};

export default appReducer;
