import {
  TASK_CREATE_REQUESTING,
  TASK_CREATE_SUCCESS,
  TASK_CREATE_ERROR,
  TASK_QUICK_ADD,
  TASK_QUICK_REMOVE,
} from "../actions/taskAction";

const initialState = {
  createLoading: false,
  createdTask: false,
};

const taskReducer = (state = initialState, action) => {
  switch (action.type) {
    case TASK_CREATE_REQUESTING:
      return {
        ...state,
        newTask: null,
        createLoading: true,
        createdTask: false,
      };
    case TASK_CREATE_SUCCESS:
      return {
        ...state,
        newTask: action.payload.task,
        createLoading: false,
        createdTask: true,
      };
    case TASK_CREATE_ERROR:
      return {
        ...state,
        newTask: null,
        createLoading: false,
        error: action.payload,
      };
    case TASK_QUICK_ADD:
      return {
        ...state,
        quickAddTask: action.payload,
      };
    case TASK_QUICK_REMOVE:
      return {
        ...state,
        quickAddTask: null,
      };
    default:
      return state;
  }
};

export default taskReducer;
