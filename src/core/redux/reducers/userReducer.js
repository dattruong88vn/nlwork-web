import {
  FETCH_USER_INFO_REQUESTING,
  FETCH_USER_INFO_SUCCESS,
  FETCH_USER_INFO_ERROR,
  UPDATE_AVATAR_REQUESTING,
  UPDATE_AVATAR_SUCCESS,
  UPDATE_AVATAR_ERROR,
  UPDATE_INFO_REQUESTING,
  UPDATE_INFO_SUCCESS,
  UPDATE_INFO_ERROR,
  CHANGE_PASSWORD_REQUESTING,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_ERROR,
  CHANGE_AVATAR,
  UPDATE_USER_STORED,
} from "../actions/userAction";

const initialState = {
  loading: false,
  newAvatar: null,
  updateInfoSuccess: false,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USER_INFO_REQUESTING:
      return { ...state, loading: true };

    case FETCH_USER_INFO_SUCCESS:
      return {
        ...state,
        userInfo: action.payload || {},
        loading: false,
      };

    case FETCH_USER_INFO_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case UPDATE_AVATAR_REQUESTING:
      return { ...state, loading: true };

    case UPDATE_AVATAR_SUCCESS:
      return {
        ...state,
        newAvatar: action.payload,
        loading: false,
      };

    case UPDATE_AVATAR_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case UPDATE_INFO_REQUESTING:
      return { ...state, loading: true, updateInfoSuccess: false };

    case UPDATE_INFO_SUCCESS:
      return {
        ...state,
        updateInfoSuccess: true,
        loading: false,
      };

    case UPDATE_INFO_ERROR:
      return {
        ...state,
        updateInfoSuccess: false,
        loading: false,
        errorUpdate: action.payload,
      };
    case CHANGE_PASSWORD_REQUESTING:
      return {
        ...state,
        loading: true,
        changePwdSuccess: false,
        errorChangePwd: null,
      };

    case CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        loading: false,
        changePwdSuccess: true,
      };

    case CHANGE_PASSWORD_ERROR:
      return {
        ...state,
        loading: false,
        errorChangePwd: action.payload,
      };
    case CHANGE_AVATAR: {
      return {
        ...state,
        userInfo: { ...state.userInfo, avatar: action.payload },
      };
    }
    case UPDATE_USER_STORED: {
      return {
        ...state,
        userInfo: { ...state.userInfo, ...action.payload },
      };
    }
    default:
      return state;
  }
};

export default userReducer;
