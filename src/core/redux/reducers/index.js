import { combineReducers } from "redux";
import authReducer from "./authReducer";
import appReducer from "./appReducer";
import projectReducer from "./projectReducer";
import notifyReducer from "./notifyReducer";
import companyReducer from "./companyReducer";
import userReducer from "./userReducer";
import settingReducer from "./settingReducer";
import taskReducer from "./taskReducer";
import taskGroupReducer from "./taskGroupReducer";

export default combineReducers({
  auth: authReducer,
  app: appReducer,
  project: projectReducer,
  notify: notifyReducer,
  company: companyReducer,
  user: userReducer,
  settings: settingReducer,
  task: taskReducer,
  taskGroup: taskGroupReducer,
});
