import {
  UPDATE_NOTIFY_REQUESTING,
  UPDATE_NOTIFY_SUCCESS,
  UPDATE_NOTIFY_ERROR,
  UPDATE_LANGUAGE_REQUESTING,
  UPDATE_LANGUAGE_SUCCESS,
  UPDATE_LANGUAGE_ERROR,
  UPDATE_TIMEZONE_REQUESTING,
  UPDATE_TIMEZONE_SUCCESS,
  UPDATE_TIMEZONE_ERROR,
} from "../actions/settingAction";

const initialState = {
  loading: false,
  success: false,
};

const settingReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_NOTIFY_REQUESTING:
      return { ...state, loading: true, success: false };
    case UPDATE_NOTIFY_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
      };
    case UPDATE_NOTIFY_ERROR:
      return {
        ...state,
        loading: false,
        error: action?.payload || "",
      };
    case UPDATE_LANGUAGE_REQUESTING:
      return { ...state, loading: true, updateLangSuccess: false };
    case UPDATE_LANGUAGE_SUCCESS:
      return {
        ...state,
        loading: false,
        updateLangSuccess: true,
      };
    case UPDATE_LANGUAGE_ERROR:
      return {
        ...state,
        loading: false,
        error: action?.payload || "",
      };
    case UPDATE_TIMEZONE_REQUESTING:
      return { ...state, loadingTz: true, updateTzSuccess: false };
    case UPDATE_TIMEZONE_SUCCESS:
      return {
        ...state,
        loadingTz: false,
        updateTzSuccess: true,
      };
    case UPDATE_TIMEZONE_ERROR:
      return {
        ...state,
        loadingTz: false,
        error: action?.payload || "",
      };
    default:
      return state;
  }
};

export default settingReducer;
