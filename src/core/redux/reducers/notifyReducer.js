import {
  LIST_NOTIFY_REQUESTING,
  LIST_NOTIFY_SUCCESS,
  LIST_NOTIFY_ERROR,
  MARK_READ_REQUESTING,
  MARK_READ_SUCCESS,
  MARK_READ_ERROR,
  MARK_ALL_READ_REQUESTING,
  MARK_ALL_READ_SUCCESS,
  MARK_ALL_READ_ERROR,
  DELETE_NOTIFY_REQUESTING,
  DELETE_NOTIFY_SUCCESS,
  DELETE_NOTIFY_ERROR,
  NOTIFY_UNREAD_REQUESTING,
  NOTIFY_UNREAD_SUCCESS,
  NOTIFY_UNREAD_ERROR,
  LIST_NOTIFY_SCROLL_REQUESTING,
  LIST_NOTIFY_SCROLL_SUCCESS,
  LIST_NOTIFY_SCROLL_ERROR,
  SET_NOTIFICATION_UNREAD,
  SET_NOTIFICATION_LIST,
} from "../actions/notifyAction";

const initialState = {
  loading: false,
  listNotify: [],
  unread: 0,
  fetchedAllList: false,
};

const notifyReducer = (state = initialState, action) => {
  switch (action.type) {
    case LIST_NOTIFY_REQUESTING:
      return { ...state, loading: true };

    case LIST_NOTIFY_SUCCESS:
      return {
        ...state,
        listNotify: action.payload || [],
        loading: false,
      };

    case LIST_NOTIFY_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case MARK_READ_REQUESTING:
      return { ...state, loading: true };

    case MARK_READ_SUCCESS: {
      const newListNotify = [...state.listNotify];
      const indexOfNotify = newListNotify.findIndex(
        (item) => item.id === action.payload.id
      );
      newListNotify[indexOfNotify].status = 0;
      return { ...state, loading: false, listNotify: newListNotify };
    }

    case MARK_READ_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case MARK_ALL_READ_REQUESTING:
      return { ...state, loading: true };

    case MARK_ALL_READ_SUCCESS:
      const listNotifyAllRead = [...state.listNotify].map((item) => ({
        ...item,
        status: 0,
      }));
      return {
        ...state,
        loading: false,
        listNotify: listNotifyAllRead,
        unread: 0,
      };
    case MARK_ALL_READ_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case DELETE_NOTIFY_REQUESTING:
      return { ...state, loading: true };

    case DELETE_NOTIFY_SUCCESS:
      const listNotifyDeleted = [...state.listNotify].filter(
        (item) => item.id !== action.payload.id
      );
      return {
        ...state,
        loading: false,
        listNotify: listNotifyDeleted,
      };

    case DELETE_NOTIFY_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case NOTIFY_UNREAD_REQUESTING:
      return { ...state, loading: true, unread: 0 };

    case NOTIFY_UNREAD_SUCCESS:
      return {
        ...state,
        loading: false,
        unread: action.payload || 0,
      };

    case NOTIFY_UNREAD_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case LIST_NOTIFY_SCROLL_REQUESTING:
      return { ...state, scrollLoading: true };

    case LIST_NOTIFY_SCROLL_SUCCESS:
      let listNotifyMore = [...state.listNotify];
      let fetchedAllList = true;
      if (action.payload.length) {
        fetchedAllList = false;
        listNotifyMore = [...state.listNotify, ...action.payload];
      }
      return {
        ...state,
        scrollLoading: false,
        fetchedAllList,
        listNotify: listNotifyMore,
      };

    case LIST_NOTIFY_SCROLL_ERROR:
      return {
        ...state,
        scrollLoading: false,
        error: action.payload,
      };

    case SET_NOTIFICATION_UNREAD:
      return { ...state, unread: action.payload || 0 };

    case SET_NOTIFICATION_LIST:
      return { ...state, listNotify: action.payload || [] };

    default:
      return state;
  }
};

export default notifyReducer;
