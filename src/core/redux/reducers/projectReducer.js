import _ from "lodash";

import {
  PROJECT_FAVORITES_REQUESTING,
  PROJECT_FAVORITES_SUCCESS,
  PROJECT_FAVORITES_ERROR,
  PROJECT_ADD_FEATURED_SUCCESS,
} from "../actions/projectAction";

const initialState = {
  loading: false,
  loadingList: false,
  featuredProjects: null,
  projects: [],
  totalFeaturedPrj: null,
};

const projectReducer = (state = initialState, action) => {
  switch (action.type) {
    case PROJECT_FAVORITES_REQUESTING:
      return { ...state, loading: true };
    case PROJECT_FAVORITES_SUCCESS:
      return {
        ...state,
        featuredProjects: action.payload?.projects || [],
        totalFeaturedPrj: action.payload.total,
        loading: false,
      };
    case PROJECT_FAVORITES_ERROR:
      return {
        ...state,
        loading: false,
        error: action?.payload || "",
      };
    case PROJECT_ADD_FEATURED_SUCCESS:
      return {
        ...state,
        featuredProjects: [
          ...handleAddFeatured(
            action.payload.pin,
            state.featuredProjects,
            action.payload.data
          ),
        ],
      };
    default:
      return state;
  }
};

const handleAddFeatured = (pin, data, newData) => {
  if (pin) return [newData, ...data];
  return _.filter(data, (x) => x.id !== newData.id);
};

export default projectReducer;
