import {
  LOGIN_USER_REQUESTING,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR,
  SEND_EMAIL_REQUESTING,
  SEND_EMAIL_SUCCESS,
  SEND_EMAIL_ERROR,
} from "../actions/authAction";

const initialState = {
  loading: false,
  userInfo: {},
  error: "",
  logged: false,
  resendLoading: false,
  resendError: "",
  resendSuccess: false,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_USER_REQUESTING:
      return {
        ...state,
        loading: true,
        forgotError: "",
        error: "",
      };
    case LOGIN_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        logged: true,
        userInfo: action.payload,
        error: "",
      };
    case LOGIN_USER_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case SEND_EMAIL_REQUESTING:
      return {
        ...state,
        resendLoading: true,
        resendError: "",
      };
    case SEND_EMAIL_SUCCESS:
      return {
        ...state,
        resendLoading: false,
        resendError: "",
        resendSuccess: true,
      };
    case SEND_EMAIL_ERROR:
      return {
        ...state,
        resentLoading: false,
        resentError: action.payload,
      };
    default:
      return state;
  }
};

export default authReducer;
