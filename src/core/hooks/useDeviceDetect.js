import { useEffect, useState } from "react";

function useDeviceDetect(props) {
  const [isAndroid, setIsAndroid] = useState(false);
  const [isIos, setIsIos] = useState(false);

  useEffect(() => {
    const userAgent = window?.navigator && navigator.userAgent;
    const deviceIos = !window.MSStream && /iPad|iPhone|iPod/.test(userAgent);
    const deviceAndroid = userAgent.match(
      /Mobile|Windows Phone|Lumia|Android|webOS|iPhone|iPod|Blackberry|PlayBook|BB10|Opera Mini|\bCrMo\/|Opera Mobi/i
    );

    deviceIos && setIsIos(true);
    deviceAndroid && setIsAndroid(true);
  }, []);

  return { isIos, isAndroid };
}

useDeviceDetect.propTypes = {};

export default useDeviceDetect;
