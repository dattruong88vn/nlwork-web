import { useTranslation } from "react-i18next";

function useTimeAgo(prevDate) {
  const { t } = useTranslation();
  const timeAgo = () => {
    const diff = Number(new Date()) - prevDate * 1000;
    const minute = 60 * 1000;
    const hour = minute * 60;
    const day = hour * 24;
    const month = day * 30;
    const year = day * 365;
    switch (true) {
      case diff < minute:
        const seconds = Math.round(diff / 1000);
        return seconds < 1
          ? t("now")
          : `${seconds} ${seconds > 1 ? t("seconds") : t("second")} ${t(
              "ago"
            )}`;
      case diff < hour:
        return Math.round(diff / minute) + t("minutes_ago");
      case diff < day:
        return Math.round(diff / hour) + t("hours_ago");
      case diff < month:
        return Math.round(diff / day) + t("days_ago");
      case diff < year:
        return Math.round(diff / month) + t("months_ago");
      case diff > year:
        return Math.round(diff / year) + t("years_ago");
      default:
        return "";
    }
  };
  return timeAgo();
}

export default useTimeAgo;
