import useClickOutside from "./useClickOutside";
import useDebounce from "./useDebounce";
import useFetch from "./useFetch";
import { useSiteTitle } from "./useSiteTitle";
import useTimeAgo from "./useTimeAgo";
import useTimestamp from "./useTimestamp";
import useToast from "./useToast";
import usePermissions from "./usePermissions";
import { useWindowDimensions } from "./useWindowDimension";
import useDeviceDetect from "./useDeviceDetect";

export {
  useClickOutside,
  useDebounce,
  useFetch,
  useSiteTitle,
  useTimeAgo,
  useTimestamp,
  useToast,
  useWindowDimensions,
  usePermissions,
  useDeviceDetect,
};
