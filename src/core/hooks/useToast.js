import React from "react";
import { ToastContext } from "app/components/toast/ToastProvider";

function useToast() {
  const toastHelpers = React.useContext(ToastContext);
  return toastHelpers;
}

export default useToast;
