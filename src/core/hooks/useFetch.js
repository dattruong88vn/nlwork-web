import React from "react";
import { KEY_TOKEN, I18N_KEY } from "app/const/App";
import { API_ENDPOINT } from "app/const/Api";
import { stringify } from "query-string";

const useFetch = (url, data, opts) => {
  const token = localStorage.getItem(KEY_TOKEN);
  const language = localStorage.getItem(I18N_KEY);

  const optionsFetch = {
    "cache-control": "no-cache",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "Accept-Language": language,
      Authorization: "Bearer " + token,
      ...opts,
    },
  };

  const [response, setResponse] = React.useState(null);
  const [error, setError] = React.useState(null);
  const [isLoading, setIsLoading] = React.useState(false);
  React.useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const res = await fetch(
          API_ENDPOINT + url + "?" + stringify(data),
          optionsFetch
        );
        const json = await res.json();
        setResponse(json);
        setIsLoading(false);
      } catch (error) {
        setError(error);
      }
    };
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return { response, error, isLoading };
};

export default useFetch;
