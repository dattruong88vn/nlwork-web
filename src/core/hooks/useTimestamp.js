import { useSelector } from "react-redux";
import moment from "moment-timezone";

function useTimestamp() {
  const { company_info } = useSelector((state) => state.user.userInfo);
  const getTime = (time, isTimestamp = false, format = "YYYY/MM/DD") => {
    let result;
    if (isTimestamp) {
      result = moment.tz(time * 1000, company_info?.timezone);
    } else {
      result = moment.tz(time.format(format), company_info?.timezone).unix();
    }
    return result;
  };

  return { getTime };
}

export default useTimestamp;
