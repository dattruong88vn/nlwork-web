import { useCallback } from "react";
import { useSelector } from "react-redux";
import { find } from "lodash";
import {
  TASK_PERMISSION_DEFAULT,
  TASK_PERMISSION_MANAGE,
  TASK_MANAGE_KEY,
  PROJECT_CREATE_KEY,
  PROJECT_UPDATE_KEY,
  TASK_IN_PROJECT_PERMISSION_DEFAULT,
  CONVERSATION_MANAGE_KEY,
  CONVERSATION_PERMISSION_DEFAULT,
  CONVERSATION_PERMISSION_MANAGE,
  DRIVE_PERMISSION_DEFAULT,
  DRIVE_MANAGE_KEY,
  DRIVE_PERMISSION_MANAGE,
  MILESTONE_MANAGE_KEY,
  MILESTONE_PERMISSION_DEFAULT,
  MILESTONE_PERMISSION_MANAGE,
} from "app/const/Permissions";

const TASK_ROLE_FOLLOWER = "task.follower";
const TASK_ROLE_ASSIGNEE = "task.assignee";
const TASK_ROLE_CREATOR = "task.creator";

/**
 * Custom hooks for check permission
 * @returns {function} getPermission
 */
function usePermissions() {
  const { id: userId, company_permissions: companyPermissions } = useSelector(
    (state) => state.user.userInfo
  );
  const { settings } = useSelector((state) => state.company);

  const getPermission = useCallback((data = {}, type = "TASK") => {
    switch (type) {
      case "TASK":
        return permissionTask(data);
      case "TASK_IN_PROJECT":
        return permissionTaskInProject(data);
      case "PROJECT":
        return permissionProject(data);
      case "DELETE_PROJECT":
        return canDeleteProject(data);
      case "CREATE_PROJECT":
        return canCreateProject();
      case "UPDATE_PROJECT":
        return canUpdateProject(data);
      case "CONVERSATION":
        return permissionConversation(data);
      case "DRIVE":
        return permissionDrive(data);
      case "MILESTONE":
        return permissionMilestone(data);
      default:
        break;
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getProject = (projectId) => {
    const listProject = settings?.list_project || [];
    return find(listProject, ["id", projectId]);
  };

  const permissionTask = (data) => {
    let role;
    let permission = TASK_PERMISSION_DEFAULT;
    const permissionFromProject = {};
    const { assignee, created_by, followers, project_id } = data;
    const { task_permissions, permissions } = getProject(project_id);

    if (permissions.includes(TASK_MANAGE_KEY)) return TASK_PERMISSION_MANAGE;

    if (created_by === userId) {
      role = TASK_ROLE_CREATOR;
    } else {
      if (followers?.includes(userId)) role = TASK_ROLE_FOLLOWER;
      if (assignee?.includes(userId)) role = TASK_ROLE_ASSIGNEE;
    }

    if (role) {
      const listPermission = find(task_permissions, ["value", role])
        .permissions;

      for (const key in listPermission) {
        permissionFromProject[key.split(".")[1]] = listPermission[key];
      }

      return { ...permission, ...permissionFromProject };
    }

    return { ...permission };
  };

  const permissionProject = (data) => {};

  const canDeleteProject = (data) => {
    const { created_by } = data;
    if (created_by === userId) return true;
    return false;
  };

  const canCreateProject = () => {
    if (companyPermissions?.includes(PROJECT_CREATE_KEY)) return true;
    return false;
  };

  const canUpdateProject = (data) => {
    const { project_id } = data;
    const { permissions } = getProject(project_id);

    if (permissions?.includes(PROJECT_UPDATE_KEY)) return true;
    return false;
  };

  const permissionTaskInProject = (data) => {
    let role;
    let permission = TASK_IN_PROJECT_PERMISSION_DEFAULT;
    const permissionFromProject = {};
    const { assignee, created_by, followers, project_id } = data;
    const { task_permissions, permissions } = getProject(project_id);

    if (permissions.includes(TASK_MANAGE_KEY)) return TASK_PERMISSION_MANAGE;

    if (created_by === userId) {
      role = TASK_ROLE_CREATOR;
    } else {
      if (followers?.includes(userId)) role = TASK_ROLE_FOLLOWER;
      if (assignee?.includes(userId)) role = TASK_ROLE_ASSIGNEE;
    }

    if (role) {
      const listPermission = find(task_permissions, ["value", role])
        .permissions;

      for (const key in listPermission) {
        permissionFromProject[key.split(".")[1]] = listPermission[key];
      }

      return { ...permission, ...permissionFromProject };
    }

    return { ...permission };
  };

  const permissionConversation = (data) => {
    const { project_id, created_by } = data;
    const { permissions } = getProject(project_id);

    if (
      permissions?.includes(CONVERSATION_MANAGE_KEY) ||
      created_by === userId
    ) {
      return CONVERSATION_PERMISSION_MANAGE;
    }
    return CONVERSATION_PERMISSION_DEFAULT;
  };

  const permissionDrive = (data) => {
    const { project_id, created_by } = data;
    const { permissions } = getProject(project_id);

    if (permissions?.includes(DRIVE_MANAGE_KEY) || created_by === userId) {
      return DRIVE_PERMISSION_MANAGE;
    }
    return DRIVE_PERMISSION_DEFAULT;
  };

  const permissionMilestone = (data) => {
    const { project_id, created_by } = data;
    const { permissions } = getProject(project_id);

    if (permissions?.includes(MILESTONE_MANAGE_KEY) || created_by === userId) {
      return MILESTONE_PERMISSION_MANAGE;
    }
    return MILESTONE_PERMISSION_DEFAULT;
  };

  return { getPermission };
}

export default usePermissions;
