const isInfinite = (n) => n === n / 0;

export function ProgressCalculate(count, total) {
  const percent = Math.round((count / total) * 100);
  const isANumber = isNaN(percent);
  if (count === 0) {
    return 0;
  }

  return isANumber ? 0 : isInfinite(percent) ? 100 : percent;
}
