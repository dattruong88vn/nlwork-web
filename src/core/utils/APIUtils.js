import { API_ENDPOINT } from "app/const/Api";
import { KEY_TOKEN, LIVE_STATUS } from "app/const/App";
import { stringify } from "query-string";
import { loadingAction } from "core/redux/actions/appAction";

/**
 * Get data from API by GET
 *
 * @param {String} url
 * @param {*} dispatch
 */
export function getFetch(url, dispatch, offLoading) {
  const token = localStorage.getItem(KEY_TOKEN);
  if (!offLoading) {
    dispatch(loadingAction(true));
  }
  return new Promise((resolve, reject) => {
    fetch(API_ENDPOINT + url, {
      method: "GET",
      "cache-control": "no-cache",
      headers: {
        token,
      },
    })
      .then((response) => {
        //update moẻ to check status
        if (!offLoading) {
          dispatch(loadingAction(false));
        }
        return response.json();
      })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.log("Err:", error);
        reject("Oops! Please try again");
      });
  }).catch((err) => {
    if (!offLoading) {
      dispatch(loadingAction(false));
    }
    // alert(err);
    console.log("Err: ", err);
  });
}

/**
 * Submit data to API by POST. Using this function for action INSERT
 *
 * @param {String} url
 * @param array data
 * @param {*} dispatch
 */
export function postFetch(url, data, dispatch) {
  const token = localStorage.getItem(KEY_TOKEN);

  dispatch(loadingAction(true));
  return new Promise((resolve, reject) => {
    console.log(token);
    fetch(API_ENDPOINT + url, {
      method: "POST",
      "cache-control": "no-cache",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        token,
      },
      body: JSON.stringify(data),
    })
      .then((response) => {
        //update moẻ to check status
        dispatch(loadingAction(false));
        return response.json();
      })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.log("Err:", error);
        // alert(`error API ${error}`);
        reject("Oops! Please try again");
      });
  }).catch((err) => {
    // alert(err);
    console.log("Err: ", err);
  });
}

export function postFetchWithoutToken(url, data, dispatch) {
  dispatch(loadingAction(true));
  return new Promise((resolve, reject) => {
    fetch(API_ENDPOINT + url, {
      method: "POST",
      "cache-control": "no-cache",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => {
        //update moẻ to check status
        dispatch(loadingAction(false));
        return response.json();
      })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.log("Err:".error);
        // alert(`error API ${error}`);
        reject("Oops! Please try again");
      });
  }).catch((err) => {
    // alert(err);
    console.log("Err: ", err);
  });
}

/**
 * Submit data to API by PATCH. Using this function for action UPDATE
 *
 * @param String url
 * @param {*} data
 * @param {*} dispatch
 */
export function patchFetch(url, data, dispatch) {
  dispatch(loadingAction(true));
  return new Promise((resolve, reject) => {
    fetch(API_ENDPOINT + url, {
      method: "PATCH",
      "cache-control": "no-cache",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then((response) => {
        dispatch(loadingAction(false));
        //update moẻ to check status
        return response.json();
      })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.log("Err:".error);
        reject("Oops! Please try again");
      });
  }).catch((err) => {
    console.log("Err: ", err);
  });
}

/**
 * Delete data to API by DELETE. Using this function for action DELETE
 * @param {*} url
 * @param {*} data
 * @param {*} dispatch
 */
export function deleteFetch(url, data, dispatch) {
  const token = localStorage.getItem(KEY_TOKEN);
  dispatch(loadingAction(true));
  return new Promise((resolve, reject) => {
    fetch(API_ENDPOINT + url, {
      method: "DELETE",
      "cache-control": "no-cache",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        token: token,
      },
      body: JSON.stringify(data),
    })
      .then((response) => {
        //update later to check status and tracking
        dispatch(loadingAction(false));
        return response.json();
      })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.log("Err:".error);
        reject("Oops! Please try again");
      });
  }).catch((err) => {
    dispatch(loadingAction(false));
    console.log("Err: ", err);
  });
}

export function getFetchPDF(url, dispatch) {
  const token = localStorage.getItem(KEY_TOKEN);
  dispatch(loadingAction(true));
  return new Promise((resolve, reject) => {
    fetch(API_ENDPOINT + url, {
      method: "GET",
      "cache-control": "no-cache",
      headers: {
        token: token,
      },
    })
      .then((response) => {
        //update moẻ to check status
        dispatch(loadingAction(false));
        return response;
      })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.log("Err:", error);
        reject("Oops! Please try again");
      });
  }).catch((err) => {
    dispatch(loadingAction(false));
    // alert(err);
    console.log("Err: ", err);
  });
}

export function fetchSaga(url, data, opts) {
  return new Promise((resolve, reject) => {
    fetch(API_ENDPOINT + url, {
      method: "POST",
      "cache-control": "no-cache",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        ...opts,
      },
      body: JSON.stringify(data),
    })
      .then((response) => {
        //update moẻ to check status
        return response.json();
      })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.log("Err:".error);
        // alert(`error API ${error}`);
        reject("Oops! Please try again");
      });
  }).catch((err) => {
    // alert(err);
    console.log("Err: ", err);
  });
}

export function putWithToken(url, data = {}, opts = {}) {
  const token = localStorage.getItem(KEY_TOKEN);

  return new Promise((resolve, reject) => {
    fetch(API_ENDPOINT + url, {
      method: "PUT",
      "cache-control": "no-cache",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
        ...opts,
      },
      body: JSON.stringify(data),
    })
      .then((response) => {
        //update moẻ to check status
        return response.json();
      })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.log("Err:".error);
        // alert(`error API ${error}`);
        reject("Oops! Please try again");
      });
  }).catch((err) => {
    // alert(err);
    console.log("Err: ", err);
  });
}

export function deleteWithToken(url, data = {}, opts = {}) {
  const token = localStorage.getItem(KEY_TOKEN);
  const liveStatus = sessionStorage.getItem(LIVE_STATUS);

  if (liveStatus === "offline") return;

  return new Promise((resolve, reject) => {
    fetch(API_ENDPOINT + url, {
      method: "DELETE",
      "cache-control": "no-cache",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
        ...opts,
      },
      body: JSON.stringify(data),
    })
      .then((response) => {
        //update moẻ to check status
        return response.json();
      })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.log("Err:".error);
        // alert(`error API ${error}`);
        reject("Oops! Please try again");
      });
  }).catch((err) => {
    // alert(err);
    console.log("Err: ", err);
  });
}

export function postWithToken(url, data = {}, opts = {}) {
  const token = localStorage.getItem(KEY_TOKEN);
  const liveStatus = sessionStorage.getItem(LIVE_STATUS);

  if (liveStatus === "offline") return;

  return new Promise((resolve, reject) => {
    fetch(API_ENDPOINT + url, {
      method: "POST",
      "cache-control": "no-cache",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
        ...opts,
      },
      body: JSON.stringify(data),
    })
      .then((response) => {
        //update moẻ to check status
        return response.json();
      })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.log("Err:".error);
        // alert(`error API ${error}`);
        reject("Oops! Please try again");
      });
  }).catch((err) => {
    // alert(err);
    console.log("Err: ", err);
  });
}

export function fetchWithToken(url, data, opts) {
  const token = localStorage.getItem(KEY_TOKEN);
  const liveStatus = sessionStorage.getItem(LIVE_STATUS);

  if (liveStatus === "offline") return;

  return new Promise((resolve, reject) => {
    fetch(API_ENDPOINT + url + "?" + stringify(data), {
      method: "GET",
      "Cache-Control": "public",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
        ...opts,
      },
    })
      .then((response) => {
        //update moẻ to check status
        return response.json();
      })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.log("Err:".error);
        // alert(`error API ${error}`);
        reject("Oops! Please try again");
      });
  }).catch((err) => {
    // alert(err);
    console.log("Err: ", err);
  });
}

export function putFormData(url, formData) {
  const token = localStorage.getItem(KEY_TOKEN);
  const liveStatus = sessionStorage.getItem(LIVE_STATUS);

  if (liveStatus === "offline") return;

  return new Promise((resolve, reject) => {
    fetch(API_ENDPOINT + url, {
      method: "PUT",
      headers: {
        Authorization: "Bearer " + token,
      },
      body: formData,
    })
      .then((response) => {
        //update moẻ to check status
        return response.json();
      })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.log("Err:".error);
        // alert(`error API ${error}`);
        reject("Oops! Please try again");
      });
  }).catch((err) => {
    // alert(err);
    console.log("Err: ", err);
  });
}

export function postFormData(url, formData) {
  const token = localStorage.getItem(KEY_TOKEN);
  const liveStatus = sessionStorage.getItem(LIVE_STATUS);

  if (liveStatus === "offline") return;

  return new Promise((resolve, reject) => {
    fetch(API_ENDPOINT + url, {
      method: "POST",
      headers: {
        Authorization: "Bearer " + token,
      },
      body: formData,
    })
      .then((response) => {
        //update moẻ to check status
        return response.json();
      })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.log("Err:".error);
        // alert(`error API ${error}`);
        reject("Oops! Please try again");
      });
  }).catch((err) => {
    // alert(err);
    console.log("Err: ", err);
  });
}

export function callAPI(url, method, data, header) {
  const token = localStorage.getItem(KEY_TOKEN);
  const liveStatus = sessionStorage.getItem(LIVE_STATUS);

  if (liveStatus === "offline") return;

  return new Promise((resolve, reject) => {
    fetch(API_ENDPOINT + url, {
      method,
      "cache-control": "no-cache",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
        ...header,
      },
      body: JSON.stringify(data),
    })
      .then((response) => {
        //update moẻ to check status
        return response.json();
      })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.log("Err:".error);
        // alert(`error API ${error}`);
        reject("Oops! Please try again");
      });
  }).catch((err) => {
    // alert(err);
    console.log("Err: ", err);
  });
}

export function formApi(url, data, callback, method = "POST", options) {
  const token = localStorage.getItem(KEY_TOKEN);
  const liveStatus = sessionStorage.getItem(LIVE_STATUS);

  const body = new FormData();

  if (Object.keys(data).length) {
    for (const [key, value] of data) {
      console.log(key, value);
      body.append(key, value);
    }
  }

  if (liveStatus === "offline") return;

  return new Promise((resolve, reject) => {
    fetch(API_ENDPOINT + url, {
      method,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
        ...options,
      },
      body,
    })
      .then((response) => response.json())
      .then((response) => resolve(response))
      .catch((error) => reject("Oops! Please try again"));
  }).catch((err) => console.error("Err: ", err));
}
