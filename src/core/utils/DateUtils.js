import moment from "moment";

export function convertDate(date) {
  const dateFormat = moment(date, ["DD-MM-YYYY", "MM-DD-YYYY", "MM-DD-YYYY"]);
  return dateFormat.format("YYYY-MM-DD");
}
