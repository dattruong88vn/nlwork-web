import { isEqual, isString } from "lodash";
import moment from "moment-timezone";

export const ConvertListTask = (data = []) => {
  const newData = [...data];
  const idMapping = newData.reduce((acc, el, i) => {
    acc[el.id] = i;
    return acc;
  }, {});

  data.forEach((el) => {
    const parentEl =
      newData[idMapping[el?.PARENT_ID || el?.my_group_id || el?.group_id]];
    if (parentEl && el.TYPE === "task") {
      parentEl.children = [...(parentEl.children || []), el];
    }
  });

  data.forEach((el) => {
    if (
      (el.PARENT_ID || el?.my_group_id || el?.group_id) &&
      el.TYPE === "task"
    ) {
      const index = newData.indexOf(el);
      newData.splice(index, 1);
    }
  });
  return newData;
};

export const ConvertTasksBoard = (data = []) => {
  let countUnCategory = 0;
  const newData = [...data];

  const unCategory = {
    name: "uncategoried",
    id: "uncategoried",
    TYPE: "group",
    children: [],
  };

  const idMapping = newData.reduce((acc, el, i) => {
    acc[el.id] = i;
    return acc;
  }, {});

  data.forEach((el) => {
    const parentEl = newData[idMapping[el.PARENT_ID]];
    if (parentEl && el.TYPE === "task") {
      parentEl.children = [...(parentEl.children || []), el];
    }
  });

  data.forEach((el) => {
    if (!el.PARENT_ID) {
      countUnCategory += 1;
      unCategory.children.push(el);
    }
  });

  data.forEach((el) => {
    if (el.TYPE === "task" || el.type === 1) {
      const index = newData.indexOf(el);
      newData.splice(index, 1);
    }
  });
  unCategory.total_task = countUnCategory;
  newData.unshift(unCategory);
  return newData;
};

/**
 * @function getParameterByName get param query string from url
 * @param {string} name
 * @param {string} url
 * @returns {string} value of param
 */
export function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return "";
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

/**
 * @function getClassForTask get class name for task by date
 * @param {*} date
 * @returns {object} object contain class name for color, icon, content
 */
export const getClassForTask = (date, timezone = "Asia/Ho_Chi_Minh") => {
  if (!date) {
    return { content: "", case: null };
  }

  let result = {
    content: "",
    case: null,
    colorClass: "grey-7",
    iconClass: "grey-7",
  };

  const today = moment.tz(moment(), timezone);
  const tomorrow = moment.tz(moment().add(1, "day").startOf("day"), timezone);
  const dateFromProps = moment.tz(moment.unix(date), timezone);

  if (tomorrow.isSame(dateFromProps, "day")) {
    result = {
      content: "tomorrow",
      case: "tomorrow",
      colorClass: "black-3",
      iconClass: "black-3",
      bgClass: "bg-blue-9",
      contentClass: "blue-1 bg-blue-9",
    };
  } else if (today.isAfter(dateFromProps)) {
    result = {
      content: "out_of_date",
      case: "out_of_date",
      colorClass: "red",
      iconClass: "red",
      bgClass: "bg-red-2",
      contentClass: "red bg-red-2",
    };
  } else if (today.isSame(dateFromProps, "day")) {
    result = {
      content: "today",
      case: "today",
      colorClass: "green-2",
      iconClass: "green-2",
      contentClass: "bg-green-5 green-2",
    };
  }
  return result;
};

export function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

export const formatBytes = (bytes, decimals = 2) => {
  if (bytes === 0) return "0 B";

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
};

export const replaceAllSpecialChar = (string) => {
  return isString(string) && string.replace(/[^\w\s]/gi, "");
};


/**
 * Check two array is same value
 * @param {array} arr1
 * @param {array} arr2
 * @returns {boolean} result of compare
 */
export const isTwoArraySameValue = (arr1, arr2) => {
  return isEqual(arr1.sort(), arr2.sort());
};
