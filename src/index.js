import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import { I18nextProvider } from "react-i18next";
import i18n from "assets/i18n";
import store from "core/redux/store/configureStore";

import App from "./App";
import * as serviceWorker from "./serviceWorker";
import "assets/styles/common.scss";
import "assets/fonts/fontawesome.min.css";
import ToastProvider from "app/components/toast/ToastProvider";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <I18nextProvider i18n={i18n}>
        <ToastProvider>
          <App />
        </ToastProvider>
      </I18nextProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
