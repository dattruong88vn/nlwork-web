import React, { useRef, useState } from "react";
import moment from "moment-timezone";
import { isArray } from "lodash";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import { replaceAllSpecialChar } from "core/utils";
import useClickOutside from "core/hooks/useClickOutside";
import { MwButton } from "app/components";
import { GroupCheckbox } from "./components";
import MwInput from "../input";

function MwPanelFilter({
  dataPanel,
  onFilter,
  filterTime,
  dataGet,
  className,
}) {
  const { t } = useTranslation();
  const [refPanel, expanded, setExpanded] = useClickOutside(false);
  const [totalFilter, setTotalFilter] = useState(0);
  const [dataFilter, setDataFilter] = useState({});
  const refFromTime = useRef(null);
  const refToTime = useRef(null);

  const handleExpand = () => setExpanded(!expanded);

  const handleClosePanel = () => {
    let count = 0;

    for (let index = 1; index < refPanel.current.length; index++) {
      const { checked, type, value } = refPanel.current[index];
      if (type === "checkbox" && checked) count += 1;
      if (type === "text" && value.trim().length > 0) count += 1;
    }

    setTotalFilter(count);
    setExpanded(false);
  };

  const handleReset = (e) => {
    e && e.preventDefault();

    for (let index = 1; index < refPanel.current.length; index++) {
      const { type } = refPanel.current[index];
      if (type === "checkbox") {
        refPanel.current[index].checked = false;
      }
    }

    if (refFromTime.current) {
      refFromTime.current.handleClear();
    }

    if (refToTime.current) {
      refToTime.current.handleClear();
    }
  };

  const handleSubmit = (e) => {
    e && e.preventDefault();
    const data = {};
    const keys =
      (dataGet?.length && dataGet.map((el) => replaceAllSpecialChar(el.key))) ||
      [];

    for (let index = 1; index < refPanel.current.length; index++) {
      const { checked, dataset } = refPanel.current[index];
      if (keys.includes(dataset.check)) {
        // eslint-disable-next-line no-loop-func
        dataGet.forEach((el) => {
          if (dataset.check === replaceAllSpecialChar(el.key) && checked) {
            if (el.type === "array") {
              data[el.key] = isArray(data[el.key])
                ? [...data[el.key], dataset?.id]
                : [dataset?.id];
            }
            if (el.type === "string") {
              data[el.key] = dataset?.id;
            }
          }
        });
      }
    }

    if (filterTime) {
      const toTime = refPanel.current["to_time"].value;
      const fromTime = refPanel.current["from_time"].value;

      if (toTime.trim()) {
        data["to_time"] = moment(toTime, "DD/MM/YYYY").startOf("day").unix();
      }

      if (fromTime.trim()) {
        data["from_time"] = moment(fromTime, "DD/MM/YYYY").endOf("day").unix();
      }
    }

    onFilter && onFilter(data);
    setDataFilter(data);
    handleClosePanel();
  };

  return (
    <form
      ref={refPanel}
      className={`dropdown filter ${
        expanded ? "overflow-unset" : ""
      } ${className}`}
    >
      <span
        onClick={handleExpand}
        className={`dropbtn btn btn-filter btn-h-32 black-3 border-grey-6 d-flex align-center justify-space-between ${
          expanded ? "active" : ""
        } ${!!totalFilter && "active"}`}
        title={t("filter")}
      >
        <i className="fal fa-filter" />
        {!!totalFilter && <span className="filter-num">{totalFilter}</span>}
      </span>
      <div
        className={`dropdown-content sp-full ${
          expanded ? "show" : ""
        } no-hidden`}
        style={{ zIndex: 9 }}
      >
        {expanded && (
          <>
            <div className="dropdown-back d-flex align-center white px-4 py-3 dp-pc-hide">
              <i
                className="far fa-arrow-left fw-bold fs-16"
                onClick={handleExpand}
                style={{ cursor: "pointer" }}
              />
              <span className="fs-18 fw-bold ml-4 flex-1-0 text-left">
                {t("filter")}
              </span>
              <div className="form">
                <div className="d-flex btn-modal-dr">
                  <MwButton
                    title={`${t("reset")}`}
                    className="btn btn-2 p-0 fs-14 center-2"
                    onClick={handleReset}
                  />
                </div>
              </div>
            </div>

            <div className="px-3 pb-0 scroll-sp mobile-scroll-modal">
              {dataPanel?.map((element, index) => (
                <GroupCheckbox
                  key={index}
                  dataFilter={dataFilter || {}}
                  {...element}
                />
              ))}

              {filterTime && (
                <div className="pt-3 bg-white">
                  <div className="pb-3">
                    <p className="fw-bold fs-14">{t("time")}</p>
                    <div className="row mx-n3">
                      <div className="col">
                        <MwInput
                          ref={refFromTime}
                          isDate
                          name="from_time"
                          className="txt-input h-32 pr-0"
                          placeholder={t("since")}
                          date={
                            dataFilter?.from_time &&
                            moment(+dataFilter?.from_time * 1000)._d
                          }
                        />
                      </div>
                      <div className="col">
                        <MwInput
                          ref={refToTime}
                          isDate
                          name="to_time"
                          className="txt-input h-32 pr-0 w-100"
                          placeholder={t("to_date")}
                          date={
                            dataFilter?.to_time &&
                            moment(+dataFilter?.to_time * 1000)._d
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
              )}

              <div className="grid-2 gc-30 group-btn-filter">
                <MwButton
                  title={t("reset")}
                  onClick={handleReset}
                  className="btn bor-0 btn-h-32 btn-flex mb-3 float-right dp-sp-hide"
                />
                <MwButton
                  title={t("save")}
                  className="btn btn-2 btn-h-32 btn-flex mb-3 float-right dp-sp-hide"
                  onClick={handleSubmit}
                />
              </div>
            </div>
            <div className="dp-pc-hide wrapper-panel-button">
              <MwButton
                title={t("save")}
                className="btn btn-2 btn-h-32 btn-flex float-right w-100"
                onClick={handleSubmit}
              />
            </div>
          </>
        )}
      </div>
    </form>
  );
}

MwPanelFilter.propTypes = {
  dataPanel: PropTypes.array.isRequired,
  dataGet: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  filterTime: PropTypes.bool,
  onFilter: PropTypes.func.isRequired,
  className: PropTypes.string,
};

MwPanelFilter.defaultProps = {
  filterTime: false,
  className: "",
};

export default MwPanelFilter;
