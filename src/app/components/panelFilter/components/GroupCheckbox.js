import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import MwCheckbox from "app/components/checkbox";
import { useTranslation } from "react-i18next";
import { replaceAllSpecialChar } from "core/utils";
import { isArray } from "lodash";

function GroupCheckbox({
  title,
  grid,
  items,
  dataFilter,
  className,
  ...restProps
}) {
  return (
    <div className={`border-bottom-grey-6 pb-3 pt-3 ${className}`}>
      <p className="fw-bold fs-14">{title}</p>
      {grid === "grid-2" ? (
        <GroupGridTwoColumn
          items={items}
          dataFilter={dataFilter}
          {...restProps}
        />
      ) : (
        <GroupGridOneColumn
          items={items}
          dataFilter={dataFilter}
          {...restProps}
        />
      )}
    </div>
  );
}

const GroupGridTwoColumn = ({
  items,
  prefixName,
  prefixId,
  dataFilter = {},
  className,
}) => {
  const [dataDefault, setDataDefault] = useState([]);

  useEffect(() => {
    const copyData = { ...dataFilter };
    const result = {};

    for (const key in copyData) {
      if (copyData.hasOwnProperty(key)) {
        const value = copyData[key];
        result[replaceAllSpecialChar(key)] = value;
      }
    }

    setDataDefault(result);
  }, [dataFilter]);

  return (
    <div className={`grid-2 ${className}`}>
      {items?.map((item, indexElement) => (
        <div key={item.id}>
          <MwCheckbox
            id={`${prefixId || ""}-${item.id}`}
            data-id={item.id}
            data-check={prefixName}
            name={item?.nameLabel || `${prefixName || ""}`}
            isHorizontal
            customLabel={{ className: "fs-13 fw-normal" }}
            label={item?.name || ""}
            defaultValue={dataDefault[prefixName]?.includes(item.id)}
          />
        </div>
      ))}
    </div>
  );
};

const GroupGridOneColumn = ({
  items,
  limit,
  prefixName,
  dataFilter,
  className,
  emptyText = "",
  keyLabel = "name",
}) => {
  const { t } = useTranslation();
  const [list, setList] = useState(items || []);
  const [displayMore, setDisplayMore] = useState(true);
  const [dataDefault, setDataDefault] = useState([]);

  useEffect(() => {
    const copyData = { ...dataFilter };
    const result = {};

    for (const key in copyData) {
      if (copyData.hasOwnProperty(key)) {
        const value = copyData[key];
        result[replaceAllSpecialChar(key)] = value;
      }
    }

    setDataDefault(result);
  }, [dataFilter]);

  useEffect(() => {
    limit && items?.length && setList([...items].slice(0, limit));
  }, [items, limit]);

  const handleLoadmore = (e) => {
    e && e.preventDefault();
    setDisplayMore(false);
    setList([...list, ...items?.slice(limit, items?.length)]);
  };

  return (
    <>
      {list?.map((item, index) => {
        let label = "";

        isArray(keyLabel)
          ? keyLabel.map((key) => (label += `${item[key]} `))
          : (label = item[keyLabel]);

        return (
          <div className={`row mx-n3 mb-3 ${className}`} key={item.id}>
            <div className="col">
              <MwCheckbox
                id={`${item.id}`}
                data-id={item.id}
                data-check={prefixName}
                isHorizontal
                customLabel={{ className: "fs-13 fw-normal" }}
                label={label}
                defaultValue={dataDefault[prefixName]?.includes(item.id)}
              />
            </div>
          </div>
        );
      })}

      {displayMore && items?.length - limit > 0 && (
        <div className="row mx-n3">
          <div className="col">
            <a href="/" className="fs-13 fw-normal" onClick={handleLoadmore}>
              {t("see_more")} ({items?.length - limit})
            </a>
          </div>
        </div>
      )}

      {!!!list?.length && <p className="mb-0">{emptyText}</p>}
    </>
  );
};

GroupCheckbox.propTypes = {
  items: PropTypes.array,
  title: PropTypes.string,
  grid: PropTypes.string,
};

GroupCheckbox.defaultProps = {
  items: [],
  grid: "grid-2",
};

export default GroupCheckbox;
