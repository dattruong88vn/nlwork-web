import React, { useRef, useState } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import { uniqueId } from "lodash";
import MwInput from "app/components/input";

function CustomOptionsAttachment({ onAddAttachment }) {
  const { t } = useTranslation();
  const [error, setError] = useState({});
  const refUpload = useRef(null);
  const { features } = useSelector((state) => state.user.userInfo);
  const MAX_UPLOAD_FILE_SIZE = features.max_upload_file_size;

  const handleUploadClick = (e) => {
    refUpload.current.onClick();
  };

  const handleUpload = (e) => {
    const files = e.target.files;
    if (!files?.length) return;

    for (let index = 0; index < e.target.files.length; index++) {
      const file = e.target.files[index];
      const reader = new FileReader();
      reader.readAsDataURL(file);

      if (file.size / 1024 / 1024 < MAX_UPLOAD_FILE_SIZE) {
        if (file && onAddAttachment) {
          const id = uniqueId("attachment-");
          reader.onloadend = function (e) {
            onAddAttachment({ id, file, url: reader.result });
          };
        }
      } else {
        setError(t("just_support_img_2mb"));
      }
    }
  };

  return (
    <span
      className="rdw-custom-wrapper mw-richtext__footer--icon"
      onClick={handleUploadClick}
    >
      <i className="far fa-paperclip" />
      <MwInput
        className="dp-hide"
        ref={refUpload}
        onChange={handleUpload}
        classError="left"
        type="file"
        multiple
      />
    </span>
  );
}

CustomOptionsAttachment.propTypes = {
  onAddAttachment: PropTypes.func.isRequired,
};

export default CustomOptionsAttachment;
