import React, { forwardRef, useImperativeHandle, useState } from "react";
import PropTypes from "prop-types";
import { EditorState, convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import { Editor } from "react-draft-wysiwyg";
import Emoji from "app/const/Emoji";
import { CustomOptionsAttachment } from "./components";
import MwAttachments from "../attachment";
import "draft-js/dist/Draft.css";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import "./styles/index.scss";

const MwRichText = forwardRef(({ placeholder, label, error }, ref) => {
  const [isFocus, setIsFocus] = useState(false);
  const [stylesActive, setStylesActive] = useState([]);
  const [editorState, setEditorState] = useState(() =>
    EditorState.createEmpty()
  );
  const [placeholderValue, setPlaceholderValue] = useState(placeholder);
  const [attachments, setAttachments] = useState([]);

  useImperativeHandle(ref, () => ({
    handleSubmitRichText,
  }));

  const handleSubmitRichText = () => {
    const content = draftToHtml(convertToRaw(editorState.getCurrentContent()));
    return { content, attachments };
  };

  const handleFocus = () => {
    setIsFocus(true);
  };

  const handleBlur = () => {
    setIsFocus(false);
  };

  const handleChange = (value) => {
    setEditorState(value);
  };

  const handleAddAttachment = (newAttachment) => {
    setAttachments((attachments) => [newAttachment, ...attachments]);
  };

  const handleRemoveAttachment = (id) => {
    const newAttachments = [...attachments].filter((item) => item.id !== id);
    setAttachments(newAttachments);
  };

  return (
    <>
      {label && <label>{label}</label>}
      <Editor
        onClick={handleFocus}
        wrapperClassName={`mw-richtext ${isFocus && "focused"} ${
          error && "isError"
        }`}
        toolbarClassName="mw-richtext__footer"
        onFocus={handleFocus}
        onBlur={handleBlur}
        editorState={editorState}
        onEditorStateChange={handleChange}
        placeholder={placeholderValue}
        toolbar={{
          options: ["inline", "list", "emoji"],
          inline: {
            inDropdown: false,
            className: "mw-richtext__footer--icon",
            options: ["bold", "italic", "underline"],
            bold: {
              icon: <i />,
              className: "far fa-bold",
            },
            italic: {
              icon: <i />,
              className: "far fa-italic",
            },
            underline: {
              icon: <i />,
              className: "far fa-underline",
            },
          },
          list: {
            inDropdown: false,
            className: "mw-richtext__footer--icon",
            component: undefined,
            dropdownClassName: undefined,
            options: ["unordered", "ordered"],
            unordered: { icon: <i />, className: "far fa-list" },
            ordered: { icon: <i />, className: "far fa-list-ol" },
          },
          emoji: {
            icon: <i />,
            className: "mw-richtext__footer--icon far fa-smile",
            component: undefined,
            popupClassName: "mw-richtext__footer--popup",
            emojis: Emoji,
          },
        }}
        toolbarCustomButtons={[
          <CustomOptionsAttachment onAddAttachment={handleAddAttachment} />,
        ]}
      />
      {error && <div className="msg-error">{error}</div>}
      <div className="d-flex mt-2 flex-wrap">
        <MwAttachments
          attachments={attachments}
          onRemove={handleRemoveAttachment}
        />
      </div>
    </>
  );
});

MwRichText.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  error: PropTypes.string,
};

export default MwRichText;
