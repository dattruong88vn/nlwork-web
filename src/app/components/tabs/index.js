import React from "react";

function TabsComponent({ tabs, activeTab, handleTab, children, ...restProps }) {
  const handleClick = (value) => {
    if (handleTab) {
      handleTab(value);
    }
  };

  return (
    <>
      <div className="tabs px-6 px-sp-0 border-bottom-grey-6" {...restProps}>
        {tabs.map((tab, index) => (
          <div
            key={index}
            className={`tabs-item ${activeTab === tab.value ? "active" : ""}`}
            onClick={() => handleClick(tab.value)}
          >
            {tab.label}
          </div>
        ))}
      </div>
    </>
  );
}

export default TabsComponent;
