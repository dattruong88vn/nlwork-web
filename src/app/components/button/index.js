import React from "react";
import PropTypes from "prop-types";
import "./styles/index.scss";

function MwButton({
  title,
  loading,
  onClick,
  preventEvent,
  ...restPropsButton
}) {
  const clickHandle = (e) => {
    preventEvent && e.preventDefault();
    if (onClick && !loading) {
      e.preventDefault();
      onClick();
    }
  };

  return (
    <>
      <button onClick={clickHandle} className="btn" {...restPropsButton}>
        {loading && <div className="loader" />}
        {title}
      </button>
    </>
  );
}

MwButton.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  onClick: PropTypes.func,
  preventEvent: PropTypes.bool,
};

export default MwButton;
