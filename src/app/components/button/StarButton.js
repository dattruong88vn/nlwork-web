import React from "react";
import PropTypes from "prop-types";

function MwStarButton({ favorite, onClick, className, classIcon }) {
  const [isFavorite, setIsFavorite] = React.useState(false);

  React.useEffect(() => {
    setIsFavorite(favorite);
  }, [favorite]);

  const handleClick = () => {
    setIsFavorite(!isFavorite);
    if (onClick) {
      onClick(!isFavorite);
    }
  };

  return (
    <div className={`star-wrapper ${isFavorite && "active"}`}>
      <i
        className={`fas fa-star yellow star-tick ${classIcon || "fs-18"}`}
        onClick={handleClick}
      />
      <i
        className={`grey-7 fal fa-star yellow star-tick ${
          classIcon || "fs-18"
        }`}
        onClick={handleClick}
      />
    </div>
  );
}

MwStarButton.propTypes = {
  favorite: PropTypes.bool,
  onClick: PropTypes.func,
  className: PropTypes.string,
};

MwStarButton.defaultProps = {
  favorite: false,
  onClick: null,
  className: "",
};

export default MwStarButton;
