import React from "react";
import PropTypes from "prop-types";
import useClickOutside from "core/hooks/useClickOutside";

function MwDropDown({
  title,
  iconBack,
  iconBackClass,
  titleClass,
  activeBtnClass,
  contentClass,
  onClick,
  children,
  titleLink,
  ...restProps
}) {
  const [ref, expanded, setExpanded] = useClickOutside(false);

  const classesTitle = `dropdown ${titleClass} ${
    expanded ? "overflow-unset" : ""
  }`;

  const classesIconBack = `dropdown-back ${iconBackClass}`;

  const classesActive = `dropbtn  ${activeBtnClass} ${
    expanded ? "active" : ""
  }`;

  const classesContent = `dropdown-content ${contentClass} ${
    expanded ? "show" : ""
  }`;

  const handleExpand = () => {
    setExpanded(!expanded);
    if (onClick) {
      onClick();
    }
  };

  const handleExpandWthTitle = () => {
    if (!title) {
      setExpanded(!expanded);
      if (onClick) {
        onClick();
      }
    }
  };

  return (
    <div
      ref={ref}
      className={classesTitle}
      onClick={handleExpand}
      title={titleLink}
      {...restProps}
    >
      <div className={classesActive} onClick={handleExpandWthTitle}>
        {title}
      </div>
      <div className={classesContent}>
        {iconBack && (
          <div className={classesIconBack}>
            <i
              className="far fa-arrow-left fw-bold fs-16"
              onClick={handleExpand}
            />
            {iconBack}
          </div>
        )}
        {children}
      </div>
    </div>
  );
}

MwDropDown.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
    PropTypes.bool,
  ]),
  onClick: PropTypes.func,
  activeBtnClass: PropTypes.string,
  iconBackClass: PropTypes.string,
  titleClass: PropTypes.string,
  contentClass: PropTypes.string,
  titleLink: PropTypes.string,
};
MwDropDown.defaultProps = {
  title: null,
  children: null,
  onClick: null,
  activeBtnClass: "",
  contentClass: "",
  iconBackClass: "",
  titleClass: "",
  titleLink: "",
};

export default MwDropDown;
