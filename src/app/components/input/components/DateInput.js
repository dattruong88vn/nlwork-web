import React, {
  useState,
  useEffect,
  forwardRef,
  useImperativeHandle,
} from "react";
import PropTypes from "prop-types";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const DateInput = forwardRef(
  (
    { date, placeholderText, dateFormat, onChange, inline, children, ...rest },
    ref
  ) => {
    const [state, setState] = useState(true);
    const [value, setValue] = useState(date);

    useEffect(() => {
      const setFlag = () => {
        if (state && date) {
          setValue(date);
          setState(false);
        }
      };

      setFlag();
    }, [date, state]);

    useImperativeHandle(ref, () => ({
      handleClearDate,
    }));

    const handleClearDate = () => {
      setValue("");
    };

    const handleChange = (date) => {
      setValue(date);
      if (onChange) {
        onChange(date);
      }
    };

    return (
      <div className="date">
        {!inline && <i className="fal fa-calendar-alt" />}

        <DatePicker
          selected={value}
          dateFormat={dateFormat}
          className="txt-input w-100"
          onChange={(date) => handleChange(date)}
          placeholderText={placeholderText}
          inline={inline}
          autoComplete="off"
          {...rest}
        >
          {children}
        </DatePicker>
      </div>
    );
  }
);

DateInput.propTypes = {
  date: PropTypes.oneOfType([PropTypes.instanceOf(Date), PropTypes.string]),
  placeholderText: PropTypes.string,
  inline: PropTypes.bool,
  onChange: PropTypes.func,
  dateFormat: PropTypes.string,
};

DateInput.defaultProps = {
  date: null,
  placeholderText: null,
  inline: false,
  onChange: null,
  dateFormat: "dd/MM/yyyy",
};

export default DateInput;
