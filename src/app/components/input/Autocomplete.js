import React, { useState, useImperativeHandle, forwardRef } from "react";
import { useSelector } from "react-redux";
import Select, { components } from "react-select";
import MwAvatar from "../avatar";

const DropdownIndicator = (props) => {
  return (
    <components.DropdownIndicator {...props}>
      <i className="fal fa-plus" />
    </components.DropdownIndicator>
  );
};

const MultiValueRemove = (props) => {
  return (
    <components.MultiValueRemove {...props}>
      <span className="tags-member-delete">
        <i className="fal fa-times" />
      </span>
    </components.MultiValueRemove>
  );
};

const MultiValueLabel = ({ data }) => {
  return (
    <>
      <MwAvatar className="img-circle w-24 ml-0" {...data} />
      <span>
        {data.first_name} {data.last_name}
      </span>
    </>
  );
};

const MultiValueContainer = (props) => {
  return (
    <span className="tags-member" style={{ margin: 5 }}>
      <components.MultiValueContainer {...props} />
    </span>
  );
};

const MwInputAutocomplete = forwardRef(
  (
    {
      label,
      customLabel,
      tooltip,
      wrapperClass,
      defaultValue,
      emptyMessage,
      ...restProps
    },
    ref
  ) => {
    const { members } = useSelector((state) => state.company);
    const [selected, setSelected] = useState([]);

    useImperativeHandle(ref, () => ({
      getValue: () => {
        return selected;
      },
    }));

    const getOptionValue = (value) => {
      return (
        <span
          className="tags-member"
          style={{ margin: 5, background: "transparent" }}
        >
          <MwAvatar className="img-circle w-24 ml-0" {...value} />
          <span>
            {value.first_name} {value.last_name}
          </span>
        </span>
      );
    };

    const handleChange = (e) => {
      setSelected(Array.isArray(e) ? e.map((x) => x.id) : []);
    };

    return (
      <div className={wrapperClass}>
        <div className="lb-wrap">
          {label && (
            <label className="mt-25" {...customLabel}>
              {label}
            </label>
          )}
          {tooltip}
        </div>
        <Select
          isMulti
          className="basic-multi-select"
          classNamePrefix="select"
          options={members}
          getOptionLabel={getOptionValue}
          getOptionValue={(v) => `${v?.first_name} ${v?.last_name}`}
          onChange={handleChange}
          noOptionsMessage={() => emptyMessage}
          styles={{
            control: (base) => ({
              ...base,
              padding: 2,
            }),
            multiValueRemove: () => ({
              cursor: "pointer",
            }),
            multiValue: () => ({
              backgroundColor: "transparent",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }),
            dropdownIndicator: (base) => ({ ...base, cursor: "pointer" }),
            clearIndicator: () => ({ display: "none" }),
          }}
          components={{
            MultiValueLabel,
            MultiValueContainer,
            MultiValueRemove,
            DropdownIndicator,
          }}
          {...restProps}
        />
      </div>
    );
  }
);

export default MwInputAutocomplete;
