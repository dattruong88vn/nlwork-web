import React, {
  forwardRef,
  useState,
  useRef,
  useEffect,
  useImperativeHandle,
} from "react";
import PropTypes from "prop-types";
import DateInput from "./components/DateInput";
import "./styles/index.scss";

const MwInput = forwardRef(
  (
    {
      label,
      labelRight,
      leftComponent,
      rightComponent,
      tooltip,
      type,
      date,
      defaultValue,
      error,
      success,
      required,
      isDate,
      isTextarea,
      onChange,
      onFocus,
      customLabel,
      classError,
      classWrapper,
      ...restInputProps
    },
    ref
  ) => {
    const [value, setValue] = useState("");
    const [typeInput, setTypeInput] = useState(type);
    const refInput = useRef(ref);

    useEffect(() => {
      defaultValue && setValue(defaultValue);
    }, [defaultValue]);

    useImperativeHandle(ref, () => ({
      handleClear,
      onClick: () => refInput.current.click(),
    }));

    const handleClear = () => {
      setValue("");
      if (refInput?.current) {
        refInput.current.handleClearDate();
      }
    };

    const changeTypeHandle = () => {
      typeInput === "password"
        ? setTypeInput("text")
        : setTypeInput("password");
    };

    const handleFocus = () => {
      onFocus && onFocus();
    };

    const handleChange = (e) => {
      setValue(e.target.value);
      onChange && onChange(e);
    };

    return (
      <>
        <div className="lb-wrap">
          {label && (
            <label className="mt-25" {...customLabel}>
              {label}
            </label>
          )}
          {tooltip}
          {labelRight}
        </div>
        <div
          className={`inp-wrap ico-input ${error && "error"} ${
            success && "success"
          } ${classWrapper}`}
        >
          {leftComponent}
          {isDate ? (
            <DateInput
              ref={refInput}
              required={required}
              date={date}
              className="txt-input"
              type={typeInput}
              placeholderText={restInputProps.placeholder}
              onChange={onChange}
              {...restInputProps}
            />
          ) : isTextarea ? (
            <textarea
              ref={refInput}
              value={value}
              required={required}
              onChange={handleChange}
              onFocus={handleFocus}
              {...restInputProps}
            />
          ) : (
            <input
              ref={refInput}
              value={value}
              required={required}
              className="txt-input"
              onChange={handleChange}
              onFocus={handleFocus}
              type={typeInput}
              errortext={error}
              autoComplete="off"
              aria-autocomplete="none"
              spellCheck={false}
              {...restInputProps}
            />
          )}

          {rightComponent}
          {success && <i className="far fa-check" />}
          {error && <div className={`msg-error ${classError}`}>{error}</div>}

          {type === "password" && (
            <button
              type="button"
              onClick={changeTypeHandle}
              className={
                typeInput === "password"
                  ? "fas fa-eye icon"
                  : "fas fa-eye fa-eye-slash icon"
              }
            />
          )}
        </div>
      </>
    );
  }
);

MwInput.propTypes = {
  label: PropTypes.string,
  labelRight: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  type: PropTypes.string,
  error: PropTypes.string,
  classWrapper: PropTypes.string,
  success: PropTypes.bool,
  required: PropTypes.bool,
  customLabel: PropTypes.object,
  onChange: PropTypes.func,
};
MwInput.defaultProps = {
  label: null,
  labelRight: null,
  type: "text",
  error: null,
  classWrapper: "",
  success: false,
  required: false,
  customLabel: {},
  onChange: () => {},
};

export default MwInput;
