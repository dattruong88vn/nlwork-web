import React from "react";
import { useTranslation } from "react-i18next";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import "./styles/index.scss";

function OfflineNotice({ isOffline }) {
  const { t } = useTranslation();

  const Notice = () => {
    return (
      <div className="offline-notice">
        <i className="fal fa-wifi-slash" />
        {t("network_down")}
      </div>
    );
  };

  return isOffline
    ? ReactDOM.createPortal(<Notice />, document.getElementById("root"))
    : null;
}

OfflineNotice.propTypes = {
  isOffline: PropTypes.bool,
};

OfflineNotice.defaultProps = {
  isOffline: false,
};

export default OfflineNotice;
