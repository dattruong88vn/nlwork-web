import React from "react";
import PropTypes from "prop-types";
import { ReactComponent as EmptyIcon } from "assets/images/empty/empty.svg";

function EmptyList({ emptyText, descriptionText, iconEmpty }) {
  return (
    <div className="px-4 pb-15 pt-sp-15 empty w-100">
      <div className="d-flex justify-center align-center flex-column">
        {iconEmpty || (
          <EmptyIcon className="img-fluid mb-7 mb-sp-4 mt-30 icon-empty-list" />
        )}
        <p className="text-center fw-normal fs-18 fs-sp-16 mt-0 mb-1">
          {emptyText}
        </p>
        <p className="m-0 text-center fs-14 fs-sp-12 grey-7">
          {descriptionText}
        </p>
      </div>
    </div>
  );
}

EmptyList.propTypes = {
  emptyText: PropTypes.string,
  descriptionText: PropTypes.string,
};

EmptyList.defaultProps = {
  emptyText: null,
  descriptionText: null,
};

export default EmptyList;
