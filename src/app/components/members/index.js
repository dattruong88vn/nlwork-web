import React from "react";
import ReactDOM from "react-dom";
import { usePopper } from "react-popper";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";
import LinkAvatar from "./components/LinkAvatar";

const MoreMembers = ({ title, count }) => {
  const [referenceRef, setReferenceRef] = React.useState(null);
  const [popperRef, setPopperRef] = React.useState(null);
  const [visible, setVisible] = React.useState(false);

  const { styles, attributes } = usePopper(referenceRef, popperRef, {
    placement: "top",
    modifiers: [
      {
        name: "offset",
        enabled: true,
        options: {
          offset: [0, 10],
        },
      },
    ],
  });

  return (
    <span
      className="circle-bl"
      ref={setReferenceRef}
      href="/"
      onClick={(e) => e.preventDefault()}
      onMouseEnter={() => setVisible(true)}
      onMouseLeave={() => setVisible(false)}
    >
      {ReactDOM.createPortal(
        visible && (
          <div
            ref={setPopperRef}
            style={{
              ...styles.popper,
              zIndex: 999999,
            }}
            className="tooltip-tags"
            {...attributes.popper}
          >
            {title}
          </div>
        ),
        document.body
      )}
      {count}+
    </span>
  );
};

function MwMembers({
  members,
  className,
  hasIcon,
  limit,
  circleClass,
  customClass,
}) {
  const { members: membersCompany } = useSelector((state) => state.company);
  const [users, setUsers] = React.useState([]);
  const [title, setTitle] = React.useState("");

  React.useEffect(() => {
    let title = "";
    const tagSliced = members.slice(limit, members.length);

    tagSliced.forEach((member, index) => {
      membersCompany.forEach((memberCompany) => {
        if (member?.id === memberCompany.id || member === memberCompany.id) {
          tagSliced.length - 1 === index
            ? (title += `${memberCompany.first_name} ${memberCompany.last_name}`)
            : (title += `${memberCompany.first_name} ${memberCompany.last_name},`);
        }
      });
    });

    setTitle(title);
  }, [limit, members, membersCompany]);

  React.useEffect(() => {
    const users = [];
    members.forEach((member) => {
      membersCompany.forEach((memberCompany) => {
        if (member?.id === memberCompany.id || member === memberCompany.id) {
          users.push(memberCompany);
        }
      });
    });
    setUsers(users);
  }, [members, membersCompany]);

  return members.length ? (
    <div className={`d-flex align-center members ${className}`}>
      {hasIcon && <i className="fas fa-users dp-pc-hide mr-2 grey-7 fs-18" />}

      {users.map((user, index) => {
        return (
          index < limit && (
            <LinkAvatar key={index} user={user} customClass={customClass} />
          )
        );
      })}
      {members.length - limit > 0 && (
        <MoreMembers title={title} count={members.length - limit} />
      )}
    </div>
  ) : null;
}

MwMembers.propTypes = {
  hasIcon: PropTypes.bool,
  className: PropTypes.string,
  members: PropTypes.array,
  limit: PropTypes.number,
};

MwMembers.defaultProps = {
  hasIcon: true,
  className: null,
  members: [],
  limit: 3,
};
export default MwMembers;
