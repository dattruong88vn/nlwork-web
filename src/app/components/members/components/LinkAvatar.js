import React from "react";
import ReactDOM from "react-dom";
import MwAvatar from "app/components/avatar";
import { usePopper } from "react-popper";

function LinkAvatar({ user, customClass = "mr-1 w-24" }) {
  const [referenceRef, setReferenceRef] = React.useState(null);
  const [popperRef, setPopperRef] = React.useState(null);
  const [visible, setVisible] = React.useState(false);

  const { styles, attributes } = usePopper(referenceRef, popperRef, {
    placement: "top",
    modifiers: [
      {
        name: "offset",
        enabled: true,
        options: {
          offset: [0, 10],
        },
      },
    ],
  });

  return (
    <a
      ref={setReferenceRef}
      href="/"
      onClick={(e) => e.preventDefault()}
      className={customClass}
      onMouseEnter={() => setVisible(true)}
      onMouseLeave={() => setVisible(false)}
    >
      {ReactDOM.createPortal(
        visible && (
          <div
            ref={setPopperRef}
            style={{
              ...styles.popper,
              zIndex: 999999,
            }}
            className="tooltip-avatar"
            {...attributes.popper}
          >
            {`${user.first_name} ${user.last_name}`}
          </div>
        ),
        document.body
      )}

      <MwAvatar className="img-circle" {...user} />
    </a>
  );
}

export default LinkAvatar;
