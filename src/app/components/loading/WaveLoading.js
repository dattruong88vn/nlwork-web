import React from "react";
import {} from "./styles/wave.scss";

function WaveLoading() {
  return (
    <div className="placeholder">
      <div className="spinner">
        <div className="rect1"></div>
        <div className="rect2"></div>
        <div className="rect3"></div>
        <div className="rect4"></div>
        <div className="rect5"></div>
      </div>
    </div>
  );
}

export default WaveLoading;
