import React from "react";
import "./styles/table.scss";

function ListLoading({ line }) {
  const OneLine = () => (
    <tr style={{ border: 0 }}>
      <td className="td-3">
        <span></span>
      </td>
    </tr>
  );

  return (
    <table>
      <tbody>
        {line ? (
          <OneLine />
        ) : (
          <>
            <tr>
              <td className="td-1">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
            </tr>
            <tr>
              <td className="td-1">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
            </tr>
            <tr>
              <td className="td-1">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
            </tr>
            <tr>
              <td className="td-1">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
            </tr>
            <tr>
              <td className="td-1">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
            </tr>
            <tr>
              <td className="td-1">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
            </tr>
            <tr>
              <td className="td-1">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
            </tr>
            <tr>
              <td className="td-1">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
            </tr>
            <tr>
              <td className="td-1">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
            </tr>
            <tr>
              <td className="td-1">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
            </tr>
            <tr>
              <td className="td-1">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
              <td className="td-3">
                <span></span>
              </td>
            </tr>
          </>
        )}
      </tbody>
    </table>
  );
}

export default ListLoading;
