import React from "react";
import { withTranslation } from "react-i18next";
import { ReactComponent as Logo } from "assets/images/logo.svg";
import "./styles/style.scss";

function Loading({ t }) {
  return (
    <div className="loading">
      <Logo className="loading__logo" />
      <h2 className="title-1">{t("please_wait")}</h2>
    </div>
  );
}

export default withTranslation()(Loading);
