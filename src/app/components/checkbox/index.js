import React from "react";
import PropTypes from "prop-types";

function MwCheckbox({
  id,
  label,
  type,
  isHorizontal,
  wrapperClass,
  defaultValue,
  customLabel,
  onChange,
  ...restProps
}) {
  const [checked, setChecked] = React.useState(false);

  const handleChange = (e) => {
    setChecked(e.target.checked);
    onChange && onChange(e);
  };

  React.useEffect(() => {
    setChecked(defaultValue);
  }, [defaultValue]);

  const HorizontalCheckBox = ({ id, ...rest }) => {
    return (
      <div className={wrapperClass}>
        <input
          id={id}
          type="checkbox"
          onChange={handleChange}
          checked={checked}
          {...rest}
        />
        <label
          htmlFor={id}
          className="fw-normal fs-14 fw-normal"
          {...customLabel}
        >
          {label}
        </label>
      </div>
    );
  };

  const CheckboxComponent = (type) => {
    switch (type) {
      case "radio-color":
        return (
          <div className={wrapperClass}>
            <label {...customLabel}>
              <input type="radio" onChange={handleChange} {...restProps} />
              <i className="fal fa-check" />
            </label>
          </div>
        );
      case "radio":
        return (
          <div className={wrapperClass}>
            <input
              id={id}
              onChange={handleChange}
              type="radio"
              {...restProps}
            />
            <label htmlFor={id} className="fs-13 fw-normal" {...customLabel}>
              {label}
            </label>
          </div>
        );
      default:
        return (
          <div className={wrapperClass}>
            {label && <span className="fs-13">{label}</span>}
            <label className="el-switch">
              <input
                type="checkbox"
                onChange={handleChange}
                checked={checked}
                {...restProps}
              />
              <span className="el-switch-style" />
            </label>
          </div>
        );
    }
  };

  return isHorizontal ? (
    <HorizontalCheckBox id={id} {...restProps} />
  ) : (
    CheckboxComponent(type)
  );
}

MwCheckbox.propTypes = {
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  wrapperClass: PropTypes.string,
};

MwCheckbox.defaultProps = {
  label: null,
  wrapperClass: "checkbox",
};

export default MwCheckbox;
