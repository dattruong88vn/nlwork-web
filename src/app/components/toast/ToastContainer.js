import React from "react";
import PropTypes from "prop-types";
import { createPortal } from "react-dom";
import MwToast from "./Toast";

const ToastContainer = ({ toasts = [] }) => {
  return createPortal(
    <>
      {toasts.map((toast, index) => {
        return <MwToast key={index} {...toast} />;
      })}
    </>,
    document.getElementById("toast-root")
  );
};

ToastContainer.propTypes = {
  toast: PropTypes.array,
};

export default ToastContainer;
