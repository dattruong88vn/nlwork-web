import React from "react";
import PropTypes from "prop-types";
import useToast from "core/hooks/useToast";
import "./styles/index.scss";

function MwToast({
  visible,
  content,
  placement,
  textAgree,
  onAgree,
  textDegree,
  onDegree,
  id,
  timeout,
  status,
}) {
  const { removeToast } = useToast();

  React.useEffect(() => {
    const timer = setTimeout(() => {
      removeToast(id);
    }, timeout);

    return () => {
      clearTimeout(timer);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleClose = () => {
    removeToast(id);
  };

  const handleAgree = () => {
    if (onAgree) {
      onAgree();
    }
  };

  const handleDegree = () => {
    if (onDegree) {
      onDegree();
    }
  };

  return (
    <div className={`toast ${placement} ${status}`}>
      <span className="toast__content">{content}</span>
      <span className="toast__action">
        {textAgree && <span onClick={handleAgree}>{textAgree}</span>}
        {textDegree && <span onClick={handleDegree}>{textDegree}</span>}
      </span>
      <div className="toast__close" onClick={handleClose}>
        <i className="fal fa-times" />
      </div>
    </div>
  );
}

MwToast.propTypes = {
  timeout: PropTypes.number,
  id: PropTypes.string,
  content: PropTypes.string,
  status: PropTypes.string,
  placement: PropTypes.string,
  textAgree: PropTypes.string,
  onAgree: PropTypes.func,
  textDegree: PropTypes.string,
  onDegree: PropTypes.func,
};

MwToast.defaultProps = {
  timeout: 3000,
  id: null,
  content: null,
  placement: "right-bottom",
  status: "success",
  textAgree: null,
  onAgree: null,
  textDegree: null,
  onDegree: null,
};

export default MwToast;
