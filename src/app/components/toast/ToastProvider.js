import React from "react";
import ToastContainer from "./ToastContainer";

export const ToastContext = React.createContext(null);

function ToastProvider({ children }) {
  const [toasts, setToasts] = React.useState([]);

  /**
   * Add toast message
   * @var {object} config
   */
  const addToast = React.useCallback(
    (config) => {
      setToasts((toasts) => [...toasts, { ...config }]);
    },
    [setToasts]
  );

  const removeToast = React.useCallback(
    (id) => {
      setToasts((toasts) => toasts.filter((t) => t.id !== id));
    },
    [setToasts]
  );

  const removeAllToast = React.useCallback(
    (id) => {
      setToasts([]);
    },
    [setToasts]
  );

  return (
    <ToastContext.Provider value={{ addToast, removeToast, removeAllToast }}>
      <ToastContainer toasts={toasts} />
      {children}
    </ToastContext.Provider>
  );
}

export default ToastProvider;
