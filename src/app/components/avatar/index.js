import React from "react";
import PropTypes from "prop-types";
import { AVATAR_COLOR } from "app/const/Colors";

function MwAvatar({
  avatar,
  first_name,
  last_name,
  color,
  className,
  styleDefaultAvatar,
  ...restProps
}) {
  const DefaultAvt = () => {
    return (
      <span
        className={`circle-bl ${className}`}
        style={{
          backgroundColor: color || AVATAR_COLOR,
          color: "white",
          ...styleDefaultAvatar,
        }}
      >
        {first_name[0] && first_name[0].toUpperCase()}
        {last_name[0] && last_name[0].toUpperCase()}
      </span>
    );
  };

  return avatar ? (
    <img
      src={avatar}
      className={`img-circle ${className}`}
      {...restProps}
      alt="Mw Avatar"
    />
  ) : (
    <DefaultAvt />
  );
}

MwAvatar.propTypes = {
  avatar: PropTypes.string,
  first_name: PropTypes.string,
  last_name: PropTypes.string,
  color: PropTypes.string,
};

MwAvatar.defaultProps = {
  avatar: null,
  first_name: "",
  last_name: "",
  color: "",
};

export default React.memo(MwAvatar);
