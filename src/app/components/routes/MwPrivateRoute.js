import React from "react";
import { Route, Redirect } from "react-router-dom";
import { KEY_TOKEN } from "app/const/App";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const token = localStorage.getItem(KEY_TOKEN);
  return (
    <Route
      {...rest}
      render={(props) =>
        token ? <Component {...props} /> : <Redirect to="/auth/login" />
      }
    />
  );
};

export default PrivateRoute;
