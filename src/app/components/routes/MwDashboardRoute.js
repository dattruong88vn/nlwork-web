import React from "react";
import { Route } from "react-router-dom";
import MainLayout from "app/modules/layouts/MainLayout";

const DashBoardRoute = ({ component: Component, title, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => (
        <MainLayout titlePage={title}>
          <Component {...props} />
        </MainLayout>
      )}
    />
  );
};

export default DashBoardRoute;
