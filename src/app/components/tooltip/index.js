import React from "react";
import PropTypes from "prop-types";

const MwTooltip = React.forwardRef(({ children }, ref) => {
  const [visible, setVisible] = React.useState(false);

  const closeTooltip = () => {
    setVisible(false);
  };

  React.useImperativeHandle(ref, () => {
    return { openTooltip: () => setVisible(true) };
  });

  return (
    <div className="lh-1 ml-1 tooltip">
      <i
        className="fas fa-question-circle grey-7"
        onClick={() => setVisible(true)}
      />
      <div className={`tooltip-content ${visible ? "active" : ""}`}>
        <span>{children}</span>
        <span className="close" onClick={closeTooltip}>
          <i className="fal fa-times" />
        </span>
      </div>
    </div>
  );
});

MwTooltip.propTypes = {
  children: PropTypes.string,
};

MwTooltip.defaultProps = {
  children: PropTypes.string,
};
export default MwTooltip;
