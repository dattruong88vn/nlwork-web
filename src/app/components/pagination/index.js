import React from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";

function MwPagination({ itemPerPage, totalItem, total, onChange, isFiltered }) {
  const { t } = useTranslation();
  const [currentPage, setCurrentPage] = React.useState(1);
  const totalPage = Math.ceil(totalItem / itemPerPage);
  const [pages, setPages] = React.useState([]);

  React.useEffect(() => {
    isFiltered && setCurrentPage(1);
  }, [isFiltered]);

  React.useEffect(() => {
    getPager(totalItem);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [totalItem]);

  const handleChangePage = (e, page) => {
    e && e.preventDefault();
    if (page < 1 || page > totalPage || page === currentPage) return;

    getPager(totalItem);
    setCurrentPage(page);
    onChange(page);
  };

  const getPager = (totalItems) => {
    let startPage, endPage;

    if (totalPage <= 10) {
      startPage = 1;
      endPage = totalPage;
    } else {
      if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (currentPage + 4 >= totalPage) {
        startPage = totalPage - 9;
        endPage = totalPage;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }

    const pages = [...Array(endPage + 1 - startPage).keys()].map(
      (i) => startPage + i
    );

    setPages(pages);
  };

  return (
    !!totalItem && (
      <div className="px-4 pt-5 pb-7 pb-sp-0 d-flex justify-space-between align-center flex-sp-column">
        <p className="grey-7 m-0 mb-sp-3">
          {t("having")}{" "}
          <span className="black-3 fw-bold">{` ${totalItem} `}</span>{" "}
          {t("project")}
        </p>

        <ul className="breadcrumb p-0 m-0">
          <li>
            <a href="/" onClick={(e) => handleChangePage(e, currentPage - 1)}>
              <i className="fal fa-angle-left" />
            </a>
          </li>
          {pages?.length ? (
            pages.map((page, index) => (
              <li key={page}>
                <a
                  href="/"
                  onClick={(e) => handleChangePage(e, page)}
                  className={`${currentPage === page ? "active" : ""}`}
                >
                  {page}
                </a>
              </li>
            ))
          ) : (
            <li key={0}>
              <a
                href="/"
                onClick={(e) => e.preventDefault()}
                className="active"
              >
                1
              </a>
            </li>
          )}
          <li>
            <a href="/" onClick={(e) => handleChangePage(e, currentPage + 1)}>
              <i className="fal fa-angle-right" />
            </a>
          </li>
        </ul>
      </div>
    )
  );
}

MwPagination.propTypes = {
  itemPerPage: PropTypes.number,
  totalItem: PropTypes.number,
  total: PropTypes.number,
  onChange: PropTypes.func,
};

MwPagination.defaultProps = {
  itemPerPage: 1,
  totalItem: 1,
  total: 1,
  onChange: null,
};

export default React.memo(MwPagination);
