import React from "react";
import PropTypes from "prop-types";

function MwSelect({
  label,
  defaultValue,
  onChange,
  options,
  ...restSelectProps
}) {
  const [value, setValue] = React.useState();

  React.useEffect(() => {
    setValue(defaultValue);
  }, [defaultValue]);

  const handleChange = (e) => {
    setValue(e.target.value);
    if (onChange) {
      onChange(e);
    }
  };

  return (
    <>
      {label && <label className="fs-12 mb-1">{label}</label>}
      <div className="select-wrapper">
        <select
          value={value}
          className="txt-input"
          onChange={handleChange}
          {...restSelectProps}
        >
          {options.length &&
            options.map((option, index) => (
              <option key={index} value={option.value}>
                {option.content}
              </option>
            ))}
        </select>
        <i className="far fa-angle-down" />
      </div>
    </>
  );
}

MwSelect.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({})),
};

MwSelect.defaultProps = {
  options: [],
};

export default MwSelect;
