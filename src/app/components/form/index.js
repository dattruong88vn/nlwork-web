import React from "react";

const Form = React.forwardRef(
  ({ onSubmit, children, ...restFormProps }, ref) => {
    const handleSubmit = (e) => {
      e.preventDefault();
      handleValidate();
      if (onSubmit && handleValidate) {
        onSubmit();
      }
    };

    const handleValidate = () => {
      console.log(ref.current);
    };

    return (
      <div>
        <form ref={ref} onSubmit={handleSubmit} {...restFormProps}>
          {React.cloneElement(children, {
            loggedIn: true,
          })}
        </form>
      </div>
    );
  }
);

export default Form;
