import React from "react";
import PropTypes from "prop-types";
import Truncate from "./TrunCate";

function MwShowMoreText({
  lines,
  moreText,
  className,
  lessText,
  anchorClass,
  children,
}) {
  const [expanded, setExpanded] = React.useState(false);
  const [truncated, setTruncated] = React.useState(false);

  const truncateRef = React.useRef(null);

  const handleTruncate = (value) => {
    if (value !== truncated) {
      setTruncated(truncated);
      truncated && truncateRef.onResize();
    }
  };

  const toggleLines = (e) => {
    e && e.preventDefault();
    setExpanded(!expanded);
  };

  return (
    <div className={className}>
      <Truncate
        lines={!expanded && lines}
        ellipsis={
          <span>
            ...{" "}
            <a href="/" className={anchorClass} onClick={toggleLines}>
              {moreText}
            </a>
          </span>
        }
        onTruncate={handleTruncate}
        ref={truncateRef}
      >
        {children}
      </Truncate>
      {!truncated && expanded && (
        <span>
          <a href="/" className={anchorClass} onClick={toggleLines}>
            {lessText}
          </a>
        </span>
      )}
    </div>
  );
}

MwShowMoreText.propTypes = {
  children: PropTypes.node,
  lines: PropTypes.number,
  moreText: PropTypes.string,
  lessText: PropTypes.string,
  className: PropTypes.string,
};

MwShowMoreText.defaultProps = {
  lines: 3,
  moreText: "Show more",
  lessText: "Show less",
};

export default MwShowMoreText;
