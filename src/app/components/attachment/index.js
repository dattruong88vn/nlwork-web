import React from "react";
import PropTypes from "prop-types";
import { ItemAttachment } from "./components";
import "./styles/index.scss";

function MwAttachments({ name, attachments, ...restProps }) {
  return attachments?.length ? (
    attachments?.map((attachment, index) => (
      <ItemAttachment key={index} attachment={attachment} {...restProps} />
    ))
  ) : (
    <div></div>
  );
}

MwAttachments.propTypes = {
  name: PropTypes.string,
  attachments: PropTypes.array,
};

export default MwAttachments;
