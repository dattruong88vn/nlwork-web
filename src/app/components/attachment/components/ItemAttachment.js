import React, { useState } from "react";
import PropTypes from "prop-types";
import { FileMime } from "app/const/MimeType";
import { ReactComponent as PdfIcon } from "assets/images/pdf.svg";
import { ReactComponent as ExcelIcon } from "assets/images/exel.svg";
import { ReactComponent as DocsIcon } from "assets/images/docs.svg";
import { ReactComponent as FolderIcon } from "assets/images/drive.svg";

function ItemAttachment({ attachment, onRemove }) {
  const [progress, setProgress] = useState(0);

  const FileType = ({ mime, size = null }) => {
    let type;

    for (const [key, value] of Object.entries(FileMime)) {
      if (value.includes(mime)) {
        type = key;
      }
    }

    switch (type) {
      case "image":
        return (
          <img src={attachment?.url} alt="" className={!size ? "w-24" : ""} />
        );
      case "pdf":
        return <PdfIcon {...size} />;
      case "excel":
        return <ExcelIcon {...size} />;
      case "word":
        return <DocsIcon {...size} />;
      case "folder":
        return <FolderIcon {...size} />;
      default:
        return (
          <img src={attachment?.url} alt="" className={!size ? "w-24" : ""} />
        );
    }
  };

  const handleRemove = (e) => {
    e && e.preventDefault();
    if (onRemove) {
      onRemove(attachment?.id || "");
    }
  };

  return (
    <div className="attach mr-4 mr-sp-1 mt-1 border-rad5 border-grey-6 px-2 d-flex align-center justify-space-between">
      <span>
        <FileType mime={attachment?.mime} />
      </span>
      <div className="dp-inline-block ml-1 mr-3 mr-sp-2 attach-wrapper-progress">
        <div className="mb-1 lh-12 d-flex justify-space-between align-center">
          <span className="text-over fs-13">{attachment?.file?.name}</span>
          {!!progress && (
            <span className="fs-12 green-4 fw-500 ml-3">{progress}%</span>
          )}
        </div>
        <div className={`progress-bar --green per-${progress}`}>
          <span />
        </div>
      </div>
      <a href="/" onClick={handleRemove} className="attach-close grey-1">
        <i className="far fa-times" />
      </a>
    </div>
  );
}

ItemAttachment.propTypes = {
  onRemove: PropTypes.func,
  attachment: PropTypes.shape({}),
};

export default ItemAttachment;
