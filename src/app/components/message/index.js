import React from "react";
import PropTypes from "prop-types";

function MwMessage({ message, active, timeout, isDisplayNone, ...restProps }) {
  const [visible, setVisible] = React.useState(false);

  React.useEffect(() => {
    let timer;
    setVisible(active);
    timer = setTimeout(() => {
      setVisible(false);
    }, timeout);

    return () => {
      setVisible(false);
      clearTimeout(timer);
    };
  }, [active, timeout]);

  return (
    <div
      className="msg-success msg-icon mt-30"
      style={{
        visibility: visible ? "visible" : "hidden",
        display: !visible && isDisplayNone ? "none" : "block",
      }}
      {...restProps}
    >
      <i className="far fa-check ico" /> {message}
    </div>
  );
}

MwMessage.propTypes = {
  message: PropTypes.string,
  timeout: PropTypes.number,
  active: PropTypes.bool,
};
MwMessage.defaultProps = {
  message: null,
  timeout: 2000,
  active: false,
};

export default MwMessage;
