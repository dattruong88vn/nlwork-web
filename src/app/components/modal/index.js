import React, { forwardRef, useState, useImperativeHandle } from "react";
import { createPortal } from "react-dom";
import PropTypes from "prop-types";

const MwModal = forwardRef(
  (
    {
      id,
      title,
      wrapperClass,
      headClass,
      bodyClass,
      footerClass,
      closeModal,
      rightTitleMobile,
      children,
      footer,
      contentClass,
      backTitle,
      onUnmount,
      onMount,
    },
    ref
  ) => {
    const rootModal = document.getElementById("modal-root");
    const [visible, setVisible] = useState(false);

    const showModal = () => {
      setVisible(true);
    };

    const hideModal = () => {
      onUnmount && onUnmount();
      setVisible(false);
    };

    useImperativeHandle(ref, () => ({
      showModal,
      hideModal,
    }));

    if (!rootModal) return null;

    const classesWrapper = `modal ${wrapperClass}`;
    const classesHead = `modal-head ${headClass}`;
    const classesBody = `modal-body ${bodyClass}`;
    const classesFooter = `modal-footer ${footerClass}`;
    const classesContent = contentClass
      ? contentClass
      : `modal-content p-0 scroll`;

    return visible
      ? createPortal(
          <div
            id={id}
            className={classesWrapper}
            style={
              visible
                ? { height: "auto", opacity: 1, visibility: "visible" }
                : { height: 0, opacity: 0, visibility: "hidden" }
            }
          >
            <div className="backdrop" onClick={hideModal} />
            <div className={classesContent}>
              <div className={classesHead}>
                {title}

                <button className="btn-close" onClick={hideModal} type="button">
                  <i className="fal fa-times" />
                </button>
              </div>
              {backTitle && (
                <div className="dropdown-back d-flex align-center white px-4 py-3 dp-pc-hide">
                  <button className="btn btn-2 p-0" onClick={hideModal}>
                    <i className="far fa-arrow-left fw-bold fs-16" />
                  </button>
                  <span className="flex-1-0 fs-18 fw-bold ml-4">{title}</span>
                  {rightTitleMobile}
                </div>
              )}

              <div className={classesBody}>{children}</div>
              <div className={classesFooter}>{footer}</div>
            </div>
          </div>,
          rootModal
        )
      : null;
  }
);
MwModal.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  visible: PropTypes.bool,
  onClose: PropTypes.func,
  // children: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  footer: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  wrapperClass: PropTypes.string,
  headClass: PropTypes.string,
  bodyClass: PropTypes.string,
  footerClass: PropTypes.string,
  backTitle: PropTypes.bool,
  onUmount: PropTypes.func,
  onMount: PropTypes.func,
};

MwModal.defaultProps = {
  title: null,
  visible: false,
  onClose: null,
  children: null,
  wrapperClass: "modal-full d-flex justify-center align-center py-8 py-sp-0",
  headClass: "dp-sp-hide",
  bodyClass: "px-4 scroll-sp",
  footer: null,
  footerClass: "d-flex justify-space-between px-4 pb-4 dp-sp-hide",
  backTitle: true,
  onUnmount: () => {},
  onMount: () => {},
};

export default React.memo(MwModal);
