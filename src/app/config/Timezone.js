export const Timezone = [
  {
    content: "(GMT-12:00) International Date Line West",
    name: "International Date Line West",
    active: 0,
    value: "Africa/Abidjan",
  },
  {
    content: "(GMT-11:00) Midway Island",
    name: "Africa/Accra",
    active: 0,
    value: "Pacific/Midway",
  },
  {
    content: "(GMT-10:00) Hawaii",
    name: "Hawaii",
    active: 0,
    value: "Asia/Samarkand",
  },
  {
    content: "(GMT-08:00) Pacific Time (US & Canada)",
    name: "Pacific Time (US & Canada)",
    active: 0,
    value: "Asia/Singapore",
  },
  {
    content: "(GMT-08:00) Tijuana",
    name: "Tijuana",
    active: 0,
    value: "Asia/Tashkent",
  },
  {
    content: "(GMT+07:00) Ho Chi Minh",
    name: "Tijuana",
    active: 0,
    value: "Asia/Ho_Chi_Minh",
  },
];
