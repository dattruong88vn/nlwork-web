export const TASK_PERMISSION_MANAGE = {
  create: true,
  update: true,
  delete: true,
  view: true,
};

export const TASK_PERMISSION_ASSIGNEE = {
  create: false,
  update: true,
  delete: false,
  view: true,
};

export const TASK_PERMISSION_FOLLOWER = {
  create: false,
  update: false,
  delete: false,
  view: true,
};

export const TASK_PERMISSION_DEFAULT = {
  create: false,
  update: false,
  delete: false,
  view: true,
};

export const TASK_IN_PROJECT_PERMISSION_DEFAULT = {
  create: true,
  view: true,
  update: false,
  delete: false,
};

export const TASK_IN_PROJECT_PERMISSION_MANAGE = {
  create: true,
  view: true,
  update: true,
  delete: true,
};

export const TASK_IN_PROJECT_PERMISSION_ASSIGNEE = {
  create: false,
  update: true,
  delete: false,
  view: true,
};

export const TASK_IN_PROJECT_PERMISSION_FOLLOWER = {
  create: false,
  update: false,
  delete: false,
  view: true,
};

export const CONVERSATION_PERMISSION_DEFAULT = {
  create: true,
  update: false,
  delete: false,
  view: true,
};

export const CONVERSATION_PERMISSION_MANAGE = {
  create: true,
  update: true,
  delete: true,
  view: true,
};

export const DRIVE_PERMISSION_DEFAULT = {
  create: true,
  update: false,
  delete: false,
  view: true,
};

export const DRIVE_PERMISSION_MANAGE = {
  create: true,
  update: true,
  delete: true,
  view: true,
};

export const MILESTONE_PERMISSION_DEFAULT = {
  create: false,
  update: false,
  delete: false,
  view: true,
};

export const MILESTONE_PERMISSION_MANAGE = {
  create: true,
  delete: true,
  update: true,
  view: true,
};

export const TASK_MANAGE_KEY = "task.manage";
export const DRIVE_MANAGE_KEY = "drive.manage";
export const CONVERSATION_MANAGE_KEY = "conversation.manage";
export const MILESTONE_MANAGE_KEY = "milestone.manage";
export const PROJECT_CREATE_KEY = "project.create";
export const PROJECT_UPDATE_KEY = "project.manage";
