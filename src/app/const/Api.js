export const API_VERSION = "/v1";

export const API_ENDPOINT = "https://dev.mework.vn";
/* (function () {
  if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
    return "http://dev.mework.vn";
  } else {
    return "http://prod.mework.vn";
  }
})();*/

// Auth
export const LOGIN = "/auth/login";
export const REGISTER = "/auth/register";
export const REGISTER_ACTIVATE = "/auth/signup/activate";
export const RESEND_ACTIVATE = "/auth/signup/resendactivationmail";
export const RESEND_EMAIL = "/";
export const FORGOT_PASSWORD = "/user/profile/forgotpassword";
export const RESET_PASSWORD = "/user/profile/resetpassword";
export const LOGIN_INFO = "/auth/loginInfo";

// Project
export const PROJECT_LIST = "/project/list";
export const PROJECT_FEATURED = "/project/list/favorite";
export const PROJECT_CREATE = "/project/create";
export const PROJECT_DELETE = "/project/delete";
export const PROJECT_LIST_DETAIL = "/task/list/inproject";
export const PROJECT_LIST_DETAIL_CALENDAR = "/task/project/calendarinmonth";
export const PROJECT_RETRIEVE = "/project/retrieve";
export const PROJECT_SUMMARY = "/project/summary";
export const PROJECT_PIN = "/project/pin";

// Notification
export const NOTIFICATION_LIST = "/notification/list";
export const NOTIFICATION_MARK_READ = "/notification/markread";
export const NOTIFICATION_MARK_ALL_READ = "/notification/markread/all";
export const NOTIFICATION_DELETE = "/notification/delete";
export const NOTIFICATION_UNREAD = "/notification/total/unread";

// Company
export const COMPANY_SETTING = "/company/setting";
export const COMPANY_MEMBERS = "/company/members";

// User
export const UPDATE_AVATAR = "/user/profile/updateavatar";
export const UPDATE_INFORMATION = "/user/profile/update";
export const FETCH_INFORMATION = "/user/profile/retrieve";
export const USER_CHANGE_PASSWORD = "/user/profile/changepassword";

// Settings
export const NOTIFY_UPDATE = "/usersetting/update/notification";
export const UPDATE_USER_SETTING = "/usersetting/update/setting";

// Task
export const LIST_TASK = "/task/list";
export const CREATE_TASK = "/task/create";
export const UPDATE_TASK_STATUS = "/task/update/status";
export const PIN_TASK = "/task/pin";
export const UPDATE_DUE_DATE = "/task/update/duedate";
export const LIST_TASK_IN_MONTH = "/task/calendar/inmonth";
export const TASK_TOTAL_FOLLOWING = "/task/total";

// Task group
export const TASK_GROUP_UPDATE = "/taskgroup/update";
export const TASK_GROUP_DELETE = "/taskgroup/delete";
export const EDIT_TASK_GROUP = "/taskgroup/update";
export const DELETE_TASK_GROUP = "/taskgroup/delete";
export const TASK_GROUP_CREATE = "/taskgroup/create";

// Milestones
export const MILESTONE_LIST = "/milestone/list";
export const MILESTONE_CREATE = "/milestone/create";
export const MILESTONE_UPDATE = "/milestone/update";
export const MILESTONE_DELETE = "/milestone/delete";

// Drive
export const DRIVE_LIST = "/drive/list";
export const DRIVE_RENAME = "/drive/rename";
export const DRIVE_ADD_FILE = "/drive/addfile";
export const DRIVE_ADD_FOLDER = "/drive/addfolder";
export const DRIVE_DELETE_FILE = "/drive/deletefile";
export const DRIVE_DELETE_FOLDER = "/drive/deletefolder";
export const DRIVE_COPY_FILE = "/drive/copyfile";
export const DRIVE_MOVE = "/drive/move";
export const DRIVE_ADD_COMMENT = "/comment/create";

// Comment
export const COMMENT_LIST = "/comment/list";
export const COMMENT_DELETE = "/comment/delete";
export const COMMENT_UPDATE = "/comment/update";

// Conversation
export const CONVERSATION_CREATE = "/conversation/create";
export const CONVERSATION_LIST = "/conversation/list";

// Code status
export const CODE_SUCCESS = 200;
export const CODE_FAILURE = 403;
