export const TaskSortOptions = [
  { id: 0, title: "priorities", key: "priority", selected: false },
  { id: 1, title: "sort_by_create_date", key: "date_created", selected: false },
  { id: 2, title: "sort_by_due_date", key: "due_date", selected: false },
  { id: 3, title: "sort_by_alphabet", key: "alphabet", selected: false },
  { id: 4, title: "sort_by_update_date", key: "date_updated", selected: false },
  { id: 5, title: "sort_by_featured", key: "featured", selected: false },
];

export const ProjectSortOptions = [
  { id: 0, title: "sort_by_create_date", key: "date_created", selected: false },
  { id: 1, title: "sort_by_due_date", key: "end_date", selected: false },
  { id: 2, title: "sort_by_alphabet", key: "alphabet", selected: false },
];

export const TaskFollowingSortOptions = [
  { id: 0, title: "priorities", key: "priority", selected: false },
  { id: 1, title: "sort_by_create_date", key: "date_created", selected: false },
  { id: 2, title: "sort_by_due_date", key: "due_date", selected: false },
  { id: 3, title: "sort_by_alphabet", key: "alphabet", selected: false },
  { id: 4, title: "sort_by_update_date", key: "date_updated", selected: false },
  { id: 5, title: "status", key: "status", selected: false },
];

export const MilestoneSortOptions = [
  { id: 0, title: "sort_by_alphabet", key: "alphabet", selected: false },
  { id: 1, title: "newest", key: "newest", selected: false },
];

export const DriveSortOptions = [
  { id: 0, title: "sort_by_alphabet", key: "alphabet", selected: false },
  { id: 1, title: "newest", key: "newest", selected: false },
];

export const ConversationSortOptions = [
  { id: 0, title: "pin", key: "pin", selected: true },
  { id: 1, title: "newest", key: "newest", selected: false },
];
