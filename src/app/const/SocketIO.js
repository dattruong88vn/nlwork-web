export const SOCKET_ENDPOINT = "http://wsdev.mework.vn:8090";

export const SOCKET_OPTIONS = {
  transports: ["websocket", "polling"],
  forceNew: true,
};

// Realtime Room
export const JOIN_ROOM_MY_TASK = "joinMyTask";
export const LEAVE_ROOM_MY_TASK = "leaveMyTask";
export const JOIN_ROOM_PROJECT = "joinProject";
export const LEAVE_ROOM_PROJECT = "leaveProject";
export const JOIN_ROOM_USER = "joinUser";
export const LEAVE_ROOM_USER = "leaveUser";

// Realtime task
export const EVENT_TASK_ADD = "taskAdd";
export const EVENT_TASK_REMOVE = "taskRemove";
export const EVENT_TASK_UPDATE_NAME = "taskUpdateName";
export const EVENT_TASK_UPDATE_STATUS = "taskUpdateStatus";
export const EVENT_TASK_UPDATE_PRIORITY = "taskUpdatePriority";
export const EVENT_TASK_UPDATE_CHECKLIST = "taskUpdateChecklist";
export const EVENT_TASK_UPDATE_DUE_DATE = "taskUpdateDueDate";
export const EVENT_TASK_UPDATE_MILESTONE = "taskUpdateMilestone";
export const EVENT_TASK_UPDATE_ASSIGNEE = "taskUpdateAssignee";

export const EVENT_TASK_COMMENT_ADD = "commentAdd";
export const EVENT_TASK_COMMENT_REMOVE = "commentRemove";
export const EVENT_TASK_COMMENT_UPDATE = "commentUpdate";

// Realtime notification
export const NOTIFICATION_ADD = "notificationAdd";
export const NOTIFICATION_REMOVE = "notificationRemove";
export const NOTIFICATION_READ = "notificationRead";
export const NOTIFICATION_READ_ALL = "notificationReadAll";
