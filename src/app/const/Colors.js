export const COLORS_PROJECT = [
  {
    color: "#ffad0d",
    active: true,
  },
  {
    color: "#8b45ff",
    active: false,
  },
  {
    color: "#18bfff",
    active: false,
  },
  {
    color: "#ff07c2",
    active: false,
  },
  {
    color: "#ff6f2b",
    active: false,
  },
  {
    color: "#06d49a",
    active: false,
  },
];

export const COLOR_PRIORITY = "#b5b8c0";
export const COLOR_STATUS = "#0b66c8";
export const COLOR_CIRCLE = "#e1e4e8";
export const AVATAR_COLOR = "#f03d3d";
