export const APP_VERSION = "1.0";

// KEY APP
export const KEY_TOKEN = "token";

export const I18N_KEY = "i18nextLng";
export const LIMIT_LIST_NOTIFY = 15;
export const LIVE_STATUS = "live_status";
export const DATE_MAX = 2147501177;
export const DATE_MIN = 29177;

export const KEY_CODE_ESCAPE = 27;
