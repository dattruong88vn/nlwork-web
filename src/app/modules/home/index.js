import React from "react";
import "./styles/index.scss";
import { useSiteTitle } from "core/hooks/useSiteTitle";

function HomePage({ getUsers }) {
  useSiteTitle("home_page");
  return (
    <div className="d-flex justify-space-between align-center wl pt-6 pt-sp-0 mb-6">
      <div className="wl-left">
        <p className="wl-name mb-2 mb-sp-1">Welcome Thuong</p>
        <div className="wl-time fw-400">Thứ 5, 20/10/2020</div>
      </div>
    </div>
  );
}

export default HomePage;
