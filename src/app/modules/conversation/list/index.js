import React, { useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { usePermissions, useToast } from "core/hooks";
import { fetchWithToken, postFormData } from "core/utils/APIUtils";
import { AddConversation } from "app/modules/conversation/components";
import TableLoading from "app/components/loading/TableLoading";
import {
  CODE_SUCCESS,
  CONVERSATION_CREATE,
  CONVERSATION_LIST,
} from "app/const/Api";
import { HeaderListConversation, ListConversation } from "./components";
import "./styles/index.scss";

function Conversations({ projectId }) {
  const { t } = useTranslation();
  const { getPermission } = usePermissions();
  const { addToast } = useToast();
  const [loading, setLoading] = useState(true);
  const [loadingCreate, setLoadingCreate] = useState(false);
  const [conversations, setConversations] = useState([]);
  const [permissionCreate, setPermissionCreate] = useState(false);
  const refAddModal = useRef(null);

  useEffect(() => {
    const permissionInfo = getPermission({ project_id: projectId });

    setPermissionCreate(permissionInfo?.create);
  }, [getPermission, projectId]);

  useEffect(() => {
    fetchWithToken(CONVERSATION_LIST, {
      project_id: projectId,
      fields: "total_comment,total_conversation",
      sort: "pin",
    })
      .then((result) => {
        if (result.meta.code === CODE_SUCCESS) {
          setConversations(result.data.items);
          setLoading(false);
        }
      })
      .catch((err) => {
        console.error(err);
        setLoading(false);
      });
  }, [projectId]);

  const handleFilter = (sort) => {
    fetchWithToken(CONVERSATION_LIST, {
      project_id: projectId,
      fields: "total_comment,total_conversation",
      sort,
    })
      .then((result) => {
        if (result.meta.code === CODE_SUCCESS) {
          setConversations(result.data.items);
          setLoading(false);
        }
      })
      .catch((err) => {
        console.error(err);
        setLoading(false);
      });
  };

  const handleOpenAddModal = (e) => {
    e && e.preventDefault();
    refAddModal.current.showModal();
  };

  const handleSubmitAdd = (data) => {
    setLoadingCreate(true);
    const formData = new FormData();

    formData.append("project_id[]", projectId);
    formData.append("subject", data?.subject || "");
    formData.append("content", data?.content || "");

    if (data?.attachments?.length) {
      for (const key in data.attachments) {
        const element = data.attachments[key];
        if (element?.file) {
          formData.append("attachments[]", element.file || "");
        }
      }
    }

    postFormData(CONVERSATION_CREATE, formData)
      .then((res) => {
        if (res?.meta?.code === CODE_SUCCESS) {
          refAddModal.current.hideModal();
          addToast({
            id: "add_conversation",
            content: t("add_conversation_success"),
            placement: "right-bottom",
            timeout: 3000,
          });
          setLoadingCreate(false);
          setConversations((conversations) => [...res.data, ...conversations]);
        }
      })
      .catch((err) => {
        console.error(err);
        setLoadingCreate(false);
      });
  };

  return (
    <>
      <AddConversation
        ref={refAddModal}
        onSubmit={handleSubmitAdd}
        loading={loadingCreate}
      />
      <HeaderListConversation
        permissionCreate={permissionCreate}
        onFilter={handleFilter}
        onOpenAddModal={handleOpenAddModal}
      />

      {loading ? (
        <>
          <TableLoading />
        </>
      ) : (
        <>
          <div className="dp-sp-hide">
            <div className="table pt-sp-4">
              <div className="thead dp-sp-hide">
                <div className="table-row fs-12 d-flex">
                  <div className="table-head w-50 pl-4">
                    {t("name_conversation")}
                  </div>
                  <div className="table-head w-20">{t("created_by")}</div>
                  <div className="table-head w-15">{t("comment")}</div>
                  <div className="table-head w-15">{t("create_date")}</div>
                </div>
              </div>
            </div>
          </div>

          <ListConversation
            conversations={conversations}
            projectId={projectId}
          />
        </>
      )}
    </>
  );
}

Conversations.propTypes = {
  projectId: PropTypes.string.isRequired,
};

export default Conversations;
