import React from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import LazyLoad from "react-lazyload";
import EmptyList from "app/components/emptyList";
import ItemConversation from "./ItemConversation";

function ListConversation({ conversations = [] }) {
  const { t } = useTranslation();

  return !conversations?.length ? (
    <EmptyList emptyText={t("")} />
  ) : (
    conversations.map((conversation, index) => (
      <LazyLoad key={index} once height={70} debounce={100} offset={[200, 0]}>
        <ItemConversation {...conversation} />
      </LazyLoad>
    ))
  );
}

ListConversation.propTypes = {
  conversations: PropTypes.arrayOf(PropTypes.shape({})),
  projectId: PropTypes.string.isRequired,
};

export default ListConversation;
