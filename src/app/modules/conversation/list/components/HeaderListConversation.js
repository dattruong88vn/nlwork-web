import React, { useState } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { SortTask } from "app/modules/task/components";
import { ConversationSortOptions } from "app/const/SortOptions";

function HeaderListConversation({
  permissionCreate = true,
  onFilter,
  onOpenAddModal,
}) {
  const { t } = useTranslation();
  const [sortValue, setSortValue] = useState("pin");

  const handleSort = (value) => {
    setSortValue(value);
    onFilter && onFilter(value);
  };

  return (
    <>
      <div className="p-pc-2 justify-space-between align-center border-bottom-grey-6 relative d-flex">
        <div className="d-flex align-center">
          <SortTask
            wrapperClass="dp-sp-hide"
            sortValue={ConversationSortOptions}
            handleSort={handleSort}
            activeSort={!!sortValue}
            showSelected={false}
            clearUnmount={false}
            isProject
          />

          {permissionCreate && (
            <Link
              to="/"
              className="dp-sp-hide btn btn-add-project btn-h-32 white fs-14 fs-sp-12 pl-sp-2 d-flex align-center justify-space-between"
              onClick={onOpenAddModal}
            >
              <i className="fal fa-plus fs-16 mr-2 mr-sp-1" />{" "}
              {t("create_new_conversation")}
            </Link>
          )}
        </div>

        <div className="d-flex flex-sp-1-0 justify-sp-space-between align-center relative p-2 px-sp-3 py-sp-2 bg-sp-grey-2">
          <div className="d-flex align-center">
            <SortTask
              wrapperClass="dp-pc-hide"
              sortValue={ConversationSortOptions}
              handleSort={handleSort}
              activeSort={!!sortValue}
              showSelected={false}
              clearUnmount={false}
              isProject
            />
          </div>
        </div>
      </div>
    </>
  );
}

HeaderListConversation.propTypes = {
  permissionCreate: PropTypes.bool,
};

export default HeaderListConversation;
