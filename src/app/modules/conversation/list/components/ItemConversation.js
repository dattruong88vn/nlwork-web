import React, { useEffect, useState } from "react";
import moment from "moment-timezone";
import PropTypes from "prop-types";
import find from "lodash/find";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { ReactComponent as IconPined } from "assets/images/pined.svg";

function ItemConversation({
  subject,
  id,
  created_by,
  created,
  total_comment,
  featured,
}) {
  const { t } = useTranslation();
  const [creator, setCreator] = useState({});
  const [date, setDate] = useState(null);
  const members = useSelector((state) => state.company.members);
  const { timezone } = useSelector((state) => state.user.userInfo.company_info);

  useEffect(() => {
    if (created_by) {
      const creatorFined = find(members, ["id", created_by]);
      setCreator(creatorFined);
    }
    created &&
      setDate(moment.tz(+created * 1000, timezone).format("DD/MM/YYYY"));
  }, [created, created_by, members, timezone]);

  return (
    <div className={`conversation__item hover-5 ${!!featured && "pined"}`}>
      <div>
        <Link to={`/project-detail/conversation/detail/${id}`}>
          {subject} {!!featured && <IconPined />}
        </Link>
        <div className="grey-7 fs-12">
          {t("created_by")}
          <Link
            to="/"
            className="ml-1 blue-1 mr-1 fs-12"
          >{`${creator?.first_name} ${creator?.last_name}`}</Link>
          <span className="fs-11 mx-2">•</span>
          <span>{date}</span>
        </div>
      </div>
      <div>{`${creator?.first_name} ${creator?.last_name}`}</div>
      <div>
        {!!total_comment && (
          <span className="mr-4 fs-12 d-flex align-center grey-7">
            <i className="fal fa-comment-alt fs-15 mr-1 "></i> {total_comment}
          </span>
        )}
      </div>
      <div>{date}</div>
    </div>
  );
}

ItemConversation.propTypes = {
  subject: PropTypes.string,
  id: PropTypes.string,
  created_by: PropTypes.string,
  created: PropTypes.string,
  total_comment: PropTypes.number,
  featured: PropTypes.number,
};

export default ItemConversation;
