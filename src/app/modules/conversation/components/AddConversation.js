import React, {
  forwardRef,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { MwButton, MwInput, MwModal, MwRichText } from "app/components";
import "../styles/index.scss";

const AddConversation = forwardRef(({ loading, onSubmit }, ref) => {
  const { t } = useTranslation();
  const [errors, setErrors] = useState({});
  const refModal = useRef(null);
  const refForm = useRef(null);
  const refRichText = useRef(null);

  useImperativeHandle(ref, () => ({
    hideModal,
    showModal,
  }));

  const hideModal = () => {
    refModal.current.hideModal();
  };

  const showModal = () => {
    refModal.current.showModal();
  };

  const handleSubmit = () => {
    setErrors({});
    const error = {};
    const content = refRichText.current.handleSubmitRichText();
    const subject = refForm.current["subject"].value;
    const contentValue = content.content.replace(
      /(<p[^>]+?>|<p>|<\/p>)/gim,
      ""
    );

    if (subject?.length > 250) {
      error["subject"] = t("subject_max_250");
    }

    if (!contentValue.trim()?.length) {
      error["content"] = t("pls_enter_content_conversation");
    }

    if (!subject?.trim()?.length) {
      error["subject"] = t("pls_enter_title_conversation");
    }

    if (Object.keys(error).length) {
      return setErrors(error);
    }

    if (subject?.trim()?.length && contentValue?.trim()?.length) {
      onSubmit && onSubmit({ subject, ...content });
    }
  };

  const MobileRightTitle = () => {
    return (
      <div className="form">
        <div className="d-flex btn-modal-dr">
          <MwButton
            title={`${t("create")}`}
            loading={loading}
            className="btn btn-2 p-0 fs-15 fw-bold center-2"
            onClick={handleSubmit}
          />
        </div>
      </div>
    );
  };

  const FooterModal = (params) => (
    <>
      <div className="d-flex form">
        <MwButton
          title={t("cancel")}
          onClick={hideModal}
          className="btn btn-4 w-87 fs-13 fw-normal transparent"
        />
        <MwButton
          title={`${t("create")}`}
          loading={loading}
          className="btn btn-2 w-87 fs-14 fw-bold center-2"
          onClick={handleSubmit}
          tabIndex="3"
        />
      </div>
    </>
  );

  return (
    <MwModal
      ref={refModal}
      id="add-conversation"
      title={t("create_new_conversation")}
      rightTitleMobile={<MobileRightTitle />}
      footer={<FooterModal />}
      onUnmount={() => setErrors({})}
    >
      <form ref={refForm} onSubmit={(e) => e.preventDefault()}>
        <div className="form-group mb-5">
          <MwInput
            label={t("title_conversation")}
            name="subject"
            error={errors?.subject}
            customLabel={{ className: "fs-12 mb-1" }}
            placeholder={t("enter_title")}
            autoFocus
            tabIndex="1"
          />
        </div>
        <div className="form-group">
          <MwRichText
            ref={refRichText}
            error={errors?.content}
            label={t("conversation")}
            placeholder={t("enter_content_conversation")}
          />
        </div>
      </form>
    </MwModal>
  );
});

AddConversation.propTypes = {
  loading: PropTypes.bool,
  onSubmit: PropTypes.func.isRequired,
};

export default AddConversation;
