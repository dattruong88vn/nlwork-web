import Conversations from "./list";
import ConversationDetail from "./detail";

export { Conversations, ConversationDetail };
