import React from "react";
import { useTranslation } from "react-i18next";
import { useSelector, useDispatch } from "react-redux";
import PropTypes from "prop-types";
import MwCheckbox from "app/components/checkbox";
import { updateNotifyRequest } from "core/redux/actions/settingAction";
import MwButton from "app/components/button";

function SettingNotification({ tabActive, tabIndex }) {
  const { t } = useTranslation();
  const { userInfo } = useSelector((state) => state.user);
  const { loading, success } = useSelector((state) => state.settings);
  const refForm = React.useRef(null);
  const { setting_info } = userInfo || {};
  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData();

    for (let index = 0; index < refForm.current.length - 1; index++) {
      const element = refForm.current[index];
      const value = element.checked ? 1 : 0;
      formData.append(`set[${element.name}]`, value);
    }
    dispatch(updateNotifyRequest(formData));
  };

  return (
    <div
      className={`tabs-content pt-5 px-6 px-sp-3 ${
        tabActive === tabIndex ? "show" : ""
      }`}
    >
      <form ref={refForm} onSubmit={handleSubmit}>
        <div className="border-bottom-grey-6 pb-4">
          <p className="mb-2 fs-15 fw-bold">{t("task")}</p>
          <p className="mb-5 fs-13 grey-7 fw-normal">{t("notice_task")}</p>
          <MwCheckbox
            name="task.on_mework"
            defaultValue={setting_info?.notification?.task?.on_mework}
            label={t("notify_on_mework")}
            wrapperClass="d-flex align-center justify-space-between"
          />
        </div>
        <div className="border-bottom-grey-6 pb-4 pt-4">
          <p className="mb-5 fs-15 fw-bold">{t("place_you_receive_notify")}</p>
          <MwCheckbox
            name="task.push_notification"
            defaultValue={setting_info?.notification?.task?.push_notification}
            label={t("push_notify")}
            wrapperClass="d-flex align-center justify-space-between mb-6"
          />
          <MwCheckbox
            name="task.email"
            defaultValue={setting_info?.notification?.task?.email}
            label={t("email")}
            wrapperClass="d-flex align-center justify-space-between mb-6"
          />
          <MwCheckbox
            name="task.on_web"
            defaultValue={setting_info?.notification?.task?.on_web}
            label={t("notify_on_web")}
            wrapperClass="d-flex align-center justify-space-between"
          />
        </div>
        <div className="border-bottom-grey-6 pb-4 pt-4">
          <p className="mb-2 fs-15 fw-bold">{t("comment")}</p>
          <p className="mb-5 fs-13 grey-7 fw-normal">{t("notice_comment")}</p>
          <MwCheckbox
            name="comment.on_mework"
            defaultValue={setting_info?.notification?.comment?.on_mework}
            label={t("notify_on_mework")}
            wrapperClass="d-flex align-center justify-space-between"
          />
        </div>
        <div className="border-bottom-grey-6 pb-4 pt-4 mb-8">
          <p className="mb-5 fs-15 fw-bold">{t("place_you_receive_notify")}</p>
          <MwCheckbox
            name="comment.push_notification"
            defaultValue={
              setting_info?.notification?.comment?.push_notification
            }
            label={t("push_notify")}
            wrapperClass="d-flex align-center justify-space-between mb-6"
          />
          <MwCheckbox
            name="comment.email"
            defaultValue={setting_info?.notification?.comment?.email}
            label={t("email")}
            wrapperClass="d-flex align-center justify-space-between mb-6"
          />
          <MwCheckbox
            name="comment.on_web"
            defaultValue={setting_info?.notification?.comment?.on_web}
            label={t("notify_on_web")}
            wrapperClass="d-flex align-center justify-space-between"
          />
        </div>

        <div class="pt-4 mb-8">
          <div class="checkbox checkbox-primary mb-3">
            <input type="checkbox" id="cb-12" name="demo1" checked="" />
            <label for="cb-12" class="fs-13 fw-bold">
              Daily summaries
              <br />
              <span class="fs-12 grey-7 fw-normal">
                New task assigned to you and upcoming due dates
              </span>
            </label>
          </div>
          <div class="checkbox checkbox-primary mb-3">
            <input type="checkbox" id="cb-22" name="demo2" />
            <label for="cb-22" class="fs-13 fw-bold">
              Weekly Report
              <br />
              <span class="fs-12 grey-7 fw-normal">
                Status updates on projects in your portfolios
              </span>
            </label>
          </div>
        </div>

        <div className="form-group text-pc-right text-sp-center">
          <MwButton
            loading={loading}
            title={t("update")}
            type="submit"
            className="btn btn-2 px-6 fs-14 fw-bold center"
          />
        </div>
      </form>
      {success && (
        <div className="msg-success msg-icon mt-30">
          <i className="far fa-check ico"></i> {t("updated_notify_setting")}
        </div>
      )}
    </div>
  );
}

SettingNotification.proTypes = {
  tabActive: PropTypes.number,
  tabIndex: PropTypes.number,
};

SettingNotification.defaultProps = {
  tabActive: 1,
  tabIndex: 1,
};

export default SettingNotification;
