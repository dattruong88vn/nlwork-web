import React from "react";
import { useTranslation } from "react-i18next";
import MwTabs from "app/components/tabs";
import UpdateInfo from "./UpdateInfo";
import SettingNotification from "./SettingNotification";
import Settings from "./settings/Settings";

function AccountSetting({ activeDefault = 1 }) {
  const { t } = useTranslation();
  const [activeTab, handleTab] = React.useState(1);

  React.useEffect(() => {
    handleTab(activeDefault);
  }, [activeDefault]);

  const ListTab = React.useMemo(
    () => [
      { label: t("info"), value: 1 },
      { label: t("notification"), value: 2 },
      { label: t("setting"), value: 3 },
    ],
    [t]
  );

  return (
    <>
      <MwTabs tabs={ListTab} activeTab={activeTab} handleTab={handleTab} />
      <div className="tabs-body">
        <UpdateInfo tabActive={activeTab} tabIndex={1} />
        <SettingNotification tabActive={activeTab} tabIndex={2} />
        <Settings tabActive={activeTab} tabIndex={3} />
      </div>
    </>
  );
}

export default AccountSetting;
