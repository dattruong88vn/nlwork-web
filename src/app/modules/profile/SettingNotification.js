import React from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import PropTypes from "prop-types";
import MwMessage from "app/components/message";
import MwCheckbox from "app/components/checkbox";
import MwButton from "app/components/button";
import { putFormData } from "core/utils/APIUtils";
import { CODE_SUCCESS, NOTIFY_UPDATE } from "app/const/Api";
import { fetchUserRequest } from "core/redux/actions/userAction";

function SettingNotification({ tabActive, tabIndex }) {
  const { t } = useTranslation();
  const { notification } = useSelector(
    (state) => state.user.userInfo.setting_info
  );
  const [loading, setLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const [visible, setVisible] = React.useState(false);
  const dispatch = useDispatch();
  const refForm = React.useRef(null);

  React.useEffect(() => {
    if (success) {
      setVisible(true);
    }
  }, [success]);

  React.useEffect(() => {
    let timer;
    setVisible(success);
    if (success) {
      timer = setTimeout(() => {
        setVisible(false);
      }, 10000);
    }
    return () => {
      clearTimeout(timer);
    };
  }, [success]);

  const handleSubmit = (e) => {
    e && e.preventDefault();
    setLoading(true);
    setSuccess(false);
    const formData = new FormData();

    for (let index = 0; index < refForm.current.length - 1; index++) {
      const element = refForm.current[index];
      const value = element.checked ? 1 : 0;
      formData.append(`set[${element.name}]`, value);
    }

    putFormData(NOTIFY_UPDATE, formData).then((res) => {
      if (res.meta.code === CODE_SUCCESS) {
        setLoading(false);
        setSuccess(true);
        dispatch(fetchUserRequest());
      } else {
        setLoading(false);
      }
    });
  };

  return (
    <div
      className={`tabs-content pt-5 px-6 px-sp-3 ${
        tabActive === tabIndex ? "show" : ""
      }`}
    >
      <form ref={refForm} onSubmit={handleSubmit}>
        <div className="border-bottom-grey-6 pb-4">
          <p className="mb-2 fs-15 fw-bold">{t("task")}</p>
          <p className="mb-5 fs-13 grey-7 fw-normal">{t("notice_task")}</p>
          <MwCheckbox
            name="task.on_mework"
            defaultValue={notification?.task?.on_mework}
            label={t("notify_on_mework")}
            wrapperClass="d-flex align-center justify-space-between"
          />
        </div>
        <div className="border-bottom-grey-6 pb-4 pt-4">
          <p className="mb-5 fs-15 fw-bold">{t("place_you_receive_notify")}</p>
          <MwCheckbox
            name="task.push_notification"
            defaultValue={notification?.task?.push_notification}
            label={t("push_notify")}
            wrapperClass="d-flex align-center justify-space-between mb-6"
          />
          <MwCheckbox
            name="task.email"
            defaultValue={notification?.task?.email}
            label={t("email")}
            wrapperClass="d-flex align-center justify-space-between mb-6"
          />
          <MwCheckbox
            name="task.on_web"
            defaultValue={notification?.task?.on_web}
            label={t("notify_on_web")}
            wrapperClass="d-flex align-center justify-space-between"
          />
        </div>
        <div className="border-bottom-grey-6 pb-4 pt-4">
          <p className="mb-2 fs-15 fw-bold">{t("comment")}</p>
          <p className="mb-5 fs-13 grey-7 fw-normal">{t("notice_comment")}</p>
          <MwCheckbox
            name="comment.on_mework"
            defaultValue={notification?.comment?.on_mework}
            label={t("notify_on_mework")}
            wrapperClass="d-flex align-center justify-space-between"
          />
        </div>
        <div className="border-bottom-grey-6 pb-4 pt-4">
          <p className="mb-5 fs-15 fw-bold">{t("place_you_receive_notify")}</p>
          <MwCheckbox
            name="comment.push_notification"
            defaultValue={notification?.comment?.push_notification}
            label={t("push_notify")}
            wrapperClass="d-flex align-center justify-space-between mb-6"
          />
          <MwCheckbox
            name="comment.email"
            defaultValue={notification?.comment?.email}
            label={t("email")}
            wrapperClass="d-flex align-center justify-space-between mb-6"
          />
          <MwCheckbox
            name="comment.on_web"
            defaultValue={notification?.comment?.on_web}
            label={t("notify_on_web")}
            wrapperClass="d-flex align-center justify-space-between"
          />
        </div>
        <div className="pt-4 mb-8">
          <MwCheckbox
            isHorizontal
            name="daily_report.email"
            defaultValue={+notification?.daily_report.email === 1}
            id="daily_report"
            customLabel={{ className: "fs-13 fw-bold" }}
            wrapperClass="checkbox checkbox-primary mb-3"
            label={
              <>
                {t("daily_summary")}
                <br />
                <span className="fs-12 grey-7 fw-normal">
                  {t("daily_summary_description")}
                </span>
              </>
            }
          />
          <MwCheckbox
            isHorizontal
            name="weekly_report.email"
            defaultValue={+notification?.weekly_report.email === 1}
            id="weekly_report"
            customLabel={{ className: "fs-13 fw-bold" }}
            wrapperClass="checkbox checkbox-primary"
            label={
              <>
                {t("weekly_report")}
                <br />
                <span className="fs-12 grey-7 fw-normal">
                  {t("weekly_report_description")}
                </span>
              </>
            }
          />
        </div>
        {visible && (
          <MwMessage
            active={visible}
            isDisplayNone
            message={t("updated_notify_setting")}
            className="msg-success msg-icon mb-5"
            timeout={10000}
          />
        )}

        <div className="form-group text-pc-right text-sp-center">
          <MwButton
            loading={loading}
            title={t("update")}
            type="submit"
            className="btn btn-2 px-6 fs-14 fw-bold center"
            onClick={handleSubmit}
            preventEvent
          />
        </div>
      </form>
    </div>
  );
}

SettingNotification.proTypes = {
  tabActive: PropTypes.number,
  tabIndex: PropTypes.number,
};

SettingNotification.defaultProps = {
  tabActive: 1,
  tabIndex: 1,
};

export default SettingNotification;
