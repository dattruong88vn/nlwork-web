import React from "react";
import PropTypes from "prop-types";

const Accordion = React.forwardRef(
  (
    {
      title,
      subTitle,
      textCollapse,
      collapsed,
      children,
      className,
      classWrapper,
      success,
      errors,
      loading,
    },
    ref
  ) => {
    const refChildren = React.useRef(null);
    const height = refChildren?.current?.children[0]?.clientHeight;
    const [maxHeight, setMaxHeight] = React.useState();
    const [visible, setVisible] = React.useState(false);

    React.useEffect(() => {
      setMaxHeight(height);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [errors]);

    React.useEffect(() => {
      height !== 0 && setMaxHeight(height + 70);
    }, [height]);

    React.useEffect(() => {
      let timer;

      if (success) {
        height !== 0 && setMaxHeight(height + 70);
        timer = setTimeout(() => {
          setMaxHeight(maxHeight);
        }, 10000);
      }

      return () => {
        clearTimeout(timer);
        setMaxHeight(maxHeight);
      };
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [success]);

    const handleOpen = () => {
      setVisible(true);
    };

    const handleClose = () => {
      setVisible(false);
    };

    React.useImperativeHandle(ref, () => ({
      handleClose,
    }));

    return (
      <div className={className}>
        <div className="d-flex justify-space-between align-center">
          <div className={classWrapper}>
            <p className="fs-13 fw-bold mb-1">{title}</p>
            <p className="fs-12 grey-7 m-0">{subTitle}</p>
          </div>
          <span
            className={`fw-500 blue-1 nowrap accordion-toggle ${
              visible ? "visibility-hide" : ""
            }`}
            onClick={handleOpen}
          >
            {textCollapse}
          </span>
        </div>
        <div
          className="panel"
          ref={refChildren}
          style={{ maxHeight: visible ? maxHeight || height + 70 : 0 }}
        >
          {children}
        </div>
      </div>
    );
  }
);

Accordion.propTypes = {
  onClick: PropTypes.func,
  className: PropTypes.string,
  classWrapper: PropTypes.string,
};

Accordion.defaultProps = {
  onClick: null,
  className: "accordion border-bottom-grey-6 pb-3",
  classWrapper: "",
};

export default Accordion;
