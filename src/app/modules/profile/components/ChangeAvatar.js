import React from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { updateAvatarRequest } from "core/redux/actions/userAction";
import MwAvatar from "app/components/avatar";
import Input from "app/components/input";

function ChangeAvatar() {
  const { t } = useTranslation();
  const [img, setImg] = React.useState();
  const { newAvatar, userInfo } = useSelector((state) => state.user);
  const [error, setError] = React.useState("");
  const hiddenRef = React.useRef(null);

  const handleClick = (e) => {
    hiddenRef.current.onClick();
  };
  const dispatch = useDispatch();

  const handleChange = (e) => {
    setError("");
    const file = e.target.files[0];
    if (!file) {
      return;
    }
    if (!file.name.match(/\.(jpg|jpeg|png|gif)$/)) {
      return setError(t("just_support_img_2mb"));
    }
    if (file.size / 1024 / 1024 < 1) {
      const formData = new FormData();
      if (file) {
        formData.append("avatar", file, file.name);
        dispatch(updateAvatarRequest(formData));
      }
    } else {
      setError(t("just_support_img_2mb"));
    }
  };

  // Reload avt when user update avatar
  React.useEffect(() => {
    if (newAvatar) {
      setImg(newAvatar.link);
    }
  }, [newAvatar]);

  // Initial avatar form DB
  React.useEffect(() => {
    setImg(userInfo?.avatar);
  }, [userInfo]);

  return (
    <>
      <div className="form-group mb-5">
        <div className="media media-profile d-flex flex-sp-column">
          <div className="media-img media-center mr-6 mr-sp-0">
            <MwAvatar
              avatar={img}
              first_name={userInfo?.first_name}
              last_name={userInfo?.last_name}
              styleDefaultAvatar={{
                width: "100%",
                height: "100%",
                fontSize: "2rem",
              }}
            />
          </div>

          <div className="media-body mt-sp-2 flex-1-0">
            <p className="mt-0 mb-3 fw-bold fs-13">{t("avatar")}</p>
            <div className="media-upload">
              <button className="btn btn-file btn-h-32" onClick={handleClick}>
                {t("upload_new_avatar")}
              </button>
              <Input
                error={error}
                className="dp-hide"
                ref={hiddenRef}
                onChange={handleChange}
                classError="left"
                type="file"
              />
            </div>
            <p className="mt-3 mb-0 fs-12 grey-7 fw-normal">
              {t("support_img_2mb")}
            </p>
          </div>
        </div>
      </div>
    </>
  );
}

export default ChangeAvatar;
