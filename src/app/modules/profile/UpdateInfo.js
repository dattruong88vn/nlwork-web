import React from "react";
import moment from "moment-timezone";
import { useTranslation } from "react-i18next";
import { useSelector, useDispatch } from "react-redux";
import { updateUserInfoStored } from "core/redux/actions/userAction";
import ChangeAvatar from "./components/ChangeAvatar";
import MwInput from "app/components/input";
import MwButton from "app/components/button";
import { phoneValidate } from "core/utils/PhoneUtils";
import { convertDate } from "core/utils/DateUtils";
import { putWithToken } from "core/utils/APIUtils";
import { CODE_SUCCESS, UPDATE_INFORMATION } from "app/const/Api";
import MwMessage from "app/components/message";
import MwSelect from "app/components/select";

function UpdateInfo({ tabActive, tabIndex }) {
  const { t } = useTranslation();
  const { userInfo } = useSelector((state) => state.user);
  const { company_roles } = useSelector((state) => state.company.settings);
  const [roles, setRoles] = React.useState([]);
  const [role, setRole] = React.useState(null);
  const [errors, setErrors] = React.useState({});
  const [loading, setLoading] = React.useState(false);
  const [visibleNotification, setVisibleNotification] = React.useState(false);
  const [valueInput, setValueInput] = React.useState({});

  const dispatch = useDispatch();
  const refForm = React.useRef(null);

  React.useEffect(() => {
    let timer;
    if (visibleNotification) {
      setVisibleNotification(true);
      timer = setTimeout(() => {
        setVisibleNotification(false);
      }, 10000);
    }

    return () => {
      clearTimeout(timer);
      setVisibleNotification(false);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [visibleNotification]);

  React.useEffect(() => {
    const roles = [];
    let role = null;
    company_roles.forEach((el) => {
      roles.push({ content: el.name, value: el.value });
      if (el.id === userInfo.company_role_id) {
        role = el;
      }
    });

    setRole(role);
    setRoles(roles);
  }, [company_roles, userInfo]);

  // Set default value from userInfo to input fields
  React.useEffect(() => {
    if (userInfo) {
      const {
        first_name,
        last_name,
        phone,
        intro,
        birthday,
        address,
        email,
      } = userInfo;
      setValueInput({
        first_name,
        last_name,
        phone,
        intro,
        birthday,
        address,
        email,
      });
    }
  }, [userInfo]);

  const handleSubmit = (e) => {
    e && e.preventDefault();
    const errors = {};
    setErrors({});

    const phone = refForm.current["phone"].value;
    const last_name = refForm.current["last_name"].value;
    const first_name = refForm.current["first_name"].value;
    const birthday = refForm.current["birthday"].value;
    const address = refForm.current["address"].value;
    const intro = refForm.current["intro"].value;
    const data = {};

    for (let index = 0; index < refForm.current.length; index++) {
      const { required, value, name, type, dataset } = refForm.current[index];
      if (type === "text" && required && !value.trim().length) {
        errors[name] = dataset.helper;
      }
    }

    if (first_name.length > 100) errors["first_name"] = t("limit_first_name");
    if (last_name.length > 100) errors["last_name"] = t("limit_last_name");
    if (address.length > 255) errors["address"] = t("limit_address");
    if (intro.length > 255) errors["intro"] = t("limit_intro");

    if (phone && !phoneValidate(phone))
      errors["phone"] = t("pls_enter_correct_phone");

    if (birthday) {
      if (
        !convertDate(birthday) ||
        !moment(birthday, "DD/MM/YYYY", true).isValid()
      ) {
        errors["birthday"] = t("invalid_date");
      }
    }

    if (Object.keys(errors).length) return setErrors(errors);

    for (let index = 0; index < refForm.current.length; index++) {
      const element = refForm.current[index];
      data[element.name] = element.value;
    }

    setLoading(true);
    putWithToken(UPDATE_INFORMATION, {
      ...data,
      birthday: birthday ? convertDate(birthday) : "",
    }).then((res) => {
      if (res.meta.code === CODE_SUCCESS) {
        setLoading(false);
        setVisibleNotification(true);
        dispatch(updateUserInfoStored(res.data));
      } else {
        setLoading(false);
      }
    });
  };

  const handleChange = (e) => {};

  return (
    <div
      className={`tabs-content pt-8 px-6 px-sp-3 ${
        tabActive === tabIndex ? "show" : ""
      }`}
    >
      <ChangeAvatar />
      <form ref={refForm}>
        <div className="form-group row mx-n3 mb-5">
          <div className="col">
            <MwInput
              required
              defaultValue={valueInput?.last_name}
              name="last_name"
              customLabel={{ className: "fs-12 mb-1" }}
              placeholder={t("enter_your_last_name")}
              label={t("last_name")}
              onChange={handleChange}
              data-helper={t("pls_enter_last_name")}
              error={errors?.last_name}
            />
          </div>
          <div className="col">
            <MwInput
              required
              defaultValue={valueInput?.first_name}
              name="first_name"
              customLabel={{ className: "fs-12 mb-1" }}
              placeholder={t("enter_your_first_name")}
              label={t("first_name")}
              onChange={handleChange}
              data-helper={t("pls_enter_first_name")}
              error={errors?.first_name}
            />
          </div>
        </div>
        <div className="form-group row mx-n3 mb-5">
          <div className="col-6 col-sp-12 mb-sp-5">
            <MwInput
              defaultValue={valueInput?.phone}
              name="phone"
              customLabel={{ className: "fs-12 mb-1" }}
              placeholder={t("enter_your_phone")}
              label={t("phone")}
              onChange={handleChange}
              error={errors?.phone}
            />
          </div>
          <div className="col-6 col-sp-12">
            <MwInput
              isDate
              maxDate={new Date()}
              date={userInfo?.birthday && new Date(userInfo?.birthday)}
              name="birthday"
              customLabel={{ className: "fs-12 mb-1" }}
              placeholder={t("enter_your_birth_day")}
              label={t("birth_day")}
              onChange={handleChange}
              data-helper={t("pls_enter_birthday")}
              error={errors?.birthday}
            />
          </div>
        </div>
        <div className="form-group row mx-n3 mb-5">
          <div className="col-6 col-sp-12 mb-sp-5">
            <MwInput
              defaultValue={valueInput?.email}
              name="email"
              customLabel={{ className: "fs-12 mb-1" }}
              label={t("email")}
              disabled
            />
          </div>
          <div className="col-6 col-sp-12">
            <MwSelect
              name="position_employee"
              label={t("position_employee")}
              options={roles}
              defaultValue={role?.value}
              disabled
            />
          </div>
        </div>
        <div className="form-group row mx-n3 mb-5">
          <div className="col">
            <MwInput
              defaultValue={valueInput?.address}
              customLabel={{ className: "fs-12 mb-1" }}
              placeholder={t("enter_your_address")}
              label={t("address")}
              name="address"
              onChange={handleChange}
              error={errors?.address}
            />
          </div>
        </div>

        <div className="form-group mb-8">
          <label className="fs-12 mb-1">{t("introduce_yourself")}</label>
          <textarea
            name="intro"
            className="txt-input"
            defaultValue={valueInput?.intro}
            rows="5"
            placeholder={t("introduce_yourself")}
          ></textarea>
          {errors?.intro && <div className={`msg-error`}>{errors?.intro}</div>}
        </div>
        <MwMessage
          active={visibleNotification}
          isDisplayNone
          message={t("updated_user_info")}
          className="msg-success msg-icon mb-5"
          timeout={10000}
        />

        <div className="form-group text-pc-right text-sp-center">
          <MwButton
            loading={loading}
            title={t("update")}
            className="btn btn-2 px-6 fs-14 fw-bold center"
            onClick={handleSubmit}
            preventEvent
          />
        </div>
      </form>
    </div>
  );
}

export default UpdateInfo;
