import React from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import avatar from "assets/images/duy.PNG";
import { updateAvatarRequest } from "core/redux/actions/userAction";
import { API_ENDPOINT } from "app/const/Api";

function ChangeAvatar() {
  const { t } = useTranslation();
  const [img, setImg] = React.useState(avatar);
  const { newAvatar } = useSelector((state) => state.user);
  const hiddenRef = React.useRef(null);
  const handleClick = (e) => {
    hiddenRef.current.click();
  };
  const dispatch = useDispatch();

  const handleChange = (e) => {
    var formData = new FormData();
    if (e.target.files[0]) {
      formData.append("avatar", e.target.files[0], e.target.files[0].name);
      dispatch(updateAvatarRequest(formData));
    }
  };
  React.useEffect(() => {
    if (newAvatar) {
      setImg(API_ENDPOINT + "/" + newAvatar.link);
    }
  }, [newAvatar]);

  return (
    <>
      <div class="form-group mb-5">
        <div class="media media-profile d-flex flex-sp-column">
          <div class="media-img mr-6 mr-sp-0">
            <img src={img} alt="" class="img-circle" />
          </div>
          <div class="media-body mt-sp-2 flex-1-0">
            <p class="mt-0 mb-3 fw-bold fs-13">{t("avatar")}</p>
            <div class="media-upload">
              <button class="btn btn-file btn-h-32" onClick={handleClick}>
                {t("upload_new_avatar")}
              </button>
              <input
                ref={hiddenRef}
                onChange={handleChange}
                type="file"
                className="dp-hide"
              />
            </div>
            <p class="mt-3 mb-0 fs-12 grey-7 fw-normal">
              {t("support_img_2mb")}
            </p>
          </div>
        </div>
      </div>
    </>
  );
}

export default ChangeAvatar;
