import React from "react";
import { useTranslation } from "react-i18next";
import { putWithToken } from "core/utils/APIUtils";
import { CODE_SUCCESS, USER_CHANGE_PASSWORD } from "app/const/Api";
import MwInput from "app/components/input";
import Accordion from "../components/Accordion";
import MwMessage from "app/components/message";
import MwButton from "app/components/button";

function ChangePassword() {
  const { t } = useTranslation();
  const refAccordion = React.useRef(null);
  const [loading, setLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const [errors, setErrors] = React.useState({});
  const [visibleMessage, setVisibleMessage] = React.useState(false);

  const [heightForm, setHeightForm] = React.useState();

  const refForm = React.useRef(null);

  React.useEffect(() => {
    let timer;
    if (success) {
      setVisibleMessage(true);
      timer = setTimeout(() => {
        setVisibleMessage(false);
      }, 10000);
    }
    return () => {
      clearTimeout(timer);
    };
  }, [success]);

  React.useEffect(() => {
    refForm.current.clientHeight && setHeightForm(refForm.current.clientHeight);
  }, [errors]);

  React.useEffect(() => {
    refForm.current.clientHeight && setHeightForm(refForm.current.clientHeight);
  }, []);

  const handleSubmit = (e) => {
    e && e.preventDefault();
    setErrors({});
    setLoading(true);
    setSuccess(false);
    setVisibleMessage(false);
    const errors = {};
    const { new_password, re_new_password, old_password } = refForm.current;

    for (let index = 0; index < refForm.current.length; index++) {
      const { required, value, name, type, dataset } = refForm.current[index];
      if (type === "password" && value.trim().length < 6) {
        errors[name] = t("password_limit");
      }
      if (type === "password" && required && !value) {
        errors[name] = dataset.helper;
      }
    }
    if (Object.keys(errors).length) {
      setLoading(false);
      return setErrors(errors);
    }

    const data = {
      new_password: new_password.value,
      old_password: old_password.value,
    };

    if (re_new_password.value === new_password.value) {
      putWithToken(USER_CHANGE_PASSWORD, data).then((res) => {
        if (res.meta.code === CODE_SUCCESS) {
          setSuccess(true);
        } else {
          old_password.focus();
          setErrors({ ...errors, old_password: res.meta.message });
        }
        setLoading(false);
      });
    } else {
      re_new_password.focus();
      setErrors({ ...errors, re_new_password: t("password_not_match") });
      setLoading(false);
    }
  };

  const handleClose = () => {
    setErrors();
    refAccordion.current.handleClose();
  };

  return (
    <Accordion
      ref={refAccordion}
      title={t("password")}
      subTitle={t("update_password")}
      textCollapse={t("change")}
      success={success}
      errors={errors}
      loading={loading}
      heightForm={heightForm}
    >
      <form ref={refForm} className="pt-3">
        <div className="form-group mb-5">
          <MwInput
            required
            error={errors?.old_password}
            type="password"
            name="old_password"
            label={t("current_password")}
            placeholder={t("enter_current_password")}
            customLabel={{ className: "fs-12 mb-1" }}
            data-helper={t("pls_enter_password")}
          />
        </div>
        <div className="form-group mb-5">
          <MwInput
            required
            type="password"
            name="new_password"
            error={errors?.new_password}
            label={t("new_password")}
            placeholder={t("enter_new_password")}
            customLabel={{ className: "fs-12 mb-1" }}
            data-helper={t("pls_enter_new_password")}
          />
        </div>
        <div className="form-group">
          <MwInput
            required
            type="password"
            error={errors?.re_new_password}
            name="re_new_password"
            label={t("confirm_new_password")}
            placeholder={t("enter_new_password")}
            customLabel={{ className: "fs-12 mb-1" }}
            data-helper={t("pls_enter_re_password")}
          />
        </div>
        <MwMessage
          active={visibleMessage}
          message={t("set_password_successfully")}
          className={`msg-success msg-icon ${
            visibleMessage ? "mt-2" : "p-0 m-0"
          }`}
          timeout={10000}
          style={{
            height: visibleMessage ? 36 : 0,
            overflow: "hidden",
          }}
        />

        <div className="form-group text-right mt-5 pb-3">
          <MwButton
            onClick={handleClose}
            title={t("cancel")}
            type="reset"
            className="btn btn-5 px-6 fs-13 fw-normal mr-haft-3"
          />
          <MwButton
            title={t("save")}
            className="btn btn-2 px-6 fs-13 fw-bold center"
            loading={loading}
            onClick={handleSubmit}
            preventEvent
          />
        </div>
      </form>
    </Accordion>
  );
}

export default React.memo(ChangePassword);
