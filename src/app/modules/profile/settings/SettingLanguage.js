import React from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import Accordion from "../components/Accordion";
import MwSelect from "app/components/select";
import { putFormData } from "core/utils/APIUtils";
import { CODE_SUCCESS, UPDATE_USER_SETTING } from "app/const/Api";
import MwButton from "app/components/button";
import MwMessage from "app/components/message";

function SettingLanguage() {
  const { t, i18n } = useTranslation();
  const refForm = React.useRef(null);
  const refAccordion = React.useRef(null);
  const userLng = useSelector(
    (state) => state.user.userInfo.setting_info.setting.language
  );
  const [loading, setLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const [usrLng, setUsrLng] = React.useState();
  const [visibleMessage, setVisibleMessage] = React.useState(false);

  React.useEffect(() => {
    userLng && setUsrLng(userLng);
  }, [userLng]);

  React.useEffect(() => {
    let timer;
    if (success) {
      setVisibleMessage(true);
      timer = setTimeout(() => {
        setVisibleMessage(false);
      }, 10000);
    }
    return () => {
      clearTimeout(timer);
    };
  }, [success]);

  const languages = [
    { content: t("language_english"), value: "en" },
    { content: t("language_vietnam"), value: "vi" },
  ];

  const handleSubmit = (e) => {
    setLoading(true);
    setSuccess(false);
    e && e.preventDefault();
    const { language } = refForm.current;
    const languageLocal = language.value === "en" ? "en-US" : "vi-VN";

    const formData = new FormData();
    formData.append("set[language]", language.value);

    putFormData(UPDATE_USER_SETTING, formData).then((res) => {
      if (res.meta.code === CODE_SUCCESS) {
        setLoading(false);
        setSuccess(true);
        i18n.changeLanguage(languageLocal);
      } else {
        setLoading(false);
      }
    });
  };

  const handleClose = () => {
    refAccordion.current.handleClose();
  };

  return (
    <Accordion
      ref={refAccordion}
      title={t("language")}
      subTitle={
        <>
          {t("select_language_display")}{" "}
          <span className="black-3">
            {localStorage.getItem("i18nextLng") === "vi-VN"
              ? t("language_vietnam")
              : t("language_english")}
          </span>
        </>
      }
      textCollapse={t("change")}
      className="accordion border-bottom-grey-6 py-3"
      success={success}
      messageSuccess={t("set_language_successfully")}
      onSubmit={handleSubmit}
      loading={loading}
    >
      <form ref={refForm} className="pt-3">
        <div className="form-group">
          <MwSelect
            name="language"
            label={t("select_language")}
            options={languages}
            defaultValue={usrLng}
          />
        </div>
        <MwMessage
          active={visibleMessage}
          message={t("set_language_successfully")}
          className={`msg-success msg-icon ${
            visibleMessage ? "mt-2" : "p-0 m-0"
          }`}
          timeout={10000}
          style={{
            height: visibleMessage ? 36 : 0,
            overflow: "hidden",
          }}
        />

        <div className="form-group text-right mt-5 pb-3">
          <MwButton
            onClick={handleClose}
            title={t("cancel")}
            type="reset"
            className="btn btn-5 px-6 fs-13 fw-normal mr-haft-3"
          />
          <MwButton
            title={t("save")}
            className="btn btn-2 px-6 fs-13 fw-bold center"
            loading={loading}
            onClick={handleSubmit}
            preventEvent
          />
        </div>
      </form>
    </Accordion>
  );
}

export default React.memo(SettingLanguage);
