import React from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { Timezone } from "app/config/Timezone";
import Accordion from "../components/Accordion";
import MwSelect from "app/components/select";
import { updateTimezoneRequest } from "core/redux/actions/settingAction";
import { putFormData } from "core/utils/APIUtils";
import { CODE_SUCCESS, UPDATE_USER_SETTING } from "app/const/Api";
import { fetchUserRequest } from "core/redux/actions/userAction";
import MwMessage from "app/components/message";
import MwButton from "app/components/button";

function SettingTimeZone() {
  const { t } = useTranslation();
  const refForm = React.useRef(null);
  const refAccordion = React.useRef(null);
  const { timezone } = useSelector(
    (state) => state.user.userInfo.setting_info.setting
  );
  const [loading, setLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const [currentTz, setCurrentTz] = React.useState();
  const [tempTz, setTempTz] = React.useState();
  const [visibleMessage, setVisibleMessage] = React.useState(false);
  const dispatch = useDispatch();

  React.useEffect(() => {
    timezone && filterTz(timezone);
  }, [timezone]);

  React.useEffect(() => {
    let timer;
    if (success) {
      setVisibleMessage(true);
      timer = setTimeout(() => {
        setVisibleMessage(false);
      }, 10000);
    }
    return () => {
      clearTimeout(timer);
    };
  }, [success]);

  const filterTz = (timezone) => {
    const tz = Timezone.filter((el) => el.value === timezone)[0];
    setCurrentTz(tz.content);
    setTempTz(tz.value);
  };

  const handleSubmit = (e) => {
    e && e.preventDefault();
    setLoading(true);
    setSuccess(false);
    const { timezone } = refForm.current;
    const formData = new FormData();
    formData.append("set[timezone]", timezone.value);
    dispatch(updateTimezoneRequest(formData));
    putFormData(UPDATE_USER_SETTING, formData).then((res) => {
      if (res.meta.code === CODE_SUCCESS) {
        setLoading(false);
        setSuccess(true);
        dispatch(fetchUserRequest());
      } else {
        setLoading(false);
      }
    });
    filterTz(tempTz);
  };

  const handleChange = (e) => {
    setTempTz(e.target.value);
  };

  const handleClose = () => {
    refAccordion.current.handleClose();
  };

  return (
    <Accordion
      ref={refAccordion}
      success={success}
      title={t("timezone")}
      classWrapper="pr-15 pr-sp-2"
      subTitle={
        <>
          {t("select_timezone_description")}
          <br /> <span className="black-3">{currentTz}</span>
        </>
      }
      textCollapse={t("change")}
      className="accordion border-bottom-grey-6 py-3"
      messageSuccess={t("set_tz_successfully")}
      onSubmit={handleSubmit}
      loading={loading}
    >
      <form ref={refForm} className="pt-3">
        <div className="form-group ">
          <MwSelect
            defaultValue={tempTz}
            onChange={handleChange}
            name="timezone"
            label={t("select_timezone")}
            options={Timezone}
          />
        </div>
        <MwMessage
          active={visibleMessage}
          message={t("set_tz_successfully")}
          className={`msg-success msg-icon ${
            visibleMessage ? "mt-2" : "p-0 m-0"
          }`}
          timeout={10000}
          style={{
            height: visibleMessage ? 36 : 0,
            overflow: "hidden",
          }}
        />

        <div className="form-group text-right mt-5 pb-3">
          <MwButton
            onClick={handleClose}
            title={t("cancel")}
            type="reset"
            className="btn btn-5 px-6 fs-13 fw-normal mr-haft-3"
          />
          <MwButton
            title={t("save")}
            className="btn btn-2 px-6 fs-13 fw-bold center"
            loading={loading}
            onClick={handleSubmit}
            preventEvent
          />
        </div>
      </form>
    </Accordion>
  );
}

export default React.memo(SettingTimeZone);
