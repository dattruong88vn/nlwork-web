import React from "react";
import PropTypes from "prop-types";
import ChangePassword from "./ChangePassword";
import SettingLanguage from "./SettingLanguage";
import SettingTimeZone from "./SettingTimeZone";

function Settings({ tabActive, tabIndex }) {
  return (
    <div
      id="profile-setting"
      className={`tabs-content pt-5 px-6 px-sp-3 ${
        tabActive === tabIndex ? "show" : ""
      }`}
    >
      <ChangePassword />
      <SettingLanguage />
      <SettingTimeZone />
    </div>
  );
}

Settings.proTypes = {
  tabActive: PropTypes.number,
  tabIndex: PropTypes.number,
};

Settings.defaultProps = {
  tabActive: 1,
  tabIndex: 1,
};

export default Settings;
