import React from "react";
import { Redirect, useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import MwLoading from "app/components/loading";
import { useTranslation } from "react-i18next";
import { useSiteTitle } from "core/hooks/useSiteTitle";
import { fetchAppData } from "core/redux/actions/appAction";

function SlashPage() {
  const { t } = useTranslation("pageTitle");
  const location = useLocation();
  useSiteTitle(t("slash_page"));
  const dispatch = useDispatch();
  const { loading } = useSelector((state) => state.app);

  React.useEffect(() => {
    dispatch(fetchAppData(true));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return loading ? (
    <MwLoading />
  ) : (
    <Redirect to={location?.state?.currentUrl || "/home"} />
  );
}

export default SlashPage;
