import React, { useState, forwardRef, useEffect } from "react";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { ProjectSortOptions } from "app/const/SortOptions";
import SearchTask from "app/modules/task/components/SearchTask";
import SortTask from "app/modules/task/components/SortTask";
import AddProjectModal from "./AddProjectModal";
import { MwPanelFilter } from "app/components";

const ProjectFilter = forwardRef(
  ({ total, filterProjects, handleAdd, loading, permissionCreate }, ref) => {
    const { t } = useTranslation();

    const { settings } = useSelector((state) => state.company);
    const { project_info } = useSelector((state) => state.user.userInfo);
    const [dataPanel, setDataPanel] = useState([]);
    const [sortValue, setSortValue] = useState("");
    const [filterValue, setFilterValue] = useState({
      "statuses[]": [],
      "state[]": [],
    });
    const [keyword, setKeyword] = useState("");

    useEffect(() => {
      const data = [
        {
          title: t("status"),
          items: [
            { id: "active", name: t("active"), nameLabel: "active" },
            { id: "closed", name: t("closed"), nameLabel: "closed" },
          ],
          grid: "grid-2",
          prefixId: "project",
          prefixName: "statuses",
        },
        {
          title: t("state"),
          items: settings?.project?.state || [],
          grid: "grid-2",
          prefixId: "task",
          prefixName: "state",
          className: "bor-0",
        },
      ];

      setDataPanel(data);
    }, [t, settings, project_info]);

    const handleFilter = (filters) => {
      setFilterValue(filters);
      filterProjects({ keyword, sort: sortValue, ...filters });
    };

    const handleSort = (value) => {
      setSortValue(value);
      filterProjects({ keyword, sort: value, ...filterValue });
    };

    const handleSearch = (keyword) => {
      setKeyword(keyword);
      filterProjects({ keyword, sort: sortValue, ...filterValue });
    };

    const showModal = (e) => {
      e && e.preventDefault();
      ref.current.showModal();
    };

    return (
      <>
        <AddProjectModal ref={ref} handleAdd={handleAdd} loading={loading} />
        <div className="d-flex justify-space-between align-center border-bottom-grey-6">
          <div className="d-flex align-center">
            <SortTask
              wrapperClass="dp-sp-hide"
              sortValue={ProjectSortOptions}
              handleSort={handleSort}
              showSelected={false}
              titleClass="ml-3"
              activeSort={!!sortValue}
              isProject
            />

            {permissionCreate && (
              <a
                href="/"
                onClick={showModal}
                className="dp-sp-hide btn btn-add-project btn-h-32 white fs-14 d-flex align-center justify-space-between"
              >
                <i className="fal fa-plus fs-16 mr-2" />
                {t("create_project")}
              </a>
            )}
          </div>

          <div className="d-flex flex-sp-1-0 justify-sp-space-between align-center relative p-2 px-sp-3 py-sp-2">
            <div className="mr-2">
              <SearchTask
                isProject
                handleSearch={handleSearch}
                keyword={keyword}
              />
            </div>
            <div className="d-flex align-center">
              <SortTask
                wrapperClass="dp-pc-hide"
                sortValue={ProjectSortOptions}
                handleSort={handleSort}
                showSelected={false}
                activeSort={!!sortValue}
              />
              <MwPanelFilter
                dataPanel={dataPanel}
                dataFilter={filterValue}
                dataGet={[
                  { key: "state[]", type: "array" },
                  { key: "statuses[]", type: "array" },
                ]}
                onFilter={handleFilter}
              />
            </div>
          </div>
        </div>
      </>
    );
  }
);

export default ProjectFilter;
