import React, {
  useEffect,
  useState,
  useRef,
  useImperativeHandle,
  forwardRef,
} from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import moment from "moment-timezone";
import MwDropDown from "app/components/dropdown";
import MwModal from "app/components/modal";
import MwInput from "app/components/input";
import MwButton from "app/components/button";
import MwTooltip from "app/components/tooltip";
import MwCheckbox from "app/components/checkbox";
import MwInputAutocomplete from "app/components/input/Autocomplete";
import { COLORS_PROJECT } from "app/const/Colors";
import { DATE_MAX, DATE_MIN } from "app/const/App";

const AddProjectModal = forwardRef(({ loading, handleAdd }, ref) => {
  const { t } = useTranslation();
  const refForm = useRef(null);
  const refInput = useRef(null);
  const refModal = useRef(null);
  const refTooltip = useRef(null);
  const refAccordion = useRef(null);
  const [errors, setErrors] = useState({});
  const [expand, setExpand] = useState(false);
  const { state: stateProject } = useSelector(
    (state) => state.company.settings.project
  );
  const { timezone } = useSelector((state) => state.user.userInfo.company_info);
  const stateOntrack = stateProject.filter((item) => item.type === 1)[0].id;

  useEffect(() => {
    if (!loading) {
      hideModal();
      setExpand(false);
    }
  }, [loading]);

  useImperativeHandle(ref, () => ({
    showModal: () => {
      refModal.current.showModal();
    },
  }));

  const handleExpand = (e) => {
    e.preventDefault();
    setExpand(!expand);
  };

  const hideModal = () => {
    refModal.current.hideModal();
  };

  const handleSubmit = () => {
    setErrors({});
    const errorsForm = {};
    const formData = new FormData();
    const data = {};
    const managers = refInput.current.getValue();
    const name = refForm.current["name"].value;
    const startDate = moment
      .tz(moment(refForm.current["start_date"].value, "DD/MM/YYYY"), timezone)
      .unix();
    const endDate = moment
      .tz(moment(refForm.current["end_date"].value, "DD/MM/YYYY"), timezone)
      .unix();

    for (let index = 0; index < refForm.current.length; index++) {
      const element = refForm.current[index];
      if (element.required && !element.value) {
        element.focus();
        errorsForm[element.name] = t("don_let_this_field_blank");
      } else {
        switch (element.name) {
          case "color":
          case "access_level":
            if (element.checked) {
              formData.append(element.name, element.value);
              data["setting_info"] = {
                ...data["setting_info"],
                [element.name]: element.value,
                default_view: "list",
              };
            }
            break;
          case "start_date":
          case "end_date":
            if (isNaN(element.value)) {
              formData.append(
                element.name,
                moment(element.value, "DD/MM/YYYY").unix()
              );
              data[element.name] = moment(element.value, "DD/MM/YYYY").unix();
            }
            break;
          default:
            formData.append(element.name, element.value);
            break;
        }
      }
    }

    const nameErr = (errMsg) => {
      errorsForm["name"] = t(errMsg);
      refForm.current["name"].focus();
    };

    if (name.length < 2) nameErr("name_prj_limit_2");
    if (name.trim().length === 0) nameErr("don_let_this_field_blank");
    if (name.length > 100) nameErr("name_prj_limit_100");

    if (startDate > endDate) {
      errorsForm["end_date"] = t("start_date_cant_greater_end_date");
    }

    if (endDate > DATE_MAX) {
      errorsForm["end_date"] = `${t("end_date_cant_greater_than")} ${moment(
        DATE_MAX
      ).format("DD/MM/YYYY")}`;
    }

    if (startDate < DATE_MIN) {
      errorsForm["start_date"] = `${t("start_date_cant_lesser_than")} ${moment(
        DATE_MIN
      ).format("DD/MM/YYYY")}`;
    }

    if (Object.keys(errorsForm).length) return setErrors(errorsForm);

    for (let index = 0; index < managers.length; index++) {
      const element = managers[index];
      formData.append("managers[]", element);
    }

    formData.append("status_id", stateOntrack);
    handleAdd(formData, {
      ...data,
      description: refForm.current["description"].value,
    });
  };

  const handleClose = () => {
    setExpand(false);
    setErrors({});
  };

  const MobileRightTitle = () => {
    return (
      <div className="form">
        <div className="d-flex btn-modal-dr">
          <MwButton
            title={`${t("save_project")}`}
            className="btn btn-2 p-0 fs-15 fw-bold"
            onClick={handleSubmit}
          />
          <MwDropDown
            title={
              <>
                <i className="fal fa-angle-down" />
                <i className="fal fa-angle-up" />
              </>
            }
          >
            <ul className="nav hover-2 px-0 py-2 border-0">
              <li className="nav-item">
                <a href="/" className="nav-link pl-3 py-2">
                  <i className="fas fa-check-square grey-7 mr-1" />{" "}
                  {t("open_job")}
                </a>
              </li>
              <li className="nav-item">
                <a href="/" className="nav-link pl-3 py-2">
                  <i className="fas fa-users grey-7 mr-1" />
                  {t("add_member")}
                </a>
              </li>
            </ul>
          </MwDropDown>
        </div>
      </div>
    );
  };

  const FooterModal = () => (
    <>
      <div />
      <div className="d-flex form">
        <MwButton
          title={t("cancel")}
          onClick={hideModal}
          className="btn btn-4 w-87 fs-13 fw-normal transparent"
        />
        <div className="d-flex btn-modal-dr">
          <MwButton
            title={t("save_project")}
            loading={loading}
            className="btn btn-2 w-87 fs-14 fw-bold center-2"
            onClick={handleSubmit}
          />
          <MwDropDown
            title={
              <>
                <i className="fal fa-angle-down" />
                <i className="fal fa-angle-up" />
              </>
            }
            contentClass="top"
          >
            <ul className="nav hover-2 px-0 py-2 border-0">
              <li className="nav-item">
                <a href="/" className="nav-link pl-3 py-2">
                  <i className="fas fa-check-square grey-7 mr-1" />{" "}
                  {t("open_job")}
                </a>
              </li>
              <li className="nav-item">
                <a href="/" className="nav-link pl-3 py-2">
                  <i className="fas fa-users grey-7 mr-1" />
                  {t("add_member")}
                </a>
              </li>
            </ul>
          </MwDropDown>
        </div>
      </div>
    </>
  );

  return (
    <MwModal
      id="add-project"
      ref={refModal}
      title={t("add_project")}
      rightTitleMobile={<MobileRightTitle />}
      footer={<FooterModal />}
      onUnmount={handleClose}
    >
      {/* Content of modal */}
      <form ref={refForm}>
        <div className="form-group mb-5">
          <MwInput
            label={t("name_project")}
            required
            customLabel={{ className: "fs-12 mb-1 required" }}
            name="name"
            error={errors?.name}
            placeholder={t("enter_name_project")}
            autoFocus
            className="txt-input pr-2"
          />
        </div>
        <div className="form-group mb-3">
          <MwInputAutocomplete
            label={t("manage_project")}
            ref={refInput}
            error={errors?.manage_project}
            placeholder={t("enter_manage_project")}
            customLabel={{ className: "fs-12 mb-1 required" }}
            emptyMessage={t("list_empty")}
            tooltip={
              <MwTooltip ref={refTooltip}>{t("tooltip_add_project")}</MwTooltip>
            }
          />
        </div>
        <div className="form-group mb-5">
          <MwInput
            isTextarea
            label={t("description_project")}
            name="description"
            error={errors?.description}
            customLabel={{ className: "fs-12 mb-1" }}
            rows="5"
            placeholder={t("enter_description_project")}
            className="txt-input pr-2"
          />
        </div>
        <div className="form-group mb-5">
          <label className="fs-12 mb-3">{t("access")}</label>
          <MwCheckbox
            id="private"
            name="access_level"
            defaultChecked
            wrapperClass="radio radio-primary mb-3"
            type="radio"
            value="public"
            label={
              <>
                {t("private")}
                <br />
                <span className="fs-12 grey-7">{t("private_description")}</span>
              </>
            }
          />
          <MwCheckbox
            id="public"
            name="access_level"
            value="public"
            wrapperClass="radio radio-primary mb-3"
            type="radio"
            label={
              <>
                {t("public")}
                <br />
                <span className="fs-12 grey-7">{t("public_description")}</span>
              </>
            }
          />
        </div>
        <div className="accordion">
          <div className="d-flex justify-space-between align-center form-group mb-5">
            <a
              href="/"
              className="blue-1 d-flex align-center fw-normal lh-1"
              onClick={handleExpand}
            >
              {t("advantage_option")}{" "}
              {!expand ? (
                <i className="blue-1 fal fa-angle-down fs-20 ml-2 lh-1" />
              ) : (
                <i className="blue-1 fal fa-angle-up fs-20 ml-2 lh-1" />
              )}
            </a>
          </div>
          <div
            className="panel"
            style={
              expand
                ? {
                    overflow: "unset",
                    maxHeight: refAccordion?.current?.clientHeight || 0,
                  }
                : { overflow: "hidden" }
            }
          >
            <div ref={refAccordion}>
              <div className="form-group row mx-n3 mb-5">
                <div className="col">
                  <MwInput
                    isDate
                    label={t("start_date")}
                    name="start_date"
                    customLabel={{ className: "fs-12 mb-1" }}
                    placeholder={t("start_date")}
                    className="txt-input pr-2"
                    error={errors?.start_date}
                  />
                </div>
                <div className="col">
                  <MwInput
                    isDate
                    label={t("finish_date")}
                    name="end_date"
                    customLabel={{ className: "fs-12 mb-1" }}
                    placeholder={t("finish_date")}
                    className="txt-input pr-2"
                    error={errors?.end_date}
                  />
                </div>
              </div>
              <div className="form-group">
                <label className="fs-12 mb-2">{t("choose_color")}</label>
                <div className="d-flex">
                  {COLORS_PROJECT.map((color, index) => (
                    <MwCheckbox
                      key={index}
                      name="color"
                      value={color.color}
                      wrapperClass="color-choose"
                      type="radio-color"
                      defaultChecked={index === 0}
                      customLabel={{ style: { backgroundColor: color.color } }}
                    />
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </MwModal>
  );
});

export default AddProjectModal;
