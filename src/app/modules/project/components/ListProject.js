import React from "react";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import EmptyList from "app/components/emptyList";
import ProjectItem from "./ProjectItem";

function ListProject({
  listProjects = [],
  total,
  handleDelete,
  colorFinished,
}) {
  const { t } = useTranslation();

  return listProjects.length ? (
    <div className="table table-style-1 px-sp-0 pt-sp-4">
      <div className="thead dp-sp-hide">
        <div className="table-row fs-12">
          <div className="table-head pl-4">{t("project_name")}</div>
          <div className="table-head">{t("start_date")}</div>
          <div className="table-head">{t("date_complete")}</div>
          <div className="table-head">{t("member")}</div>
          <div className="table-head">{t("status")}</div>
          <div className="table-head"></div>
        </div>
      </div>

      {listProjects.map((project) => (
        <ProjectItem
          key={project.id}
          total={total}
          handleDelete={handleDelete}
          colorFinished={colorFinished}
          {...project}
        />
      ))}
    </div>
  ) : (
    <EmptyList
      emptyText={t("current_no_project")}
      descriptionText={t("any_project_add_will_here")}
    />
  );
}

ListProject.propTypes = {
  listProjects: PropTypes.array,
  total: PropTypes.number,
  handleDelete: PropTypes.func,
  colorFinished: PropTypes.string,
};

ListProject.defaultProps = {
  listProjects: [],
  total: 0,
  handleDelete: null,
};

export default ListProject;
