import React from "react";
import { useSelector } from "react-redux";
import MwAvatar from "app/components/avatar";

function MwMembers({ members }) {
  const { members: membersCompany } = useSelector((state) => state.company);
  const [users, setUsers] = React.useState([]);

  React.useEffect(() => {
    const users = [];
    members.forEach((member) => {
      membersCompany.forEach((memberCompany) => {
        if (member === memberCompany.id) {
          users.push(memberCompany);
        }
      });
    });
    setUsers(users);
  }, [members, membersCompany]);

  return members?.length ? (
    <div className="d-flex align-center">
      <i className="fas fa-users dp-pc-hide mr-2 grey-7 fs-18" />
      {users.map((user, index) => {
        return (
          index <= 2 && (
            <MwAvatar key={index} className="img-circle w-24 mr-1" {...user} />
          )
        );
      })}
      {members.length - 3 > 0 && (
        <span className="circle-bl">{members.length - 3}+</span>
      )}
    </div>
  ) : null;
}

export default MwMembers;
