import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import { Link } from "react-router-dom";
import moment from "moment";
import PropTypes from "prop-types";
import MwDropDown from "app/components/dropdown";
import MwMembers from "app/components/members";
import { useTranslation } from "react-i18next";
import { usePopper } from "react-popper";
import { ProgressCalculate } from "core/utils/ProgressCalculate";
import { COLOR_CIRCLE } from "app/const/Colors";

function ProjectItem({
  id,
  name,
  total,
  total_task,
  total_task_done,
  description,
  setting_info,
  start_date,
  end_date,
  colorFinished,
  handleDelete,
  featured,
  statuses,
  members_detail,
  ...rest
}) {
  const { t } = useTranslation();
  const [referenceRef, setReferenceRef] = useState(null);
  const [popperRef, setPopperRef] = useState(null);
  const [visible, setVisible] = useState(false);
  const [cssCircle, setCssCircle] = useState(null);

  useEffect(() => {
    const percent = ProgressCalculate(total_task_done, total_task);

    if (percent < 50) {
      const nextDeg = 90 + 3.6 * percent;
      setCssCircle(
        `linear-gradient(90deg, ${COLOR_CIRCLE} 50%,transparent 50%,transparent),linear-gradient(${nextDeg}deg, ${colorFinished} 50%,  ${COLOR_CIRCLE} 50%,  ${COLOR_CIRCLE})`
      );
    } else {
      const nextDeg = -90 + 3.6 * (percent - 50);
      setCssCircle(
        `linear-gradient(${nextDeg}deg,${colorFinished} 50%,transparent 50%,transparent),linear-gradient(270deg, ${colorFinished} 50%,  ${COLOR_CIRCLE} 50%, ${COLOR_CIRCLE})`
      );
    }
  }, [colorFinished, total_task, total_task_done]);

  const { styles, attributes } = usePopper(referenceRef, popperRef, {
    placement: "top",
    modifiers: [
      {
        name: "offset",
        enabled: true,
        options: {
          offset: [0, 10],
        },
      },
    ],
  });

  const onDelete = (e) => {
    e.preventDefault();
    handleDelete && handleDelete(id);
  };

  return (
    <>
      <div className={`table-row hover-5 ${featured && "project-featured"}`}>
        <div className="table-cell pl-4 p-sp-0">
          <div
            className="before"
            style={{ backgroundColor: setting_info.color || "transparent" }}
          />
          <p className="m-0 fs-14 fs-sp-15 fw-bold">
            <Link
              to={{
                pathname: `/project-detail/${setting_info.default_view}/${id}`,
                state: {
                  name,
                  setting_info,
                  fromLink: true,
                  total_task_done,
                  total_task,
                  featured,
                  members: members_detail,
                  id,
                },
              }}
              className="black-3 lineclamp-2"
            >
              <span
                ref={setReferenceRef}
                onMouseEnter={() => setVisible(true)}
                onMouseLeave={() => setVisible(false)}
              >
                {name}
              </span>
            </Link>
          </p>
          {description && (
            <p
              className="m-0 fs-13 grey-7 dp-sp-hide lineclamp-1"
              style={{ maxWidth: 250 }}
            >
              {description}
            </p>
          )}
        </div>
        <div
          className="table-cell text-nowrap px-sp-0 py-sp-1 d-sp-flex align-center sp-grey-7 fs-sp-14"
          style={!start_date ? { flex: 0 } : {}}
        >
          <i className="fal fa-calendar-alt grey-7 dp-pc-hide mr-2 fs-18"></i>
          {start_date ? moment.unix(start_date).format("DD/MM/YYYY") : "N/A"}
        </div>
        <div className="table-cell text-nowrap px-sp-2 pb-sp-1 pt-sp-1 sp-grey-7 fs-sp-14">
          {end_date ? moment.unix(end_date).format("DD/MM/YYYY") : "N/A"}
        </div>
        <div className="table-cell w-140 px-sp-0 py-sp-1">
          <MwMembers
            className="d-flex align-center mr-8"
            members={members_detail}
          />
        </div>
        <div className="table-cell px-sp-0 py-sp-1">
          <div className="progress">
            <div className="d-flex align-center justify-space-between mb-1 dp-sp-hide">
              <p className="m-0 fs-12 fw-bold">{t("progress_done")}</p>
              <p className="m-0 fs-12 fw-normal d-flex align-center">
                {ProgressCalculate(total_task_done, total_task)}%
              </p>
            </div>
            <div
              className={`progress-bar none-bg-img --green per-${ProgressCalculate(
                total_task_done,
                total_task
              )}`}
              style={{
                backgroundImage: cssCircle,
              }}
            >
              <span style={{ background: colorFinished }}>
                {ProgressCalculate(total_task_done, total_task)}%
              </span>
            </div>
            <div className="progress-value">
              <div className="dropdown overflow-hidden w-100 pt-2">
                <a
                  href="/"
                  className="dropbtn w-100 btn btn-block btn-filter btn-h-32 black-3 border-grey-6 d-flex align-center justify-space-between"
                >
                  {ProgressCalculate(total_task_done, total_task)}%
                  <i className="fal fa-angle-down dp-sp-hide"></i>
                  <i className="fal fa-angle-up dp-sp-hide"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="table-cell pr-4 pr-sp-0 text-right">
          <MwDropDown
            activeBtnClass="grey-4 fas fa-ellipsis-v pl-2 px-sp-1"
            contentClass="w-100"
          >
            <ul className="nav hover-3 px-0 py-1 border-0">
              <li className="nav-item">
                <a href="/" className="nav-link px-0 py-2" onClick={onDelete}>
                  <i className="fas fa-trash-alt mr-1"></i> {t("delete")}
                </a>
              </li>
            </ul>
          </MwDropDown>
        </div>
      </div>
      {ReactDOM.createPortal(
        visible && (
          <div
            ref={setPopperRef}
            style={{
              ...styles.popper,
              zIndex: 1001,
            }}
            className="tooltip-avatar"
            {...attributes.popper}
          >
            {name}
          </div>
        ),
        document.body
      )}
    </>
  );
}

ProjectItem.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  total: PropTypes.number,
  total_task: PropTypes.number,
  total_task_done: PropTypes.number,
  description: PropTypes.string,
  setting_info: PropTypes.object,
  start_date: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  end_date: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  members: PropTypes.array,
  handleDelete: PropTypes.func,
};

ProjectItem.defaultProps = {
  id: null,
  name: null,
  total: 0,
  total_task: 0,
  total_task_done: 0,
  description: null,
  setting_info: {},
  start_date: null,
  end_date: "",
  members: [],
  handleDelete: null,
};

export default ProjectItem;
