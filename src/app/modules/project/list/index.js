import React, { useState, useEffect, useRef } from "react";
import find from "lodash/find";
import { useTranslation } from "react-i18next";
import { useSelector, useDispatch } from "react-redux";
import { useSiteTitle } from "core/hooks/useSiteTitle";
import {
  deleteWithToken,
  fetchWithToken,
  postFormData,
} from "core/utils/APIUtils";
import {
  CODE_SUCCESS,
  PROJECT_DELETE,
  PROJECT_LIST,
  PROJECT_CREATE,
} from "app/const/Api";
import { useWindowDimensions } from "core/hooks/useWindowDimension";
import { companyAddProject } from "core/redux/actions/companyAction";
import { usePermissions, useToast } from "core/hooks";
import MwPagination from "app/components/pagination";
import ProjectFilter from "../components/ProjectFilter";
import ListProject from "../components/ListProject";
import "../styles/list.scss";
import ListLoading from "app/components/loading/ListLoading";

function ProjectList() {
  const { t } = useTranslation();
  useSiteTitle("project_list");
  const { getPermission } = usePermissions();
  const { width } = useWindowDimensions();
  const { addToast } = useToast();
  const [total, setTotal] = useState();
  const [loading, setLoading] = useState(false);
  const [isFiltered, setIsFiltered] = useState(false);
  const [createLoading, setCreateLoading] = useState();
  const [dataFilter, setDataFilter] = useState({
    "statuses[]": [],
    "state[]": [],
  });
  const [projects, setProjects] = useState([]);
  const [colorFinished, setColorFinished] = useState("");
  const [permissionCreate, setPermissionCreate] = useState(true);
  const statuses = useSelector((state) => state.company.settings.task.status);
  const { status: statusesProject, state: statesProject } = useSelector(
    (state) => state.company.settings.project
  );
  const statusClosed = statusesProject.filter((status) => status.value === 2)[0]
    .key;
  const stateEmergency = statesProject.filter((state) => state.type === 3)[0]
    .id;
  const stateSlowProgress = statesProject.filter((state) => state.type === 2)[0]
    .id;

  const ref = useRef(null);
  const dispatch = useDispatch();

  useEffect(() => {
    const permissionInfo = getPermission({}, "CREATE_PROJECT");

    setPermissionCreate(permissionInfo);
  }, [getPermission]);

  useEffect(() => {
    const statusFinished = find(statuses, ["type", 5]);
    setColorFinished(statusFinished?.color);
  }, [statuses]);

  // add class my-task to content div
  useEffect(() => {
    const bodyDiv = document.getElementsByTagName("body")[0];
    bodyDiv.className = "project-list";
  }, []);

  // remove class dashboard when device in mobile < 767
  useEffect(() => {
    const dashElement = document.getElementById("main-content");

    if (width < 767 && dashElement) {
      dashElement.classList.remove("dashboard");
    } else {
      dashElement.classList.add("dashboard");
    }
  }, [width]);

  useEffect(() => {
    getProjects(1);
  }, []);

  const getProjects = (page, data, isFiltered) => {
    setLoading(true);
    const limit = 20;
    const offset = page === 0 ? limit : (page - 1) * limit;

    fetchWithToken(PROJECT_LIST, {
      fields: "total_task,members_detail",
      total: true,
      limit,
      offset,
      "statuses[]": "active",
      ...data,
    })
      .then((res) => {
        if (res.meta.code === CODE_SUCCESS) {
          setProjects(res.data.projects || []);
          isFiltered
            ? setTotal(res.data.total || 0)
            : setTotal(res.data.total || 0);
        }
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const checkTaskWithFilter = (data, filters) => {
    let checkFilterStatus = true;
    let checkFilterState = true;

    if (filters["keyword"] && data.name.search(filters["keyword"]) < 0) {
      return false;
    }

    if (filters !== null) {
      if (filters["statuses[]"]?.includes(statusClosed)) {
        checkFilterState = false;
      }
      if (
        filters["state[]"]?.includes(stateEmergency) ||
        filters["state[]"]?.includes(stateSlowProgress)
      ) {
        checkFilterState = false;
      }
      return checkFilterState && checkFilterStatus;
    }
  };

  const handleAdd = (data, settingInfo) => {
    setCreateLoading(true);
    postFormData(PROJECT_CREATE, data)
      .then((res) => {
        setCreateLoading(false);
        if (res.meta.code === CODE_SUCCESS) {
          const isFiltered = checkTaskWithFilter(res.data, dataFilter);
          console.log(res.data, settingInfo);
          if (isFiltered) {
            setProjects([{ ...res.data, ...settingInfo }, ...projects]);
            setTotal((total) => total + 1);
          }
          addToast({
            id: "add_project",
            content: t("add_project_successfully"),
            placement: "right-bottom",
            timeout: 3000,
          });
          dispatch(companyAddProject(res.data));
        } else {
          addToast({
            id: "add_project",
            content:
              typeof res.meta.message === "string"
                ? res.meta.message
                : t("reset_password_failure"),
            placement: "right-bottom",
            timeout: 3000,
            status: "error",
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleFilter = (data) => {
    setDataFilter(data);
    getProjects(1, data, true);
    setIsFiltered(true);
  };

  const onChangePage = (page) => {
    getProjects(page, dataFilter);
    setIsFiltered(false);
  };

  const handleDelete = (project_id) => {
    deleteWithToken(PROJECT_DELETE, { project_id })
      .then((res) => {
        if (res.meta.code === CODE_SUCCESS) {
          setProjects((projects) =>
            projects.filter((project) => project.id !== project_id)
          );
          setTotal((total) => total - 1);
        } else {
          addToast({
            id: "add_project",
            content: t("no_have_access"),
            placement: "right-bottom",
            timeout: 3000,
            status: "error",
          });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const showModal = (e) => {
    e.preventDefault();
    ref.current.showModal();
  };

  return (
    <>
      <div className="py-2 px-sp-3 d-flex align-center justify-space-between bg-grey-2">
        <p className="m-0 fs-20 fs-sp-17 fw-bold">{t("project_list")}</p>
        <a
          href="/"
          className="dp-pc-hide btn btn-add-project btn-h-32 white fs-14 fs-sp-12 px-sp-2 d-flex align-center justify-space-between"
          onClick={showModal}
        >
          <i className="fal fa-plus fs-16 fs-sp-14 mr-2 mr-sp-1" />
          {t("create_project")}
        </a>
      </div>
      <div className="white-box p-0">
        <ProjectFilter
          ref={ref}
          total={total}
          loading={createLoading}
          filterProjects={handleFilter}
          handleAdd={handleAdd}
          permissionCreate={permissionCreate}
        />

        {loading ? (
          <ListLoading />
        ) : (
          <>
            <ListProject
              listProjects={projects}
              total={total}
              handleDelete={handleDelete}
              colorFinished={colorFinished}
            />
          </>
        )}

        <MwPagination
          itemPerPage={20}
          totalItem={total}
          onChange={onChangePage}
          isFiltered={isFiltered}
        />
      </div>
    </>
  );
}

export default ProjectList;
