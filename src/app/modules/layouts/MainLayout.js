import React from "react";
import { useWindowDimensions } from "core/hooks/useWindowDimension";
import { Redirect, useLocation } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import MwModal from "app/components/modal";
import SidebarLink from "./sidebar/SidebarLink";
import SideBar from "./SideBar";
import HeaderBar from "./HeaderBar";
import "./styles/mainLayout.scss";
import AccountSetting from "../profile";
import AddTaskModal from "../task/components/addTask/AddTaskModal";

function MainLayout({ children }) {
  const { t, i18n } = useTranslation(["pageTitle", "common"]);
  const ref = React.useRef();
  const refSettingAccount = React.useRef(null);
  const [activeTab, setActiveTab] = React.useState(1);
  const location = useLocation();
  const localExpand = localStorage.getItem("expand");
  const [toggle, setToggle] = React.useState(true);
  const [active, setActive] = React.useState(false);
  const { width } = useWindowDimensions();
  const { loading } = useSelector((state) => state.app);
  const { userInfo } = useSelector((state) => state.user);

  React.useEffect(() => {
    localExpand === "true" ? setToggle(true) : setToggle(false);
  }, [localExpand]);

  React.useEffect(() => {
    const currentLng = localStorage.getItem("i18nextLng");
    const userLng = userInfo?.setting_info?.setting.language;
    const userLngText =
      userInfo?.setting_info?.setting.language === "vi" ? "vi-VN" : "en-US";

    currentLng !== userLngText && userLng && i18n.changeLanguage(userLngText);
  }, [userInfo, i18n]);

  // Handle resize window and toggle sidebar with width window
  React.useEffect(() => {
    // For screen tablet and smaller
    if (width < 1200 && !localExpand) {
      setToggle(true);
    }
    // For screen mobile and desktop
    if ((width < 767 || width >= 1200) && !localExpand) {
      setToggle(false);
    }
  }, [width, localExpand]);

  const mobileNavHandle = (e) => {
    setActive(!active);
  };

  const navLinkMobile = [
    {
      title: "home_page",
      leftComponent: <i className="fad fa-home-lg-alt" />,
      to: "/home",
    },
    {
      title: "my_task",
      leftComponent: <i className="fas fa-clipboard-list" />,
      to: "/mytask",
    },
    {
      leftComponent: <i className="fal fa-plus" />,
      to: "/mytask",
      className: "col add-btn",
      linkClassName: "add orange",
      onClick: () => {
        ref.current.showModal();
      },
    },
    {
      title: "project",
      leftComponent: <i className="fas fa-file-invoice" />,
      to: "/projects",
    },
    {
      title: "extend",
      leftComponent: <i className="fal fa-bars" />,
      to: "/",
      onClick: mobileNavHandle,
    },
  ];

  const handleToggle = () => {
    setToggle(!toggle);
    localStorage.setItem("expand", !toggle);
  };

  /**
   *
   * @param {number} tabIndex number to open modal setting and tab active default
   */
  const handleOpenSetting = (tabIndex) => {
    setActiveTab(tabIndex);
    refSettingAccount.current.showModal();
  };

  return !loading ? (
    <>
      <MwModal
        id="account-setting"
        ref={refSettingAccount}
        wrapperClass="modal-full d-flex justify-end"
        headClass="px-6 text-left dp-sp-hide"
        bodyClass="px-0 scroll-sp"
        title={t("common:setting")}
      >
        <AccountSetting activeDefault={activeTab} />
      </MwModal>
      <AddTaskModal ref={ref} />
      <div
        id="main-content"
        className={`dashboard ${active ? "active fixed" : ""} ${
          toggle ? "is-collapsed" : ""
        }`}
      >
        <SideBar toggleMenu={handleToggle} />
        <HeaderBar setActive={setActive} openModal={handleOpenSetting} />
        <div className="page-container">
          <div className="main-content px-sp-0 mb-3 pb-3">{children}</div>
        </div>
        <div className="nav-sp dp-pc-hide">
          <div className="row">
            {navLinkMobile.map((elm, index) => (
              <SidebarLink key={index} className="col" {...elm} />
            ))}
          </div>
        </div>
      </div>
    </>
  ) : (
    <Redirect
      to={{ pathname: "/", state: { currentUrl: location.pathname } }}
    />
  );
}

export default MainLayout;
