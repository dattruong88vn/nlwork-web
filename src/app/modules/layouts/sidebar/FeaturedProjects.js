import React from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import Collapse from "./Collapse";

function FeaturedProjects({ projects, total }) {
  const { t } = useTranslation();
  const { featuredProjects, totalFeaturedPrj } = useSelector(
    (state) => state.project
  );
  return (
    <Collapse
      mainNavTile={t("project")}
      subMenu={featuredProjects}
      total={totalFeaturedPrj}
    />
  );
}

export default FeaturedProjects;
