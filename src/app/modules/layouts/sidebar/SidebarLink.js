import React from "react";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";

// TODO bring it to global components
function SidebarLink({
  title,
  to,
  leftComponent,
  linkClassName,
  onClick,
  ...rest
}) {
  const { t } = useTranslation("sidebar");
  const clickHandle = (e) => {
    if (onClick) {
      e.preventDefault();
      onClick();
    }
  };

  return (
    <li {...rest}>
      <NavLink
        className={linkClassName}
        activeClassName={onClick ? "" : "active"}
        to={to}
        title={t(title)}
        onClick={clickHandle}
      >
        {leftComponent}
        <span>{t(title)}</span>
      </NavLink>
    </li>
  );
}

SidebarLink.propTypes = {
  to: PropTypes.string,
};

SidebarLink.defaultProps = {
  to: "/",
};

export default SidebarLink;
