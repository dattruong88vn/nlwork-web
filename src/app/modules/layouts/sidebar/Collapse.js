import React, { useRef, useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import "assets/styles/common/_sidebar.scss";
import { Link, useParams } from "react-router-dom";

function Collapse({ mainNavTile, subMenu, total }) {
  const [expand, setExpand] = useState(true);
  const [height, setHeight] = useState(0);
  const [top, setTop] = useState(0);
  const refElm = useRef(null);
  const refNav = useRef(null);
  const { t } = useTranslation();
  const { id: projectId } = useParams();

  const handleExpand = () => setExpand(!expand);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => {
    setTop(refNav.current.offsetTop);
  });

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => {
    setHeight(refElm?.current?.clientHeight);
  });

  const ListItem = ({
    name,
    id,
    setting_info,
    total_task_done,
    total_task,
    featured,
    members,
  }) => {
    return (
      <li className="nav-item">
        <Link
          className={`nav-link nav-dot ${projectId === id && "active"}`}
          to={{
            pathname: `/project-detail/${setting_info.default_view}/${id}`,
            state: {
              name,
              setting_info,
              fromLink: true,
              total_task_done,
              total_task,
              featured,
              members,
              id,
            },
          }}
        >
          <i className="fas fa-circle" style={{ color: setting_info.color }} />
          <span>{name}</span>
        </Link>
      </li>
    );
  };

  const EmptyComponent = () => (
    <li className="nav-item">
      <div className="nav-link">
        <span>{t("favorite_project_by_click")}</span>
        <i
          className="grey-7 fal fa-star yellow star-tick fs-18 ml-1"
          style={{ fontSize: 14 }}
        />
      </div>
    </li>
  );

  const ListProject = () => {
    return subMenu.map(
      (elm, index) => index < 5 && <ListItem key={elm.id} {...elm} />
    );
  };

  return (
    <>
      <ul className="nav px-0 nav-project hover-3 dp-sp-hide nav-featured">
        <li className="nav-item dropdown" ref={refNav}>
          <div className="relative">
            <div
              onClick={handleExpand}
              className={`nav-link has-submenu justify-space-between pr-4 py-3 ${
                expand ? "collapsed" : ""
              }`}
            >
              <div className="d-flex align-center">
                <i className="fal fa-angle-down hide-conllapsed" />
                <i className="fal fa-angle-up hide-conllapsed" />
                <span>{mainNavTile}</span>
              </div>
            </div>
          </div>
          <div
            className={`sub-nav ${expand ? "is-collapsed" : ""}`}
            style={{ height: expand ? height : 0 }}
          >
            <div ref={refElm}>
              <ul className="px-0 hover-2">
                {subMenu.length ? <ListProject /> : <EmptyComponent />}
              </ul>
              {total > 5 && (
                <ul className="nav px-0 load-more">
                  <li className="nav-item">
                    <Link to="/projects" className="nav-link border-0">
                      <i className="fal fa-angle-down"></i>
                      <span>
                        {t("see_more")} ({total - 5})
                      </span>
                    </Link>
                  </li>
                </ul>
              )}
            </div>
          </div>
        </li>
      </ul>

      <div
        id="sub-nav-collapsed"
        className="sub-nav"
        style={{
          top,
        }}
      >
        <div>
          <ul className="px-0 hover-2 mb-0">
            {subMenu.length ? <ListProject /> : <EmptyComponent />}
          </ul>
          {total > 5 && (
            <ul className="nav px-0 load-more">
              <li className="nav-item">
                <Link className="nav-link border-0" to="/projects">
                  <i className="fal fa-angle-down"></i>
                  <span>
                    {t("see_more")} ({total - 5})
                  </span>
                </Link>
              </li>
            </ul>
          )}
        </div>
      </div>
    </>
  );
}

Collapse.propTypes = {
  subMenu: PropTypes.array,
  mainNavTile: PropTypes.string,
};
Collapse.defaultProps = {
  mainNavTile: "",
  subMenu: [],
};

export default Collapse;
