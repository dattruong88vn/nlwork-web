import React from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

//TODO convert to collapse components
function FavoriteReport() {
  const { t } = useTranslation();
  const [collapse, setCollapse] = React.useState(true);
  const heightSubNav = React.useRef(0);
  const refElm = React.useRef(null);

  const handleCollapse = () => {
    setCollapse(!collapse);
  };

  React.useEffect(() => {
    heightSubNav.current = refElm.current.offsetHeight;
  }, []);

  return (
    <ul className="nav px-0 hover-3 nav-report border-0 dp-sp-hide">
      <li className="nav-item dropdown">
        <div className="relative">
          <div
            className={`nav-link has-submenu justify-space-between pr-4 py-3 ${
              collapse ? "collapsed" : ""
            }`}
            onClick={handleCollapse}
          >
            <div className="d-flex align-center">
              <i className="fal fa-angle-down hide-conllapsed"></i>
              <i className="fal fa-angle-up hide-conllapsed"></i>
              <span>{t("report")}</span>
            </div>
          </div>
        </div>
        <div className={`sub-nav ${collapse ? "is-collapsed" : ""}`}>
          <div ref={refElm}>
            <ul className="px-0 hover-2">
              <li className="nav-item">
                <Link className="nav-link border-0" to="#">
                  <i className="fas fa-analytics"></i>
                  <span>{t("activity_reports")}</span>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </li>
    </ul>
  );
}

export default FavoriteReport;
