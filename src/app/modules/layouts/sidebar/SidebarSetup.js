import React from "react";
import SidebarLink from "./SidebarLink";

const setupLink = [
  {
    title: "help_support",
    leftComponent: <i className="far fa-life-ring" />,
    to: "/unknown",
  },
];

function SidebarSetup() {
  return (
    <div className="sidebar-setup">
      <div className="sidebar-nav">
        <ul className="nav hover-3 py-2 px-0 border-top border-0">
          {setupLink.map((link, index) => (
            <SidebarLink
              {...link}
              key={index}
              className="nav-item"
              linkClassName="nav-link"
            />
          ))}
        </ul>
      </div>
    </div>
  );
}

export default React.memo(SidebarSetup);
