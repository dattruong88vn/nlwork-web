import React from "react";
import { useTranslation } from "react-i18next";
import logo from "assets/images/logo.svg";
import SidebarLink from "./sidebar/SidebarLink";
import FavoriteReport from "./sidebar/FavoriteReport";
import SidebarSetup from "./sidebar/SidebarSetup";
import FeaturedProjects from "./sidebar/FeaturedProjects";
import { Link } from "react-router-dom";

const navLink = [
  {
    title: "home_page",
    leftComponent: <i className="fad fa-home-lg-alt" />,
    to: "/home",
  },
  {
    title: "my_task",
    leftComponent: <i className="fas fa-clipboard-list" />,
    to: "/mytask",
  },
  {
    title: "list_project",
    leftComponent: <i className="fas fa-file-invoice" />,
    to: "/projects",
  },
];

const navLinkMobile = [
  {
    title: "my_task",
    leftComponent: <i className="fas fa-clipboard-list" />,
    to: "/mytask",
  },
];

function SideBar({ toggleMenu }) {
  const { t } = useTranslation();

  /**
   * @function toggleHandle handle toggle function from props
   */
  const toggleHandle = () => {
    if (toggleMenu) {
      toggleMenu();
    }
  };

  return (
    <div className="sidebar">
      <div className="sidebar-inner">
        <div className="px-2 py-3 dp-pc-hide">
          <span className="fs-18 fw-bold black-1">{t("extend")}</span>
        </div>
        <div className="scroll-sp h-100 grid-1">
          <div className="sidebar-logo dp-sp-hide">
            <span onClick={toggleHandle} className="sidebar-toggle">
              <i className="fal fa-bars"></i>
            </span>
            <Link to="/home">
              <img src={logo} alt="MeWork logo" />
            </Link>
          </div>
          <div className="sidebar-nav scroll">
            <ul className="nav hover-3 px-0 pb-3 py-sp-2">
              {navLink.map((nav, index) => (
                <SidebarLink
                  key={index}
                  className="nav-item dp-sp-hide"
                  linkClassName="nav-link"
                  {...nav}
                />
              ))}
            </ul>
            <FeaturedProjects />
            <FavoriteReport />
            <ul className="dp-pc-hide nav hover-3 py-3 px-0 py-sp-2">
              {navLinkMobile.map((nav, index) => (
                <SidebarLink
                  key={index}
                  className="nav-item"
                  linkClassName="nav-link"
                  {...nav}
                />
              ))}
            </ul>
          </div>
          <SidebarSetup />
        </div>
      </div>
    </div>
  );
}

export default React.memo(SideBar);
