import React, { useEffect, memo } from "react";
import socketClient from "socket.io-client";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { useSelector, useDispatch } from "react-redux";
import useClickOutside from "core/hooks/useClickOutside";
import {
  markAllNotifyRequest,
  listNotifyScrollRequest,
  setNotificationUnread,
  setNotificationList,
} from "core/redux/actions/notifyAction";
import { ReactComponent as EmptyIcon } from "assets/images/empty/empty-notification.svg";
import ItemNotification from "./ItemNotification";
import {
  JOIN_ROOM_USER,
  LEAVE_ROOM_USER,
  NOTIFICATION_REMOVE,
  NOTIFICATION_READ,
  SOCKET_ENDPOINT,
  SOCKET_OPTIONS,
  NOTIFICATION_READ_ALL,
  NOTIFICATION_ADD,
} from "app/const/SocketIO";
import "../styles/notification.scss";

function NotificationList({ setActive, openSettingModal }) {
  const { t } = useTranslation();
  const { listNotify, scrollLoading, fetchedAllList, unread } = useSelector(
    (state) => state.notify
  );
  const { id: userId } = useSelector((state) => state.user.userInfo);
  const [ref, expand, setExpand] = useClickOutside(false);
  const dispatch = useDispatch();

  useEffect(() => {
    const ws = socketClient(SOCKET_ENDPOINT, SOCKET_OPTIONS);
    ws.emit(JOIN_ROOM_USER, userId);

    ws.on(NOTIFICATION_ADD, handleRealtimeAdd);
    ws.on(NOTIFICATION_REMOVE, handleRealtimeRemove);
    ws.on(NOTIFICATION_READ, handleRealtimeRead);
    ws.on(NOTIFICATION_READ_ALL, handleRealtimeReadAll);

    return () => ws.emit(LEAVE_ROOM_USER, userId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [listNotify, unread, userId]);

  const handleRealtimeRemove = (notify) => {
    handleRemoveNotify(notify);
    notify?.status && handleUnreadCount("DECREMENT");
  };

  const handleRealtimeAdd = (notify) => {
    dispatch(setNotificationList([notify, ...listNotify]));
    handleUnreadCount("INCREMENT");
  };

  const handleRealtimeRead = (notify) => {
    handleUpdateNotify(notify, "status");
    handleUnreadCount("DECREMENT");
  };

  const handleRealtimeReadAll = (notify) => {
    const newNotifications = listNotify.map((item) => (item.status = 0));
    handleUnreadCount(newNotifications);
  };

  const handleUpdateNotify = (notify, keyUpdate) => {
    const newNotifications = [...listNotify];

    newNotifications.forEach((item) => {
      if (item.id === notify.id) {
        item[keyUpdate] = notify[keyUpdate];
      }
    });

    dispatch(setNotificationList(newNotifications));
  };

  const handleRemoveNotify = (notify) => {
    const newNotifications = listNotify.filter((item) => item.id !== notify.id);
    dispatch(setNotificationList(newNotifications));
  };

  const handleUnreadCount = (operation) => {
    switch (operation) {
      case "INCREMENT":
        dispatch(setNotificationUnread(unread + 1));
        break;
      case "DECREMENT":
        dispatch(setNotificationUnread(unread - 1));
        break;
      default:
        dispatch(setNotificationUnread(0));
        break;
    }
  };

  const handleToggle = () => {
    setExpand(!expand);
    setActive(false);
  };

  const handleScroll = (e) => {
    const { scrollHeight, scrollTop, clientHeight } = e.target;
    const isBottom = scrollHeight - scrollTop === clientHeight;

    if (isBottom && !scrollLoading && !fetchedAllList) {
      dispatch(listNotifyScrollRequest({ offset: listNotify?.length }));
    }
  };

  const handleMarkAllRead = (e) => {
    e && e.preventDefault();
    dispatch(markAllNotifyRequest());
  };

  const handleOpenSetting = (e) => {
    e && e.preventDefault();
    openSettingModal(2);
    setExpand(false);
  };

  const EmptyListNotification = () => {
    return (
      <div className="empty-notification">
        <EmptyIcon />
        <p className="m-0 text-center fs-15 fs-sp-13 grey-7">
          {t("empty_notification")}
        </p>
      </div>
    );
  };

  return (
    <div ref={ref} className="notification-head">
      <div className="dropdown">
        <span
          onClick={handleToggle}
          className="dropbtn fas fa-bell circle-hover"
        >
          {!!unread && (
            <span className="notifi-num d-flex justify-center align-center">
              {unread}
            </span>
          )}
        </span>
        <div className={`dropdown-content sp-full ${expand ? "show" : ""}`}>
          <div className="d-flex justify-space-between align-center px-4 py-3 notification-head-tick dp-sp-hide">
            <span>{t("notification")}</span>
            <div className="d-flex align-center">
              {!!listNotify.length && (
                <a
                  href="/"
                  onClick={handleMarkAllRead}
                  className="mark-all-read"
                >
                  {t("mark_all_read")}
                </a>
              )}

              <a href="/" onClick={handleOpenSetting}>
                <i className="fas fa-cog" />
              </a>
            </div>
          </div>
          <div className="dropdown-back d-flex align-center white px-4 py-3 dp-pc-hide">
            <i
              onClick={() => setExpand(false)}
              className="far fa-arrow-left fw-bold fs-16"
            />
            <span className="flex-1-0 fs-18 fw-bold ml-4">
              {t("notification")}
            </span>
            {!!listNotify.length && (
              <div className="check-all" onClick={handleMarkAllRead} />
            )}
          </div>
          <div className="scroll-sp" onScroll={handleScroll}>
            <ul className="nav hover-1 px-0 scroll mt-sp-2 notification-head-list">
              {listNotify?.length ? (
                <ListNotification notifications={listNotify} />
              ) : (
                <EmptyListNotification />
              )}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

const ListNotification = memo(({ notifications = [] }) => {
  return notifications.map((notify) => (
    <ItemNotification key={notify.id} {...notify} />
  ));
});

NotificationList.propTypes = {
  setActive: PropTypes.func,
  openSettingModal: PropTypes.func,
};

NotificationList.defaultProps = {
  setActive: () => {},
  openSettingModal: () => {},
};

export default NotificationList;
