import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import useTimeAgo from "core/hooks/useTimeAgo";
import { useTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import useClickOutside from "core/hooks/useClickOutside";
import {
  markNotifyRequest,
  deleteNotifyRequest,
} from "core/redux/actions/notifyAction";

function ItemNotification({
  target,
  created,
  content,
  id,
  user_id,
  action,
  status,
}) {
  const { name, item_id, type } = target;
  const { t } = useTranslation(["common", "notification"]);

  const [ref, visible, setVisible] = useClickOutside(false);
  const timeAgo = useTimeAgo(created);
  const { members } = useSelector((state) => state.company);
  const dispatch = useDispatch();

  const user = members?.filter((item) => item.id === user_id)[0];

  const content_display = `${t(`notification:${action}`)} ${t(
    `notification:${type}`
  )}`;

  const handleMarkRead = (e, isLink) => {
    if (!isLink) e && e.preventDefault();
    setVisible(false);
    dispatch(markNotifyRequest({ id }));
  };

  const handleDelete = (e) => {
    e.preventDefault();
    setVisible(false);
    dispatch(deleteNotifyRequest({ id }));
  };

  const handleExpand = (e) => {
    e && e.preventDefault();
    setVisible(!visible);
  };

  return (
    <li className="nav-item">
      <Link
        className={`nav-link ${!!status ? "" : "unread"}`}
        to={`${type === "task" ? `/task/${item_id}` : `/task/${item_id}`}`}
        onClick={(e) => handleMarkRead(e, true)}
      >
        {user?.avatar ? (
          <img src={`${user.avatar}`} alt={name} />
        ) : (
          <i className="fas fa-user-circle avatar" />
        )}

        <span className="pl-3">
          <strong>
            {user?.first_name} {user?.last_name}
          </strong>{" "}
          {content_display} <strong>{name}</strong>
          <span className="time">{timeAgo}</span>
        </span>
      </Link>

      <div
        className={`dropdown sub-dropdown ${visible ? "overflow-unset" : ""}`}
        ref={ref}
      >
        <span
          onClick={handleExpand}
          className={`dropbtn action-btn fas fa-ellipsis-v ${
            visible ? "active" : ""
          }`}
        />
        <div className={`dropdown-content ${visible ? "show" : ""}`}>
          <ul className="nav hover-2 px-0 py-2 border-0">
            {status !== 0 && (
              <li className="nav-item" onClick={handleMarkRead}>
                <a className="nav-link py-1" href="/">
                  <i className="fal fa-check" />
                  <span>{t("header:mark_as_read_one")}</span>
                </a>
              </li>
            )}
            <li className="nav-item" onClick={handleDelete}>
              <a className="nav-link py-1" href="/">
                <i className="fal fa-times" />
                <span>{t("header:delete_notification")}</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </li>
  );
}

export default React.memo(ItemNotification);
