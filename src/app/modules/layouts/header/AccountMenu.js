import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import useClickOutside from "core/hooks/useClickOutside";
import SidebarLink from "../sidebar/SidebarLink";
import { KEY_TOKEN } from "app/const/App";
import MwAvatar from "app/components/avatar";

function AccountMenu({ setActive, openSettingModal }) {
  const { t } = useTranslation(["common", "header"]);
  const { userInfo } = useSelector((state) => state.user);
  const { company_permissions } = useSelector(
    (state) => state.company.settings
  );
  const [permission, setPermission] = useState(false);
  const history = useHistory();
  const [ref, isComponentVisible, setIsComponentVisible] = useClickOutside(
    false
  );

  useEffect(() => {
    setPermission(company_permissions?.includes("company.manage"));
  }, [company_permissions]);

  //Close dropdown handle
  const closeDropdown = () => {
    setIsComponentVisible(false);
  };

  const handleLogout = (e) => {
    localStorage.removeItem(KEY_TOKEN);
    history.push("/auth/login");
  };

  const introLink = [
    {
      title: "docs_intro",
      leftComponent: <i className="fas fa-book" />,
      to: "/unknow",
      className: "nav-item",
      linkClassName: "nav-link",
    },
    {
      title: "know_basic",
      leftComponent: <i className="fas fa-file-alt" />,
      to: "/unknow",
      className: "nav-item",
      linkClassName: "nav-link",
    },
    {
      title: "youtube",
      leftComponent: <i className="fab fa-youtube" />,
      to: "/unknow",
      className: "nav-item",
      linkClassName: "nav-link",
    },
    {
      title: "facebook_group",
      leftComponent: <i className="fab fa-facebook" />,
      to: "/unknow",
      className: "nav-item",
      linkClassName: "nav-link",
    },
  ];

  const extraLink = [
    {
      title: "chat_with_support",
      leftComponent: <i className="fad fa-headset" />,
      to: "/unknow",
      className: "nav-item",
      linkClassName: "nav-link",
    },
    {
      title: "request_a_feature",
      leftComponent: <i className="fas fa-envelope-open-text" />,
      to: "/unknow",
      className: "nav-item",
      linkClassName: "nav-link",
    },
  ];

  const actionLink = [
    {
      title: "message",
      leftComponent: <i className="fas fa-bell" />,
      to: "/unknow",
      className: "nav-item",
      linkClassName: "nav-link",
    },
    {
      title: "setting",
      leftComponent: <i className="fas fa-cog" />,
      to: "/unknow",
      className: "nav-item",
      linkClassName: "nav-link",
    },
  ];

  const AccountListDropdown = ({ data }) => {
    return data.map((item, index) => <SidebarLink key={index} {...item} />);
  };

  const AccountInfo = () => {
    const { id, first_name, last_name, email } = userInfo;
    return (
      <ul className="nav px-0 border-0 mb-2 dp-pc-hide">
        <li className="nav-item">
          <Link to={`/user/${id}`} className="d-flex align-center px-3 py-4">
            <div className="user-avatar dropbtn">
              <MwAvatar
                avatar={userInfo?.avatar}
                first_name={userInfo?.first_name}
                last_name={userInfo?.last_name}
              />
            </div>
            <span className="flex-1-0 pl-3 fw-bold fs-14 black-1">
              {`${first_name} ${last_name}`}
              <br />
              <span className="fw-normal fs-13 grey-5">{email}</span>
            </span>
            <i className="far fa-chevron-right grey-5" />
          </Link>
        </li>
      </ul>
    );
  };

  return (
    <div ref={ref} className="user">
      <div className={`dropdown ${isComponentVisible && "overflow-unset"}`}>
        <div
          className={`user-avatar dropbtn ${isComponentVisible && "active"}`}
          onClick={() => {
            setIsComponentVisible(!isComponentVisible);
            setActive(false);
          }}
        >
          <MwAvatar
            avatar={userInfo?.avatar}
            first_name={userInfo?.first_name}
            last_name={userInfo?.last_name}
          />
        </div>

        <div
          className={`dropdown-content sp-full pt-2 pt-sp-0 ${
            isComponentVisible && "show"
          }`}
        >
          <div className="dropdown-back d-flex align-center white px-4 py-3 dp-pc-hide">
            <i
              className="far fa-arrow-left fw-bold fs-16"
              onClick={closeDropdown}
            ></i>
            <span className="fs-18 fw-bold ml-4">{t("header:account")}</span>
          </div>

          <div className="scroll-sp">
            {userInfo && <AccountInfo />}

            <ul className="nav hover-1 px-0 pb-2 py-sp-2 mb-sp-2 border-0">
              <AccountListDropdown data={introLink} />
            </ul>

            <ul className="nav hover-1 px-0 py-2 mb-sp-2 border-0 dp-pc-hide">
              <AccountListDropdown data={actionLink} />
            </ul>

            <ul className="nav hover-1 px-0 py-2 mb-sp-2 border-0">
              <AccountListDropdown data={extraLink} />
            </ul>

            <ul className="nav px-0 hover-1 mb-sp-2 dp-sp-hide border-0">
              <SidebarLink
                className="nav-item"
                linkClassName={`nav-link ${!permission && "py-4"}`}
                title={"account"}
                to="/unknow"
                onClick={openSettingModal}
                leftComponent={<i className="fas fa-user-circle" />}
              />
              {permission && (
                <>
                  <SidebarLink
                    className="nav-item"
                    linkClassName="nav-link"
                    title={"system_setting"}
                    to="/unknow"
                    onClick={(e) => e.preventDefault()}
                    leftComponent={<i className="fas fa-cog" />}
                  />
                  <SidebarLink
                    className="nav-item"
                    linkClassName="nav-link"
                    title={"application_integration"}
                    to="/unknow"
                    onClick={openSettingModal}
                    leftComponent={<i className="fal fa-desktop-alt" />}
                  />
                </>
              )}
            </ul>

            <ul className="nav px-0 hover-1 mb-sp-0 border-0">
              <SidebarLink
                className="nav-item"
                linkClassName="nav-link py-4 sp-red"
                title={"logout"}
                onClick={handleLogout}
                leftComponent={<i className="fas fa-sign-out-alt" />}
              />
            </ul>

            <p className="dp-pc-hide text-center grey-4 mt-8 fw-normal">
              Version 1.1.2
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

AccountMenu.propTypes = {
  setActive: PropTypes.func,
};

AccountMenu.defaultProps = {
  setActive: () => {},
};

export default AccountMenu;
