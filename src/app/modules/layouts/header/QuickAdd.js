import React from "react";
import AddTaskModal from "app/modules/task/components/addTask/AddTaskModal";
import { postFormData } from "core/utils/APIUtils";
import { CODE_SUCCESS, CREATE_TASK } from "app/const/Api";
import useToast from "core/hooks/useToast";
import { useTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import { taskQuickAdd } from "core/redux/actions/taskAction";

function QuickAdd() {
  const ref = React.useRef();
  const { t } = useTranslation();
  const { addToast } = useToast();
  const [loading, setLoading] = React.useState(false);
  const dispatch = useDispatch();

  const handleShowModal = (e) => {
    e && e.preventDefault();
    ref.current.showModal();
  };

  const handleSubmitAddTask = (formData) => {
    setLoading(true);
    postFormData(CREATE_TASK, formData).then((res) => {
      if (res.meta.code === CODE_SUCCESS) {
        addToast({
          id: "add_task_successfully",
          content: t("add_task_successfully"),
          placement: "right-bottom",
          timeout: 3000,
        });
        dispatch(taskQuickAdd(res.data.task));
      } else {
        console.error(res.meta.message);
      }
      setLoading(false);
      ref.current.hideModal();
    });
  };

  return (
    <>
      <a
        href="/"
        className="add orange dp-sp-hide mr-3"
        onClick={handleShowModal}
      >
        <i className="fal fa-plus" />
      </a>
      <AddTaskModal
        ref={ref}
        isQuickAdd
        handleSubmit={handleSubmitAddTask}
        loading={loading}
      />
    </>
  );
}

export default QuickAdd;
