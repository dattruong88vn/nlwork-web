import React from "react";
import MwInput from "app/components/input";
import { useTranslation } from "react-i18next";
function Search() {
  const { t } = useTranslation();
  const [focus, setFocus] = React.useState(false);
  const handleFocus = (value) => {
    setFocus(value);
  };

  return (
    <div className="search">
      <form className={`d-flex align-center ${focus && "focus"}`}>
        <MwInput
          placeholder={t("header:search")}
          onFocus={() => handleFocus(true)}
          leftComponent={<i className="far fa-search icon-left-search" />}
          rightComponent={
            <span onClick={() => handleFocus(false)} className="search-close">
              <i className="fal fa-times" />
            </span>
          }
        />
      </form>
      <div className="search-result">
        <ul className="nav px-0 py-2 hover-1">
          <li className="nav-item">
            <a href="/" className="nav-link pr-3">
              Lorem Ipsum is simply dummy text of the printing and typesetting
            </a>
          </li>
          <li className="nav-item">
            <a href="/" className="nav-link pr-3">
              Lorem Ipsum is simply dummy text of the printing and typesetting
            </a>
          </li>
          <li className="nav-item">
            <a href="/" className="nav-link pr-3">
              Lorem Ipsum is simply dummy text of the printing and typesetting
            </a>
          </li>
        </ul>
        <div className="search-advanced">
          <a href="/">
            <i className="fal fa-file-search" />
            <span>{t("header:advanced_search")}</span>
          </a>
        </div>
      </div>
    </div>
  );
}

export default Search;
