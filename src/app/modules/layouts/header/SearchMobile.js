import React, { useState } from "react";
import MwInput from "app/components/input";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import SidebarLink from "../sidebar/SidebarLink";

const listSearch = [
  {
    to: "/unknown",
    title: "Home page design",
    leftComponent: <i className="far fa-search"></i>,
    linkClassName: "nav-link",
    className: "nav-item",
    onClick: () => {},
  },
  {
    to: "/unknown",
    title: "Home page design",
    leftComponent: <i className="far fa-search"></i>,
    linkClassName: "nav-link",
    className: "nav-item",
    onClick: () => {},
  },
  {
    to: "/unknown",
    title: "Home page design",
    leftComponent: <i className="far fa-search"></i>,
    linkClassName: "nav-link",
    className: "nav-item",
    onClick: () => {},
  },
  {
    to: "/unknown",
    title: "Home page design",
    leftComponent: <i className="far fa-search"></i>,
    linkClassName: "nav-link",
    className: "nav-item",
    onClick: () => {},
  },
];

function SearchMobile({ setActive }) {
  const { t } = useTranslation();
  const [isSearch, setSearch] = useState(false);
  const [keyword, setKeyword] = useState("");

  // Render list search from mockup data
  const ListSearch = ({ data }) => {
    return data.length
      ? data.map((item, index) => <SidebarLink key={index} {...item} />)
      : "Null";
  };

  // Handle input change
  const handleChange = (e) => {
    setKeyword(e.target.value);
  };

  const handleReset = () => {
    setKeyword("");
  };

  return (
    <div className="search-sp dp-pc-hide">
      <div className={`dropdown ${isSearch && "overflow-unset"}`}>
        <span
          className={`dropbtn fal fa-search fs-18 white circle-hover ${
            isSearch && "active"
          }`}
          onClick={() => {
            setSearch(true);
            setActive(false);
          }}
        ></span>
        <div className={`dropdown-content sp-full ${isSearch && "show"}`}>
          <div className="d-flex justify-space-between align-center px-4 py-3 notification-tick dp-sp-hide">
            <span>{t("header:message")}</span>
            <div className="d-flex align-center">
              <a href="/">{t("header:mark_as_read")}</a>
              <a href="/">
                <i className="fas fa-cog" />
              </a>
            </div>
          </div>
          <div className="dropdown-back d-flex align-center white px-4 py-2 dp-pc-hide">
            <i
              className="far fa-arrow-left fw-bold fs-16"
              onClick={() => setSearch(false)}
            />
            <div className="search ml-3 flex-1-0">
              <form id="search-form" className="d-flex align-center">
                <MwInput
                  type="text"
                  placeholder={t("header:search")}
                  className="txt-input"
                  value={keyword}
                  onChange={handleChange}
                  rightComponent={
                    <span className="search-close">
                      <i className="fal fa-times"></i>
                    </span>
                  }
                />
              </form>
            </div>
          </div>
          <div className="scroll-sp">
            <ul className="nav hover-1 px-0 pb-2 py-sp-2 mb-sp-2 border-0">
              <ListSearch data={listSearch} />
            </ul>
            <ul className="nav px-0 border-0">
              <li className="nav-item">
                <a className="nav-link blue-1 py-3" href="/">
                  <i className="fal fa-file-search" />
                  <span className="fs-14 pl-sp-1" onClick={handleReset}>
                    {t("header:advanced_search")}
                  </span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

SearchMobile.propTypes = {
  setActive: PropTypes.func,
};

SearchMobile.defaultProps = {
  setActive: () => {},
};

export default SearchMobile;
