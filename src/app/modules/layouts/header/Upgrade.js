import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";

function Upgrade({ totalDay, nowDay }) {
  const { t } = useTranslation();
  const dayFree = `${nowDay}/${totalDay} ${t("day_free")}`;
  const percentLeft = Math.round((nowDay / totalDay) * 10) * 10;
  return (
    <>
      <div className="free-date mr-4 dp-sp-hide">
        <p className="fs-12 white fw-normal mb-2">{dayFree}</p>
        <div className={`progress-bar --white per-${percentLeft}`}>
          <span></span>
        </div>
      </div>
      <Link to="#/" className="btn btn-green white mr-5 dp-sp-hide">
        {t("upgrade")}
      </Link>
    </>
  );
}

Upgrade.propTypes = {
  totalDay: PropTypes.number,
  nowDay: PropTypes.number,
};
Upgrade.defaultProps = {
  totalDay: 7,
  nowDay: 3,
};

export default Upgrade;
