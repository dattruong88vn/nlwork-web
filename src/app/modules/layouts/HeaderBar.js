import React from "react";
import {
  AccountMenu,
  SearchMobile,
  Search,
  QuickAdd,
  Upgrade,
  ListNotification,
} from "./header";

function HeaderBar({ setActive, openModal }) {
  return (
    <div className="header">
      <div className="header-container d-flex justify-space-between align-center">
        <div className="header-left dp-sp-hide">
          <Search setActive={setActive} />
        </div>
        <div className="header-right d-flex align-center">
          <Upgrade />
          <QuickAdd />
          <SearchMobile setActive={setActive} />

          <ListNotification
            setActive={setActive}
            openSettingModal={openModal}
          />
          <AccountMenu setActive={setActive} openSettingModal={openModal} />
        </div>
      </div>
    </div>
  );
}

export default HeaderBar;
