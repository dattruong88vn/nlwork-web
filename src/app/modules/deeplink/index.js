import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useDeviceDetect } from "core/hooks";
import { getParameterByName } from "core/utils";
import { MwLoading } from "app/components";

function Deeplink() {
  const { isIos, isAndroid } = useDeviceDetect();
  const history = useHistory();
  const { search } = history.location;
  const typeService = getParameterByName("typeService", search);

  useEffect(() => {
    const handleService = (type) => {
      const id = getParameterByName("id", search);
      const token = getParameterByName("token", search);
      const email = getParameterByName("email", search);

      switch (type) {
        case "task":
          history.replace(`/task-detail/${id}`);
          break;
        case "active":
          history.replace(`/auth/activate-user/?token=${token}`);
          break;
        case "reset-password":
          history.replace(`/auth/reset-password?token=${token}&email=${email}`);
          break;
        default:
          break;
      }
    };

    if (isAndroid) {
      history.replace(`/deeplink${search}`);
    }

    if (isIos) {
      try {
        window.location = `mework://${search}`;
      } catch (e) {
        handleService(typeService);
      }
    }

    if (!isIos && !isAndroid) {
      handleService(typeService);
    }
  }, [history, isAndroid, isIos, search, typeService]);

  return (
    <>
      <MwLoading />;
    </>
  );
}

export default Deeplink;
