import React from "react";
import ReactDOM from "react-dom";
import MwButton from "app/components/button";
import MwDropDown from "app/components/dropdown";
import MwInput from "app/components/input";
import { useTranslation } from "react-i18next";
import TaskItem from "./taskItem/TaskItem";
import { useSelector } from "react-redux";
import { usePopper } from "react-popper";

function TaskGroupBoard({
  name,
  items,
  handleEdit,
  handleSubmitAdd,
  handleComplete,
  defaultView,
  projectId,
  groupId,
  loading,
  onDelete,
  isUnCategory,
  handleChange,
  value,
  handleSubmitEdit,
  editing,
  error,
}) {
  const { t } = useTranslation();
  const [errorAdd, setErrorAdd] = React.useState("");
  const [height, setHeight] = React.useState(93);
  const [expand, setExpand] = React.useState(false);
  const { status } = useSelector((state) => state.company.settings.task);
  const statusCreate = status.filter((status) => status.type === 1)[0].id;
  const refAddTask = React.useRef(null);
  const refInput = React.useRef(null);

  const [referenceRef, setReferenceRef] = React.useState(null);
  const [popperRef, setPopperRef] = React.useState(null);
  const [visible, setVisible] = React.useState(false);

  React.useEffect(() => {
    return () => {
      setErrorAdd("");
    };
  }, [expand]);

  const { styles, attributes } = usePopper(referenceRef, popperRef, {
    placement: "top",
    modifiers: [
      {
        name: "offset",
        enabled: true,
        options: {
          offset: [0, 10],
        },
      },
    ],
  });

  React.useEffect(() => {
    if (expand) {
      if (errorAdd || error) {
        setHeight(153);
      } else {
        setHeight(135);
      }
    } else {
      setHeight(93);
    }
  }, [error, expand, errorAdd]);

  React.useEffect(() => {
    if (editing) {
      if (error) {
        setHeight(153);
      } else {
        setHeight(115);
      }
      setExpand(false);
    } else {
      setHeight(93);
    }
  }, [editing, error]);

  const handleExpand = () => {
    setExpand(!expand);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setErrorAdd("");
    setHeight(153);

    const name = refAddTask.current["name"];

    if (!name.value || !name.value.trim().length) {
      setHeight(153);
      return setErrorAdd(t("don_let_this_field_blank"));
    }

    const formData = new FormData();
    formData.append("name", name.value);
    formData.append("status", statusCreate);

    projectId && formData.append("project_id", projectId);
    !isUnCategory && formData.append("group_id", groupId);

    if (handleSubmitAdd) {
      handleSubmitAdd(formData);
      refInput.current.handleClear();
    }
  };

  return (
    <div
      className="group border-rad5 bg-grey-2 relative"
      style={{ paddingTop: height }}
    >
      <div className="job-fixed px-3 pt-3 border-rad5 bg-grey-2">
        <div className="title d-flex align-center justify-space-between mb-3">
          <b
            className="black-3 fs-15 fw-500"
            style={{ display: editing ? "none" : "block" }}
          >
            <span
              ref={setReferenceRef}
              onMouseEnter={() => setVisible(true)}
              onMouseLeave={() => setVisible(false)}
            >
              {isUnCategory ? t(name) : `${value}`}
            </span>

            {ReactDOM.createPortal(
              visible && (
                <div
                  ref={setPopperRef}
                  style={{
                    ...styles.popper,
                    zIndex: 1001,
                  }}
                  className="tooltip-avatar"
                  {...attributes.popper}
                >
                  {isUnCategory ? t(name) : `${value}`}
                </div>
              ),
              document.body
            )}
          </b>

          <div
            style={{ display: editing ? "none" : "block" }}
            className="d-flex align-center"
          >
            {items && (
              <span
                style={{ display: editing ? "none" : "block" }}
                className={`fs-12 border-rad3 text-center dp-inline-block bg-grey-7 white ${
                  !isUnCategory && "mr-2"
                }`}
              >
                {items.length}
              </span>
            )}
            {!isUnCategory && (
              <MwDropDown
                style={{ display: editing ? "none" : "block" }}
                activeBtnClass="grey-4 fas fa-ellipsis-v"
                contentClass="w-sp-100"
              >
                <ul className="nav hover-2 px-0 py-2 border-0">
                  <li className="nav-item">
                    <a
                      href="/"
                      className="nav-link px-3 py-2"
                      onClick={handleEdit}
                    >
                      {t("edit")}
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      href="/"
                      className="nav-link px-3 py-2"
                      onClick={onDelete}
                    >
                      {t("delete")}
                    </a>
                  </li>
                </ul>
              </MwDropDown>
            )}
          </div>
          <div
            className={`text-edit-field flex-1-0 mr-2 ${
              !editing && "disabled"
            }`}
            style={{ display: editing ? "block" : "none" }}
          >
            <MwInput
              name="name"
              className="txt-input fs-15 fw-bold h-32"
              classError="left"
              onChange={handleChange}
              defaultValue={value}
              error={error}
              value={value}
            />
            <div
              className="d-flex pt-2 pb-0 text-edit-btn"
              style={{ display: editing ? "block" : "none" }}
            >
              <MwButton
                title={t("save")}
                className="btn btn-2 mx-sp-1 btn-h-32 py-1 submit"
                onClick={handleSubmitEdit}
              />
              <a
                href="/"
                className="btn btn-4 btn-h-32 py-1 mr-5 mx-sp-1 close fw-normal"
                onClick={handleEdit}
              >
                {t("cancel")}
              </a>
            </div>
          </div>
        </div>
        <div className="add-job mb-2">
          <span
            className="add-job-toggle px-2 fs-13 dp-inline-block border-rad5 w-100 bg-white grey-7"
            onClick={handleExpand}
            style={{ display: expand ? "none" : "block" }}
          >
            <i className="fal fa-plus fs-16 mr-2" /> {t("add_task")}
          </span>
          <form
            ref={refAddTask}
            className="add-job-field"
            onSubmit={handleSubmit}
            style={{ display: expand ? "block" : "none" }}
          >
            <div className="d-flex align-center">
              <MwInput
                ref={refInput}
                className="txt-input h-32 py-0"
                name="name"
                error={errorAdd}
                placeholder={t("enter_task_name")}
              />
            </div>
            <div className="add-job-btn mt-2">
              <MwButton
                title={t("save")}
                loading={loading}
                className="btn btn-2 mx-sp-1 btn-h-32 w-87 py-1 submit center-2"
              />
              <MwButton
                title={t("cancel")}
                onClick={handleExpand}
                className="btn btn-4 btn-h-32 py-1 mr-5 mx-sp-1 close fw-normal"
                style={{ background: "transparent" }}
              />
            </div>
          </form>
        </div>
      </div>
      <div
        className="job-sroll scroll overflowx-auto px-3 pb-1"
        style={{ overflowX: "unset !important" }}
      >
        {!!items?.length &&
          items.map((item) => (
            <TaskItem
              key={item.id}
              handleComplete={handleComplete}
              view={defaultView}
              {...item}
            />
          ))}
      </div>
    </div>
  );
}

export default TaskGroupBoard;
