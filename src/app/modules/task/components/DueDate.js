import React, { useEffect, useState } from "react";
import { usePopper } from "react-popper";
import ReactDOM from "react-dom";
import useClickOutside from "core/hooks/useClickOutside";
import { useTranslation } from "react-i18next";
import MwButton from "app/components/button";
import MwInput from "app/components/input";

function DueDate({
  dueDate,
  classes,
  date,
  handleSetDueDate,
  handleDateChange,
  loading,
  hideContentDueDate,
  status,
  disable,
}) {
  const { t } = useTranslation();
  const [referenceRef, setReferenceRef] = useState(null);
  const [popperRef, setPopperRef] = useState(null);
  const [ref, visible, setVisible] = useClickOutside(false);
  const [colorStatus, setColorStatus] = useState();

  useEffect(() => {
    status?.type === 6 && dueDate && setColorStatus(status?.color);
  }, [dueDate, status]);

  const { styles, attributes } = usePopper(referenceRef, popperRef, {
    placement: "bottom",
    modifiers: [
      {
        name: "offset",
        enabled: true,
        options: {
          offset: [150, 10],
        },
      },
    ],
  });

  const handleExpand = () => {
    !dueDate && !hideContentDueDate && !disable && setVisible(!visible);
  };

  return (
    <>
      <span
        onClick={handleExpand}
        ref={setReferenceRef}
        className={`dropbtn d-flex align-center ${
          dueDate && !hideContentDueDate ? classes?.iconClass : "grey-7"
        } `}
      >
        <i
          className="fal fa-calendar-alt fs-16 mr-2"
          style={colorStatus ? { color: colorStatus } : {}}
        />
        <span
          style={colorStatus ? { color: colorStatus } : {}}
          className={
            dueDate && !hideContentDueDate ? classes?.colorClass : "grey-7"
          }
        >
          {!dueDate ? t("add_date") : date}
        </span>
      </span>
      {ReactDOM.createPortal(
        visible && (
          <div
            ref={setPopperRef}
            style={{
              ...styles.popper,
              zIndex: 99999,
            }}
            {...attributes.popper}
          >
            {!dueDate && (
              <div
                ref={ref}
                style={styles.offset}
                className={`dropdown-content ${visible ? "show" : ""}`}
              >
                <MwInput isDate inline onChange={handleDateChange}>
                  <MwButton
                    title={t("save")}
                    onClick={handleSetDueDate}
                    className="btn btn-2 btn-h-32 btn-flex mb-3 center-2"
                    loading={loading}
                    style={{
                      width: "100px",
                      marginRight: "15px",
                      float: "right",
                    }}
                  />
                  <MwButton
                    title={t("cancel")}
                    onClick={handleExpand}
                    className="btn btn-h-32 btn-flex mb-3 center-2 fw-normal"
                    style={{
                      width: "100px",
                      marginRight: "15px",
                      float: "right",
                      border: 0,
                    }}
                  />
                </MwInput>
              </div>
            )}
          </div>
        ),
        document.body
      )}

      {hideContentDueDate && classes?.content === "out_of_date"
        ? null
        : classes?.content && (
            <span className={`fs-12 tags-date ml-2 ${classes?.contentClass}`}>
              {t(classes?.content)}
            </span>
          )}
    </>
  );
}

export default React.memo(DueDate);
