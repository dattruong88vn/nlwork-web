import React, { useEffect, useState, useRef } from "react";
import find from "lodash/find";
import { useSelector } from "react-redux";
import moment from "moment-timezone";
import PropTypes from "prop-types";
import { getClassForTask } from "core/utils";
import { postWithToken, putFormData } from "core/utils/APIUtils";
import { PIN_TASK, UPDATE_DUE_DATE } from "app/const/Api";
import TaskItemList from "./TaskItemList";
import TaskItemColumn from "./TaskItemColumn";
import TaskItemBoard from "./TaskItemBoard";
import TaskItemDefault from "./TaskItemDefault";
import TaskItemFollowing from "./TaskItemFollowing";

function TaskItem({
  id,
  name,
  tags,
  featured_my_task,
  due_date,
  total_checklist,
  total_comment,
  project_id,
  total_attachment,
  priority_id,
  handleComplete,
  status_id,
  status: statusNewTask,
  view,
  featured_in_project,
  assignee = [],
  followers = [],
  isProject,
  created_by,
}) {
  const [checked, setChecked] = useState(false);
  const [loading, setLoading] = useState(false);
  const [pin, setPin] = useState(
    (featured_in_project && isProject) || featured_my_task
  );
  const members = () => {
    const result = followers ? [...followers] : [];
    if (assignee) result.push(assignee);
    return [...new Set(result)];
  };

  const { timezone } = useSelector((state) => state.user.userInfo.company_info);
  const [classes, setClasses] = useState(getClassForTask(due_date, timezone));
  const [dueDate, setDueDate] = useState(due_date);
  const [newDueDate, setNewDueDate] = useState(
    moment.tz(timezone).endOf("day").utc().unix()
  );
  const refTimer = useRef(null);
  const { priority = [], status = [] } = useSelector(
    (state) => state.company.settings.task
  );
  const [statusValue, setStatusValue] = useState(
    find(status, ["id", status_id || statusNewTask])
  );
  const [priorityValue, setPriorityValue] = useState(
    find(priority, ["id", priority_id])
  );

  useEffect(() => {
    const statusInfo = find(status, ["id", status_id || statusNewTask]);
    checked && statusInfo?.type === 5 && setChecked(false);
    const priorityInfo = find(priority, ["id", priority_id]);
    setStatusValue(statusInfo);
    setPriorityValue(priorityInfo);
  }, [checked, priority, priority_id, status, statusNewTask, status_id]);

  useEffect(() => {
    setClasses(getClassForTask(due_date, timezone));
    setDueDate(due_date);
  }, [due_date, timezone]);

  const convertDate = (date) => {
    return moment.tz(moment.unix(date), timezone).format("DD/MM");
  };

  const handleChange = () => {
    if (checked) {
      setChecked(!checked);
      return clearTimeout(refTimer.current);
    }

    if (handleComplete) {
      setChecked(!checked);
      refTimer.current = setTimeout(() => {
        handleComplete(
          id,
          project_id,
          status_id || statusNewTask,
          pin,
          dueDate
        );
      }, 1000);
    }
  };

  const handlePin = () => {
    setPin(!pin);
    postWithToken(PIN_TASK, {
      task_id: id,
      featured: !featured_my_task,
      type: isProject ? 2 : 1,
    });
  };

  const handleDateChange = (date) => {
    const value = moment
      .tz(moment(date, "DD/MM/YYYY").endOf("day"), timezone)
      .unix();
    setNewDueDate(value);
  };

  const handleSetDueDate = (date) => {
    setLoading(true);
    const formData = new FormData();
    formData.append("task_id", id);
    formData.append("project_id", project_id);
    formData.append("value", newDueDate);

    putFormData(UPDATE_DUE_DATE, formData)
      .then(() => {
        setLoading(false);
        setDueDate(newDueDate);
        setClasses(getClassForTask(newDueDate, timezone));
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  };

  return (
    <>
      {view === "list" && (
        <TaskItemList
          id={id}
          name={name}
          tags={tags}
          totalChecklist={total_checklist}
          assignee={assignee}
          followers={followers}
          created_by={created_by}
          totalComment={total_comment}
          projectId={project_id}
          totalAttachment={total_attachment}
          handleComplete={handleComplete}
          statusNewTask={statusNewTask}
          handleChange={handleChange}
          checked={checked}
          handlePin={handlePin}
          pin={pin}
          classes={classes}
          statusValue={statusValue}
          priorityValue={priorityValue}
          dueDate={dueDate}
          date={convertDate(dueDate)}
          handleDateChange={handleDateChange}
          handleSetDueDate={handleSetDueDate}
          members={members()}
          loading={loading}
        />
      )}

      {view === "column" && (
        <TaskItemColumn
          classes={classes}
          name={name}
          checked={checked}
          statusValue={statusValue}
          priorityValue={priorityValue}
          members={members()}
          dueDate={dueDate}
          date={convertDate(dueDate)}
          totalChecklist={total_checklist}
          totalComment={total_comment}
          totalAttachment={total_attachment}
          handleChange={handleChange}
          pin={pin}
          tags={tags}
          handleDateChange={handleDateChange}
          handleSetDueDate={handleSetDueDate}
          handlePin={handlePin}
          loading={loading}
          projectId={project_id}
          created_by={created_by}
          followers={followers}
          assignee={assignee}
        />
      )}

      {view === "board" && (
        <TaskItemBoard
          classes={classes}
          name={name}
          checked={checked}
          statusValue={statusValue}
          members={members()}
          dueDate={dueDate}
          date={convertDate(dueDate)}
          totalChecklist={total_checklist}
          totalComment={total_comment}
          totalAttachment={total_attachment}
          handleChange={handleChange}
          pin={pin}
          tags={tags}
          handleDateChange={handleDateChange}
          handleSetDueDate={handleSetDueDate}
          handlePin={handlePin}
          priorityValue={priorityValue}
          loading={loading}
          projectId={project_id}
          created_by={created_by}
          followers={followers}
          assignee={assignee}
        />
      )}

      {view === "following" && (
        <TaskItemFollowing
          id={id}
          tags={tags}
          name={name}
          dueDate={dueDate}
          loading={loading}
          projectId={project_id}
          date={convertDate(dueDate)}
          handlePin={handlePin}
          pin={pin}
          createdBy={created_by}
          statusId={status_id || statusNewTask}
          priorityId={priority_id}
          members={followers}
          classes={classes}
          totalChecklist={total_checklist}
          totalComment={total_comment}
          handleSetDueDate={handleSetDueDate}
          handleDateChange={handleDateChange}
          totalAttachment={total_attachment}
          handleComplete={handleChange}
          followers={followers}
          assignee={assignee}
          statusValue={statusValue}
          priorityValue={priorityValue}
        />
      )}

      {!view && (
        <TaskItemDefault
          tags={tags}
          checked={checked}
          handleChange={handleChange}
          priorityValue={priorityValue}
          statusValue={statusValue}
          name={name}
          members={members()}
          id={id}
          totalChecklist={total_checklist}
          totalComment={total_comment}
          totalAttachment={total_attachment}
          dueDate={dueDate}
          classes={classes}
          date={convertDate(dueDate)}
          pin={pin}
          handleDateChange={handleDateChange}
          handleSetDueDate={handleSetDueDate}
          handlePin={handlePin}
          loading={loading}
        />
      )}
    </>
  );
}

TaskItem.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  status_id: PropTypes.string,
  project_id: PropTypes.string,
  tags: PropTypes.array,
  featured_my_task: PropTypes.bool,
  due_date: PropTypes.string,
  total_checklist: PropTypes.objectOf(PropTypes.number),
  total_comment: PropTypes.number,
  total_attachment: PropTypes.number,
  handleComplete: PropTypes.func,
};

TaskItem.defaultProps = {
  id: null,
  name: null,
  status_id: null,
  project_id: null,
  tags: [],
  featured_my_task: false,
  due_date: null,
  total_checklist: {},
  total_comment: 0,
  total_attachment: 0,
  handleComplete: null,
};

export default TaskItem;
