import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import MwMembers from "app/components/members";
import DueDate from "../DueDate";
import ListCount from "../ListCount";
import { COLOR_PRIORITY } from "app/const/Colors";
import { usePopper } from "react-popper";
import { useSelector } from "react-redux";
import { usePermissions } from "core/hooks";
import { TASK_IN_PROJECT_PERMISSION_DEFAULT } from "app/const/Permissions";

function TaskItemBoard({
  id,
  project_id,
  name,
  due_date,
  attachment,
  statusValue,
  classes,
  dueDate,
  handleSetDueDate,
  date,
  handleDateChange,
  handleChange,
  checked,
  members,
  hasTooltip,
  totalChecklist,
  totalComment,
  totalAttachment,
  priorityValue,
  loading,
  projectId,
  assignee,
  created_by,
  followers,
}) {
  const { getPermission } = usePermissions();
  const [permissionUpdate, setPermissionUpdate] = useState(
    TASK_IN_PROJECT_PERMISSION_DEFAULT
  );
  const [referenceRef, setReferenceRef] = useState(null);
  const [popperRef, setPopperRef] = useState(null);
  const [visible, setVisible] = useState(false);
  const [hideContentDueDate, setHideContentDueDate] = useState(false);
  const statusList = useSelector((state) => state.company.settings.task.status);
  const statusDone = statusList.filter((status) => status.type === 5)[0].id;
  const statusCancel = statusList.filter((status) => status.type === 6)[0].id;

  useEffect(() => {
    const permissionInfo = getPermission(
      { project_id: projectId, assignee, created_by, followers },
      "TASK_IN_PROJECT"
    );

    setPermissionUpdate(permissionInfo?.update);
  }, [assignee, created_by, followers, getPermission, projectId]);

  useEffect(() => {
    (statusValue?.id === statusDone || statusCancel === statusValue?.id) &&
      setHideContentDueDate(true);
  }, [statusCancel, statusDone, statusValue]);

  const { styles, attributes } = usePopper(referenceRef, popperRef, {
    placement: "top",
    modifiers: [
      {
        name: "offset",
        enabled: true,
        options: {
          offset: [0, 10],
        },
      },
    ],
  });

  const handleCompleteChange = (e) => {
    if (handleChange && statusValue?.type !== 5 && permissionUpdate) {
      handleChange(e);
    }
  };

  return (
    <>
      <a
        href="/"
        onClick={(e) => e.preventDefault()}
        className="b-job p-2 pb-4 mb-2 border-rad5 dp-block relative"
      >
        {attachment && (
          <div className="b-job-attach text-center mb-2 w-100 dp-block">
            {/* We'll active when we use */}
            {/* <img src={attachmentImg} alt="" /> */}
          </div>
        )}

        <div className="b-job-title mb-3 relative w-100">
          <span
            className={`fs-13 fw-500 word-break dp-block ${
              (checked || statusValue?.type === 5) && "txt-decoration"
            } lineclamp-2`}
          >
            <span
              ref={setReferenceRef}
              onMouseEnter={() => setVisible(true)}
              onMouseLeave={() => setVisible(false)}
            >
              {name}
            </span>
          </span>
          <div className="checkbox-absolute">
            <div className="checkbox">
              <input
                type="checkbox"
                checked={checked}
                onChange={handleCompleteChange}
                id={id}
              />
              <label
                style={{ color: priorityValue?.color || COLOR_PRIORITY }}
                onClick={handleCompleteChange}
                htmlFor={id}
                className="fw-normal fs-14 fw-normal"
              >
                &nbsp;
              </label>
            </div>
          </div>
        </div>
        <span
          className="b-job-status fs-12 fw-500 border-rad3 dp-block maxw-100 text-center"
          style={{ background: statusValue?.color || "transparent" }}
        >
          {statusValue?.name}
        </span>
        <div className="d-flex justify-space-between align-center mt-3">
          <DueDate
            dueDate={dueDate}
            handleSetDueDate={handleSetDueDate}
            handleDateChange={handleDateChange}
            date={date}
            classes={classes}
            loading={loading}
            hideContentDueDate={hideContentDueDate}
            status={statusValue}
            disable={!dueDate && !permissionUpdate}
          />
        </div>
        {((!!totalChecklist?.all && !!totalComment) || !!members?.length) && (
          <div className="mt-3 d-flex">
            <ListCount
              hideAttachment
              totalChecklist={totalChecklist}
              totalComment={totalComment}
              totalAttachment={totalAttachment}
            />

            {!totalChecklist?.all && !totalComment && !!members?.length && (
              <div style={{ height: 16 }} />
            )}
          </div>
        )}

        <MwMembers
          members={members}
          className="d-flex align-center absolute"
          hasIcon={false}
        />
      </a>
      {ReactDOM.createPortal(
        visible && (
          <div
            ref={setPopperRef}
            style={{
              ...styles.popper,
              zIndex: 1001,
            }}
            className="tooltip-avatar"
            {...attributes.popper}
          >
            {name}
          </div>
        ),
        document.body
      )}
    </>
  );
}

export default TaskItemBoard;
