import React, { useEffect, useState } from "react";
import { usePopper } from "react-popper";
import { useSelector } from "react-redux";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { usePermissions } from "core/hooks";
import { TASK_IN_PROJECT_PERMISSION_DEFAULT } from "app/const/Permissions";
import StarButton from "app/components/button/StarButton";
import TagList from "app/modules/task/components/TagList";
import MwMembers from "app/components/members";
import DueDate from "../DueDate";
import { COLOR_PRIORITY, COLOR_STATUS } from "app/const/Colors";

function TaskItemList({
  id,
  name,
  tags,
  totalChecklist,
  totalComment,
  projectId,
  totalAttachment,
  handleComplete,
  status: statusNewTask,
  handleChange,
  checked,
  handlePin,
  pin,
  classes,
  statusValue,
  priorityValue,
  dueDate,
  date,
  handleSetDueDate,
  handleDateChange,
  loading,
  members,
  assignee,
  followers,
  created_by,
}) {
  const { getPermission } = usePermissions();
  const [permissionUpdate, setPermissionUpdate] = useState(
    TASK_IN_PROJECT_PERMISSION_DEFAULT
  );
  const [referenceRef, setReferenceRef] = useState(null);
  const [popperRef, setPopperRef] = useState(null);
  const [visible, setVisible] = useState(false);
  const [hideContentDueDate, setHideContentDueDate] = useState(false);
  const statusList = useSelector((state) => state.company.settings.task.status);
  const statusDone = statusList.filter((status) => status.type === 5)[0].id;
  const statusCancel = statusList.filter((status) => status.type === 6)[0].id;

  useEffect(() => {
    const permissionInfo = getPermission(
      { project_id: projectId, assignee, created_by, followers },
      "TASK_IN_PROJECT"
    );

    setPermissionUpdate(permissionInfo?.update);
  }, [assignee, created_by, followers, getPermission, projectId]);

  useEffect(() => {
    (statusValue?.id === statusDone || statusCancel === statusValue?.id) &&
      setHideContentDueDate(true);
  }, [statusCancel, statusDone, statusValue]);

  const { styles, attributes } = usePopper(referenceRef, popperRef, {
    placement: "top",
    modifiers: [
      {
        name: "offset",
        enabled: true,
        options: {
          offset: [0, 10],
        },
      },
    ],
  });

  const handleCompleteChange = (e) => {
    if (handleChange && statusValue?.type !== 5 && permissionUpdate) {
      handleChange(e);
    }
  };

  return (
    <>
      <div className="d-flex bg-white hover-5 border-bottom-grey-6">
        <div
          className={`hover-visibility row item align-center px-1 w-100 ${
            tags?.length ? "py-2" : "py-3"
          }`}
        >
          <div className="col-4">
            <div className="checkbox">
              <input type="checkbox" id={id} />
              <label
                className="fw-normal fs-14 fw-normal none-hover"
                style={
                  statusValue?.type === 6 ? { color: statusValue.color } : {}
                }
              >
                {checked || statusValue?.id === statusDone ? (
                  <div
                    className="label-after label-after-after"
                    onClick={handleCompleteChange}
                  />
                ) : (
                  <div
                    className="label-after label-after-before"
                    style={{
                      borderColor: priorityValue?.color || COLOR_PRIORITY,
                    }}
                    onClick={handleCompleteChange}
                  />
                )}
                <span
                  ref={setReferenceRef}
                  onMouseEnter={() => setVisible(true)}
                  onMouseLeave={() => setVisible(false)}
                >
                  {name}
                </span>

                {(checked ||
                  statusValue?.type === 5 ||
                  statusValue?.type === 6) && (
                  <span
                    className="line"
                    style={
                      !checked && statusValue?.type === 6
                        ? { borderColor: statusValue.color }
                        : {}
                    }
                  />
                )}
              </label>
            </div>

            <TagList tags={tags} paddingLeft />
          </div>
          <div className="col-1">
            {!!totalChecklist?.all && (
              <span className="fs-12 d-flex align-center">
                <i className="far fa-check-square fs-17 mr-1 grey-7" />
                {`${totalChecklist?.done || 0}/${totalChecklist?.all || 0}`}
              </span>
            )}
          </div>
          <div className="col-2">
            {statusValue && (
              <div
                className="fs-13 d-flex align-center"
                style={{ color: statusValue?.color || COLOR_STATUS }}
              >
                <span className="fs-13 d-flex align-center nowrap">
                  <i className="fas fa-circle mr-2" /> {statusValue?.name}
                </span>
              </div>
            )}
          </div>

          <div className="col-2">
            <MwMembers members={members} />
          </div>

          <div className="col-2 d-flex flex-sp-wrap align-center">
            <DueDate
              dueDate={dueDate}
              handleSetDueDate={handleSetDueDate}
              handleDateChange={handleDateChange}
              date={date}
              classes={classes}
              loading={loading}
              status={statusValue}
              hideContentDueDate={hideContentDueDate}
              disable={!dueDate && !permissionUpdate}
            />
          </div>
          <div className="col-1 d-flex justify-end align-center">
            <StarButton favorite={pin} onClick={handlePin} />
          </div>
        </div>
      </div>
      {ReactDOM.createPortal(
        visible && (
          <div
            ref={setPopperRef}
            style={{
              ...styles.popper,
              zIndex: 1001,
            }}
            className="tooltip-avatar"
            {...attributes.popper}
          >
            {name}
          </div>
        ),
        document.body
      )}
    </>
  );
}

TaskItemList.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  status_id: PropTypes.string,
  project_id: PropTypes.string,
  tags: PropTypes.array,
  featured_my_task: PropTypes.bool,
  due_date: PropTypes.string,
  total_checklist: PropTypes.objectOf(PropTypes.number),
  total_comment: PropTypes.number,
  total_attachment: PropTypes.number,
  handleComplete: PropTypes.func,
};

TaskItemList.defaultProps = {
  id: null,
  name: null,
  status_id: null,
  project_id: null,
  tags: [],
  featured_my_task: false,
  due_date: null,
  total_checklist: {},
  total_comment: 0,
  total_attachment: 0,
  handleComplete: null,
};

export default TaskItemList;
