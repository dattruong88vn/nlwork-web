import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import StarButton from "app/components/button/StarButton";
import TagList from "app/modules/task/components/TagList";
import ListCount from "../ListCount";
import DueDate from "../DueDate";
import MwMembers from "app/components/members";
import { COLOR_PRIORITY } from "app/const/Colors";
import { usePopper } from "react-popper";
import { useSelector } from "react-redux";
import { usePermissions } from "core/hooks";
import { TASK_IN_PROJECT_PERMISSION_DEFAULT } from "app/const/Permissions";

function TaskItemColumn({
  id,
  name,
  tags,
  due_date,
  total_attachment,
  handleComplete,
  classes,
  members,
  statusValue,
  priorityValue,
  date,
  dueDate,
  totalChecklist,
  totalComment,
  totalAttachment,
  handleChange,
  projectId,
  checked,
  pin,
  handlePin,
  handleDateChange,
  handleSetDueDate,
  loading,
  assignee,
  created_by,
  followers,
}) {
  const { getPermission } = usePermissions();
  const [permissionUpdate, setPermissionUpdate] = useState(
    TASK_IN_PROJECT_PERMISSION_DEFAULT
  );
  const [referenceRef, setReferenceRef] = useState(null);
  const [popperRef, setPopperRef] = useState(null);
  const [visible, setVisible] = useState(false);
  const [hideContentDueDate, setHideContentDueDate] = useState(false);
  const statusList = useSelector((state) => state.company.settings.task.status);
  const statusDone = statusList.filter((status) => status.type === 5)[0].id;
  const statusCancel = statusList.filter((status) => status.type === 6)[0].id;

  useEffect(() => {
    const permissionInfo = getPermission(
      { project_id: projectId, assignee, created_by, followers },
      "TASK_IN_PROJECT"
    );

    setPermissionUpdate(permissionInfo?.update);
  }, [assignee, created_by, followers, getPermission, projectId]);

  useEffect(() => {
    (statusValue?.id === statusDone || statusCancel === statusValue?.id) &&
      setHideContentDueDate(true);
  }, [statusCancel, statusDone, statusValue]);

  const { styles, attributes } = usePopper(referenceRef, popperRef, {
    placement: "top",
    modifiers: [
      {
        name: "offset",
        enabled: true,
        options: {
          offset: [0, 10],
        },
      },
    ],
  });

  const handleCompleteChange = (e) => {
    if (handleChange && statusValue?.type !== 5 && permissionUpdate) {
      handleChange(e);
    }
  };

  return (
    <>
      <div className="task-item hover-5 w-100">
        <div className="d-flex w-100">
          <div className="border-top-grey-6 flex-1-0 pr-3 py-3 pl-0 d-flex align-center">
            <div className="checkbox">
              <input type="checkbox" id={id} />
              <label
                className="fw-normal fs-14 fw-normal none-hover"
                style={
                  statusValue?.type === 6 ? { color: statusValue.color } : {}
                }
              >
                {checked || statusValue?.type === 5 ? (
                  <div
                    className="label-after label-after-after"
                    onClick={handleCompleteChange}
                  />
                ) : (
                  <div
                    className="label-after label-after-before"
                    style={{
                      borderColor: priorityValue?.color || COLOR_PRIORITY,
                    }}
                    onClick={handleCompleteChange}
                  />
                )}

                <span
                  ref={setReferenceRef}
                  onMouseEnter={() => setVisible(true)}
                  onMouseLeave={() => setVisible(false)}
                >
                  {name}
                </span>

                {(checked ||
                  statusValue?.type === 5 ||
                  statusValue?.type === 6) && (
                  <span
                    className="line"
                    style={
                      !checked && statusValue?.type === 6
                        ? { borderColor: statusValue.color }
                        : {}
                    }
                  />
                )}
              </label>
            </div>
          </div>
          <div className="border-top-grey-6 p-3 d-flex align-center w-210">
            <ListCount
              totalChecklist={totalChecklist}
              totalComment={totalComment}
              totalAttachment={totalAttachment}
            />
          </div>
          <div className="border-top-grey-6 border-left-grey-6 p-3 d-flex align-center w-150">
            {statusValue && (
              <div
                className="fs-13 d-flex align-center"
                style={{ color: statusValue?.color }}
              >
                <span className="fs-13 d-flex align-center nowrap">
                  <i className="fas fa-circle mr-2"></i> {statusValue?.name}
                </span>
              </div>
            )}
          </div>

          <div className="border-top-grey-6 border-left-grey-6 p-3 d-flex align-center w-115">
            {!!members?.length && <MwMembers members={members} />}
          </div>

          <div className="border-top-grey-6 border-left-grey-6 p-3 d-flex align-center w-125">
            <TagList
              wrapperClass="tags d-flex w-100"
              spanClass="w-100 text-center tags-overflow mr-1"
              className="nonePadding"
              tags={tags}
              limit={1}
            />
          </div>

          <div className="border-top-grey-6 border-left-grey-6 p-3 d-flex align-center w-168">
            <DueDate
              dueDate={dueDate}
              handleSetDueDate={handleSetDueDate}
              handleDateChange={handleDateChange}
              date={date}
              classes={classes}
              hideContentDueDate={hideContentDueDate}
              status={statusValue}
              disable={!dueDate && !permissionUpdate}
            />
          </div>
          <div className="border-top-grey-6 text-right pr-0 pl-3 py-3 pr-0 d-flex align-center justify-end w-40">
            <StarButton favorite={pin} onClick={handlePin} />
          </div>
        </div>
      </div>
      {ReactDOM.createPortal(
        visible && (
          <div
            ref={setPopperRef}
            style={{
              ...styles.popper,
              zIndex: 1001,
            }}
            className="tooltip-avatar"
            {...attributes.popper}
          >
            {name}
          </div>
        ),
        document.body
      )}
    </>
  );
}

TaskItemColumn.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  status_id: PropTypes.string,
  project_id: PropTypes.string,
  tags: PropTypes.array,
  featured_my_task: PropTypes.bool,
  due_date: PropTypes.string,
  total_checklist: PropTypes.objectOf(PropTypes.number),
  total_comment: PropTypes.number,
  total_attachment: PropTypes.number,
  handleComplete: PropTypes.func,
};

TaskItemColumn.defaultProps = {
  id: null,
  name: null,
  status_id: null,
  project_id: null,
  tags: [],
  featured_my_task: false,
  due_date: null,
  total_checklist: {},
  total_comment: 0,
  total_attachment: 0,
  handleComplete: null,
};

export default TaskItemColumn;
