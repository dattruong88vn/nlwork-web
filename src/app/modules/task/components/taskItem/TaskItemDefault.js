import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import MwStarButton from "app/components/button/StarButton";
import TagList from "../TagList";
import DueDate from "../DueDate";
import { COLOR_PRIORITY } from "app/const/Colors";
import { usePopper } from "react-popper";
import { useSelector } from "react-redux";

function TaskItemDefault({
  tags,
  checked,
  handleChange,
  priorityValue,
  statusValue,
  name,
  id,
  totalChecklist,
  totalComment,
  totalAttachment,
  expand,
  handleExpand,
  dueDate,
  classes,
  date,
  handleDateChange,
  handleSetDueDate,
  pin,
  handlePin,
  loading,
}) {
  const [referenceRef, setReferenceRef] = useState(null);
  const [popperRef, setPopperRef] = useState(null);
  const [visible, setVisible] = useState(false);
  const [hideContentDueDate, setHideContentDueDate] = useState(false);
  const statusList = useSelector((state) => state.company.settings.task.status);
  const statusDone = statusList.filter((status) => status.type === 5)[0].id;
  const statusCancel = statusList.filter((status) => status.type === 6)[0].id;

  useEffect(() => {
    (statusValue.id === statusDone || statusCancel === statusValue.id) &&
      setHideContentDueDate(true);
  }, [statusCancel, statusDone, statusValue]);

  const { styles, attributes } = usePopper(referenceRef, popperRef, {
    placement: "top",
    modifiers: [
      {
        name: "offset",
        enabled: true,
        options: {
          offset: [0, 10],
        },
      },
    ],
  });

  const handleComplete = (e) => {
    handleChange && statusValue?.type !== 5 && handleChange(e);
  };

  return (
    <>
      <div className="d-flex bg-white hover-5 border-bottom-grey-6">
        <div
          className={`row align-center px-1 w-100 task ${
            tags?.length ? "py-2" : "py-4"
          }`}
        >
          <div className="col-5">
            <div className="checkbox">
              <input type="checkbox" id={id} />
              <label
                ref={setReferenceRef}
                className="fw-normal fs-14 fw-normal none-hover"
                onMouseEnter={() => setVisible(true)}
                onMouseLeave={() => setVisible(false)}
                style={
                  statusValue?.type === 6 ? { color: statusValue.color } : {}
                }
              >
                {checked || statusValue.id === statusDone ? (
                  <div
                    className="label-after label-after-after"
                    onClick={handleComplete}
                  />
                ) : (
                  <div
                    className="label-after label-after-before"
                    style={{
                      borderColor: priorityValue?.color || COLOR_PRIORITY,
                    }}
                    onClick={handleComplete}
                  />
                )}
                {name}

                {(checked ||
                  statusValue.type === 5 ||
                  statusValue.type === 6) && (
                  <span
                    className="line"
                    style={
                      !checked && statusValue?.type === 6
                        ? { borderColor: statusValue.color }
                        : {}
                    }
                  />
                )}
              </label>
            </div>
            <TagList tags={tags} paddingLeft limit={5} />
          </div>
          <div className="col-2">
            <div
              className="fs-13  d-flex align-center"
              style={{ color: statusValue?.color || "transparent" }}
            >
              <span className="fs-13 d-flex align-center nowrap">
                <i className="fas fa-circle mr-2"></i>
                {statusValue?.name}
              </span>
            </div>
          </div>
          <div className="col-2 d-flex flex-sp-wrap align-center grey-7">
            {!!totalChecklist?.all && (
              <span className="mr-4 fs-12 d-flex align-center">
                <i className="far fa-check-square fs-17 mr-1" />{" "}
                {`${totalChecklist?.done || 0}/${totalChecklist.all}`}
              </span>
            )}
            {!!totalComment && (
              <span className="mr-4 fs-12 d-flex align-center">
                <i className="fal fa-comment-alt fs-15 mr-1 " />{" "}
                {totalComment || 0}
              </span>
            )}
            {!!totalAttachment && (
              <span className="fs-12 d-flex align-center">
                <i className="fas fa-paperclip fs-15 mr-1" />{" "}
                {totalAttachment || 0}
              </span>
            )}
          </div>

          <div className="col-2 d-flex flex-sp-wrap align-center">
            <DueDate
              dueDate={dueDate}
              handleSetDueDate={handleSetDueDate}
              handleDateChange={handleDateChange}
              date={date}
              classes={classes}
              loading={loading}
              hideContentDueDate={hideContentDueDate}
              status={statusValue}
            />
          </div>

          <div className="col-1 d-flex justify-end align-center">
            <MwStarButton favorite={pin} onClick={handlePin} />
          </div>
        </div>
      </div>
      {ReactDOM.createPortal(
        visible && (
          <div
            ref={setPopperRef}
            style={{
              ...styles.popper,
              zIndex: 99999,
            }}
            className="tooltip-avatar"
            {...attributes.popper}
          >
            {name}
          </div>
        ),
        document.body
      )}
    </>
  );
}

export default TaskItemDefault;
