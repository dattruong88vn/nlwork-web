import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import { useSelector } from "react-redux";
import _ from "lodash";
import PropTypes from "prop-types";
import StarButton from "app/components/button/StarButton";
import MwMembers from "app/components/members";
import TagList from "../TagList";
import ListCount from "../ListCount";
import { COLOR_PRIORITY } from "app/const/Colors";
import { usePopper } from "react-popper";
import DueDate from "../DueDate";
import { usePermissions } from "core/hooks";
import { TASK_PERMISSION_DEFAULT } from "app/const/Permissions";

function TaskItemFollowing({
  id,
  name,
  tags,
  createBy,
  dueDate,
  assignee,
  projectId,
  priorityId,
  handleComplete,
  followers,
  statusId,
  date,
  pin,
  handlePin,
  classes,
  totalComment,
  totalChecklist,
  handleSetDueDate,
  loading,
  handleDateChange,
  statusValue,
  priorityValue,
  created_by,
}) {
  const { getPermission } = usePermissions();
  const [permission, setPermission] = useState(TASK_PERMISSION_DEFAULT?.update);
  const [isFinishTask, setIsFinishTask] = useState(false);
  const [checked, setChecked] = useState(false);
  const { status = [] } = useSelector((state) => state.company.settings.task);
  const [referenceRef, setReferenceRef] = useState(null);
  const [popperRef, setPopperRef] = useState(null);
  const [visible, setVisible] = useState(false);
  const [hideContentDueDate, setHideContentDueDate] = useState(false);
  const statusDone = status.filter((status) => status.type === 5)[0].id;
  const statusCancel = status.filter((status) => status.type === 6)[0].id;

  useEffect(() => {
    const checkPermission = getPermission({
      created_by,
      assignee,
      followers,
      project_id: projectId,
    });

    setPermission(checkPermission?.update);
  }, [assignee, created_by, followers, getPermission, projectId]);

  useEffect(() => {
    (statusValue.id === statusDone || statusCancel === statusValue.id) &&
      setHideContentDueDate(true);
  }, [statusCancel, statusDone, statusValue]);

  const { styles, attributes } = usePopper(referenceRef, popperRef, {
    placement: "top",
    modifiers: [
      {
        name: "offset",
        enabled: true,
        options: {
          offset: [0, 10],
        },
      },
    ],
  });

  useEffect(() => {
    if (statusValue.id === _.find(status, ["type", 5]).id) {
      setIsFinishTask(true);
    }
  }, [status, statusValue]);

  const handleChange = (e) => {
    if (!permission || isFinishTask) return;
    setChecked(!checked);
    if (handleComplete) {
      setTimeout(() => {
        handleComplete && handleComplete(e);
      }, 1000);
    }
  };

  return (
    <>
      <div className="d-flex bg-white hover-5 border-bottom-grey-6">
        <div
          className={`row align-center px-1 py-2 following w-100  ${
            tags?.length ? "py-2" : "py-5"
          }`}
        >
          <div className="col-4">
            <div className="checkbox">
              <input type="checkbox" id={id} />
              <label
                className="fw-normal fs-14 fw-normal none-hover"
                style={
                  statusValue?.type === 6 ? { color: statusValue.color } : {}
                }
              >
                {checked || statusValue.id === statusDone ? (
                  <div
                    className="label-after label-after-after"
                    onClick={handleChange}
                  />
                ) : (
                  <div
                    className="label-after label-after-before"
                    style={{
                      borderColor: priorityValue?.color || COLOR_PRIORITY,
                    }}
                    onClick={handleChange}
                  />
                )}
                <span
                  ref={setReferenceRef}
                  onMouseEnter={() => setVisible(true)}
                  onMouseLeave={() => setVisible(false)}
                >
                  {name}
                </span>

                {(checked ||
                  statusValue.type === 5 ||
                  statusValue.type === 6) && (
                  <span
                    className="line"
                    style={
                      !checked && statusValue?.type === 6
                        ? { borderColor: statusValue.color }
                        : {}
                    }
                  />
                )}
              </label>
            </div>
            <TagList
              className="tags mt-2 d-flex align-center pl-tags"
              tags={tags}
            />
          </div>
          <div className="col-2">
            {statusValue && (
              <div
                className="fs-13  d-flex align-center"
                style={{ color: statusValue?.color || "transparent" }}
              >
                <span className="fs-13 d-flex align-center nowrap">
                  <i className="fas fa-circle mr-2"></i>
                  {statusValue?.name}
                </span>
              </div>
            )}
          </div>

          <div className="col-2 d-flex flex-sp-wrap align-center">
            <ListCount
              totalChecklist={totalChecklist}
              totalComment={totalComment}
            />
          </div>

          <div className="col-3 d-flex justify-end align-center col-following">
            <MwMembers
              members={[assignee] || []}
              className="d-flex align-center mr-2"
              hasIcon={false}
              limit={2}
            />
            <div className="d-flex align-center">
              <DueDate
                dueDate={dueDate}
                handleSetDueDate={handleSetDueDate}
                handleDateChange={handleDateChange}
                date={date}
                classes={classes}
                loading={loading}
                hideContentDueDate={hideContentDueDate}
                status={statusValue}
                disable={!dueDate && !permission}
              />
            </div>
          </div>
          <div className="col-1 d-flex justify-end align-center">
            <StarButton favorite={pin} onClick={handlePin} />
          </div>
        </div>
      </div>
      {ReactDOM.createPortal(
        visible && (
          <div
            ref={setPopperRef}
            style={{
              ...styles.popper,
              zIndex: 1001,
            }}
            className="tooltip-avatar"
            {...attributes.popper}
          >
            {name}
          </div>
        ),
        document.body
      )}
    </>
  );
}

TaskItemFollowing.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  status_id: PropTypes.string,
  project_id: PropTypes.string,
  tags: PropTypes.array,
  featured_my_task: PropTypes.bool,
  due_date: PropTypes.string,
  total_checklist: PropTypes.objectOf(PropTypes.number),
  total_comment: PropTypes.number,
  total_attachment: PropTypes.number,
  handleComplete: PropTypes.func,
};

TaskItemFollowing.defaultProps = {
  id: null,
  name: null,
  status_id: null,
  project_id: null,
  tags: [],
  featured_my_task: false,
  due_date: null,
  total_checklist: {},
  total_comment: 0,
  total_attachment: 0,
  handleComplete: null,
};

export default TaskItemFollowing;
