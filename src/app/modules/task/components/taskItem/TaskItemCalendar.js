import React, { useState, useEffect } from "react";
import find from "lodash/find";
import { useSelector } from "react-redux";
import MwAvatar from "app/components/avatar";
import { COLOR_STATUS } from "app/const/Colors";
import "./styles/itemTaskCalendar.scss";

function TaskItemCalendar({ eventInfo, type }) {
  const { event } = eventInfo;
  const [statusValue, setStatusValue] = useState();
  const [project, setProject] = useState();
  const [user, setUser] = useState();
  const { list_project = [] } = useSelector((state) => state.company.settings);
  const { members: membersCompany } = useSelector((state) => state.company);
  const { my_project_id, general_project_id } = useSelector(
    (state) => state.user.userInfo
  );
  const { priority = [], status = [] } = useSelector(
    (state) => state.company.settings.task
  );

  useEffect(() => {
    setUser(find(membersCompany, ["id", event.extendedProps.assignee]));
  }, [event, membersCompany]);

  useEffect(() => {
    setProject(find(list_project, ["id", event.extendedProps.project_id]));
    setStatusValue(
      find(status, [
        "id",
        event.extendedProps.status_id || event.extendedProps.status,
      ])
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [priority]);

  const ItemRender = ({ type }) => {
    switch (type) {
      case "project":
        return (
          <div
            className="task-calender project"
            style={{ borderColor: statusValue?.color || COLOR_STATUS }}
            draggable={false}
          >
            {user && <MwAvatar className="img-circle w-24 m-0" {...user} />}
            <span className="m-0 ml-1">{event.extendedProps.name}</span>
          </div>
        );

      default:
        return (
          <div
            className="task-calender"
            style={{ borderColor: statusValue?.color || COLOR_STATUS }}
          >
            <span data-tooltip={event.extendedProps.name}>
              {event.extendedProps.name}
            </span>
            {my_project_id !== project?.id &&
              general_project_id !== project?.id && (
                <h4 style={{ color: project?.color || "inherit" }}>
                  {project?.name}
                </h4>
              )}
          </div>
        );
    }
  };

  return <ItemRender type={type} />;
}

export default TaskItemCalendar;
