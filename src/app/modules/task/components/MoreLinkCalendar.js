import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";

function MoreLinkCalendar({ num }) {
  const { t } = useTranslation();

  return `${t("see_more")} (${num.num})`;
}

MoreLinkCalendar.propTypes = {
  num: PropTypes.shape({}),
};

export default MoreLinkCalendar;
