import React from "react";
import PropTypes from "prop-types";

function ListCount({
  totalChecklist,
  totalComment,
  hideAttachment,
  totalAttachment,
  spanClass,
  iconClass,
}) {
  return (
    <>
      {!!totalChecklist?.all && (
        <span className={spanClass}>
          <i className="far fa-check-square fs-17 mr-1"></i>{" "}
          {`${totalChecklist?.done || 0}/${totalChecklist?.all || 0}`}
        </span>
      )}
      {!!totalComment && (
        <span className={spanClass}>
          <i className="fal fa-comment-alt fs-15 mr-1"></i> {totalComment}
        </span>
      )}
      {!!totalAttachment && !hideAttachment && (
        <span className="fs-12 grey-7 d-flex align-center">
          <i className="fas fa-paperclip fs-15 mr-1"></i>
          {totalAttachment}
        </span>
      )}
    </>
  );
}

ListCount.propTypes = {
  spanClass: PropTypes.string,
  iconClass: PropTypes.string,
};

ListCount.defaultProps = {
  spanClass: "mr-4 fs-12 grey-7 d-flex align-center",
  iconClass: "far fa-check-square fs-17 mr-1",
};

export default React.memo(ListCount);
