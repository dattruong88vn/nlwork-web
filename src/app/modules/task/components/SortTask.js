import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import MwDropdown from "app/components/dropdown";

function SortTask({
  handleSort,
  sortValue = [],
  isProject,
  wrapperClass,
  titleClass,
  showSelected,
  activeSort,
  clearUnmount = true,
}) {
  const { t } = useTranslation();
  const [selected, setSelected] = useState();
  const [listSort, setListSort] = useState(sortValue || []);

  useEffect(() => {
    return () => {
      clearUnmount && listSort.forEach((item) => (item.selected = false));
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleSelect = (e, title, id, key) => {
    e.preventDefault();
    const newListSort = [...listSort];
    newListSort.forEach((item) => (item.selected = false));

    if (handleSort && title !== selected) {
      newListSort[id].selected = true;
      setListSort(newListSort);
      setSelected(title);
      handleSort(key);
    } else if (handleSort) {
      newListSort[id].selected = false;
      setListSort(newListSort);
      setSelected("");
      handleSort();
    }
  };

  return (
    <div
      className={`mr-sp-2 ${!isProject ? "sort-box" : "mr-2"} ${wrapperClass}`}
    >
      <MwDropdown
        title={
          <>
            <i className={`fal fa-sort-alt ${showSelected ? "mr-2" : ""}`} />
            {showSelected && (
              <span className="ml-1 soft-value fw-bold">
                {selected ? t(selected) : t("sort")}
              </span>
            )}
          </>
        }
        titleClass={titleClass}
        activeBtnClass={`btn btn-filter btn-h-32 black-3 border-grey-6 d-flex align-center justify-space-between ${
          !!activeSort && "active"
        }`}
        contentClass={`${isProject ? "left" : ""}`}
        titleLink={t("sort")}
      >
        <ul className="nav hover-4 px-0 py-2 border-0">
          {listSort?.length
            ? listSort.map((item) => {
                const { id, key, title, selected } = item;
                return (
                  <li className="nav-item" key={id}>
                    <a
                      href="/"
                      className={`nav-link pl-3 py-2 ${selected && "selected"}`}
                      onClick={(e) => handleSelect(e, title, id, key)}
                    >
                      {t(title)}
                    </a>
                  </li>
                );
              })
            : null}
        </ul>
      </MwDropdown>
    </div>
  );
}

SortTask.propTypes = {
  showSelected: PropTypes.bool,
  handleSort: PropTypes.func,
  sortValue: PropTypes.array,
  isProject: PropTypes.bool,
  wrapperClass: PropTypes.string,
  titleClass: PropTypes.string,
  activeSort: PropTypes.bool,
  clearUnmount: PropTypes.bool,
};

SortTask.defaultProps = {
  showSelected: true,
};
export default SortTask;
