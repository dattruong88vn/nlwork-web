import React from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { usePopper } from "react-popper";

const MoreTag = ({ title, count }) => {
  const [referenceRef, setReferenceRef] = React.useState(null);
  const [popperRef, setPopperRef] = React.useState(null);
  const [visible, setVisible] = React.useState(false);

  const { styles, attributes } = usePopper(referenceRef, popperRef, {
    placement: "top",
    modifiers: [
      {
        name: "offset",
        enabled: true,
        options: {
          offset: [0, 10],
        },
      },
    ],
  });

  return (
    <span
      className="circle-bl"
      ref={setReferenceRef}
      href="/"
      onClick={(e) => e.preventDefault()}
      onMouseEnter={() => setVisible(true)}
      onMouseLeave={() => setVisible(false)}
    >
      {ReactDOM.createPortal(
        visible && (
          <div
            ref={setPopperRef}
            style={{
              ...styles.popper,
              zIndex: 99999,
            }}
            className="tooltip-tags"
            {...attributes.popper}
          >
            {title}
          </div>
        ),
        document.body
      )}
      {count}+
    </span>
  );
};

function TagList({ tags, limit, className, paddingLeft }) {
  let title = "";

  const tagsRender = tags.slice(0, limit);
  const tagSliced = tags.slice(limit, tags.length);
  tagSliced.forEach((element, index) => {
    tagSliced.length - 1 === index
      ? (title += `${element.name}`)
      : (title += `${element.name},`);
  });

  return tags.length ? (
    <div
      className={`tags mt-1 d-flex align-center pl-sp-0 ${className} ${
        paddingLeft ? "pl-tags" : ""
      }`}
    >
      {tagsRender.map((tag, index) => (
        <span
          key={index}
          style={{ background: tag.color }}
          className="tags-overflow"
        >
          {tag.name}
        </span>
      ))}
      {tags.length - limit > 0 && (
        <MoreTag title={title} count={tags.length - limit} />
      )}
    </div>
  ) : (
    <div />
  );
}

TagList.propTypes = {
  tags: PropTypes.array,
  limit: PropTypes.number,
  spanClass: PropTypes.string,
  wrapperClass: PropTypes.string,
};
TagList.defaultProps = {
  tags: [],
  limit: 3,
  spanClass: "tags-overflow",
  wrapperClass: "tags mt-1 d-flex align-center pl-tags pl-sp-0",
};

export default TagList;
