import React, { forwardRef, useState } from "react";
import { useTranslation } from "react-i18next";
import {
  TaskSortOptions,
  TaskFollowingSortOptions,
} from "app/const/SortOptions";
import SearchTask from "app/modules/task/components/SearchTask";
import SortTask from "app/modules/task/components/SortTask";
import useClickOutside from "core/hooks/useClickOutside";
import { MwPanelFilter } from "app/components";

const FilterTask = forwardRef(
  (
    {
      total,
      onFilter,
      isFollowing,
      handleAddTask,
      handleAddGroup,
      dataFilter,
      dataPanel,
      dataGet,
    },
    ref
  ) => {
    const [sortValue, setSortValue] = useState("");
    const [filterValue, setFilterValue] = useState({
      "statuses[]": [],
      "priorities[]": [],
      state: [],
      member: [],
      toDate: "",
      fromDate: "",
    });
    const [keyword, setKeyword] = useState("");

    const handleFilter = (filters) => {
      const resultFilter = {
        ...filters,
        overdue: filters?.state?.includes("overdue") ? 1 : 0,
        no_due_date: filters?.state?.includes("no_due_date") ? 1 : 0,
      };

      setFilterValue(filters);
      onFilter && onFilter({ keyword, sort: sortValue, ...resultFilter });
    };

    const handleSort = (value) => {
      setSortValue(value);
      onFilter && onFilter({ keyword, sort: value, ...filterValue });
    };

    const handleSearch = (keyword) => {
      setKeyword(keyword);
      onFilter && onFilter({ keyword, sort: sortValue, ...filterValue });
    };

    return (
      <>
        <div className="p-2 px-sp-3 py-sp-2 d-flex justify-space-between align-center border-bottom-grey-6 relative bg-sp-grey-2">
          <div className="d-flex align-center">
            <SortTask
              wrapperClass="dp-sp-hide"
              sortValue={
                isFollowing ? TaskFollowingSortOptions : TaskSortOptions
              }
              handleSort={handleSort}
              isProject
              showSelected={false}
              activeSort={!!sortValue}
            />

            {!isFollowing && (
              <AddButton
                onAddTask={handleAddTask}
                onAddGroup={handleAddGroup}
              />
            )}
          </div>

          <div className="d-flex flex-sp-1-0 justify-sp-space-between align-center relative">
            <div className="mr-2">
              <SearchTask handleSearch={handleSearch} keyword={keyword} />
            </div>
            <div className="d-flex align-center">
              <SortTask
                wrapperClass="dp-pc-hide"
                sortValue={
                  isFollowing ? TaskFollowingSortOptions : TaskSortOptions
                }
                handleSort={handleSort}
                showSelected={false}
                activeSort={!!sortValue}
              />

              <MwPanelFilter
                dataPanel={dataPanel}
                dataFilter={dataFilter}
                dataGet={dataGet}
                onFilter={handleFilter}
                filterTime
              />
            </div>
          </div>
        </div>
      </>
    );
  }
);

function AddButton({ onAddTask, onAddGroup }) {
  const { t } = useTranslation();
  const [refAddDropdown, expand, setExpand] = useClickOutside(false);

  const handleExpand = () => setExpand(!expand);

  return (
    <div
      className={`dropdown ${expand && "overflow-unset"}`}
      ref={refAddDropdown}
    >
      <div
        className={`dp-sp-hide btn btn-add-project has-arrow btn-h-32 white fs-14 d-flex align-center justify-space-between ${
          expand && "active"
        }`}
      >
        <div style={{ cursor: "pointer" }} onClick={onAddTask}>
          <i className="fal fa-plus fs-16 mr-2" />
          {t("add_new")}
        </div>
        <span
          onClick={handleExpand}
          className="d-flex align-center justify-center"
          style={{ cursor: "pointer" }}
        >
          <i className="fal fa-angle-down" />
          <i className="fal fa-angle-up" />
        </span>
      </div>
      <div className={`dropdown-content ${expand && "show"}`}>
        <ul className="nav hover-2 px-0 py-2 border-0">
          <li className="nav-item">
            <a href="/" onClick={onAddGroup} className="nav-link pl-3 py-2">
              {t("add_new_group")}
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default FilterTask;
