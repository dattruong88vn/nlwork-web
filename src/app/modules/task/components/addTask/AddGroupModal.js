import React from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import MwModal from "app/components/modal";
import MwButton from "app/components/button";
import MwInput from "app/components/input";
import { postFormData } from "core/utils/APIUtils";
import { CODE_SUCCESS, TASK_GROUP_CREATE } from "app/const/Api";
import { useSelector } from "react-redux";

const AddGroupModal = React.forwardRef(
  ({ handleSubmitAddGroup, projectId }, ref) => {
    const { t } = useTranslation();
    const [loading, setLoading] = React.useState(false);
    const [error, setError] = React.useState("");
    const { my_project_id } = useSelector((state) => state.user.userInfo);
    const refModalAddGroup = React.useRef(null);
    const refFormAddGroup = React.useRef(null);

    const handleAddGroup = () => {
      setLoading(true);
      setError(null);
      const formData = new FormData();
      const name = refFormAddGroup.current["name"].value;

      formData.append("name", name);

      projectId
        ? formData.append("project_id", projectId)
        : formData.append("project_id", my_project_id);

      if (name.trim()) {
        return new Promise((resolve, reject) => {
          postFormData(TASK_GROUP_CREATE, formData).then((res) => {
            if (res.meta.code === CODE_SUCCESS) {
              setLoading(false);
              handleClose();
              resolve(res.data);
            }
          });
        });
      } else {
        setLoading(false);
        refFormAddGroup.current["name"].focus();
        setError(t("don_let_this_field_blank"));
        return new Promise((resolve, reject) => {
          reject(t("don_let_this_field_blank"));
        });
      }
    };

    const handleOpen = () => {
      setError("");
      refModalAddGroup.current.showModal();
    };

    React.useImperativeHandle(ref, () => ({
      handleOpen,
      handleAddGroup,
    }));

    const handleClose = () => {
      refModalAddGroup.current.hideModal();
    };

    const FooterModal = () => {
      return (
        <>
          <MwButton
            title={t("cancel")}
            onClick={handleClose}
            className="btn btn-4 w-87 fs-13 fw-normal fw-normal"
            style={{ background: "transparent" }}
          />
          <MwButton
            title={t("add_new")}
            loading={loading}
            onClick={handleSubmitAddGroup}
            className="btn btn-2 w-87 fs-14 fw-bold center-2"
          />
        </>
      );
    };

    const MobileRightTitle = () => {
      return (
        <div className="form">
          <div className="d-flex btn-modal-dr">
            <MwButton
              title={t("add_new")}
              loading={loading}
              onClick={handleSubmitAddGroup}
              className="btn btn-2 w-87 fs-14 fw-bold center-2"
            />
          </div>
        </div>
      );
    };

    return (
      <MwModal
        id="add-section"
        ref={refModalAddGroup}
        title={t("add_task_group")}
        footer={<FooterModal />}
        rightTitleMobile={<MobileRightTitle />}
        footerClass="d-flex justify-end px-4 pb-4 dp-sp-hide"
      >
        <form ref={refFormAddGroup} className="form-group mb-5">
          <MwInput
            label={t("task_group_name")}
            name="name"
            error={error}
            autoFocus
            placeholder={t("enter_task_group_name")}
            customLabel={{ className: "fs-12 mb-1" }}
          />
        </form>
      </MwModal>
    );
  }
);

AddGroupModal.propTypes = {
  handleSubmitAddGroup: PropTypes.func,
  projectId: PropTypes.string,
};

AddGroupModal.defaultProps = {
  handleSubmitAddGroup: () => {},
};

export default AddGroupModal;
