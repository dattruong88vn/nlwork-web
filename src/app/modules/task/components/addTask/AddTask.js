import React from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";
import MwInput from "app/components/input";
import MwButton from "app/components/button";

const AddTask = React.forwardRef(
  (
    { groupId, projectId, defaultView, onSubmit, loading, closeCallback },
    ref
  ) => {
    const { t } = useTranslation();
    const [value, setValue] = React.useState("");
    const [addTask, setAddTask] = React.useState(false);
    const [error, setError] = React.useState();
    const { status } = useSelector((state) => state.company.settings.task);
    const statusCreate = status.filter((status) => status.type === 1)[0].id;
    const refForm = React.useRef(null);

    React.useEffect(() => {
      setValue("");
      return () => setError();
    }, [addTask]);

    const handleClose = (e) => {
      e && e.preventDefault();
      defaultView === "column" && closeCallback && closeCallback();
      setAddTask(false);
    };

    const handleAddTask = () => {
      setAddTask(true);
    };

    const handleChange = (e) => {
      setValue(e.target.value);
    };

    React.useImperativeHandle(ref, () => ({
      handleAddTask,
    }));

    const handleSubmit = (e) => {
      e.preventDefault();
      setError("");

      const name = refForm.current["name"];
      if (!name.value || !name.value.trim().length) {
        return setError(t("don_let_this_field_blank"));
      }

      const formData = new FormData();
      formData.append("name", name.value);
      formData.append("status", statusCreate);
      if (projectId) {
        formData.append("project_id", projectId);
      }
      formData.append("group_id", groupId);

      if (onSubmit) {
        onSubmit(formData);
        setValue("");
      }
    };

    return (
      <div
        className={`${
          defaultView !== "column" && "px-5 border-bottom-grey-6 pb-4"
        } pt-3 add-job`}
        style={
          defaultView === "column" && !addTask
            ? {
                display: "none",
              }
            : {}
        }
      >
        <form ref={refForm} onSubmit={handleSubmit}>
          <span
            className="blue-1 fs-14 add-job-toggle"
            onClick={handleAddTask}
            style={{
              display: addTask || defaultView === "column" ? "none" : "block",
            }}
          >
            <i className="fal fa-plus fs-16 mr-2" /> {t("add_task")}
          </span>
          <div
            className="add-job-field"
            style={{ display: !addTask ? "none" : "block" }}
          >
            <div className="d-flex align-center">
              <MwInput
                name="name"
                error={error}
                classError="left"
                classWrapper="mr-8"
                className="txt-input h-32 py-0"
                placeholder={t("enter_task_name")}
                value={value}
                onChange={handleChange}
              />

              <a href="/" className="d-flex align-center nowrap fs-13">
                {t("detail")}{" "}
                <i className="fal fal fa-angle-right fs-18 ml-3" />
              </a>
            </div>
            <div
              className={`add-job-btn ${
                defaultView === "column" ? "my-2" : "mt-2"
              }`}
            >
              <MwButton
                title={t("save")}
                type="submit"
                loading={loading}
                className="btn btn-2 mx-sp-1 btn-h-32 w-87 py-1 submit center-2"
              />
              <MwButton
                title={t("cancel")}
                onClick={handleClose}
                className="btn btn-4 btn-h-32 py-1 mr-5 mx-sp-1 close fw-normal"
                style={{ background: "transparent" }}
              />
            </div>
          </div>
        </form>
      </div>
    );
  }
);

AddTask.propTypes = {
  groupId: PropTypes.string,
  onSubmit: PropTypes.func,
};

AddTask.defaultProps = {
  groupId: null,
  onSubmit: null,
};

export default AddTask;
