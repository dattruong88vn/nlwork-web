import React from "react";
import { useTranslation } from "react-i18next";
import AddTaskModal from "app/modules/task/components/addTask/AddTaskModal";
import MwDropDown from "app/components/dropdown";

function AddGroup() {
  const { t } = useTranslation();
  const ref = React.useRef(null);

  const handleAddGroup = (e) => {
    e.preventDefault();
  };

  const handleAddTask = (e) => {
    e.preventDefault();
    ref.current.showModal();
  };

  return (
    <>
      <AddTaskModal ref={ref} />
      <MwDropDown
        title={
          <>
            <i className="fal fa-plus fs-16 mr-2" />
            {t("add_new")}
            <span className="d-flex align-center justify-center">
              <i className="fal fa-angle-down" />
              <i className="fal fa-angle-up" />
            </span>
          </>
        }
        titleClass="dp-pc-hide"
        activeBtnClass="btn btn-add-project has-arrow btn-h-32 white fs-14 fs-sp-12 pl-sp-2 pr-sp-8 d-flex align-center justify-space-between"
      >
        <ul className="nav hover-2 px-0 py-2 border-0">
          <li className="nav-item">
            <a href="/" className="nav-link pl-3 py-2" onClick={handleAddTask}>
              {t("add_task")}
            </a>
          </li>
          <li className="nav-item">
            <a href="/" onClick={handleAddGroup} className="nav-link pl-3 py-2">
              {t("add_new_group")}
            </a>
          </li>
        </ul>
      </MwDropDown>
    </>
  );
}

export default AddGroup;
