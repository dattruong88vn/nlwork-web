import React from "react";
import { useTranslation } from "react-i18next";
import moment from "moment-timezone";
import { useSelector } from "react-redux";
import MwDropDown from "app/components/dropdown";
import MwModal from "app/components/modal";
import MwInput from "app/components/input";
import MwButton from "app/components/button";

const AddTaskModal = React.forwardRef(
  ({ projectId, loading, handleSubmit, view, isQuickAdd }, ref) => {
    const { t } = useTranslation();
    const [error, setError] = React.useState();
    const [startDate, setStartDate] = React.useState(null);
    const [isSlotCalendar, setIsSlotCalendar] = React.useState(false);
    const { status } = useSelector((state) => state.company.settings.task);
    const { my_project_id } = useSelector((state) => state.user.userInfo);
    const statusCreate = status.filter((status) => status.type === 1)[0].id;
    const refModal = React.useRef();
    const refForm = React.useRef(null);

    const hideModal = () => {
      setError({});
      refModal.current.hideModal();
    };

    const showModal = (dataDate, isSlot) => {
      if (dataDate) setStartDate(dataDate);
      setIsSlotCalendar(isSlot);
      refModal.current.showModal();
    };

    React.useImperativeHandle(ref, () => ({
      showModal,
      hideModal,
    }));

    const onSubmit = () => {
      setError({});
      const name = refForm.current["name"];
      const description = refForm.current["description"];

      if (!name.value) {
        name.focus();
        setError((error) => ({
          ...error,
          name: t("don_let_this_field_blank"),
        }));
      }

      if (name.value && !name.value.trim().length)
        return setError((err) => ({
          ...err,
          name: t("don_let_this_field_blank"),
        }));

      if (name.value) {
        const formData = new FormData();
        formData.append("name", name.value);
        formData.append("status", statusCreate);
        formData.append("description", description.value);

        if (startDate) {
          if (isSlotCalendar) {
            formData.append("due_date", startDate);
            formData.append("show_due_time", true);
          } else {
            formData.append("due_date", moment(startDate).unix() * 1000);
            formData.append("show_due_time", false);
          }
        } else {
          formData.append("show_due_time", !!isSlotCalendar);
        }

        projectId
          ? formData.append("project_id", projectId)
          : formData.append("project_id", my_project_id);

        if (view === "calendar" && handleSubmit) {
          formData.append("due_date", moment().unix());
          handleSubmit(formData, moment().unix() * 1000);
        } else {
          handleSubmit &&
            handleSubmit(formData, startDate * 1000, isSlotCalendar);
        }
      }
    };

    const MobileRightTitle = () => {
      return (
        <div className="form">
          <div className="d-flex btn-modal-dr">
            <MwButton
              title={`${t("create&")}`}
              loading={loading}
              className="btn btn-2 p-0 fs-15 fw-bold center-2"
              onClick={onSubmit}
            />
            <MwDropDown
              title={
                <>
                  <i className="fal fa-angle-down" />
                  <i className="fal fa-angle-up" />
                </>
              }
            >
              <ul className="nav hover-2 px-0 py-2 border-0">
                <li className="nav-item">
                  <a
                    href="/"
                    onClick={(e) => e.preventDefault()}
                    className="nav-link pl-3 py-2"
                  >
                    <i className="fas fa-check-square grey-7 mr-1" />{" "}
                    {t("open_job")}
                  </a>
                </li>
              </ul>
            </MwDropDown>
          </div>
        </div>
      );
    };

    const FooterModal = (params) => (
      <>
        <a
          href="/"
          onClick={(e) => e.preventDefault()}
          className="blue-1 d-flex align-center fw-normal lh-1"
        >
          {t("detail")}
          <i className="blue-1 fal fa-angle-right fs-20 ml-2" />
        </a>
        <div className="d-flex form">
          <MwButton
            title={t("cancel")}
            onClick={hideModal}
            className="btn btn-4 w-87 fs-13 fw-normal transparent"
          />
          <div className="d-flex btn-modal-dr">
            <MwButton
              title={`${t("create&")}`}
              loading={loading}
              className="btn btn-2 w-87 fs-14 fw-bold center-2"
              onClick={onSubmit}
              tabIndex="3"
            />
            <MwDropDown
              title={
                <>
                  <i className="fal fa-angle-down" />
                  <i className="fal fa-angle-up" />
                </>
              }
              contentClass="top"
            >
              <ul className="nav hover-2 px-0 py-2 border-0">
                <li className="nav-item">
                  <a
                    href="/"
                    onClick={(e) => e.preventDefault()}
                    className="nav-link pl-3 py-2"
                  >
                    <i className="fas fa-check-square grey-7 mr-1" />{" "}
                    {t("open_job")}
                  </a>
                </li>
              </ul>
            </MwDropDown>
          </div>
        </div>
      </>
    );

    return (
      <MwModal
        ref={refModal}
        id="add-task"
        title={t("add_task")}
        rightTitleMobile={<MobileRightTitle />}
        footer={<FooterModal />}
        onUnmount={() => setError({})}
      >
        <form ref={refForm} onSubmit={(e) => e.preventDefault()}>
          <div className="form-group mb-5">
            <MwInput
              label={t("name_of_task")}
              name="name"
              error={error?.name}
              customLabel={{ className: "fs-12 mb-1" }}
              placeholder={t("enter_name_of_task")}
              autoFocus
              tabIndex="1"
            />
          </div>
          <div className="form-group">
            <MwInput
              isTextarea
              label={t("description_of_task")}
              name="description"
              error={error?.description}
              customLabel={{ className: "fs-12 mb-1" }}
              className="txt-input"
              rows="5"
              placeholder={t("enter_description_of_task")}
              tabIndex="2"
            />
          </div>
        </form>
      </MwModal>
    );
  }
);

export default AddTaskModal;
