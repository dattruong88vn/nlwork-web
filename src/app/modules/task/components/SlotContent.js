import React from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";

function SlotContent({ view }) {
  const { t } = useTranslation();

  return view !== "timeGridWeek" ? (
    <div className="overlay-task-calendar">
      <i className="blue-1 fal fa-plus mr-1" />
      {t("add_task")}
    </div>
  ) : (
    <div className="wrapper-slot-add">
      <div className="slot-add">
        <i className="blue-1 fal fa-plus mr-1" />
        {t("add_task")}
      </div>
      <div className="slot-add">
        <i className="blue-1 fal fa-plus mr-1" />
        {t("add_task")}
      </div>
      <div className="slot-add">
        <i className="blue-1 fal fa-plus mr-1" />
        {t("add_task")}
      </div>
      <div className="slot-add">
        <i className="blue-1 fal fa-plus mr-1" />
        {t("add_task")}
      </div>
      <div className="slot-add">
        <i className="blue-1 fal fa-plus mr-1" />
        {t("add_task")}
      </div>
      <div className="slot-add">
        <i className="blue-1 fal fa-plus mr-1" />
        {t("add_task")}
      </div>
      <div className="slot-add">
        <i className="blue-1 fal fa-plus mr-1" />
        {t("add_task")}
      </div>
    </div>
  );
}

SlotContent.propTypes = { view: PropTypes.string.isRequired };

export default SlotContent;
