import React, { useState, useCallback, useEffect } from "react";
import { useTranslation } from "react-i18next";
import LazyLoad from "react-lazyload";
import TaskGroup from "app/modules/task/components/TaskGroup";
import TaskItem from "app/modules/task/components/taskItem/TaskItem";
import EmptyList from "app/components/emptyList";
import ListLoading from "app/components/loading/ListLoading";

function ListTask({
  handleAddTotal,
  listTask,
  members,
  panelClass,
  defaultView,
  mode,
  isFollowing,
  projectId,
  loading,
  isProject,
  handleComplete,
  handleDeleteGroup,
}) {
  const { t } = useTranslation();
  const [tasks, setTasks] = useState(listTask);

  useEffect(() => {
    setTasks(listTask);
  }, [listTask]);

  const onDelete = (id) => {
    handleDeleteGroup && handleDeleteGroup(id, !isProject);
  };

  const LazyLoadGroup = useCallback(
    ({ ...rest }) => {
      return (
        <LazyLoad once height={70} debounce={100} offset={[200, 0]}>
          <TaskGroup
            panelClass={panelClass}
            defaultView={defaultView}
            hideAddOption={isFollowing}
            mode={mode}
            {...rest}
          />
        </LazyLoad>
      );
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [defaultView]
  );

  const LazyLoadItem = useCallback(
    ({ ...rest }) => {
      return (
        <LazyLoad once height={70} debounce={100} offset={[200, 0]}>
          <TaskItem view={defaultView} {...rest} />
        </LazyLoad>
      );
    },
    [defaultView]
  );

  const ColumnItem = useCallback(
    ({ isProject, projectId, handleComplete, isFirst, ...task }) => (
      <div
        className={`task-column sub-column px-4 ${isFirst ? "isFirst" : ""} `}
      >
        <LazyLoadItem
          key={task.id}
          handleComplete={handleComplete}
          projectId={projectId}
          isProject={isProject}
          {...task}
        />
      </div>
    ),
    []
  );

  const ViewColumn = useCallback(
    ({
      handleComplete,
      projectId,
      isProject,
      handleAddTotal,
      handleDelete,
      tasks,
    }) => {
      return tasks.map((task, index) =>
        task.TYPE === "group" || task.TYPE === "status" ? (
          <LazyLoadGroup
            key={task.id}
            handleAddTotal={handleAddTotal}
            handleDelete={handleDelete}
            projectId={projectId}
            {...task}
          />
        ) : (
          <ColumnItem
            key={task.id}
            handleComplete={handleComplete}
            projectId={projectId}
            isProject={isProject}
            isFirst={index === 0}
            {...task}
          />
        )
      );
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const DefaultView = useCallback(
    ({
      handleAddTotal,
      handleDelete,
      projectId,
      handleComplete,
      isProject,
      tasks,
    }) => {
      return tasks.map((task, index) =>
        task.TYPE === "group" || task.TYPE === "status" ? (
          <LazyLoadGroup
            key={task.id}
            handleAddTotal={handleAddTotal}
            handleDelete={handleDelete}
            projectId={projectId}
            {...task}
          />
        ) : (
          <LazyLoadItem
            key={task.id}
            handleComplete={handleComplete}
            projectId={projectId}
            isProject={isProject}
            {...task}
          />
        )
      );
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return loading ? (
    <ListLoading />
  ) : tasks?.length ? (
    defaultView !== "column" ? (
      <DefaultView
        handleAddTotal={handleAddTotal}
        handleDelete={onDelete}
        projectId={projectId}
        handleComplete={handleComplete}
        isProject={isProject}
        tasks={tasks}
      />
    ) : (
      <ViewColumn
        handleComplete={handleComplete}
        projectId={projectId}
        isProject={isProject}
        handleAddTotal={handleAddTotal}
        handleDelete={onDelete}
        tasks={tasks}
      />
    )
  ) : (
    <EmptyList
      emptyText={t("current_no_task")}
      descriptionText={t("any_task_add_will_here")}
    />
  );
}

export default ListTask;
