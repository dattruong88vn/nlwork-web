import React from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";

function SearchTask({ handleSearch, keyword, isProject }) {
  const { t } = useTranslation();
  const [toggle, setToggle] = React.useState(false);
  const [value, setValue] = React.useState("");
  const [visible, setVisible] = React.useState(false);

  const handleClick = (e) => {
    e && e.preventDefault();
    setToggle(!toggle);
  };

  const handleChange = (e) => {
    const { value } = e.target;
    setValue(value);
    value ? setVisible(true) : setVisible(false);
  };

  const handleSubmit = (e) => {
    e && e.preventDefault();
    handleSearch && handleSearch(value);
  };

  const handleFocus = (e) => {
    e && e.preventDefault();
    value && setVisible(true);
  };

  const clearSearch = (e) => {
    handleSearch && keyword && handleSearch("");
    setValue("");
    setVisible(false);
  };

  return (
    <>
      <span
        onClick={handleClick}
        className="fs-17 search-sp-toggle dp-pc-hide grey-7"
        style={{ fontSize: 17 }}
      >
        <i className="far fa-search" />
      </span>
      <div
        className={`search ${!toggle && "dp-sp-hide"} ${
          isProject && "py-sp-2 w-100 h-100"
        }`}
      >
        <div className="d-flex align-center">
          <form
            className={`d-flex align-center flex-sp-1-0 ${visible && "focus"}`}
            onSubmit={handleSubmit}
          >
            <i className="far fa-search" />
            <input
              type="text"
              value={value}
              placeholder={t("quick_search")}
              className="txt-input border-grey-6"
              onChange={handleChange}
              onFocus={handleFocus}
            />
            <span
              className="search-close"
              onClick={clearSearch}
              style={{ display: visible ? "block" : "none" }}
            >
              <i className="fal fa-times" onClick={clearSearch} />
            </span>
          </form>
          <a
            href="/"
            onClick={handleClick}
            className="ml-2 blue-1 dp-pc-hide fw-normal"
          >
            {t("cancel")}
          </a>
        </div>
      </div>
    </>
  );
}

SearchTask.propTypes = {
  handleSearch: PropTypes.func.isRequired,
  keyword: PropTypes.string.isRequired,
  isProject: PropTypes.bool,
};

export default SearchTask;
