import React, { useEffect, useState, useRef, useCallback } from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import MwDropDown from "app/components/dropdown";
import MwInput from "app/components/input";
import MwButton from "app/components/button";
import useToast from "core/hooks/useToast";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { editTaskGroupRequest } from "core/redux/actions/taskGroupAction";
import { postFormData, putFormData } from "core/utils/APIUtils";
import { CODE_SUCCESS, CREATE_TASK, UPDATE_TASK_STATUS } from "app/const/Api";
import AddTask from "./addTask/AddTask";
import TaskItem from "./taskItem/TaskItem";
import TaskGroupBoard from "./TaskGroupBoard";
import { usePopper } from "react-popper";
import { isArray } from "lodash";

function TaskGroup({
  name,
  id,
  handleAddTotal,
  handleDelete,
  itemComponent: ItemComponent,
  panelClass,
  defaultView,
  children,
  hideAddOption,
  mode,
  projectId,
}) {
  const { t } = useTranslation();
  const ref = useRef(null);
  const { status } = useSelector((state) => state.company.settings.task);
  const statusDone = status.filter((status) => status.type === 5)[0].id;
  const { addToast, removeToast, removeAllToast } = useToast();
  const [expand, setExpand] = useState(true);
  const [editing, setEditing] = useState(false);
  const [loading, setLoading] = useState(false);
  const [maxHeight, setMaxHeight] = useState(0);
  const [value, setValue] = useState(name);
  const [error, setError] = useState();
  const [temp, setTemp] = useState();
  const [items, setItems] = useState(children || []);
  const [focus, setFocus] = useState(false);

  const [referenceRef, setReferenceRef] = useState(null);
  const [popperRef, setPopperRef] = useState(null);
  const [visible, setVisible] = useState(false);

  const { styles, attributes } = usePopper(referenceRef, popperRef, {
    placement: "top",
    modifiers: [
      {
        name: "offset",
        enabled: true,
        options: {
          offset: [0, 10],
        },
      },
    ],
  });

  useEffect(() => {
    isArray(children) && setItems(children);
  }, [children]);

  const refForm = useRef(null);
  const refHeight = useRef(null);
  const refAddTask = useRef(null);

  const dispatch = useDispatch();

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => {
    setMaxHeight(refHeight?.current?.scrollHeight + 111);
  });

  // Set Temp value for input edit name group
  useEffect(() => {
    if (name) {
      setValue(name);
      setTemp(name);
    }
  }, [name]);

  const handleExpand = (value) => {
    if (defaultView === "column" && !items?.length) return;
    if (value) return setExpand(value);

    if (!focus) setExpand(!expand);
  };

  const handleSubmitEdit = (e) => {
    setError("");
    e.preventDefault();
    const name = refForm.current["name"].value;

    if (value) setTemp(value);
    if (!name || !name.trim().length)
      return setError(t("don_let_this_field_blank"));

    const formData = new FormData();
    formData.append("name", name);
    formData.append("id", id);

    dispatch(editTaskGroupRequest(formData));
    setEditing(false);
  };

  const handleSubmitEditBoard = () => {
    if (value) setTemp(value);
    setError("");

    if (!value) return setError(t("don_let_this_field_blank"));

    const formData = new FormData();
    formData.append("name", value);
    formData.append("id", id);

    dispatch(editTaskGroupRequest(formData));
    setValue(value);
    setEditing(false);
  };

  const handleSubmitAdd = async (data) => {
    setLoading(true);
    const response = await postFormData(CREATE_TASK, data);
    const newTask = response?.data?.task;
    if (response.meta.code === CODE_SUCCESS) {
      setLoading(false);
      if (defaultView === "board") {
        setItems([newTask, ...items]);
        return;
      }
      if (newTask.my_group_id || newTask.group_id === id) {
        setItems([...items, newTask]);
      }
    } else {
      setLoading(false);
      console.error("Error!!, please try again");
    }
  };

  const handleEdit = (e) => {
    setValue(temp);
    setError("");
    if (e) e.preventDefault();

    setEditing(!editing);
  };

  const onDelete = (e) => {
    setError("");
    if (e) e.preventDefault();

    if (!items.length && handleDelete) {
      handleDelete(id);
    } else {
      addToast({
        id: "cant_delete",
        content: t("cant_delete"),
        status: "error",
        placement: "right-top",
        timeout: 3000,
      });
    }
  };

  const handleChange = (e) => {
    const value = e.target.value;
    setValue(value);
    if (value) setTemp(e.target.value);
  };

  const formData = (id, project_id, status_id) => {
    const formData = new FormData();
    formData.append("task_id", id);
    formData.append("project_id", project_id);
    formData.append("status_id", status_id);
    return formData;
  };

  const handleUndo = (id, project_id, status_id) => {
    removeToast(id);
    setItems(ref.current);
    handleApi(formData(id, project_id, status_id), true);
  };

  const handleComplete = (id, project_id, status_id) => {
    ref.current = items;

    removeAllToast();

    setItems((items) => items.filter((task) => task.id !== id));

    addToast({
      id,
      content: t("done_task_success"),
      textAgree: t("undo"),
      placement: "right-bottom",
      onAgree: () => handleUndo(id, project_id, status_id),
      timeout: 5000,
    });

    handleApi(formData(id, project_id, statusDone));
  };

  const handleApi = async (formData, isUndo) => {
    const response = await putFormData(UPDATE_TASK_STATUS, formData);

    if (response.meta.code === CODE_SUCCESS) {
      if (handleAddTotal) {
        isUndo && handleAddTotal("TASKS");
      }
    } else {
      console.error("Error!!, please try again");
    }
  };

  const handleAddTask = () => {
    if (defaultView === "column") {
      refAddTask.current.handleAddTask();
      setExpand(true);
    } else {
      refAddTask.current.handleAddTask();
    }
  };

  const handleCloseColumn = () => {
    !items?.length && setExpand(false);
  };

  const ItemsColumn = useCallback(({ items, defaultView, handleComplete }) => {
    return (
      <div className="task-column border-bottom-grey-6">
        {items?.length ? (
          items.map((item) => (
            <TaskItem
              key={item.id}
              handleComplete={handleComplete}
              view={defaultView}
              {...item}
            />
          ))
        ) : (
          <div />
        )}
      </div>
    );
  }, []);

  const ViewSwitch = () => {
    switch (mode) {
      case "board":
        return (
          <TaskGroupBoard
            expand={expand}
            handleExpand={handleExpand}
            name={name}
            items={items}
            handleEdit={handleEdit}
            handleSubmitAdd={handleSubmitAdd}
            handleComplete={handleComplete}
            defaultView={defaultView}
            groupId={id}
            projectId={projectId}
            loading={loading}
            onDelete={onDelete}
            isUnCategory={id === "uncategoried"}
            handleChange={handleChange}
            value={value}
            handleSubmitEdit={handleSubmitEditBoard}
            editing={editing}
            error={error}
          />
        );

      default:
        return (
          <div className={`accordion task-group ${editing ? "edit" : ""}`}>
            <form ref={refForm} onSubmit={handleSubmitEdit}>
              <div
                className={`accordion-toggle task-group ${
                  expand ? "active" : ""
                } ${defaultView !== "column" && "border-bottom-grey-6"}`}
              >
                <div className={`text-edit ${editing && "pt-4 pb-3 pl-1"}`}>
                  <div className="d-flex align-center">
                    <div
                      className={`text-edit-arrow mr-3 ${
                        !editing && "pt-4 pl-1 pb-3"
                      }`}
                      onClick={() => handleExpand()}
                    >
                      <div
                        className="d-flex align-center"
                        onClick={() => handleExpand()}
                      >
                        <div
                          className="accordion-arrow"
                          style={{ marginLeft: 17 }}
                        >
                          <i className="grey-4 fal fa-angle-down fs-22" />
                          <i className="grey-4 fal fa-angle-up fs-22" />
                        </div>
                      </div>
                    </div>
                    <div
                      className={`text-edit-field pr-4 flex-1-0 mr-2 ${
                        !editing ? "disabled pb-3 pt-4" : "py-0"
                      }`}
                      onClick={() => handleExpand()}
                    >
                      <p className="m-0 fs-15 fw-500 px-2 pb-sp-1 pl-0">
                        <span
                          ref={setReferenceRef}
                          onMouseEnter={() => setVisible(true)}
                          onMouseLeave={() => setVisible(false)}
                        >
                          {value?.length > 80
                            ? `${value?.substring(0, 80)}...`
                            : value}
                          {ReactDOM.createPortal(
                            visible && (
                              <div
                                ref={setPopperRef}
                                style={{
                                  ...styles.popper,
                                  zIndex: 1001,
                                }}
                                className="tooltip-avatar"
                                {...attributes.popper}
                              >
                                {value}
                              </div>
                            ),
                            document.body
                          )}
                        </span>
                        <span className="grey-7 ml-1">
                          {!!items?.length && `(${items?.length})`}
                        </span>
                      </p>
                      <MwInput
                        name="name"
                        className="txt-input fs-15 fw-bold"
                        classError="left"
                        onChange={handleChange}
                        defaultValue={value}
                        error={error}
                        onFocus={() => setFocus(true)}
                        onBlur={() => setFocus(false)}
                      />
                    </div>

                    {defaultView === "column" && !editing && (
                      <p className="m-0 update fs-22 blue-1 nowrap">
                        <i className="fal fa-plus" onClick={handleAddTask} />
                      </p>
                    )}

                    <div
                      className="text-edit-action"
                      style={editing ? { display: "none" } : {}}
                    >
                      <div
                        className={`d-flex align-center ${
                          hideAddOption && "mr-2"
                        }`}
                      >
                        <div
                          className="accordion-arrow"
                          onClick={() => handleExpand()}
                        >
                          <i className="grey-4 fal fa-angle-down fs-22" />
                          <i className="grey-4 fal fa-angle-up fs-22" />
                        </div>
                        {!hideAddOption && (
                          <MwDropDown
                            activeBtnClass="grey-4 fas fa-ellipsis-v pl-2"
                            titleClass="ml-2 mr-4"
                            contentClass="w-sp-100"
                          >
                            <ul className="nav hover-2 px-0 py-2 border-0">
                              <li className="nav-item">
                                <span
                                  className="nav-link pl-3 py-2 none-bor"
                                  onClick={handleEdit}
                                >
                                  {t("edit")}
                                </span>
                              </li>
                              <li className="nav-item">
                                <span
                                  className="nav-link pl-3 py-2 none-bor"
                                  onClick={onDelete}
                                >
                                  {t("delete")}
                                </span>
                              </li>
                            </ul>
                          </MwDropDown>
                        )}
                      </div>
                    </div>
                  </div>
                  <div
                    className="pl-9 pt-2 pb-0 text-edit-btn"
                    style={{ display: editing ? "block" : "none" }}
                  >
                    <MwButton
                      title={t("save")}
                      className="btn btn-2 mx-sp-1 btn-h-32 py-1 submit"
                    />
                    <a
                      href="/"
                      className="btn btn-4 btn-h-32 py-1 mr-5 mx-sp-1 close"
                      onClick={handleEdit}
                    >
                      {t("cancel")}
                    </a>
                  </div>
                </div>
              </div>
            </form>
            <div
              ref={refHeight}
              className={`panel ${defaultView === "column" && "px-4"} ${
                expand ? "show" : ""
              }`}
              style={{
                maxHeight: expand ? maxHeight : 0,
                overflow: expand ? "unset" : "hidden",
              }}
            >
              {defaultView === "column" ? (
                <ItemsColumn
                  items={items}
                  defaultView={defaultView}
                  handleComplete={handleComplete}
                />
              ) : (
                <>
                  {!!items?.length &&
                    items.map((item) => (
                      <TaskItem
                        key={item.id}
                        handleComplete={handleComplete}
                        view={defaultView}
                        {...item}
                      />
                    ))}
                </>
              )}

              {!hideAddOption && (
                <AddTask
                  ref={refAddTask}
                  defaultView={defaultView}
                  groupId={id}
                  projectId={projectId}
                  onSubmit={handleSubmitAdd}
                  loading={loading}
                  closeCallback={handleCloseColumn}
                />
              )}
            </div>
          </div>
        );
    }
  };

  return ViewSwitch({ mode });
}

TaskGroup.propTypes = {
  name: PropTypes.string,
  id: PropTypes.string,
  children: PropTypes.arrayOf(PropTypes.shape({})),
};

TaskGroup.defaultProps = {
  name: null,
  id: null,
  children: [],
};

export default TaskGroup;
