import HomePage from "app/modules/home";
import SignIn from "app/modules/auth/signin";
import SignUpPage from "app/modules/auth/signup";
import ActivateUSerPage from "app/modules/auth/activate";
import ResetPasswordPage from "app/modules/auth/resetPassword";
import MyTaskPage from "app/modules/mytask";
import ProjectList from "app/modules/project/list";
import ProjectDetail from "app/modules/project-detail";
import DeepLink from "app/modules/deeplink";
import TaskDetail from "app/modules/task-detail";

export default {
  SignIn,
  SignUpPage,
  ActivateUSerPage,
  ResetPasswordPage,
  MyTaskPage,
  ProjectList,
  ProjectDetail,
  DeepLink,
  HomePage,
  TaskDetail,
};
