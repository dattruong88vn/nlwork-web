import React, { useState, useRef, useEffect } from "react";
import filter from "lodash/filter";
import { CODE_SUCCESS, LIST_TASK, UPDATE_TASK_STATUS } from "app/const/Api";
import { fetchWithToken, putFormData } from "core/utils/APIUtils";
import { ConvertListTask } from "core/utils";
import FilterTask from "app/modules/task/components/FilterTask";
import ListTask from "app/modules/task/components/ListTask";
import useToast from "core/hooks/useToast";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";

const fields =
  "tags,total_task,total_checklist,total_comment,total_attachment,followers,assignee";

function TasksFollowing({ tabIndex, tabActive, total, setTotal }) {
  const { t } = useTranslation();
  const { addToast, removeToast, removeAllToast } = useToast();
  const [dataPanel, setDataPanel] = useState([]);
  const [loading, setLoading] = useState(false);
  const [tasks, setTasks] = useState(null);
  const { settings } = useSelector((state) => state.company);
  const { project_info } = useSelector((state) => state.user.userInfo);
  const { status } = useSelector((state) => state.company.settings.task);
  const statusDone = status.filter((status) => status.type === 5)[0].id;

  const refTasks = useRef(null);

  useEffect(() => {
    const data = [
      {
        title: t("state"),
        items: [
          { id: "overdue", name: t("over_due"), nameLabel: "overdue" },
          {
            id: "no_due_date",
            name: t("no_over_due"),
            nameLabel: "no_due_date",
          },
        ],
        grid: "grid-2",
        prefixId: "task",
        prefixName: "state",
      },
      {
        title: t("priorities"),
        items: settings?.task?.priority || [],
        grid: "grid-2",
        prefixId: "task",
        prefixName: "priorities",
      },
      {
        title: t("project"),
        items: project_info || [],
        grid: "grid-1",
        prefixId: "task",
        prefixName: "projects",
        limit: 4,
      },
    ];

    setDataPanel(data);
  }, [t, settings, project_info]);

  useEffect(() => {
    const getTaskFollowing = () => {
      setLoading(true);

      fetchWithToken(LIST_TASK, {
        fields,
        following: true,
      })
        .then((res) => {
          if (res.meta.code === CODE_SUCCESS) {
            const sortedList = ConvertListTask(res.data.items || []);
            setTasks(sortedList);
            setTotal(res.data.total_task);
          }
          setLoading(false);
        })
        .catch((err) => {
          console.log(err);
        });
    };

    if (tabIndex === tabActive && !tasks) {
      getTaskFollowing();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tabActive, tabIndex, tasks]);

  const handleFilter = (data) => {
    setLoading(true);
    fetchWithToken(LIST_TASK, { ...data, fields, following: true }).then(
      (res) => {
        if (res.meta.code === CODE_SUCCESS) {
          const sortedList = ConvertListTask(res.data.items || []);
          const count =
            filter(res.data.items, (item) => item.TYPE === "task" && item)
              .length || 0;

          setTasks(sortedList);
          setTotal(count);
        }
        setLoading(false);
      }
    );
  };

  const formData = (id, project_id, status_id) => {
    const formData = new FormData();
    formData.append("task_id", id);
    formData.append("project_id", project_id);
    formData.append("status_id", status_id);
    return formData;
  };

  const handleUndo = (id, project_id, status_id) => {
    removeToast(id);
    setTasks(refTasks.current);

    completeApi(formData(id, project_id, status_id), true);
  };

  const completeApi = async (formData, isUndo) => {
    const response = await putFormData(UPDATE_TASK_STATUS, formData);
    if (response.meta.code === CODE_SUCCESS) {
      isUndo ? setTotal((total) => total + 1) : setTotal((total) => total - 1);
    } else {
      console.error("Error!!, please try again");
    }
  };

  const handleComplete = (id, project_id, status_id) => {
    refTasks.current = tasks;
    removeAllToast();

    addToast({
      id,
      content: t("done_task_success"),
      textAgree: t("undo"),
      placement: "right-bottom",
      onAgree: () => handleUndo(id, project_id, status_id),
      timeout: 5000,
    });

    setTasks((tasks) => tasks.filter((task) => task.id !== id));
    completeApi(formData(id, project_id, statusDone), false);
  };

  return (
    <div className={`tabs-content ${tabIndex === tabActive ? "show" : ""}`}>
      <div className="white-box p-0">
        <FilterTask
          isFollowing
          dataPanel={dataPanel}
          dataGet={[
            { key: "projects[]", type: "array" },
            { key: "priorities[]", type: "array" },
            { key: "state", type: "array" },
            { key: "member", type: "string" },
            { key: "from_time", type: "string" },
            { key: "to_time", type: "string" },
          ]}
          onFilter={handleFilter}
        />
        <ListTask
          isFollowing
          defaultView="following"
          listTask={tasks}
          loading={loading}
          handleComplete={handleComplete}
        />
      </div>
    </div>
  );
}

export default TasksFollowing;
