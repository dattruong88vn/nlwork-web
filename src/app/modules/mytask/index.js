import React, { useState, useRef, useEffect } from "react";
import socketClient from "socket.io-client";
import filter from "lodash/filter";
import moment from "moment-timezone";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { useHistory, useParams } from "react-router-dom";
import { useSiteTitle } from "core/hooks/useSiteTitle";
import { useWindowDimensions } from "core/hooks/useWindowDimension";
import { ConvertListTask } from "core/utils";
import useClickOutside from "core/hooks/useClickOutside";
import useToast from "core/hooks/useToast";
import {
  CODE_SUCCESS,
  CREATE_TASK,
  LIST_TASK,
  TASK_GROUP_DELETE,
  UPDATE_TASK_STATUS,
  TASK_TOTAL_FOLLOWING,
} from "app/const/Api";
import {
  SOCKET_ENDPOINT,
  JOIN_ROOM_MY_TASK,
  EVENT_TASK_ADD,
  EVENT_TASK_UPDATE_STATUS,
  EVENT_TASK_UPDATE_DUE_DATE,
  EVENT_TASK_UPDATE_PRIORITY,
  EVENT_TASK_UPDATE_NAME,
  LEAVE_ROOM_MY_TASK,
  EVENT_TASK_REMOVE,
  EVENT_TASK_COMMENT_ADD,
  EVENT_TASK_UPDATE_CHECKLIST,
  EVENT_TASK_COMMENT_REMOVE,
  SOCKET_OPTIONS,
} from "app/const/SocketIO";
import {
  deleteWithToken,
  fetchWithToken,
  postFormData,
  putFormData,
} from "core/utils/APIUtils";
import {
  AddTaskModal,
  AddGroupModal,
} from "app/modules/task/components/addTask";
import MwTabs from "app/components/tabs";
import TasksList from "./list";
import TasksFollowing from "./following";
import TasksCalendar from "./calendar";
import "./styles/index.scss";

const fields =
  "tags,total_task,total_checklist,total_comment,total_attachment,assignee";

function MyTask() {
  useSiteTitle("my_task");
  const { t } = useTranslation();
  const { view } = useParams();
  const history = useHistory();
  const { addToast, removeToast, removeAllToast } = useToast();
  const [refAddDropdown, expand, setExpand] = useClickOutside(false);
  const defaultActiveTab = sessionStorage.getItem("default-tab-mytask");
  const { width } = useWindowDimensions();
  const [activeTab, handleTab] = useState(defaultActiveTab || view || "list");
  const [loadingCreate, setLoadingCreate] = useState(false);
  const [totalTask, setTotalTask] = useState(null);
  const [totalFollowing, setTotalFollowing] = useState(null);
  const [loading, setLoading] = useState(true);
  const [isLoaded, setIsLoaded] = useState(false);
  const [tasks, setTasks] = useState([]);
  const [dataFilter, setDataFilter] = useState({
    "statuses[]": [],
    "priorities[]": [],
    state: [],
    member: [],
    toDate: "",
    fromDate: "",
  });
  const { status } = useSelector((state) => state.company.settings.task);
  const { timezone: timezoneCompany } = useSelector(
    (state) => state.user.userInfo.company_info
  );
  const { id: userId } = useSelector((state) => state.user.userInfo);
  const [canRemove, setCanRemove] = useState(false);
  const statusDone = status.filter((status) => status.type === 5)[0].id;
  const statusCancel = status.filter((status) => status.type === 6)[0].id;
  const refTask = useRef(null);
  const refTaskGroup = useRef(null);
  const refTasks = useRef(null);

  // Handle realtime for task
  useEffect(() => {
    const handleRealtimeAdd = (task) => {
      const isFiltered = checkTaskWithFilter(task, dataFilter);
      if (isFiltered) {
        task?.my_group_id
          ? setTasks(ConvertListTask([...tasks, task] || []))
          : setTasks((tasks) => [task, ...tasks]);
        setTotalTask((total) => total + 1);
      }
    };

    const handleRealtimeStatus = (data) => {
      const isFiltered = checkTaskWithFilter(data, dataFilter);
      const tasksContain = isTasksContain(tasks, data.id);

      if (isFiltered) {
        tasksContain ? updateTask(data, "status_id") : addTask(data);
      } else {
        if (tasksContain && !canRemove) {
          removeTask(data);
        }
      }
    };

    const handleRealtimeDelete = (data) => {
      const result = [...tasks];

      result.forEach((task, index) => {
        if (data.id === task.id) {
          result.splice(index, 1);
        } else {
          if (task.hasOwnProperty("children")) {
            task["children"] = task.children.filter(
              (item) => item.id !== data.id
            );
          }
        }
      });

      setTasks(result);
    };

    const handleRealtimeComment = (data, operation) => {
      const copyArr = [...tasks];

      copyArr.forEach((item) => {
        if (item.id === data.task.id) {
          operation === "REMOVE"
            ? (item.total_comment -= 1)
            : (item.total_comment += 1);
        } else {
          if (item.hasOwnProperty("children")) {
            item.children.forEach((itemChildren) => {
              if (itemChildren.id === data.task.id) {
                operation === "REMOVE"
                  ? (itemChildren.total_comment -= 1)
                  : (itemChildren.total_comment += 1);
              }
            });
          }
        }
      });

      setTasks(copyArr);
    };

    const updateTask = (data, keyUpdate) => {
      const result = [...tasks];

      result.forEach((item) => {
        if (item.id === data.id) {
          keyUpdate && (item[keyUpdate] = data[keyUpdate]);
        } else {
          if (item.hasOwnProperty("children")) {
            item.children.forEach((itemChildren) => {
              if (itemChildren.id === data.id) {
                keyUpdate && (itemChildren[keyUpdate] = data[keyUpdate]);
              }
            });
          }
        }
      });

      setTasks(result);
    };

    const handleRealtimeDuedate = (data) => {
      const isFiltered = checkTaskWithFilter(data, dataFilter);
      const tasksContain = isTasksContain(tasks, data.id);

      if (isFiltered) {
        tasksContain ? updateTask(data, "due_date") : addTask(data);
      } else {
        if (tasksContain) {
          removeTask(data);
        }
      }
    };

    const handleRealtimePriority = (data) => {
      const isFiltered = checkTaskWithFilter(data, dataFilter);
      const tasksContain = isTasksContain(tasks, data.id);

      if (isFiltered) {
        tasksContain ? updateTask(data, "priority_id") : addTask(data);
      } else {
        if (tasksContain) {
          removeTask(data);
        }
      }
    };

    const handleRealtimeUpdateName = (data) => updateTask(data, "name");

    const handleRealtimeChecklist = (data) => {
      updateTask(data, "total_checklist");
    };

    const ws = socketClient(SOCKET_ENDPOINT, SOCKET_OPTIONS);

    ws.emit(JOIN_ROOM_MY_TASK, userId);

    ws.on(EVENT_TASK_ADD, handleRealtimeAdd);
    ws.on(EVENT_TASK_UPDATE_STATUS, handleRealtimeStatus);
    ws.on(EVENT_TASK_UPDATE_DUE_DATE, handleRealtimeDuedate);
    ws.on(EVENT_TASK_UPDATE_PRIORITY, handleRealtimePriority);
    ws.on(EVENT_TASK_UPDATE_NAME, handleRealtimeUpdateName);
    ws.on(EVENT_TASK_REMOVE, handleRealtimeDelete);
    ws.on(EVENT_TASK_UPDATE_CHECKLIST, handleRealtimeChecklist);
    ws.on(EVENT_TASK_COMMENT_ADD, handleRealtimeComment);
    ws.on(EVENT_TASK_COMMENT_REMOVE, (data) => {
      handleRealtimeComment(data, "REMOVE");
    });

    return () => ws.emit(LEAVE_ROOM_MY_TASK, userId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tasks, dataFilter, userId, statusDone, statusCancel]);
  // End handle realtime tasks

  useEffect(() => {
    defaultActiveTab && history.push(`/mytask/${defaultActiveTab}`);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    history.push(`/mytask/${activeTab}`);
    const bodyDiv = document.getElementsByTagName("body")[0];
    activeTab === "list"
      ? (bodyDiv.className = "my-task list-custom")
      : (bodyDiv.className = "my-task");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [activeTab]);

  // remove class dashboard when device in mobile < 767
  useEffect(() => {
    const dashElement = document.getElementById("main-content");
    width < 767 && dashElement
      ? dashElement.classList.remove("dashboard")
      : dashElement.classList.add("dashboard");
  }, [width]);

  useEffect(() => {
    const getTasks = () => {
      setLoading(true);

      let arrFetch;
      if (activeTab === "list") {
        arrFetch = [
          fetchWithToken(LIST_TASK, {
            fields,
          }),
          fetchWithToken(TASK_TOTAL_FOLLOWING),
        ];
      } else {
        arrFetch = [null, fetchWithToken(TASK_TOTAL_FOLLOWING)];
      }

      Promise.all(arrFetch)
        .then((res) => {
          if (res[0]?.meta?.code === CODE_SUCCESS) {
            const sortedList = ConvertListTask(res[0]?.data?.items || []);
            setTasks(sortedList);
          }
          if (res[1]?.meta?.code === CODE_SUCCESS) {
            setTotalTask(res[1]?.data?.assigned);
            setTotalFollowing(res[1]?.data?.following);
          }
          setLoading(false);
        })
        .catch((err) => {
          setLoading(false);
          console.log(err);
        });
    };

    !isLoaded && getTasks();
    activeTab === "list" && setIsLoaded(true);
  }, [activeTab, tasks, isLoaded]);

  const checkTaskWithFilter = (data, filterData) => {
    let taskStatus = data.status_id;
    let taskDueDate = data.due_date;
    let { keyword } = filterData;
    let checkFilterStatus = false;
    let checkFilterState = false;
    let checkFilterPriority = false;
    let checkFilterProject = false;
    let checkFilterToDate = false;
    let checkFilterFromDate = false;
    let taskDueDateTime = taskDueDate
      ? moment.tz(+taskDueDate, timezoneCompany).unix()
      : 0;

    let timeRightNow = moment.tz(moment(), timezoneCompany).unix();
    if (keyword && data.name.search(keyword) < 0) {
      return false;
    }
    if (filterData != null) {
      if (filterData["statuses[]"]?.length) {
        filterData["statuses[]"].forEach((item) => {
          if (taskStatus === item) {
            checkFilterStatus = true;
          }
        });
      } else {
        if (taskStatus !== statusDone && taskStatus !== statusCancel) {
          checkFilterStatus = true;
        }
      }

      if (filterData["state"]?.length) {
        filterData["state"].forEach((item) => {
          if (
            item === "overdue" &&
            timeRightNow >= taskDueDate &&
            taskDueDate != null &&
            taskStatus !== statusDone
          ) {
            checkFilterState = true;
          }
          if (item === "no_due_date" && taskDueDate == null) {
            checkFilterState = true;
          }
        });
      } else {
        checkFilterState = true;
      }

      if (filterData["priorities[]"]?.length) {
        let priorityId = data?.priority_id;
        if (priorityId) {
          checkFilterPriority = filterData["priorities[]"]?.includes(
            priorityId
          );
        }
      } else {
        checkFilterPriority = true;
      }

      if (filterData["projects[]"]?.length) {
        let projectId = data?.project_id;
        if (projectId) {
          checkFilterProject = filterData["projects[]"]?.includes(projectId);
        }
      } else {
        checkFilterProject = true;
      }

      if (filterData["to_time"]) {
        let toDateTime = moment
          .tz(filterData["to_time"], timezoneCompany)
          .unix();

        if (
          taskDueDateTime !== 0 &&
          parseInt(toDateTime) >= parseInt(taskDueDateTime)
        ) {
          checkFilterToDate = true;
        }
      } else {
        checkFilterToDate = true;
      }

      if (filterData["from_time"]) {
        let fromDateTime = moment
          .tz(filterData["from_time"], timezoneCompany)
          .unix();

        if (
          taskDueDateTime !== 0 &&
          parseInt(fromDateTime) <= parseInt(taskDueDateTime)
        ) {
          checkFilterFromDate = true;
        }
      } else {
        checkFilterFromDate = true;
      }

      return (
        checkFilterStatus &&
        checkFilterPriority &&
        checkFilterState &&
        checkFilterToDate &&
        checkFilterFromDate &&
        checkFilterProject
      );
    } else {
      if (taskStatus !== statusDone && taskStatus !== statusCancel) {
        return true;
      }
      return false;
    }
  };

  const formData = (id, project_id, status_id) => {
    const formData = new FormData();
    formData.append("task_id", id);
    formData.append("project_id", project_id);
    formData.append("status_id", status_id);
    return formData;
  };

  const handleUndo = (id, project_id, status_id) => {
    removeToast(id);
    setTasks(refTasks.current);
    setTotalTask((total) => total + 1);
    setCanRemove(true);
    completeApi(formData(id, project_id, status_id), true);
  };

  const handleComplete = (id, project_id, status_id) => {
    const isRemove = dataFilter["statuses[]"]?.includes(statusDone);
    refTasks.current = tasks;
    removeAllToast();

    addToast({
      id,
      content: t("done_task_success"),
      textAgree: t("undo"),
      placement: "right-bottom",
      onAgree: () => handleUndo(id, project_id, status_id),
      timeout: 5000,
    });

    !isRemove && removeTask({ id });
    completeApi(formData(id, project_id, statusDone));
  };

  const handleDeleteGroup = (id, is_my_group) => {
    deleteWithToken(TASK_GROUP_DELETE, { id, is_my_group }).then((res) => {
      res.meta.code === CODE_SUCCESS && removeTask({ id });
    });
  };

  const completeApi = async (formData, isUndo) => {
    try {
      await putFormData(UPDATE_TASK_STATUS, formData);
    } catch (error) {
      console.error(error);
    }
  };

  const handleSubmitAddGroup = () => {
    refTaskGroup.current
      .handleAddGroup()
      .then((res) => {
        setTasks((listTask) => [res, ...listTask]);
        addToast({
          id: "add-section",
          content: t("add_task_group_successfully"),
        });
      })
      .catch((err) => console.error(err));
  };

  const handleFilter = async (data) => {
    try {
      setLoading(true);
      const result = await fetchWithToken(LIST_TASK, { ...data, fields });
      const sortedList = ConvertListTask(result.data.items || []);
      const count =
        filter(result.data.items, (item) => item.TYPE === "task" && item)
          .length || 0;

      setTasks(sortedList);
      setTotalTask(count);
      setDataFilter(data);
      refTasks.current = sortedList;
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  const ListTab = [
    {
      label: (
        <>
          {t("my_task")}{" "}
          {!!totalTask && <span className="dp-sp-hide">{totalTask}</span>}
        </>
      ),
      value: "list",
    },
    {
      label: (
        <>
          {t("following")}
          {!!totalFollowing && (
            <span className="dp-sp-hide">{totalFollowing}</span>
          )}
        </>
      ),
      value: "following",
    },
    { label: t("calendar"), value: "calendar" },
  ];

  const handleAddTotal = (key, operation) => {
    operation === "MINUS"
      ? setTotalTask((totalTask) => totalTask - 1)
      : setTotalTask((totalTask) => totalTask + 1);
  };

  const handleAddTask = () => refTask.current.showModal();

  const handleAddGroup = (e) => {
    e && e.preventDefault();
    setExpand(false);
    refTaskGroup.current.handleOpen();
  };

  const handleExpand = () => setExpand(!expand);

  const handleSubmitAddTask = async (formData) => {
    try {
      setLoadingCreate(true);
      await postFormData(CREATE_TASK, formData);
      addToast({
        id: "add_task_successfully",
        content: t("add_task_successfully"),
        placement: "right-bottom",
        timeout: 3000,
      });
      setLoadingCreate(false);
      refTask.current.hideModal();
    } catch (error) {
      setLoadingCreate(false);
      refTask.current.hideModal();
    }
  };

  const handleSetTab = (value) => {
    handleTab(value);
    sessionStorage.setItem("default-tab-mytask", value);
  };

  const removeTask = (data) => {
    const result = [...tasks];

    result.forEach((task, index) => {
      if (data.id === task.id) {
        result.splice(index, 1);
      } else {
        if (task.hasOwnProperty("children")) {
          task["children"] = task.children.filter(
            (item) => item.id !== data.id
          );
        }
      }
    });

    setTasks(result);
    setTotalTask((total) => total - 1);
  };

  const addTask = (data) => {
    const hasTask = !!tasks.filter((task) => task.id === data.id).length;
    const result = [...tasks];

    if (!hasTask) {
      setTotalTask((total) => total + 1);

      result.forEach((task, index) => {
        if (task.id === data.group_id) {
          task["children"] = task?.children
            ? [...task.children, data]
            : [...data];
        }
      });

      setTasks(result);
    }
  };

  const isTasksContain = (tasks, id) => {
    let isContain = false;
    const arr = [...tasks];

    arr.forEach((item) => {
      if (item.id === id) {
        isContain = true;
      } else {
        if (item.hasOwnProperty("children")) {
          item.children.forEach((itemChildren) => {
            if (itemChildren.id === id) {
              isContain = true;
            }
          });
        }
      }
    });

    return isContain;
  };

  return (
    <>
      <AddTaskModal
        ref={refTask}
        loading={loadingCreate}
        handleSubmit={handleSubmitAddTask}
      />
      <AddGroupModal
        ref={refTaskGroup}
        handleSubmitAddGroup={handleSubmitAddGroup}
      />
      <div
        className="py-3 px-sp-3 d-flex align-center justify-space-between bg-grey-2"
        style={{ minHeight: 52 }}
      >
        <p className="m-0 fs-18 fs-sp-17 fw-bold">{t("my_task_title")}</p>
        {activeTab === "list" && (
          <div className={`dropdown ${expand && "overflow-unset"}`}>
            <div className="dp-pc-hide btn btn-add-project has-arrow btn-h-32 white fs-14 d-flex align-center justify-space-between">
              <div style={{ cursor: "pointer" }} onClick={handleAddTask}>
                <i className="fal fa-plus fs-16 mr-2" />
                {t("add_new")}
              </div>
              <span
                onClick={handleExpand}
                className="d-flex align-center justify-center"
                style={{ cursor: "pointer" }}
              >
                <i className="fal fa-angle-down" />
                <i className="fal fa-angle-up" />
              </span>
            </div>
            <div className={`dropdown-content ${expand && "show"}`}>
              <ul
                ref={refAddDropdown}
                className="nav hover-2 px-0 py-2 border-0"
              >
                <li className="nav-item">
                  <a
                    href="/"
                    onClick={handleAddGroup}
                    className="nav-link pl-3 py-2"
                  >
                    {t("add_new_group")}
                  </a>
                </li>
              </ul>
            </div>
          </div>
        )}
      </div>
      <MwTabs
        tabs={ListTab}
        activeTab={activeTab}
        handleTab={handleSetTab}
        className="tabs"
      />
      <div className="tabs-body pb-2">
        <TasksList
          tabActive={activeTab}
          tabIndex="list"
          loadingCreate={loadingCreate}
          handleSubmitAddTask={handleSubmitAddTask}
          handleAddTotal={handleAddTotal}
          onFilter={handleFilter}
          handleAddTask={handleAddTask}
          handleAddGroup={handleAddGroup}
          loading={loading}
          listTask={tasks}
          handleComplete={handleComplete}
          handleDeleteGroup={handleDeleteGroup}
          dataFilter={dataFilter}
        />
        <TasksFollowing
          tabActive={activeTab}
          tabIndex="following"
          handleComplete={handleComplete}
          total={totalFollowing}
          setTotal={setTotalFollowing}
        />
        <TasksCalendar type="task" tabActive={activeTab} tabIndex="calendar" />
      </div>
    </>
  );
}

export default MyTask;
