import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import FilterTask from "app/modules/task/components/FilterTask";
import ListTask from "app/modules/task/components/ListTask";
import { useDispatch, useSelector } from "react-redux";
import { taskQuickRemove } from "core/redux/actions/taskAction";
import { useTranslation } from "react-i18next";

function TasksList({
  tabActive,
  tabIndex,
  handleAddTotal,
  listTask,
  loading,
  onFilter,
  handleAddGroup,
  handleAddTask,
  handleComplete,
  handleDeleteGroup,
}) {
  const { t } = useTranslation();
  const [tasks, setTasks] = useState(listTask);
  const [dataPanel, setDataPanel] = useState([]);
  const { settings } = useSelector((state) => state.company);
  const { project_info } = useSelector((state) => state.user.userInfo);
  const quickAddTask = useSelector((state) => state.task.quickAddTask);
  const dispatch = useDispatch();

  useEffect(() => {
    const data = [
      {
        title: t("status"),
        items: settings?.task?.status || [],
        grid: "grid-2",
        prefixId: "task",
        prefixName: "statuses",
      },
      {
        title: t("state"),
        items: [
          { id: "overdue", name: t("over_due"), nameLabel: "overdue" },
          {
            id: "no_due_date",
            name: t("no_over_due"),
            nameLabel: "no_due_date",
          },
        ],
        grid: "grid-2",
        prefixId: "task",
        prefixName: "state",
      },
      {
        title: t("priorities"),
        items: settings?.task?.priority || [],
        grid: "grid-2",
        prefixId: "task",
        prefixName: "priorities",
      },
      {
        title: t("project"),
        items: project_info || [],
        grid: "grid-1",
        prefixId: "task",
        prefixName: "projects",
        limit: 4,
      },
    ];

    setDataPanel(data);
  }, [t, settings, project_info]);

  useEffect(() => {
    setTasks(listTask);
  }, [listTask]);

  useEffect(() => {
    if (quickAddTask) {
      setTasks((tasks) => [quickAddTask, ...(tasks || [])]);
      handleAddTotal();
      dispatch(taskQuickRemove());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [quickAddTask]);

  return (
    <>
      <div className={`tabs-content ${tabIndex === tabActive ? "show" : ""}`}>
        <div className="white-box p-0">
          <FilterTask
            onFilter={onFilter}
            handleAddGroup={handleAddGroup}
            handleAddTask={handleAddTask}
            dataPanel={dataPanel}
            dataGet={[
              { key: "statuses[]", type: "array" },
              { key: "projects[]", type: "array" },
              { key: "priorities[]", type: "array" },
              { key: "state", type: "array" },
              { key: "member", type: "string" },
              { key: "from_time", type: "string" },
              { key: "to_time", type: "string" },
            ]}
          />
          <ListTask
            handleAddTotal={handleAddTotal}
            listTask={tasks}
            loading={loading}
            handleComplete={handleComplete}
            handleDeleteGroup={handleDeleteGroup}
          />
        </div>
      </div>
    </>
  );
}

TasksList.propTypes = {
  tabActive: PropTypes.string,
  tabIndex: PropTypes.string,
  handleAddTotal: PropTypes.func,
};

export default TasksList;
