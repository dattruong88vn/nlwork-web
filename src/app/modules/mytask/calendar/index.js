import React, { useEffect, useState, useRef } from "react";
import socketClient from "socket.io-client";
import filter from "lodash/filter";
import { useTranslation } from "react-i18next";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import moment from "moment-timezone";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import { useWindowDimensions } from "core/hooks/useWindowDimension";
import useToast from "core/hooks/useToast";
import { fetchWithToken, postFormData, putFormData } from "core/utils/APIUtils";
import {
  LIST_TASK_IN_MONTH,
  CODE_SUCCESS,
  CREATE_TASK,
  UPDATE_DUE_DATE,
} from "app/const/Api";
import {
  EVENT_TASK_ADD,
  EVENT_TASK_REMOVE,
  EVENT_TASK_UPDATE_DUE_DATE,
  EVENT_TASK_UPDATE_PRIORITY,
  EVENT_TASK_UPDATE_STATUS,
  JOIN_ROOM_MY_TASK,
  LEAVE_ROOM_MY_TASK,
  SOCKET_ENDPOINT,
  SOCKET_OPTIONS,
} from "app/const/SocketIO";
import {
  SlotContent,
  AddTaskModal,
  TaskItemCalendar,
  MoreLinkCalendar,
} from "app/modules/task/components";
import { AgendaButton, FilterButton } from "./components";
import "./styles/tasksCalendar.scss";

const TasksCalendar = ({
  tabIndex,
  tabActive,
  type,
  projectId,
  handleAddTotal,
  setTotalTask,
}) => {
  const { t } = useTranslation();
  const { addToast } = useToast();
  const { width } = useWindowDimensions();
  const [dataFilter, setDataFilter] = useState({
    "statuses[]": [],
    "priorities[]": [],
    state: [],
    member: [],
    toDate: "",
    fromDate: "",
  });
  const [view, setView] = useState("dayGridMonth");
  const [loading, setLoading] = useState(true);
  const [loadingCreate, setLoadingCreate] = useState(false);
  const [events, setEvents] = useState();
  const [currentMonth, setCurrentMonth] = useState();
  const [calendarApi, setCalendarApi] = useState(null);
  const [valueFilters, setValueFilters] = useState({});
  const refCalendar = useRef(null);
  const refAddTask = useRef(null);
  const { company_info } = useSelector((state) => state.user.userInfo);
  const { status } = useSelector((state) => state.company.settings.task);
  const statusDone = status.filter((status) => status.type === 5)[0].id;
  const statusCancel = status.filter((status) => status.type === 6)[0].id;
  const { id: userId } = useSelector((state) => state.user.userInfo);

  const checkTaskWithFilter = (data, filterData) => {
    let taskStatus = data.status_id;
    let checkFilterStatus = false;
    let checkFilterPriority = false;

    if (filterData != null) {
      if (filterData["statuses[]"]?.length) {
        filterData["statuses[]"].forEach((item) => {
          if (taskStatus === item) {
            checkFilterStatus = true;
          }
        });
      } else {
        if (taskStatus !== statusDone && taskStatus !== statusCancel) {
          checkFilterStatus = true;
        }
      }

      if (filterData["priorities[]"]?.length) {
        let priorityId = data?.priority_id;
        if (priorityId) {
          checkFilterPriority = filterData["priorities[]"]?.includes(
            priorityId
          );
        }
      } else {
        checkFilterPriority = true;
      }

      return checkFilterStatus && checkFilterPriority;
    } else {
      if (taskStatus !== statusDone && taskStatus !== statusCancel) {
        return true;
      }
      return false;
    }
  };

  // Handle realtime calendar
  useEffect(() => {
    const ws = socketClient(SOCKET_ENDPOINT, SOCKET_OPTIONS);

    ws.emit(JOIN_ROOM_MY_TASK, userId);

    const handleRealtimeAdd = (data) => {
      const isFiltered = checkTaskWithFilter(data, dataFilter);
      const eventsContain = filter(events, (event) => event.id === data.id)
        .length;

      !eventsContain && isFiltered && addEvent(data);
    };

    const handleRealtimeStatus = (data) => {
      const isFiltered = checkTaskWithFilter(data, dataFilter);
      const eventsContain = filter(events, (event) => event.id === data.id)
        .length;

      const newData = {
        ...data,
        start: moment
          .tz((+data.due_date - 1800) * 1000, company_info?.timezone)
          .format(),
      };

      if (isFiltered) {
        !!eventsContain ? updateEvent(data, "status_id") : addEvent(newData);
      } else {
        if (!!eventsContain) {
          removeEvent(data);
        }
      }
    };

    const handleRealtimeDuedate = (data) => {
      const isFiltered = checkTaskWithFilter(data, dataFilter);
      const tasksContain = filter(events, (event) => event.id === data.id)
        .length;

      if (isFiltered) {
        if (tasksContain) {
          const newEvent = { ...data };
          newEvent["start"] = moment
            .tz((+data.due_date - 1800) * 1000, company_info?.timezone)
            .format();
          updateEvent(newEvent, "start");
        } else {
          addEvent(data);
        }
      } else {
        if (tasksContain) {
          removeEvent(data);
        }
      }
    };

    const handleRealtimePriority = (data) => {
      const isFiltered = checkTaskWithFilter(data, dataFilter);
      const tasksContain = filter(events, (event) => event.id === data.id)
        .length;

      if (isFiltered) {
        !!tasksContain ? updateEvent(data, "priority_id") : addEvent(data);
      } else {
        if (!!tasksContain) {
          removeEvent(data);
        }
      }
    };

    ws.on(EVENT_TASK_ADD, handleRealtimeAdd);
    ws.on(EVENT_TASK_REMOVE, removeEvent);
    ws.on(EVENT_TASK_UPDATE_STATUS, handleRealtimeStatus);
    ws.on(EVENT_TASK_UPDATE_DUE_DATE, handleRealtimeDuedate);
    ws.on(EVENT_TASK_UPDATE_PRIORITY, handleRealtimePriority);

    return () => ws.emit(LEAVE_ROOM_MY_TASK, userId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [events, dataFilter, userId, view, company_info]);
  // End  Handle realtime calendar

  useEffect(() => {
    if (loading) {
      const table = document.querySelector("table.fc-scrollgrid");
      const loaderElement = document.createElement("div");
      loaderElement.innerHTML = `<div class="loader"></div>`;
      loaderElement.className = "table-loader";
      loaderElement.id = "table-loader";

      table.appendChild(loaderElement);
    } else {
      const loaderTable = document.getElementById("table-loader");
      loaderTable && loaderTable.remove();
    }
  }, [loading]);

  useEffect(() => {
    const calendarApi = refCalendar?.current?.getApi();
    const month = moment(calendarApi?.getDate()).month();
    setCurrentMonth(month);
  }, []);

  useEffect(() => {
    if (width < 992 && refCalendar.current) {
      const calendarApi = refCalendar?.current?.getApi();
      setView("timeGridDay");
      calendarApi.changeView(view);
    }
    setCalendarApi(calendarApi);
  }, [view, width, loading, calendarApi]);

  useEffect(() => {
    const date = moment().format("YYYY/MM/DD");

    if (tabActive === tabIndex && !events) return getEvent(date, valueFilters);

    events && setEvents([...events]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tabActive, tabIndex]);

  const getEvent = (date, filters) => {
    setLoading(true);
    const start = moment
      .tz(moment(date).startOf("month"), company_info?.timezone)
      .unix();
    const end = moment
      .tz(moment(date).endOf("month"), company_info?.timezone)
      .unix();

    fetchWithToken(LIST_TASK_IN_MONTH, {
      start,
      end,
      project_id: projectId,
      pretty: false,
      ...filters,
    })
      .then((res) => handleEvent(res))
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  };

  const handleEvent = (res) => {
    if (res.meta.code === CODE_SUCCESS) {
      const arr = [statusDone, statusCancel];
      const newEvents = res.data.map((el) => ({
        ...el,
        start: moment
          .tz((+el.due_date - 1800) * 1000, company_info?.timezone)
          .format(),
        editable: !arr.includes(el?.status_id),
      }));

      const count =
        filter(res.data, (item) => item.type === "task" && item).length || 0;

      setTotalTask && setTotalTask(count || 0);
      setEvents(newEvents);
    } else {
      setEvents([]);
    }
    setLoading(false);
  };

  const handleFilter = (filters) => {
    const calendarApi = refCalendar?.current?.getApi();
    const date = moment(calendarApi.getDate()).format("YYYY/MM/DD");

    setDataFilter(filters);
    getEvent(date, filters);
    setValueFilters(filters);
  };

  const titleString = (time) => {
    const { date } = time;
    const dateFormatted = moment(date?.marker, "YYYY/MM/DD");
    const month = dateFormatted.format("MMMM");
    const day = dateFormatted.date();

    switch (view) {
      case "dayGridWeek":
      case "dayGridMonth":
        return `${t(month)}, ${date?.year}`;
      case "dayGridDay":
        return `${t("date")} ${t(day)}, ${t(month)}`;
      default:
        return `${t(month)}, ${date?.year}`;
    }
  };

  const handleSetView = (view) => {
    setView(view);
    if (refCalendar.current) {
      const calendarApi = refCalendar?.current?.getApi();
      calendarApi.changeView(view);
    }
  };

  const handlePrev = () => {
    const calendarApi = refCalendar?.current?.getApi();
    calendarApi.prev();
    handleChange();
  };

  const handleNext = () => {
    const calendarApi = refCalendar?.current?.getApi();
    calendarApi.next();
    handleChange();
  };

  const handleToday = () => {
    const calendarApi = refCalendar?.current?.getApi();
    calendarApi.today();
    handleChange();
  };

  const headerString = (time) => {
    const { date } = time;
    const dateFormatted = moment(date.marker, "YYYY/MM/DD");
    const day = dateFormatted.format("dddd");
    if (view === "dayGridWeek") {
      return `${t(day)} - ${dateFormatted.format("MM/DD")}`;
    }
    return t(day);
  };

  const handleChange = () => {
    const calendarApi = refCalendar?.current?.getApi();
    const month = moment(calendarApi.getDate()).month();
    titleString(calendarApi.getDate());
    if (currentMonth !== month) {
      const date = moment(calendarApi.getDate()).format("YYYY/MM/DD");
      setCurrentMonth(month);
      getEvent(date, valueFilters);
    }
  };

  const titlePopover = (time) => {
    const { date } = time;
    const dateFormatted = moment(date.marker, "YYYY/MM/DD");
    const month = dateFormatted.format("MMMM");
    const day = dateFormatted.date();

    return `${t("date")} ${t(day)}, ${t(month)}`;
  };

  const addEvent = (data) => {
    const newEvent = { ...data };
    const arr = [statusDone, statusCancel];

    newEvent["start"] = moment
      .tz((+data.due_date - 1800) * 1000, company_info?.timezone)
      .format();
    newEvent["editable"] = !arr.includes(data["status_id"]);

    setEvents((events) =>
      events?.length ? [newEvent, ...events] : [newEvent]
    );
  };

  const removeEvent = (data) => {
    setEvents((events) => filter(events, (item) => item.id !== data.id));
  };

  const updateEvent = (data, keyUpdate) => {
    const result = [...events];
    const arr = [statusDone, statusCancel];

    result.forEach((item) => {
      if (item.id === data.id && keyUpdate) {
        item[keyUpdate] = data[keyUpdate];
        item["editable"] = !arr.includes(data["status_id"]);
      }
    });

    setEvents(result);
  };

  const customButtons = {
    filterButton: {
      text: <FilterButton handleFilter={handleFilter} />,
    },
    setViewButton: {
      text: <AgendaButton activeView={view} handleSetView={handleSetView} />,
    },
    today: {
      text: t("today"),
      click: handleToday,
    },
    prev: {
      click: handlePrev,
    },
    next: {
      click: handleNext,
    },
  };

  const cellMounted = ({ el }) => {
    let elementBg;
    const addElement = document.createElement("div");
    addElement.innerHTML = `<i class="blue-1 fal fa-plus"></i> ${t(
      "add_task"
    )}`;
    addElement.className = `fc-add-task`;

    for (let index = 0; index < el.children[0].childNodes.length; index++) {
      const element = el.children[0].childNodes[index];
      if (element.className === "fc-daygrid-day-bg") {
        elementBg = element;
      }
    }

    return elementBg?.appendChild(addElement);
  };

  const handleDrop = (info) => {
    const { startStr } = info.event;
    const { project_id } = info.event._def.extendedProps;
    const { publicId } = info.oldEvent._def;

    const due_date = moment.tz(startStr, company_info?.timezone).unix();

    const formData = new FormData();
    formData.append("task_id", publicId);
    formData.append("value", due_date);
    formData.append("project_id", project_id);

    putFormData(UPDATE_DUE_DATE, formData);
  };

  const handleSubmitAddTask = (formData, startDate) => {
    setLoadingCreate(true);

    return new Promise((resolve, reject) => {
      postFormData(CREATE_TASK, formData).then((res) => {
        if (res.meta.code === CODE_SUCCESS) {
          addToast({
            id: "add_task_successfully",
            content: t("add_task_successfully"),
            placement: "right-bottom",
            timeout: 3000,
          });
          handleAddTotal && handleAddTotal();
        } else {
          console.error(res.meta.message);
        }
        setLoadingCreate(false);
        refAddTask.current.hideModal();
        resolve(res);
      });
    });
  };

  const handleDateClick = ({ dateStr }) => {
    let timeStart;
    if (view === "dayGridMonth") {
      timeStart = moment
        .tz(dateStr, company_info?.timezone)
        .endOf("day")
        .unix();
    } else {
      timeStart = moment.tz(dateStr, company_info?.timezone).unix() + 1800;
    }

    refAddTask.current.showModal(timeStart, view !== "dayGridMonth");
  };

  return (
    <div
      className={`tabs-content p-0 mb-3 ${
        tabIndex === tabActive ? "show" : ""
      } ${type === "project" && "project-calendar"}`}
    >
      <div className="white-box p-0">
        <AddTaskModal
          loading={loadingCreate}
          handleSubmit={handleSubmitAddTask}
          projectId={projectId}
          ref={refAddTask}
        />
        <FullCalendar
          ref={refCalendar}
          dayMaxEvents={3}
          timeZone={company_info?.timezone}
          progressiveEventRendering
          contentHeight="auto"
          plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
          titleFormat={titleString}
          initialView={view}
          fixedWeekCount={false}
          customButtons={customButtons}
          headerToolbar={{
            left: "setViewButton today",
            center: "prev title next",
            right: "filterButton",
          }}
          dayHeaderFormat={headerString}
          dayPopoverFormat={titlePopover}
          firstDay={1}
          allDaySlot={false}
          events={events}
          dayCellDidMount={cellMounted}
          eventDrop={handleDrop}
          moreLinkContent={(num) => <MoreLinkCalendar num={num} />}
          eventContent={(e) => <TaskItemCalendar type={type} eventInfo={e} />}
          slotLaneContent={() => <SlotContent view={view} />}
          dateClick={handleDateClick}
          editable
        />
      </div>
    </div>
  );
};

TasksCalendar.propTypes = {
  tabIndex: PropTypes.string,
  tabActive: PropTypes.string,
  type: PropTypes.string,
};

TasksCalendar.default = {
  tabIndex: "calendar",
  tabActive: "list",
  type: "task",
};

export default TasksCalendar;
