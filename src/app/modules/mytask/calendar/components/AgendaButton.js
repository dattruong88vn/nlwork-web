import React from "react";
import PropTypes from "prop-types";
import useClickOutside from "core/hooks/useClickOutside";
import { useTranslation } from "react-i18next";

function AgendaButton({ activeView, handleSetView }) {
  const { t } = useTranslation();
  const [refViews, visible, setVisible] = useClickOutside(false);

  const handleDropdown = (e) => {
    e.preventDefault();
    setVisible(!visible);
  };

  const handleClick = (e, view) => {
    e.preventDefault();
    if (handleSetView) {
      handleSetView(view);
    }
    setVisible(false);
  };

  return (
    <div ref={refViews} className="dropdown task-calendar-dropdown">
      <a
        href="/"
        onClick={handleDropdown}
        className={`dropbtn dp-sp-hide btn btn-add-project has-arrow btn-h-32 white fs-14 d-flex align-center justify-space-between ${
          visible ? "active" : ""
        }`}
      >
        {t(activeView)}
        <span className="d-flex align-center justify-center">
          <i className="fal fa-angle-down" />
          <i className="fal fa-angle-up" />
        </span>
      </a>
      <div className={`dropdown-content ${visible && "show"}`}>
        <ul className="nav hover-2 px-0 py-2 border-0">
          <li className="nav-item">
            <a
              href="/"
              className="nav-link pl-3 py-2"
              style={{ color: "black" }}
              onClick={(e) => handleClick(e, "dayGridMonth")}
            >
              {t("dayGridMonth")}
            </a>
          </li>
          <li className="nav-item">
            <a
              href="/"
              className="nav-link pl-3 py-2"
              style={{ color: "black" }}
              onClick={(e) => handleClick(e, "timeGridWeek")}
            >
              {t("dayGridWeek")}
            </a>
          </li>
          <li className="nav-item">
            <a
              href="/"
              className="nav-link pl-3 py-2"
              style={{ color: "black" }}
              onClick={(e) => handleClick(e, "timeGridDay")}
            >
              {t("dayGridDay")}
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}

AgendaButton.propTypes = {
  activeView: PropTypes.string,
  handleSetView: PropTypes.func,
};

AgendaButton.defaultProps = {
  activeView: "dayGridMonth",
  handleSetView: () => {},
};

export default AgendaButton;
