import MwButton from "app/components/button";
import React from "react";
import { useTranslation } from "react-i18next";
import FilterButton from "./FilterButton";
import AgendaButton from "./AgendaButton";

function HeaderToolbar({
  view,
  handleSetView,
  handlePrev,
  handleNext,
  handleToday,
  handleFilter,
  title,
}) {
  const { t } = useTranslation();

  return (
    <div className="fc-header-toolbar fc-toolbar fc-toolbar-custom fc-toolbar-ltr d-flex justify-space-between">
      <div className="fc-toolbar-chunk">
        <AgendaButton activeView={view} handleSetView={handleSetView} />
        <MwButton
          onClick={handleToday}
          className="button fc-today-button fc-button fc-button-primary button-calendar-today"
          title={t("today")}
        />
      </div>
      <div className="fc-toolbar-chunk d-flex align-center">
        <MwButton
          onClick={handlePrev}
          className="fc-prev-button fc-button fc-button-primary"
          title={<span className="fc-icon fc-icon-chevron-left"></span>}
        />
        <h2 className="fc-toolbar-title mb-0">{title}</h2>
        <MwButton
          onClick={handleNext}
          className="fc-next-button fc-button fc-button-primary"
          title={<span className="fc-icon fc-icon-chevron-right"></span>}
        />
      </div>
      <div className="fc-toolbar-chunk">
        <FilterButton handleFilter={handleFilter} />
      </div>
    </div>
  );
}

export default HeaderToolbar;
