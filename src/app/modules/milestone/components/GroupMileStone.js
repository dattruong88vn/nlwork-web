import React, { useRef, useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import ItemMileStone from "./ItemMileStone";

function GroupMileStone({
  status,
  items,
  isActive,
  handleDelete,
  handleUpdate,
  handleComplete,
  showModalMileStone,
  defaultExpand,
  projectId,
}) {
  const { t } = useTranslation();
  const [height, setHeight] = useState(0);
  const [expand, setExpand] = useState(defaultExpand || false);
  const refPanel = useRef(null);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => {
    setHeight(refPanel.current?.scrollHeight);
  });

  useEffect(() => {
    if (!items?.length) setExpand(false);
  }, [items]);

  const handleExpand = () => setExpand(!expand);

  return (
    <div className="px-4 pb-4 px-sp-0">
      <div className="accordion">
        <div
          className={`pt-4 pl-1 pr-4 pb-3 border-bottom-grey-6 accordion-toggle ${
            expand && "active"
          }`}
          onClick={handleExpand}
        >
          <div className="d-flex align-center justify-sp-space-between">
            <div className="accordion-arrow mr-2 order-sp-2">
              <i className="grey-4 fal fa-angle-down fs-22" />
              <i className="grey-4 fal fa-angle-up fs-22" />
            </div>
            <div className="px-2  order-sp-1">
              <div className="m-0 fs-16 fw-500">
                {t(`milestone_${status}`)}
                <span className="fw-500 fs-16 grey-7 dp-pc-hide">
                  {" "}
                  ({items.length})
                </span>
              </div>
            </div>
          </div>
        </div>
        <div
          className="panel"
          ref={refPanel}
          style={expand ? { maxHeight: height, overflow: "unset" } : {}}
        >
          <div className="table table-milestone">
            {items.map((item, index) => (
              <ItemMileStone
                showModalMileStone={showModalMileStone}
                handleDelete={handleDelete}
                handleComplete={handleComplete}
                projectId={projectId}
                isActive={isActive}
                key={index}
                {...item}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

GroupMileStone.propTypes = {
  status: PropTypes.string,
  items: PropTypes.array,
  isActive: PropTypes.bool,
  handleDelete: PropTypes.func,
  handleUpdate: PropTypes.func,
  handleComplete: PropTypes.func,
  showModalMileStone: PropTypes.func,
};

export default GroupMileStone;
