import React, {
  useRef,
  useState,
  forwardRef,
  useImperativeHandle,
} from "react";
import moment from "moment-timezone";
import { useTranslation } from "react-i18next";
import MwModal from "app/components/modal";
import MwInput from "app/components/input";
import MwButton from "app/components/button";
import useTimestamp from "core/hooks/useTimestamp";
import { useSelector } from "react-redux";

const AddMileStoneModal = forwardRef(
  ({ handleSubmit, loading, handleUpdate, projectId }, ref) => {
    const { t } = useTranslation();
    const [error, setError] = useState("");
    const [statusTitle, setStatusTitle] = useState("");
    const [isUpdate, setIsUpdate] = useState(false);
    const [isOpen, setIsOpen] = useState(false);
    const [defaultData, setDefaultData] = useState({});
    const { timezone } = useSelector(
      (state) => state.user.userInfo.company_info
    );
    const refModal = useRef(null);
    const refForm = useRef(null);

    const { getTime } = useTimestamp();

    const hideModal = () => {
      refModal.current.hideModal();
    };

    const showModal = (data, status) => {
      setError({});
      setStatusTitle(status);

      switch (status) {
        case "update_milestone":
          setIsUpdate(true);
          setIsOpen(false);
          setDefaultData(data);
          break;
        case "open_milestone":
          setIsUpdate(false);
          setIsOpen(true);
          setDefaultData(data);
          break;
        default:
          setIsUpdate(false);
          setIsOpen(false);
          setDefaultData({});
          break;
      }

      refModal.current.showModal();
    };

    useImperativeHandle(ref, () => ({
      showModal,
      hideModal,
    }));

    const onSubmit = () => {
      setError({});
      const formData = new FormData();
      const errors = {};

      const start_time = refForm.current["start_time"].value;
      const end_time = refForm.current["end_time"].value;

      for (let index = 0; index < refForm.current.length; index++) {
        const { required, value, name } = refForm.current[index];
        if (required && !value.trim()) {
          errors[name] = t("don_let_this_field_blank");
        } else {
          formData.append(name, value);
        }
      }

      if (
        getTime(moment(start_time, "DD/MM/YYYY")) >
        getTime(moment(end_time, "DD/MM/YYYY"))
      ) {
        setError({
          end_time: t("end_time_must_greater_than_start_date"),
        });
        return;
      }

      if (!Object.keys(errors).length) {
        formData.append("project_id", projectId);
        formData.append(
          "start_time",
          moment
            .tz(moment(start_time, "DD/MM/YYYY"), timezone)
            .startOf("day")
            .unix()
        );
        formData.append(
          "end_time",
          moment
            .tz(moment(end_time, "DD/MM/YYYY"), timezone)
            .endOf("day")
            .unix()
        );
        isUpdate && formData.append("id", defaultData?.id);

        isUpdate ? handleUpdate(formData) : handleSubmit(formData);
      } else {
        setError(errors);
      }
    };

    const MobileRightTitle = () => {
      return !isOpen ? (
        <div className="form">
          <div className="d-flex btn-modal-dr">
            <MwButton
              title={isUpdate ? `${t("update")}` : `${t("create")}`}
              loading={loading}
              className="btn btn-2 p-0 fs-15 fw-bold center-2"
              onClick={onSubmit}
            />
          </div>
        </div>
      ) : (
        <div />
      );
    };

    const FooterModal = () =>
      !isOpen ? (
        <>
          <MwButton
            title={t("cancel")}
            onClick={hideModal}
            className="btn btn-4 w-87 fs-13 fw-normal transparent"
          />
          <MwButton
            title={isUpdate ? `${t("update")}` : `${t("create")}`}
            onClick={onSubmit}
            loading={loading}
            className="btn btn-2 w-87 fs-14 fw-bold center-2"
          />
        </>
      ) : (
        <div />
      );

    return (
      <MwModal
        ref={refModal}
        id="add-milestone"
        title={t(statusTitle)}
        rightTitleMobile={<MobileRightTitle />}
        footer={<FooterModal />}
        footerClass="modal-footer d-flex justify-end px-4 pb-4 dp-sp-hide"
      >
        <form ref={refForm}>
          <div className="form-group mb-5">
            <MwInput
              label={t("name_milestone")}
              name="name"
              error={error?.name}
              customLabel={{ className: "fs-12 mb-1 required" }}
              placeholder={t("enter_name_milestone")}
              required
              defaultValue={defaultData?.name}
              disabled={isOpen}
            />
          </div>
          <div className="form-group row mx-n3 mb-5">
            <div className="col">
              <MwInput
                isDate
                label={t("start_date")}
                customLabel={{ className: "fs-12 mb-1 required" }}
                placeholder={t("start_date")}
                name="start_time"
                error={error?.start_time}
                required
                date={
                  defaultData?.start_time &&
                  moment.tz(+defaultData?.start_time * 1000, timezone)._d
                }
                disabled={isOpen}
              />
            </div>
            <div className="col">
              <MwInput
                isDate
                label={t("end_date")}
                customLabel={{ className: "fs-12 mb-1 required" }}
                placeholder={t("end_date")}
                name="end_time"
                required
                error={error?.end_time}
                date={
                  defaultData?.end_time &&
                  moment.tz(+defaultData?.end_time * 1000, timezone)._d
                }
                disabled={isOpen}
              />
            </div>
          </div>

          <div className="form-group">
            <MwInput
              isTextarea
              label={t("description")}
              name="description"
              customLabel={{ className: "fs-12 mb-1" }}
              className="txt-input"
              rows="5"
              placeholder={t("enter_description")}
              defaultValue={defaultData?.description}
              disabled={isOpen}
            />
          </div>
        </form>
      </MwModal>
    );
  }
);

export default AddMileStoneModal;
