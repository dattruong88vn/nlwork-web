import React, { useState, useEffect, memo } from "react";
import ReactDOM from "react-dom";
import _ from "lodash";
import PropTypes from "prop-types";
import moment from "moment-timezone";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import { usePopper } from "react-popper";
import { usePermissions, useTimeAgo } from "core/hooks";
import { ProgressCalculate } from "core/utils/ProgressCalculate";
import MwAvatar from "app/components/avatar";
import { COLOR_CIRCLE } from "app/const/Colors";
import { MILESTONE_PERMISSION_DEFAULT } from "app/const/Permissions";
import MwDropDown from "app/components/dropdown";

function ItemMileStone({
  name,
  start_time,
  end_time,
  created_by,
  isActive,
  handleDelete,
  handleComplete,
  showModalMileStone,
  description,
  id,
  total_task_done,
  total_task,
  projectId,
}) {
  const { t } = useTranslation();
  const { getPermission } = usePermissions();
  const timeAgo = useTimeAgo(end_time);
  const [permissionUpdate, setPermissionUpdate] = useState(
    MILESTONE_PERMISSION_DEFAULT
  );
  const [isPast, setIsPast] = useState(false);
  const [colorFinished, setColorFinished] = useState();
  const { timezone } = useSelector(
    (state) => state.user.userInfo.setting_info.setting
  );
  const { status } = useSelector((state) => state.company.settings.task);
  const { members } = useSelector((state) => state.company);
  const [referenceRef, setReferenceRef] = useState(null);
  const [popperRef, setPopperRef] = useState(null);
  const [visible, setVisible] = useState(false);
  const [cssCircle, setCssCircle] = useState(null);

  useEffect(() => {
    const permissionInfo = getPermission(
      { project_id: projectId, created_by },
      "MILESTONE"
    );

    setPermissionUpdate(permissionInfo?.update);
  }, [created_by, getPermission, projectId]);

  useEffect(() => {
    const percent = ProgressCalculate(total_task_done, total_task);

    if (percent < 50) {
      const nextDeg = 90 + 3.6 * percent;
      setCssCircle(
        `linear-gradient(90deg, ${COLOR_CIRCLE} 50%,transparent 50%,transparent),linear-gradient(${nextDeg}deg, ${colorFinished} 50%,  ${COLOR_CIRCLE} 50%,  ${COLOR_CIRCLE})`
      );
    } else {
      const nextDeg = -90 + 3.6 * (percent - 50);
      setCssCircle(
        `linear-gradient(${nextDeg}deg,${colorFinished} 50%,transparent 50%,transparent),linear-gradient(270deg, ${colorFinished} 50%,  ${COLOR_CIRCLE} 50%, ${COLOR_CIRCLE})`
      );
    }
  }, [colorFinished, total_task, total_task_done]);

  useEffect(() => {
    const statusFinish = _.find(status, ["type", 5]);
    setColorFinished(statusFinish.color);
  }, [status]);

  const { styles, attributes } = usePopper(referenceRef, popperRef, {
    placement: "top",
    modifiers: [
      {
        name: "offset",
        enabled: true,
        options: {
          offset: [0, 10],
        },
      },
    ],
  });

  useEffect(() => {
    setIsPast(moment.unix(end_time).isBefore(moment()));
  }, [end_time]);

  const user = _.find(members, ["id", created_by]);

  const convertDate = (date) => {
    return moment.tz(date * 1000, timezone).format("DD-MM-YYYY");
  };

  const onDelete = (e) => {
    if (e) e.preventDefault();
    handleDelete && handleDelete(id, isActive);
  };

  const onComplete = (e) => {
    if (e) e.preventDefault();
    handleComplete && handleComplete(id);
  };

  const onArchive = (e) => {
    if (e) e.preventDefault();
  };

  const onUpdate = (e) => {
    if (e) e.preventDefault();
    showModalMileStone &&
      showModalMileStone(
        { name, id, start_time, end_time, description },
        "update_milestone"
      );
  };

  const onOpen = (e) => {
    e.preventDefault();
    showModalMileStone &&
      showModalMileStone(
        { name, id, start_time, end_time, description },
        "open_milestone"
      );
  };

  const preventDefault = (e) => e && e.preventDefault();

  const dropdownActive = [
    { title: t("edit"), icon: "fal fa-pen mr-1", onClick: onUpdate },
    { title: t("complete"), icon: "fal fa-check mr-1", onClick: onComplete },
    { title: t("delete"), icon: "fal fa-trash-alt mr-1", onClick: onDelete },
  ];

  const dropdownCompleted = [
    { title: t("delete"), icon: "fal fa-trash-alt mr-1", onClick: onDelete },
    { title: t("archive"), icon: "fal fa-save mr-1", onClick: onArchive },
    { title: t("open"), icon: "fal fa-folder-open mr-1", onClick: onOpen },
  ];

  return (
    <>
      <div className="table-row">
        <div className="table-cell pl-0 p-sp-0 w-20">
          <p className="m-0 fs-14 fs-sp-15 fw-500 lineclamp-2">
            <span
              ref={setReferenceRef}
              onMouseEnter={() => setVisible(true)}
              onMouseLeave={() => setVisible(false)}
            >
              {name}
            </span>
          </p>
        </div>
        <div
          className={`table-cell w-15 text-nowrap px-sp-0 py-sp-1 d-sp-flex align-center  fs-sp-14 ${
            isPast && isActive ? "sp-red" : "sp-grey-7"
          }`}
        >
          <i
            className={`fal fa-calendar-alt dp-pc-hide mr-2 fs-18 ${
              isPast && isActive ? "sp-red" : " grey-7"
            }`}
          />
          {convertDate(start_time)}
        </div>
        <div
          className={`table-cell w-15 text-nowrap px-sp-2 pb-sp-1 pt-sp-1 sp-grey-7 fs-sp-14 ${
            isPast && isActive && "sp-red"
          }`}
        >
          {convertDate(end_time)}
          {isActive && isPast && (
            <>
              <br className="dp-sp-hide" />
              <span className="red fs-12 tags-date bg-red-2 sp-mr-1">
                {timeAgo}
              </span>
            </>
          )}
        </div>
        <div className="table-cell w-20 px-sp-0 py-sp-1">
          <div className="d-flex align-center flex-wrap">
            <a
              href="/"
              className="mr-1"
              data-tooltip={`${user?.first_name}
            ${user?.last_name}`}
            >
              <MwAvatar className="img-circle w-24" {...user} />
            </a>
            <span className="fs-13 sp-grey-7">
              {user?.first_name} {user?.last_name}
            </span>
          </div>
        </div>
        <div className="table-cell w-25 px-sp-0 py-sp-1">
          <div className="progress full">
            <div className="d-flex align-center justify-space-between mb-1 dp-sp-hide">
              <p className="m-0 fs-12">
                <span className="black-3">{`${total_task_done}/${total_task}`}</span>{" "}
                <span className="grey-7">{t("task_complete")}</span>
              </p>
              <p className="m-0 fs-12 fw-normal d-flex align-center">
                {ProgressCalculate(total_task_done, total_task)}%{" "}
              </p>
            </div>
            <div
              className={`progress-bar --green per-${ProgressCalculate(
                total_task_done,
                total_task
              )}`}
              style={{
                backgroundImage: cssCircle,
              }}
            >
              <span style={{ background: colorFinished }}>
                {" "}
                {ProgressCalculate(total_task_done, total_task)}%
              </span>
            </div>
            <div className="progress-value">
              <div className="dropdown overflow-hidden w-100 pt-2">
                <a
                  href="/"
                  onClick={(e) => e.preventDefault()}
                  className="dropbtn w-100 btn btn-block btn-filter btn-h-32 black-3 border-grey-6 d-flex align-center justify-space-between"
                >
                  {ProgressCalculate(total_task_done, total_task)}%{" "}
                  <i className="fal fa-angle-down dp-sp-hide" />
                  <i className="fal fa-angle-up dp-sp-hide" />
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="table-cell w-5 pr-0 text-right">
          <MwDropDown activeBtnClass="dropbtn grey-4 fas fa-ellipsis-v pl-2">
            <ul className="nav hover-3 px-0 py-1 border-0">
              {isActive &&
                dropdownActive.map((item, index) => (
                  <li className="nav-item" key={index}>
                    <a
                      href="/"
                      onClick={permissionUpdate ? item.onClick : preventDefault}
                      className={`nav-link py-2 ${
                        !permissionUpdate && "disabled"
                      }`}
                    >
                      <i className={item.icon} /> {item.title}
                    </a>
                  </li>
                ))}

              {!isActive &&
                dropdownCompleted.map((item, index) => (
                  <li className="nav-item" key={index}>
                    <a
                      href="/"
                      onClick={permissionUpdate ? item.onClick : preventDefault}
                      className={`nav-link py-2 ${
                        !permissionUpdate && "disabled"
                      }`}
                    >
                      <i className={item.icon} /> {item.title}
                    </a>
                  </li>
                ))}
            </ul>
          </MwDropDown>
        </div>
      </div>
      {ReactDOM.createPortal(
        visible && (
          <div
            ref={setPopperRef}
            style={{
              ...styles.popper,
              zIndex: 1001,
            }}
            className="tooltip-avatar"
            {...attributes.popper}
          >
            {name}
          </div>
        ),
        document.body
      )}
    </>
  );
}

ItemMileStone.propTypes = {
  name: PropTypes.string,
  start_time: PropTypes.string,
  end_time: PropTypes.string,
  created_by: PropTypes.string,
  isActive: PropTypes.bool,
  handleDelete: PropTypes.func,
  handleComplete: PropTypes.func,
  showModalMileStone: PropTypes.func,
  description: PropTypes.string,
  id: PropTypes.string,
  projectId: PropTypes.string.isRequired,
};

export default memo(ItemMileStone);
