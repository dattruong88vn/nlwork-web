import React from "react";
import MwButton from "app/components/button";
import MwCheckbox from "app/components/checkbox";
import useClickOutside from "core/hooks/useClickOutside";
import { useTranslation } from "react-i18next";

function MilestonePanel({ handleFilter }) {
  const { t } = useTranslation();
  const [ref, expanded, setExpanded] = useClickOutside(false);
  const [totalFilter, setTotalFilter] = React.useState(0);
  const refDataFilter = React.useRef({});

  const handleExpand = () => {
    setExpanded(!expanded);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = {};
    const statuses = [];

    for (let index = 0; index < ref.current.length; index++) {
      const element = ref.current[index];
      if (element.type === "checkbox" && element.checked) {
        statuses.push(element.name);
      }
    }

    data["statuses[]"] = statuses;
    handleFilter && handleFilter(data);
    refDataFilter.current = data;
    handleClose();
  };

  const handleClose = () => {
    setExpanded(false);
    let count = 0;
    for (let index = 0; index < ref.current.length; index++) {
      const element = ref.current[index];
      if (element.type === "checkbox" && element.checked) count += 1;
    }

    setTotalFilter(count);
  };

  return (
    <form
      ref={ref}
      onSubmit={handleSubmit}
      className={`dropdown filter ${expanded ? "overflow-unset" : ""}`}
    >
      <span
        onClick={handleExpand}
        className={`dropbtn btn btn-filter btn-h-32 black-3 border-grey-6 d-flex align-center justify-space-between ${
          expanded ? "active" : ""
        } ${!!totalFilter && "active"}`}
      >
        <i className="fal fa-filter" />
        {!!totalFilter && <span className="filter-num">{totalFilter}</span>}
      </span>
      <div
        className={`dropdown-content sp-full ${
          expanded ? "show" : ""
        } no-hidden`}
      >
        {expanded && (
          <>
            <div className="dropdown-back d-flex align-center white px-4 py-3 dp-pc-hide">
              <i
                className="far fa-arrow-left fw-bold fs-16"
                onClick={handleExpand}
                style={{ cursor: "pointer" }}
              />
              <span className="fs-18 fw-bold ml-4 flex-1-0">{t("filter")}</span>
              <div className="form">
                <div className="d-flex btn-modal-dr">
                  <MwButton
                    title={`${t("save")}`}
                    className="btn btn-2 p-0 fs-15 fw-bold center-2"
                  />
                </div>
              </div>
            </div>
            <div className="px-3 pt-3 pb-0 scroll-sp">
              <div className="pb-3">
                <p className="fw-bold fs-14">{t("state")}</p>
                <div className="grid-2">
                  <div>
                    <MwCheckbox
                      id="milestone-ft5"
                      name="active"
                      isHorizontal
                      customLabel={{ className: "fs-13 fw-normal" }}
                      label={t("milestone_active")}
                      defaultValue={refDataFilter?.current[
                        "statuses[]"
                      ]?.includes("active")}
                    />
                  </div>

                  <div>
                    <MwCheckbox
                      id="milestone-ft6"
                      name="completed"
                      isHorizontal
                      customLabel={{ className: "fs-13 fw-normal" }}
                      label={t("milestone_completed")}
                      defaultValue={refDataFilter?.current[
                        "statuses[]"
                      ]?.includes("completed")}
                    />
                  </div>
                </div>
              </div>
              <MwButton
                title={t("save")}
                className="btn btn-2 btn-h-32 btn-flex mb-3 float-right dp-sp-hide"
              />
            </div>
          </>
        )}
      </div>
    </form>
  );
}

export default MilestonePanel;
