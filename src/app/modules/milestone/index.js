import React, { useState, useRef, useEffect } from "react";
import _ from "lodash";
import PropTypes from "prop-types";
import {
  deleteWithToken,
  fetchWithToken,
  postFormData,
  putFormData,
} from "core/utils/APIUtils";
import {
  CODE_SUCCESS,
  MILESTONE_LIST,
  MILESTONE_CREATE,
  MILESTONE_DELETE,
  MILESTONE_UPDATE,
} from "app/const/Api";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import { usePermissions } from "core/hooks";
import { MilestoneSortOptions } from "app/const/SortOptions";
import TableLoading from "app/components/loading/TableLoading";
import { TopFilter } from "../project-detail/components";
import MileStoneList from "./list";
import AddMileStoneModal from "./components/AddMileStoneModal";
import "./styles/milestone.scss";

function MileStone({ projectId, created_by }) {
  const { t } = useTranslation();
  const { getPermission } = usePermissions();
  const [items, setItems] = useState(null);
  const [permissionCreate, setPermissionCreate] = useState(false);
  const [itemsActive, setItemsActive] = useState(null);
  const [itemsCompleted, setItemsCompleted] = useState(null);
  const [loading, setLoading] = useState(true);
  const [loadingCreate, setLoadingCreate] = useState(false);
  const [dataFilter, setDataFilter] = useState({});
  const [dataPanel, setDataPanel] = useState([]);

  const { settings } = useSelector((state) => state.company);

  const refMilestoneModal = useRef(null);

  useEffect(() => {
    const permissionInfo = getPermission(
      { project_id: projectId, created_by },
      "MILESTONE"
    );

    setPermissionCreate(permissionInfo?.create);
  }, [created_by, getPermission, projectId]);

  useEffect(() => {
    const data = [
      {
        title: t("status"),
        items: [
          {
            id: "active",
            name: t("milestone_active"),
            nameLabel: "milestone_active",
          },
          {
            id: "completed",
            name: t("milestone_completed"),
            nameLabel: "milestone_completed",
          },
        ],
        grid: "grid-2",
        prefixId: "milestone",
        prefixName: "statuses",
        className: "bor-0",
      },
    ];

    setDataPanel(data);
  }, [t, settings]);

  useEffect(() => {
    getMilestones();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [projectId]);

  const getMilestones = (filters) => {
    fetchWithToken(MILESTONE_LIST, { project_id: projectId, ...filters }).then(
      (res) => {
        if (res.meta.code === CODE_SUCCESS) {
          const sorted = _.chain(res.data.items).groupBy("status").value();
          setItems(res.data.items);
          setItemsActive([...(sorted["active"] || [])]);
          setItemsCompleted([...(sorted["completed"] || [])]);
        }
        setLoading(false);
      }
    );
  };

  const showModal = (data, status) => {
    refMilestoneModal.current.showModal(data, status);
  };

  const hideModal = () => {
    setLoadingCreate(false);
    refMilestoneModal.current.hideModal();
  };

  const handleFilter = (filters) => {
    getMilestones(filters);
    setDataFilter(filters);
  };

  const handleSubmitAdd = (data) => {
    setLoadingCreate(true);
    postFormData(MILESTONE_CREATE, data)
      .then((res) => {
        if (res.meta.code === CODE_SUCCESS) {
          let newItems = [...itemsActive, res.data];

          if (dataFilter["sort"] === "alphabet") {
            newItems = newItems.sort(function (a, b) {
              if (a.name < b.name) return -1;
              if (a.name > b.name) return 1;
              return 0;
            });
          }

          setItemsActive(newItems);
        }
        hideModal();
      })
      .catch(() => {
        hideModal();
      });
  };

  const handleDelete = (id, isActive) => {
    deleteWithToken(MILESTONE_DELETE, { project_id: projectId, id })
      .then((res) => {
        if (res.meta.code === CODE_SUCCESS) {
          if (isActive) {
            const newItems = itemsActive.filter(
              (item) => item.id !== res.data.id
            );
            setItemsActive(newItems);
          } else {
            const newItems = itemsCompleted.filter(
              (item) => item.id !== res.data.id
            );
            setItemsCompleted(newItems);
          }
        }
      })
      .catch((err) => console.log(err));
  };

  const updateItems = (item) => {
    if (item.status === "active") {
      const newItems = [...itemsActive];
      const index = _.findIndex(newItems, { id: item.id });
      newItems[index] = item;
      setItemsActive(newItems);
    } else {
      const newItems = [...itemsCompleted];
      const index = _.findIndex(newItems, { id: item.id });
      newItems[index] = item;
      setItemsActive(newItems);
    }
  };

  const handleUpdate = (data) => {
    setLoadingCreate(true);

    putFormData(MILESTONE_UPDATE, data)
      .then((res) => {
        if (res.meta.code === CODE_SUCCESS) {
          updateItems(res.data);
        }
        hideModal();
      })
      .catch((err) => {
        hideModal();
        console.log(err);
      });
  };

  const handleComplete = (id) => {
    const formData = new FormData();
    formData.append("project_id", projectId);
    formData.append("id", id);
    formData.append("status", "completed");

    putFormData(MILESTONE_UPDATE, formData)
      .then((res) => {
        if (res.meta.code === CODE_SUCCESS) {
          let newItemsComplete = [...itemsCompleted, res.data];
          const newItems = itemsActive.filter(
            (item) => item.id !== res.data.id
          );

          if (dataFilter["sort"] === "alphabet") {
            newItemsComplete = newItemsComplete.sort(function (a, b) {
              if (a.name < b.name) return -1;
              if (a.name > b.name) return 1;
              return 0;
            });
          }

          if (dataFilter["sort"] === "newest") {
            newItemsComplete = newItemsComplete.sort(
              (a, b) => +b.created - +a.created
            );
          }

          setItemsActive(newItems);
          setItemsCompleted(newItemsComplete);
        }
      })
      .catch((err) => console.log(err));
  };

  return loading ? (
    <TableLoading />
  ) : (
    <>
      <AddMileStoneModal
        ref={refMilestoneModal}
        handleSubmit={handleSubmitAdd}
        handleUpdate={handleUpdate}
        projectId={projectId}
        loading={loadingCreate}
      />
      <TopFilter
        isFilterTime={false}
        dataPanel={dataPanel}
        onFilter={handleFilter}
        onAddTask={() => showModal({}, "add_milestone")}
        hasAddGroup={false}
        sortOptions={MilestoneSortOptions}
        dataGet={[{ key: "statuses[]", type: "array" }]}
        permissionCreate={permissionCreate}
      />
      <div className="dp-sp-hide">
        <div className="table pt-sp-4">
          <div className="thead dp-sp-hide">
            <div className="table-row fs-12 d-flex">
              <div className="table-head w-20 pl-4">{t("name_milestone")}</div>
              <div className="table-head w-15">{t("start_date")}</div>
              <div className="table-head w-15">{t("end_date")}</div>
              <div className="table-head w-20">{t("person_in_charge")}</div>
              <div className="table-head w-25">{t("task")}</div>
              <div className="table-head w-5"></div>
            </div>
          </div>
        </div>
      </div>

      <MileStoneList
        length={items?.length || 0}
        itemsActive={itemsActive}
        itemsCompleted={itemsCompleted}
        dataFilter={dataFilter}
        handleDelete={handleDelete}
        handleUpdate={handleUpdate}
        handleComplete={handleComplete}
        showModalMileStone={showModal}
        projectId={projectId}
      />
    </>
  );
}

MileStone.propTypes = {
  projectId: PropTypes.string,
};

MileStone.defaultProps = {
  projectId: null,
};

export default MileStone;
