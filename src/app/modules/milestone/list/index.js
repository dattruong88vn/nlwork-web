import React from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import EmptyList from "app/components/emptyList";
import GroupMileStone from "../components/GroupMileStone";

function MileStoneList({
  length,
  itemsActive,
  itemsCompleted,
  handleDelete,
  handleComplete,
  showModalMileStone,
  dataFilter,
  projectId,
}) {
  const { t } = useTranslation();

  return !itemsActive?.length &&
    !itemsCompleted?.length &&
    !dataFilter["statuses[]"]?.length &&
    !length ? (
    <EmptyList emptyText={t("empty_milestone")} />
  ) : (
    <>
      {(!dataFilter["statuses[]"]?.length ||
        dataFilter["statuses[]"]?.includes("active")) && (
        <GroupMileStone
          handleDelete={handleDelete}
          status="active"
          isActive
          items={itemsActive || []}
          handleComplete={handleComplete}
          showModalMileStone={showModalMileStone}
          defaultExpand
          projectId={projectId}
        />
      )}

      {(!dataFilter["statuses[]"]?.length ||
        dataFilter["statuses[]"]?.includes("completed")) && (
        <GroupMileStone
          handleDelete={handleDelete}
          status="completed"
          items={itemsCompleted || []}
          handleComplete={handleComplete}
          showModalMileStone={showModalMileStone}
          projectId={projectId}
        />
      )}
    </>
  );
}
MileStoneList.propTypes = {
  itemsActive: PropTypes.array,
  itemsCompleted: PropTypes.array,
  handleDelete: PropTypes.func,
  handleComplete: PropTypes.func,
  showModalMileStone: PropTypes.func,
};

export default MileStoneList;
