import React, { useRef, forwardRef, useImperativeHandle } from "react";
import useToast from "core/hooks/useToast";
import { useTranslation } from "react-i18next";
import AddGroupModal from "app/modules/task/components/addTask/AddGroupModal";
import AddTaskModal from "app/modules/task/components/addTask/AddTaskModal";

const AddModal = forwardRef(
  ({ projectId, handleSubmitTask, callback, loading, view }, ref) => {
    const { t } = useTranslation();
    const { addToast } = useToast();
    const refTask = useRef(null);
    const refTaskGroup = useRef(null);

    useImperativeHandle(
      ref,
      () => ({
        openAddGroup: () => refTaskGroup.current.handleOpen(),
        openAddTask: () => refTask.current.showModal(),
        closeAddTask: () => refTask.current.hideModal(),
        openAddTaskCalendar: () => refTask.current.showModal(null, true),
      }),
      []
    );

    const handleSubmitAddGroup = () => {
      refTaskGroup.current.handleAddGroup().then((res) => {
        callback(res);
        addToast({
          id: "add-section-project-detail",
          content: t("add_task_group_successfully"),
        });
      });
    };

    return (
      <>
        <AddTaskModal
          handleSubmit={handleSubmitTask}
          view={view}
          ref={refTask}
          projectId={projectId}
          loading={loading}
        />
        <AddGroupModal
          ref={refTaskGroup}
          handleSubmitAddGroup={handleSubmitAddGroup}
          projectId={projectId}
        />
      </>
    );
  }
);

export default AddModal;
