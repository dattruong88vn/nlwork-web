import React from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { useClickOutside } from "core/hooks";

function AddButton({ hasAddGroup, onOpenModal }) {
  const { t } = useTranslation();
  const [refAddDropdown, expand, setExpand] = useClickOutside(false);

  const handleExpand = () => setExpand(!expand);

  const handleOpenModal = (isTask) => {
    onOpenModal && onOpenModal(isTask);
    setExpand(false);
  };

  return (
    <div
      className={`dropdown ${expand && "overflow-unset"}`}
      ref={refAddDropdown}
    >
      <div
        className={`dp-sp-hide btn btn-add-project btn-h-32 white fs-14 d-flex align-center justify-space-between ${
          expand && "active"
        } ${hasAddGroup && "has-arrow"}`}
      >
        <div
          style={{ cursor: "pointer" }}
          onClick={() => handleOpenModal(true)}
        >
          <i className="fal fa-plus fs-16 mr-2" />
          {t("add_new")}
        </div>
        {hasAddGroup && (
          <span
            onClick={handleExpand}
            className="d-flex align-center justify-center"
            style={{ cursor: "pointer" }}
          >
            <i className="fal fa-angle-down" />
            <i className="fal fa-angle-up" />
          </span>
        )}
      </div>
      {hasAddGroup && (
        <div className={`dropdown-content ${expand && "show"}`}>
          <ul className="nav hover-2 px-0 py-2 border-0">
            <li className="nav-item">
              <a
                href="/"
                onClick={(e) => {
                  e.preventDefault();
                  handleOpenModal(false);
                }}
                className="nav-link pl-3 py-2"
              >
                {t("add_new_group")}
              </a>
            </li>
          </ul>
        </div>
      )}
    </div>
  );
}

AddButton.propTypes = {
  hasAddGroup: PropTypes.bool,
  onOpenModal: PropTypes.func,
};

AddButton.defaultProps = {
  hasAddGroup: true,
  onOpenModal: () => {},
};

export default AddButton;
