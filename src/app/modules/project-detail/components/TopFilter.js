import React, { useState } from "react";
import PropTypes from "prop-types";
import SearchTask from "app/modules/task/components/SearchTask";
import SortTask from "app/modules/task/components/SortTask";
import { MwPanelFilter } from "app/components";
import AddButton from "./AddButton";

const TopFilter = ({
  onAddTask,
  onAddGroup,
  onFilter,
  dataPanel,
  dataGet,
  sortOptions,
  isFilterTime = true,
  showSort = true,
  hasAddGroup = true,
  isHidden = false,
  permissionCreate = true,
}) => {
  const [filterValue, setFilterValue] = useState({});
  const [keyword, setKeyword] = useState("");
  const [sortValue, setSortValue] = useState("");

  const handleFilter = (filters) => {
    const resultFilter = {
      ...filters,
      overdue: filters?.state?.includes("overdue") ? 1 : 0,
      no_due_date: filters?.state?.includes("no_due_date") ? 1 : 0,
    };

    setFilterValue(filters);
    onFilter && onFilter({ keyword, sort: sortValue, ...resultFilter });
  };

  const handleSort = (value) => {
    setSortValue(value);
    onFilter({ keyword, sort: value, ...filterValue });
  };

  const handleOpenModal = (isTask) => {
    if (onAddTask && isTask) onAddTask();
    if (onAddGroup && !isTask) onAddGroup();
  };

  const handleSearch = (keyword) => {
    setKeyword(keyword);
    onFilter && onFilter({ keyword, sort: sortValue, ...filterValue });
  };

  return (
    <div
      className={`justify-space-between align-center border-bottom-grey-6 relative p-0 ${
        isHidden ? "d-none" : "d-flex"
      }`}
    >
      <div className="d-flex align-center sp-ml-4">
        {showSort && (
          <SortTask
            wrapperClass="dp-sp-hide"
            sortValue={sortOptions}
            handleSort={handleSort}
            activeSort={!!sortValue}
            showSelected={false}
            isProject
          />
        )}

        {permissionCreate && (
          <AddButton hasAddGroup={hasAddGroup} onOpenModal={handleOpenModal} />
        )}
      </div>

      <div className="d-flex flex-sp-1-0 justify-sp-space-between align-center relative p-2 px-sp-3 py-sp-2 bg-sp-grey-2">
        <div className="mr-2">
          <SearchTask handleSearch={handleSearch} keyword={keyword} />
        </div>
        <div className="d-flex align-center">
          {showSort && (
            <SortTask
              wrapperClass="dp-pc-hide"
              sortValue={sortOptions}
              handleSort={handleSort}
              activeSort={!!sortValue}
              showSelected={false}
            />
          )}

          <MwPanelFilter
            dataPanel={dataPanel}
            onFilter={handleFilter}
            dataGet={dataGet}
            filterTime={isFilterTime}
          />
        </div>
      </div>
    </div>
  );
};

TopFilter.propTypes = {
  onAddTask: PropTypes.func,
  onAddGroup: PropTypes.func,
  onFilter: PropTypes.func,
  dataPanel: PropTypes.array,
  dataGet: PropTypes.array,
  isFilterTime: PropTypes.bool,
  showSort: PropTypes.bool,
  hasAddGroup: PropTypes.bool,
  permissionCreate: PropTypes.bool,
};

export default TopFilter;
