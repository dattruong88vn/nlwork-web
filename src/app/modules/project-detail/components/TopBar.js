import React, { useEffect, useState, useRef } from "react";
import find from "lodash/find";
import MwStarButton from "app/components/button/StarButton";
import MwDropDown from "app/components/dropdown";
import { useTranslation } from "react-i18next";
import useClickOutside from "core/hooks/useClickOutside";
import TableLoading from "app/components/loading/TableLoading";
import { ProgressCalculate } from "core/utils/ProgressCalculate";
import ProjectInfo from "app/modules/project-detail/info";
import { useSelector } from "react-redux";
import BoxSearchDrive from "app/modules/drive/components/BoxSearchDrive";
import { Link } from "react-router-dom";

const defaultViews = [
  { name: "list", iconClass: "far fa-list-alt" },
  { name: "column", iconClass: "far fa-th-list" },
  { name: "board", iconClass: "far fa-columns" },
];

const viewList = ["list", "column", "board", "calendar"];

function TopBar({
  name,
  view,
  featured,
  total_tasks,
  totalTask,
  setFeatured,
  handleSetView,
  handleAddTask,
  handleAddGroup,
  statuses,
  description,
  end_date,
  start_date,
  projectId,
  handleFilterDrive,
  total_task,
  fromLink,
  total_task_done,
  members,
}) {
  const { t } = useTranslation();
  const [ref, expand, setExpand] = useClickOutside(false);
  const [isFeatured, setIsFeatured] = useState(featured || false);
  const [colorFinished, setColorFinished] = useState("");
  const [percentProgress, setPercentProgress] = useState(0);
  const [visible, setVisible] = useState(false);
  const refMilestoneInfo = useRef(null);
  const { status } = useSelector((state) => state.company.settings.task);

  useEffect(() => {
    setVisible(false);
  }, [projectId]);

  useEffect(() => {
    setIsFeatured(featured);
  }, [featured]);

  useEffect(() => {
    if (fromLink) {
      const statusFinish = find(status, ["type", 5]);
      setColorFinished(statusFinish.color);
    } else {
      const statusFinished = find(statuses, ["status_type", 5]);
      setColorFinished(statusFinished?.status_color);
    }
  }, [fromLink, status, statuses]);

  useEffect(() => {
    if (fromLink) {
      const percent = ProgressCalculate(total_task_done, total_task);
      setPercentProgress(percent);
    }
  }, [fromLink, total_task_done, total_task]);

  useEffect(() => {
    if (total_task && statuses?.length && !fromLink) {
      const countFinished = find(statuses, ["status_type", 5]).total_tasks;
      const percent = ProgressCalculate(countFinished, total_task);
      setPercentProgress(percent);
    }
  }, [fromLink, statuses, total_task]);

  const onSetView = (e, view) => {
    e && e.preventDefault();
    handleSetView(e, view);
    setExpand(false);
  };

  const handleFeatured = () => {
    setIsFeatured(!isFeatured);
    setFeatured && setFeatured(!isFeatured);
  };

  const handleExpand = (e) => {
    e.preventDefault();
    setExpand(!expand);
  };

  const showMileStoneInfo = (e) => {
    e && e.preventDefault();
    setVisible(true);
    refMilestoneInfo.current.showModal();
  };

  return (
    <>
      <ProjectInfo
        ref={refMilestoneInfo}
        progress={percentProgress}
        name={name}
        description={description}
        endDate={end_date}
        startDate={start_date}
        totalTasks={total_tasks || total_task}
        statuses={statuses}
        colorFinished={colorFinished}
        projectId={projectId}
        visible={visible}
        fromLink={fromLink}
        members={members}
      />
      <div className="mx-n4 mx-sp-0 d-flex flex-column project-detail-head">
        <div className="row bg-white head-white">
          <div className="bg-white flex-1-0 px-4 px-sp-0 py-1 py-sp-0 d-flex align-center justify-space-between">
            <div className="d-flex align-center dp-sp-hide head-white__progress pr-4">
              <MwStarButton
                classIcon="fs-18 mr-4"
                favorite={isFeatured}
                onClick={handleFeatured}
              />
              {!name ? (
                <TableLoading line />
              ) : (
                <div className="d-flex flex-start">
                  <div className="progress full">
                    <p className="mx-0 mb-1 mt-0 fs-15 fw-500">{name}</p>
                    <div className="d-flex align-center">
                      <div
                        className={`progress-bar --green  flex-1-0 per-${percentProgress}`}
                      >
                        <span style={{ background: colorFinished }}></span>
                      </div>
                      <span className="ml-2 grey-7 fs-12">
                        {" "}
                        {percentProgress}%
                      </span>
                    </div>
                  </div>
                </div>
              )}
            </div>
            <div ref={ref} className="d-flex align-center">
              <ul className="nav-2 d-flex mr-4 mr-sp-0 flex-sp-1-0 d-flex align-center">
                <li className="flex-sp-1-0 dropdown">
                  <a
                    href="/"
                    onClick={handleExpand}
                    className={`dropbtn fs-13 btn-add-work has-arrow  ${
                      (expand || ["list", "column", "board"].includes(view)) &&
                      "active bg-grey-2"
                    }`}
                  >
                    {t("task")} {!!totalTask && <span>{totalTask}</span>}
                    <div className="d-flex align-center justify-center dp-sp-hide">
                      <i
                        className="fal fa-angle-down"
                        style={{ display: expand ? "none" : "block" }}
                      />
                      <i
                        className="fal fa-angle-up"
                        style={{ display: expand ? "block" : "none" }}
                      />
                    </div>
                  </a>
                  <div
                    className={`dropdown-content maxw-200 dropdown-work dp-sp-hide ${
                      expand && "show"
                    }`}
                  >
                    <ul className="nav hover-2 px-0 py-2 border-0">
                      {defaultViews.map((defaultView, index) => (
                        <li className="nav-item mr-0" key={index}>
                          <Link
                            to={`/project-detail/${defaultView.name}/${projectId}`}
                            className={`nav-link px-3 py-2  ${
                              view === defaultView.name ? "active" : "nowrap"
                            }`}
                          >
                            <i
                              className={`mr-2 grey-5 ${defaultView.iconClass}`}
                            />
                            {t(defaultView.name)}
                          </Link>
                        </li>
                      ))}
                    </ul>
                  </div>
                </li>
                <li className="flex-sp-1-0">
                  <Link
                    to={`/project-detail/calendar/${projectId}`}
                    className={`fs-13 ${view === "calendar" && "active"}`}
                  >
                    Calendar
                  </Link>
                </li>
                <li className="flex-sp-1-0">
                  <Link
                    to={`/project-detail/conversation/${projectId}`}
                    className={`fs-13 ${view === "conversation" && "active"}`}
                  >
                    {t("discuss")}
                  </Link>
                </li>
                <li className="flex-sp-1-0">
                  <Link
                    to={`/project-detail/drive/${projectId}`}
                    className={`fs-13 ${view === "drive" && "active"}`}
                  >
                    {t("document")}
                  </Link>
                </li>
                <li className="flex-sp-1-0">
                  <a href="/project-detail/report" className="fs-13">
                    {t("report")}
                  </a>
                </li>
                <li className="flex-sp-1-0">
                  <a
                    href="/milestone"
                    onClick={(e) => {
                      onSetView(e, "milestone");
                    }}
                    className={`fs-13 ${view === "milestone" && "active"}`}
                  >
                    {t("milestone")}
                  </a>
                </li>
              </ul>
              <MwDropDown activeBtnClass="grey-4 fas fa-ellipsis-v">
                <ul className="nav hover-2 px-0 py-2 border-0">
                  <li className="nav-item">
                    <a
                      href="/"
                      onClick={showMileStoneInfo}
                      className="nav-link px-3 py-2"
                    >
                      <i className="fal fa-exclamation-circle mr-3" />
                      {t("project_information")}
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="/" className="nav-link px-3 py-2 nowrap">
                      <i className="far fa-save mr-3" />
                      {t("save_default_impression")}
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="/" className="nav-link px-3 py-2">
                      <i className="fal fa-cog mr-3" />
                      {t("setting_project")}
                    </a>
                  </li>
                </ul>
              </MwDropDown>
            </div>
          </div>
        </div>
        {!viewList.includes(view) && (
          <div
            className={`mx-4 mx-sp-0 py-2 px-sp-3 d-flex align-center justify-space-between bg-grey-2 ${
              view === "drive" && "dp-sp-hide"
            }`}
          >
            <p className="m-0 fs-20 fs-sp-17 fw-bold">{t(view)}</p>
            <a
              href="/"
              onClick={handleAddTask}
              className="dp-pc-hide btn btn-add-project btn-h-32 white fs-14 fs-sp-12 px-sp-2 d-flex align-center justify-space-between"
            >
              <i className="fal fa-plus fs-16 fs-sp-14 mr-2 mr-sp-1" />
              {view === "conversation"
                ? t("create_new_conversation")
                : t("add_new")}
            </a>
          </div>
        )}
        {!viewList.includes(view) &&
          view !== "milestone" &&
          view !== "conversation" && (
            <BoxSearchDrive handleFilter={handleFilterDrive} />
          )}
      </div>
      {viewList.includes(view) && (
        <>
          <div className="dp-pc-hide p-3 bg-sp-grey-2 d-flex align-center justify-space-between">
            <div className="project-name d-flex align-center">
              <MwStarButton
                classIcon="fs-16 mr-2"
                favorite={isFeatured}
                onClick={handleFeatured}
              />
              <p className="m-0 fs-16 fw-500 dp-inline-block">{name}</p>
            </div>

            <MwDropDown
              title={
                <>
                  <i className="fal fa-plus fs-16 mr-2" />
                  {t("add_new")}
                  {viewList.includes(view) && view !== "calendar" && (
                    <span className="d-flex align-center justify-center">
                      <i className="fal fa-angle-down" />
                      <i className="fal fa-angle-up" />
                    </span>
                  )}
                </>
              }
              activeBtnClass={`btn btn-add-project btn-h-32 white fs-14 d-flex align-center justify-space-between ${
                viewList.includes(view) && view !== "calendar" && "has-arrow"
              }`}
              onClick={handleAddTask}
            >
              {viewList.includes(view) && view !== "calendar" && (
                <ul className="nav hover-2 px-0 py-2 border-0">
                  <li className="nav-item">
                    <a
                      href="/"
                      onClick={handleAddTask}
                      className="nav-link pl-3 py-2"
                    >
                      {t("add_task")}
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      href="/"
                      onClick={handleAddGroup}
                      className="nav-link pl-3 py-2"
                    >
                      {t("add_new_group")}
                    </a>
                  </li>
                </ul>
              )}
            </MwDropDown>
          </div>
          <div className="tabs dp-pc-hide">
            <ul className="mb-0 p-0 d-flex justify-space-between align-center w-100">
              {defaultViews.map((defaultView, index) => (
                <li
                  key={index}
                  onClick={(e) => {
                    onSetView(e, defaultView.name);
                  }}
                  className={`tabs-item ${
                    defaultView.name === view && "active"
                  }`}
                >
                  <a href="/">{t(defaultView.name)}</a>
                </li>
              ))}
            </ul>
          </div>
        </>
      )}
    </>
  );
}

export default TopBar;
