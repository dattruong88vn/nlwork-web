import React, { useEffect, useState, useRef } from "react";
import socketClient from "socket.io-client";
import findIndex from "lodash/findIndex";
import filter from "lodash/filter";
import { useTranslation } from "react-i18next";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import moment from "moment-timezone";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import { useWindowDimensions } from "core/hooks/useWindowDimension";
import { usePermissions, useToast } from "core/hooks";
import { fetchWithToken, postFormData, putFormData } from "core/utils/APIUtils";
import {
  CODE_SUCCESS,
  CREATE_TASK,
  PROJECT_LIST_DETAIL_CALENDAR,
  UPDATE_DUE_DATE,
} from "app/const/Api";
import {
  EVENT_TASK_ADD,
  EVENT_TASK_REMOVE,
  EVENT_TASK_UPDATE_ASSIGNEE,
  EVENT_TASK_UPDATE_DUE_DATE,
  EVENT_TASK_UPDATE_PRIORITY,
  EVENT_TASK_UPDATE_STATUS,
  JOIN_ROOM_PROJECT,
  LEAVE_ROOM_PROJECT,
  SOCKET_ENDPOINT,
  SOCKET_OPTIONS,
} from "app/const/SocketIO";
import {
  TaskItemCalendar,
  AddTaskModal,
  SlotContent,
  MoreLinkCalendar,
} from "app/modules/task/components";
import { FilterButton, AgendaButton } from "./components";
import { TopFilter } from "../components";
import "./styles/tasksCalendar.scss";

const ProjectCalendar = ({
  tabIndex,
  tabActive,
  projectId,
  members,
  onFilter,
  onAddGroup,
  onAddTask,
}) => {
  const { t } = useTranslation();
  const { getPermission } = usePermissions();
  const { addToast } = useToast();
  const { width } = useWindowDimensions();
  const [view, setView] = useState("dayGridMonth");
  const [loading, setLoading] = useState(true);
  const [dataFilter, setDataFilter] = useState({});
  const [loadingCreate, setLoadingCreate] = useState(false);
  const [events, setEvents] = useState();
  const [currentMonth, setCurrentMonth] = useState();
  const [calendarApi, setCalendarApi] = useState(null);
  const [valueFilters, setValueFilters] = useState({});
  const [dataPanel, setDataPanel] = useState([]);
  const refCalendar = useRef(null);
  const refAddTask = useRef(null);
  const { company_info } = useSelector((state) => state.user.userInfo);
  const { status } = useSelector((state) => state.company.settings.task);
  const { settings } = useSelector((state) => state.company);
  const statusDone = status.filter((status) => status.type === 5)[0].id;
  const statusCancel = status.filter((status) => status.type === 6)[0].id;

  // Handle realtime calendar
  useEffect(() => {
    const ws = socketClient(SOCKET_ENDPOINT, SOCKET_OPTIONS);

    ws.emit(JOIN_ROOM_PROJECT, projectId);

    const handleRealtimeAdd = (data) => {
      const isFiltered = checkTaskWithFilter(data, dataFilter);
      const eventsContain = filter(events, (event) => event.id === data.id)
        .length;

      !eventsContain && isFiltered && addEvent(data);
    };

    const handleRealtimeStatus = (data) => {
      const isFiltered = checkTaskWithFilter(data, dataFilter);
      const eventsContain = filter(events, (event) => event.id === data.id)
        .length;

      const newData = {
        ...data,
        start: moment
          .tz((+data.due_date - 1800) * 1000, company_info?.timezone)
          .format(),
      };

      if (isFiltered) {
        !!eventsContain ? updateEvent(data, "status_id") : addEvent(newData);
      } else {
        if (!!eventsContain) {
          removeEvent(data);
        }
      }
    };

    const handleRealtimeAssignee = (data) => {
      const isFiltered = checkTaskWithFilter(data, dataFilter);
      const eventsContain = filter(events, (event) => event.id === data.id)
        ?.length;

      if (isFiltered) {
        !!eventsContain ? updateEvent(data, "assignee") : addEvent(data);
      } else {
        if (!!eventsContain) {
          removeEvent(data);
        }
      }
    };

    const handleRealtimeDuedate = (data) => {
      const isFiltered = checkTaskWithFilter(data, dataFilter);
      const tasksContain = filter(events, (event) => event.id === data.id)
        .length;

      if (isFiltered) {
        if (tasksContain) {
          const newEvent = { ...data };
          newEvent["start"] = moment
            .tz((+data.due_date - 1800) * 1000, company_info?.timezone)
            .format();
          updateEvent(newEvent, "start");
        } else {
          addEvent(data);
        }
      } else {
        if (tasksContain) {
          removeEvent(data);
        }
      }
    };

    const handleRealtimePriority = (data) => {
      const isFiltered = checkTaskWithFilter(data, dataFilter);
      const tasksContain = filter(events, (event) => event.id === data.id)
        .length;

      if (isFiltered) {
        !!tasksContain ? updateEvent(data, "priority_id") : addEvent(data);
      } else {
        if (!!tasksContain) {
          removeEvent(data);
        }
      }
    };

    ws.on(EVENT_TASK_ADD, handleRealtimeAdd);
    ws.on(EVENT_TASK_REMOVE, removeEvent);
    ws.on(EVENT_TASK_UPDATE_STATUS, handleRealtimeStatus);
    ws.on(EVENT_TASK_UPDATE_DUE_DATE, handleRealtimeDuedate);
    ws.on(EVENT_TASK_UPDATE_PRIORITY, handleRealtimePriority);
    ws.on(EVENT_TASK_UPDATE_ASSIGNEE, handleRealtimeAssignee);

    return () => ws.emit(LEAVE_ROOM_PROJECT, projectId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [events, dataFilter, projectId, view, company_info]);
  // End  Handle realtime calendar

  useEffect(() => {
    const data = [
      {
        title: t("status"),
        items: settings?.task?.status || [],
        grid: "grid-2",
        prefixId: "project",
        prefixName: "statuses",
      },
      {
        title: t("priorities"),
        items: settings?.task?.priority || [],
        grid: "grid-2",
        prefixId: "project",
        prefixName: "priorities",
      },
      {
        title: t("member"),
        items: members || [],
        grid: "grid-1",
        prefixId: "project",
        prefixName: "assignees",
        limit: 4,
        emptyText: t("don_have_member"),
        keyLabel: ["first_name", "last_name"],
        className: "bor-0",
      },
    ];

    setDataPanel(data);
  }, [t, settings, members]);

  useEffect(() => {
    if (loading) {
      const table = document.querySelector("table.fc-scrollgrid");
      const loaderElement = document.createElement("div");
      loaderElement.innerHTML = `<div class="loader"></div>`;
      loaderElement.className = "table-loader";
      loaderElement.id = "table-loader";

      table.appendChild(loaderElement);
    } else {
      const loaderTable = document.getElementById("table-loader");
      loaderTable && loaderTable.remove();
    }
  }, [loading]);

  useEffect(() => {
    const calendarApi = refCalendar?.current?.getApi();
    const month = moment(calendarApi?.getDate()).month();
    setCurrentMonth(month);
  }, []);

  useEffect(() => {
    if (width < 992 && refCalendar.current) {
      const calendarApi = refCalendar?.current?.getApi();
      setView("timeGridDay");
      calendarApi.changeView(view);
    }
    setCalendarApi(calendarApi);
  }, [view, width, loading, calendarApi]);

  useEffect(() => {
    const date = moment().format("YYYY/MM/DD");

    if (tabActive === tabIndex && !events) {
      return getEvent(date, valueFilters);
    }
    events && setEvents([...events]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tabActive, tabIndex]);

  const getEvent = (date, filters) => {
    setLoading(true);
    const start = moment
      .tz(moment(date).startOf("month"), company_info?.timezone)
      .unix();
    const end = moment
      .tz(moment(date).endOf("month"), company_info?.timezone)
      .unix();

    fetchWithToken(PROJECT_LIST_DETAIL_CALENDAR, {
      start,
      end,
      project_id: projectId,
      pretty: false,
      ...filters,
    })
      .then((res) => {
        if (res.meta.code === CODE_SUCCESS) {
          handleEvent(res.data);
        } else {
          setEvents([]);
        }
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  };

  const handleEvent = (data) => {
    const newEvents = data.map((el) => {
      return {
        ...el,
        start: moment
          .tz((+el.due_date - 1800) * 1000, company_info?.timezone)
          .format(),
        editable: isEditable(el),
      };
    });
    setEvents(newEvents);
  };

  const isEditable = (event) => {
    const { project_id, assignee, created_by } = event;
    const arr = [statusDone, statusCancel];
    const checkPermission = getPermission(
      { project_id, assignee, created_by },
      "TASK_IN_PROJECT"
    )?.update;
    const checkStatus = !arr.includes(event?.status_id);
    return checkPermission && checkStatus;
  };

  const checkTaskWithFilter = (data, filterData) => {
    let taskStatus = data.status_id;
    let checkFilterStatus = false;
    let checkFilterPriority = false;
    let checkFilterAssignee = false;

    if (filterData != null) {
      if (filterData["statuses[]"]?.length) {
        filterData["statuses[]"].forEach((item) => {
          if (taskStatus === item) {
            checkFilterStatus = true;
          }
        });
      } else {
        if (taskStatus !== statusDone && taskStatus !== statusCancel) {
          checkFilterStatus = true;
        }
      }

      if (filterData["assignees[]"]?.length) {
        let assigneeId = data?.assignee;
        if (assigneeId) {
          checkFilterAssignee = filterData["assignees[]"]?.includes(assigneeId);
        }
      } else {
        checkFilterAssignee = true;
      }

      if (filterData["priorities[]"]?.length) {
        let priorityId = data?.priority_id;
        if (priorityId) {
          checkFilterPriority = filterData["priorities[]"]?.includes(
            priorityId
          );
        }
      } else {
        checkFilterPriority = true;
      }

      return checkFilterStatus && checkFilterPriority && checkFilterAssignee;
    } else {
      if (taskStatus !== statusDone && taskStatus !== statusCancel) {
        return true;
      }
      return false;
    }
  };

  const updateEvent = (data, keyUpdate) => {
    const result = [...events];

    result.forEach((item) => {
      if (item.id === data.id && keyUpdate) {
        item[keyUpdate] = data[keyUpdate];
        item["editable"] = isEditable(data);
      }
    });

    setEvents(result);
  };

  const addEvent = (data) => {
    const newEvent = { ...data };

    newEvent["start"] = moment
      .tz((+data.due_date - 1800) * 1000, company_info?.timezone)
      .format();
    newEvent["editable"] = isEditable(data);

    setEvents((events) =>
      events?.length ? [newEvent, ...events] : [newEvent]
    );
  };

  const removeEvent = (data) => {
    setEvents((events) => filter(events, (item) => item.id !== data.id));
  };

  const handleFilter = (filters) => {
    const calendarApi = refCalendar?.current?.getApi();
    const date = moment(calendarApi.getDate()).format("YYYY/MM/DD");

    setDataFilter(filters);
    getEvent(date, filters);
    setValueFilters(filters);
    onFilter(filters, true);
  };

  const titleString = (time) => {
    const { date } = time;
    const dateFormatted = moment(date?.marker, "YYYY/MM/DD");
    const month = dateFormatted.format("MMMM");
    const day = dateFormatted.date();

    switch (view) {
      case "dayGridWeek":
      case "dayGridMonth":
        return `${t(month)}, ${date?.year}`;
      case "dayGridDay":
        return `${t("date")} ${t(day)}, ${t(month)}`;
      default:
        return `${t(month)}, ${date?.year}`;
    }
  };

  const handleSetView = (view) => {
    setView(view);
    if (refCalendar.current) {
      const calendarApi = refCalendar?.current?.getApi();
      calendarApi.changeView(view);
    }
  };

  const handlePrev = () => {
    const calendarApi = refCalendar?.current?.getApi();
    calendarApi.prev();
    handleChange();
  };

  const handleNext = () => {
    const calendarApi = refCalendar?.current?.getApi();
    calendarApi.next();
    handleChange();
  };

  const handleToday = () => {
    const calendarApi = refCalendar?.current?.getApi();
    calendarApi.today();
    handleChange();
  };

  const headerString = (time) => {
    const { date } = time;
    const dateFormatted = moment(date.marker, "YYYY/MM/DD");
    const day = dateFormatted.format("dddd");
    if (view === "dayGridWeek") {
      return `${t(day)} - ${dateFormatted.format("MM/DD")}`;
    }
    return t(day);
  };

  const handleChange = () => {
    const calendarApi = refCalendar?.current?.getApi();
    const month = moment(calendarApi.getDate()).month();
    titleString(calendarApi.getDate());
    if (currentMonth !== month) {
      const date = moment(calendarApi.getDate()).format("YYYY/MM/DD");
      setCurrentMonth(month);
      getEvent(date, valueFilters);
    }
  };

  const titlePopover = (time) => {
    const { date } = time;
    const dateFormatted = moment(date.marker, "YYYY/MM/DD");
    const month = dateFormatted.format("MMMM");
    const day = dateFormatted.date();

    return `${t("date")} ${t(day)}, ${t(month)}`;
  };

  const customButtons = {
    filterButton: {
      text: <FilterButton handleFilter={handleFilter} />,
    },
    setViewButton: {
      text: <AgendaButton activeView={view} handleSetView={handleSetView} />,
    },
    today: {
      text: t("today"),
      click: handleToday,
    },
    prev: {
      click: handlePrev,
    },
    next: {
      click: handleNext,
    },
  };

  const cellMounted = ({ el }) => {
    let elementBg;
    const addElement = document.createElement("div");
    addElement.innerHTML = `<i class="blue-1 fal fa-plus"></i> ${t(
      "add_task"
    )}`;
    addElement.className = `fc-add-task`;

    for (let index = 0; index < el.children[0].childNodes.length; index++) {
      const element = el.children[0].childNodes[index];
      if (element.className === "fc-daygrid-day-bg") {
        elementBg = element;
      }
    }

    return elementBg?.appendChild(addElement);
  };

  const handleDrop = (info) => {
    const { startStr } = info.event;
    const { project_id } = info.event._def.extendedProps;
    const { publicId } = info.oldEvent._def;
    const newEvents = [...events];

    const due_date = moment.tz(startStr, company_info?.timezone).unix();

    const formData = new FormData();
    formData.append("task_id", publicId);
    formData.append("value", due_date);
    formData.append("project_id", project_id);

    const index = findIndex(newEvents, ["id", publicId]);
    newEvents[index].start = startStr;
    setEvents(newEvents);
    putFormData(UPDATE_DUE_DATE, formData);
  };

  const handleSubmitAddTask = (formData, startDate) => {
    setLoadingCreate(true);

    return new Promise((resolve, reject) => {
      postFormData(CREATE_TASK, formData).then((res) => {
        if (res.meta.code === CODE_SUCCESS) {
          addToast({
            id: "add_task_successfully",
            content: t("add_task_successfully"),
            placement: "right-bottom",
            timeout: 3000,
          });
        }
        setLoadingCreate(false);
        refAddTask.current.hideModal();
        resolve(res);
      });
    });
  };

  const handleDateClick = ({ dateStr }) => {
    let timeStart;
    if (view === "dayGridMonth") {
      timeStart = moment
        .tz(dateStr, company_info?.timezone)
        .endOf("day")
        .unix();
    } else {
      timeStart = moment.tz(dateStr, company_info?.timezone).unix() + 1800;
    }

    refAddTask.current.showModal(timeStart, view !== "dayGridMonth");
  };

  return (
    <div
      className={`tabs-content p-0 mb-3 ${
        tabIndex === tabActive && "show"
      } project-calendar`}
    >
      <div className="white-box p-0">
        <AddTaskModal
          loading={loadingCreate}
          handleSubmit={handleSubmitAddTask}
          projectId={projectId}
          ref={refAddTask}
        />
        <TopFilter
          dataPanel={dataPanel}
          onFilter={handleFilter}
          isFilterTime={false}
          onAddTask={onAddTask}
          onAddGroup={onAddGroup}
          showSort={false}
          hasAddGroup={false}
          dataGet={[
            { key: "statuses[]", type: "array" },
            { key: "state", type: "array" },
            { key: "assignees[]", type: "array" },
            { key: "priorities[]", type: "array" },
            { key: "from_time", type: "string" },
            { key: "to_time", type: "string" },
          ]}
        />
        <FullCalendar
          ref={refCalendar}
          dayMaxEvents={3}
          timeZone={company_info?.timezone}
          progressiveEventRendering
          contentHeight="auto"
          plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
          titleFormat={titleString}
          initialView={view}
          fixedWeekCount={false}
          customButtons={customButtons}
          headerToolbar={{
            left: "prev title next",
            center: "",
            right: "today setViewButton",
          }}
          dayHeaderFormat={headerString}
          dayPopoverFormat={titlePopover}
          firstDay={1}
          allDaySlot={false}
          events={events}
          dayCellDidMount={cellMounted}
          eventDrop={handleDrop}
          eventContent={(e) => (
            <TaskItemCalendar type="project" eventInfo={e} />
          )}
          moreLinkContent={(num) => <MoreLinkCalendar num={num} />}
          slotLaneContent={<SlotContent view={view} />}
          dateClick={handleDateClick}
        />
      </div>
    </div>
  );
};

ProjectCalendar.propTypes = {
  tabIndex: PropTypes.string,
  tabActive: PropTypes.string,
};

ProjectCalendar.default = {
  tabIndex: "calendar",
  tabActive: "list",
};

export default ProjectCalendar;
