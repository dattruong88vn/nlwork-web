import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import { MwPanelFilter } from "app/components";

function FilterButton({ active, handleFilter, dataFilter }) {
  const { t } = useTranslation();
  const { project_info } = useSelector((state) => state.user.userInfo);
  const { settings } = useSelector((state) => state.company);
  const [dataPanel, setDataPanel] = useState([]);

  useEffect(() => {
    const data = [
      {
        title: t("status"),
        items: settings?.task?.status || [],
        grid: "grid-2",
        prefixId: "task",
        prefixName: "statuses",
      },
      {
        title: t("priorities"),
        items: settings?.task?.priority || [],
        grid: "grid-2",
        prefixId: "task",
        prefixName: "priorities",
      },
      {
        title: t("project"),
        items: project_info || [],
        grid: "grid-1",
        prefixId: "task",
        prefixName: "projects",
        limit: 4,
        className: "bor-0",
      },
    ];

    setDataPanel(data);
  }, [t, settings, project_info]);

  return (
    <MwPanelFilter
      dataPanel={dataPanel}
      dataFilter={dataFilter}
      dataGet={[
        { key: "statuses[]", type: "array" },
        { key: "projects[]", type: "array" },
        { key: "priorities[]", type: "array" },
      ]}
      onFilter={handleFilter}
      className="black-3"
    />
  );
}

export default FilterButton;
