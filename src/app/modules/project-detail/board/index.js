import React from "react";
import ListTask from "app/modules/task/components/ListTask";
import { useTranslation } from "react-i18next";
import MwButton from "app/components/button";
import MwInput from "app/components/input";
import { CODE_SUCCESS, TASK_GROUP_CREATE } from "app/const/Api";
import { postFormData } from "core/utils/APIUtils";

function DetailBoard({
  tasks,
  handleAddTotal,
  projectId,
  handleSubmitAddGroup,
  handleDeleteGroup,
  loadingTask,
}) {
  const { t } = useTranslation();
  const [expand, setExpand] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [error, setError] = React.useState("");
  const [height, setHeight] = React.useState(93);
  const refAddGroup = React.useRef(null);
  const refInput = React.useRef(null);

  React.useEffect(() => {
    if (expand) {
      if (error) {
        setHeight(153);
      } else {
        setHeight(135);
      }
    } else {
      setHeight(93);
    }
  }, [error, expand]);

  const handleExpand = (e) => {
    if (e) e.preventDefault();
    setError(null);
    setExpand(!expand);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);
    setError(null);
    const formData = new FormData();
    const name = refAddGroup.current["name"].value;

    formData.append("name", name);
    if (projectId) {
      formData.append("project_id", projectId);
    }

    if (name.trim()) {
      postFormData(TASK_GROUP_CREATE, formData).then((res) => {
        if (res.meta.code === CODE_SUCCESS) {
          setLoading(false);
          handleSubmitAddGroup(res.data, true);
          refInput.current.handleClear();
        }
      });
    } else {
      setLoading(false);
      setError(t("don_let_this_field_blank"));
    }
  };

  return (
    <div className="px-4 pt-4">
      <div className="d-flex align-top parent-group">
        <ListTask
          listTask={tasks}
          panelClass="px-4"
          defaultView="board"
          mode="board"
          projectId={projectId}
          isProject
          handleDeleteGroup={handleDeleteGroup}
          handleAddTotal={handleAddTotal}
          loading={loadingTask}
        />
        {loadingTask
          ? null
          : !!tasks?.length && (
              <div
                className="group relative ml-3"
                style={{ paddingTop: height }}
              >
                <div className="job-fixed bg-white">
                  <div className="add-job">
                    <div
                      className="add-job-toggle grey-7"
                      onClick={handleExpand}
                      style={{ display: expand ? "none" : "block" }}
                    >
                      <div className="d-flex align-center">
                        <span className="dp-inline-block relative bg-grey-2 border-rad5 mr-2">
                          <i className="fal fa-plus fs-21 fw-500 absolute black-3" />
                        </span>
                        {t("add_new_board")}
                      </div>
                    </div>
                    <form
                      ref={refAddGroup}
                      className="add-job-field"
                      onSubmit={handleSubmit}
                      style={{ display: expand ? "block" : "none" }}
                    >
                      <div className="d-flex align-center">
                        <MwInput
                          name="name"
                          ref={refInput}
                          error={error}
                          placeholder={t("enter_name_board")}
                          className="txt-input h-32 py-0"
                        />
                      </div>
                      <div className="add-job-btn mt-2">
                        <MwButton
                          title={t("save")}
                          loading={loading}
                          className="btn btn-2 mx-sp-1 btn-h-32 w-87 py-1 submit center-2"
                        />
                        <MwButton
                          title={t("cancel")}
                          onClick={handleExpand}
                          style={{ background: "transparent" }}
                          className="btn btn-4 btn-h-32 py-1 mr-5 mx-sp-1 close fw-normal"
                        />
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            )}
      </div>
    </div>
  );
}

export default DetailBoard;
