import React, { useState, useEffect, useRef } from "react";
import moment from "moment-timezone";
import filter from "lodash/filter";
import socketClient from "socket.io-client";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useLocation, useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useWindowDimensions } from "core/hooks/useWindowDimension";
import {
  PROJECT_LIST_DETAIL,
  CODE_SUCCESS,
  PROJECT_PIN,
  CREATE_TASK,
  UPDATE_TASK_STATUS,
  TASK_GROUP_DELETE,
  PROJECT_SUMMARY,
} from "app/const/Api";
import { ConvertListTask, ConvertTasksBoard } from "core/utils";
import {
  deleteWithToken,
  fetchWithToken,
  postFormData,
  putFormData,
  putWithToken,
} from "core/utils/APIUtils";
import { useSiteTitle } from "core/hooks/useSiteTitle";
import { projectFeaturedAdd } from "core/redux/actions/projectAction";
import useToast from "core/hooks/useToast";
import { TopBar, AddModal } from "./components";
import DetailList from "./list";
import DetailColumn from "./column";
import DetailBoard from "./board";
import DetailCalendar from "./calendar";
import Conversations from "../conversation/list";
import MileStone from "../milestone";
import ProjectDrive from "../drive";
import "./styles/project-detail.scss";
import {
  EVENT_TASK_ADD,
  EVENT_TASK_UPDATE_DUE_DATE,
  EVENT_TASK_UPDATE_NAME,
  EVENT_TASK_UPDATE_PRIORITY,
  EVENT_TASK_UPDATE_STATUS,
  EVENT_TASK_UPDATE_ASSIGNEE,
  EVENT_TASK_REMOVE,
  JOIN_ROOM_PROJECT,
  LEAVE_ROOM_PROJECT,
  SOCKET_ENDPOINT,
  EVENT_TASK_UPDATE_CHECKLIST,
  EVENT_TASK_COMMENT_ADD,
  EVENT_TASK_COMMENT_REMOVE,
  SOCKET_OPTIONS,
} from "app/const/SocketIO";
import TopFilter from "./components/TopFilter";
import { TaskSortOptions } from "app/const/SortOptions";

const fields =
  "tags,total_task,total_checklist,total_comment,total_attachment,attachments,assignee";

function ProjectDetail() {
  useSiteTitle("project_detail");
  const { t } = useTranslation();
  const { view: viewParam, id, folderId, typeFile } = useParams();
  const history = useHistory();
  const location = useLocation();
  const { addToast, removeAllToast, removeToast } = useToast();
  const [locationState, setLocationState] = useState({});
  const { width } = useWindowDimensions();
  const [projectDetail, setProjectDetail] = useState(null);
  const [statuses, setStatuses] = useState([]);
  const [members, setMembers] = useState([]);
  const [loadingCreate, setLoadingCreate] = useState(false);
  const [loading, setLoading] = useState(false);
  const [tasks, setTasks] = useState(null);
  const [totalTask, setTotalTask] = useState(null);
  const [view, setView] = useState(viewParam || "list");
  const [dataFilter, setDataFilter] = useState({});
  const [canRemove, setCanRemove] = useState(true);
  const [dataPanel, setDataPanel] = useState([]);

  const dispatch = useDispatch();
  const { company_info } = useSelector((state) => state.user.userInfo);
  const { settings } = useSelector((state) => state.company);
  const { status } = useSelector((state) => state.company.settings.task);
  const statusDone = status.filter((status) => status.type === 5)[0].id;
  const statusCancel = status.filter((status) => status.type === 6)[0].id;

  const refAddModal = useRef(null);
  const refTasks = useRef(null);
  const reftDrive = useRef(null);

  useEffect(() => {
    const data = [
      {
        title: t("status"),
        items: settings?.task?.status || [],
        grid: "grid-2",
        prefixId: "project",
        prefixName: "statuses",
      },
      {
        title: t("state"),
        items: [
          { id: "overdue", name: t("over_due"), nameLabel: "overdue" },
          {
            id: "no_due_date",
            name: t("no_over_due"),
            nameLabel: "no_due_date",
          },
        ],
        grid: "grid-2",
        prefixId: "project",
        prefixName: "state",
      },
      {
        title: t("priorities"),
        items: settings?.task?.priority || [],
        grid: "grid-2",
        prefixId: "project",
        prefixName: "priorities",
      },
      {
        title: t("member"),
        items: members || [],
        grid: "grid-1",
        prefixId: "project",
        prefixName: "assignees",
        limit: 4,
        emptyText: t("don_have_member"),
        keyLabel: ["first_name", "last_name"],
      },
    ];

    setDataPanel(data);
  }, [t, settings, members]);

  // Handle realtime for project task
  useEffect(() => {
    let projectId;
    const ws = socketClient(SOCKET_ENDPOINT, SOCKET_OPTIONS);

    if (Object.keys(locationState)?.length) {
      projectId = locationState?.id;
    } else if (projectDetail) {
      projectId = projectDetail?.id;
    }

    ws.emit(JOIN_ROOM_PROJECT, projectId);
    ws.on(EVENT_TASK_ADD, handleRealtimeAdd);
    ws.on(EVENT_TASK_REMOVE, handleRealtimeDelete);
    ws.on(EVENT_TASK_UPDATE_STATUS, handleRealtimeStatus);
    ws.on(EVENT_TASK_UPDATE_NAME, handleRealtimeUpdateName);
    ws.on(EVENT_TASK_UPDATE_DUE_DATE, handleRealtimeDuedate);
    ws.on(EVENT_TASK_UPDATE_PRIORITY, handleRealtimePriority);
    ws.on(EVENT_TASK_UPDATE_ASSIGNEE, handleRealtimeAssignee);
    ws.on(EVENT_TASK_UPDATE_CHECKLIST, handleRealtimeChecklist);
    ws.on(EVENT_TASK_COMMENT_ADD, handleRealtimeComment);
    ws.on(EVENT_TASK_COMMENT_REMOVE, (data) => {
      handleRealtimeComment(data, "REMOVE");
    });

    return () => ws.emit(LEAVE_ROOM_PROJECT, projectId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [locationState, projectDetail, tasks]);
  // End handle realtime

  useEffect(() => {
    if (Object.keys(locationState)?.length) {
      document.title = `${locationState?.name} - MeWork`;
    } else if (projectDetail) {
      document.title = `${projectDetail?.name} - MeWork`;
    }

    location?.state && setLocationState(location.state);
  }, [location, locationState, projectDetail]);

  useEffect(() => {
    history.push(`/project-detail/${viewParam || view}/${id}`);
    viewParam && setView(viewParam);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [history, id, viewParam]);

  // add class my-task to content div
  useEffect(() => {
    const bodyDiv = document.getElementsByTagName("body")[0];
    view === "milestone"
      ? (bodyDiv.className = `project-list milestone`)
      : (bodyDiv.className = `project-detail ${view}`);
  }, [id, view]);

  useEffect(() => {
    const getInfoProject = () => {
      setLoading(true);
      const fields =
        "assignee,attachments,followers,total_checklist,tags,total_attachment,total_comment,total_task";

      const promiseList = [
        null,
        fetchWithToken(PROJECT_LIST_DETAIL, {
          project_id: id,
          fields,
        }),
      ];

      if (location.state) {
        setLocationState(location.state);
        setMembers(location.state.members);
      } else {
        promiseList[0] = fetchWithToken(PROJECT_SUMMARY, {
          project_id: id,
          fields: "statuses,members",
        });
      }

      Promise.all(promiseList)
        .then((res) => {
          if (res[0]?.meta.code === CODE_SUCCESS) {
            setProjectDetail(res[0]?.data?.project_info);
            setStatuses(res[0]?.data?.statuses);
            setMembers(res[0]?.data?.members);
          }
          if (res[1].meta.code === CODE_SUCCESS) {
            const newTasks = ConvertListTask(res[1].data.items);
            setTotalTask(res[1].data.total_task || 0);
            setTasks(newTasks);
          }
          setLoading(false);
        })
        .catch((err) => {
          setLoading(false);
          console.log(err);
        });
    };

    getInfoProject();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  // remove class dashboard when device in mobile < 767
  useEffect(() => {
    const dashElement = document.getElementById("main-content");

    width < 767 && dashElement
      ? dashElement.classList.remove("dashboard")
      : dashElement.classList.add("dashboard");
  }, [width]);

  const handleRealtimeAdd = (task) => {
    const isFiltered = checkTaskWithFilter(task, dataFilter);
    if (isFiltered) {
      addTask(task);
    }
  };

  const handleRealtimeDuedate = (data) => {
    const isFiltered = checkTaskWithFilter(data, dataFilter);
    const tasksContain = isTasksContain(tasks, data.id);

    if (isFiltered) {
      tasksContain ? updateTask(data, "due_date") : addTask(data);
    } else {
      if (tasksContain) {
        removeTask(data);
      }
    }
  };

  const handleRealtimeStatus = (task) => {
    const isFiltered = checkTaskWithFilter(task, dataFilter);
    const tasksContain = isTasksContain(tasks, task.id);

    if (isFiltered) {
      tasksContain ? updateTask(task, "status_id") : addTask(task);
    } else {
      if (tasksContain && canRemove) {
        removeTask(task);
      }
    }
  };

  const handleRealtimePriority = (data) => {
    const isFiltered = checkTaskWithFilter(data, dataFilter);
    const tasksContain = isTasksContain(tasks, data.id);

    if (isFiltered) {
      tasksContain ? updateTask(data, "priority_id") : addTask(data);
    } else {
      if (tasksContain) {
        removeTask(data);
      }
    }
  };

  const handleRealtimeDelete = (data) => {
    const result = [...tasks];

    result.forEach((task, index) => {
      if (data.id === task.id) {
        result.splice(index, 1);
      } else {
        if (task.hasOwnProperty("children")) {
          task["children"] = task.children.filter(
            (item) => item.id !== data.id
          );
        }
      }
    });

    setTasks(result);
  };

  const updateTask = (data, keyUpdate) => {
    const result = [...tasks];

    result.forEach((item) => {
      if (item.id === data.id) {
        keyUpdate && (item[keyUpdate] = data[keyUpdate]);
      } else {
        if (item.hasOwnProperty("children")) {
          item.children.forEach((itemChildren) => {
            if (itemChildren.id === data.id) {
              keyUpdate && (itemChildren[keyUpdate] = data[keyUpdate]);
            }
          });
        }
      }
    });

    setTasks(result);
  };

  const addTask = (data) => {
    let isGroupItem = false;
    const hasTask = !!tasks.filter((task) => task.id === data.id).length;
    const result = [...tasks];

    if (!hasTask) {
      setTotalTask((total) => total + 1);

      result.forEach((task, index) => {
        if (task.id === data.group_id) {
          isGroupItem = true;
          task["children"] = task?.children
            ? [...task.children, data]
            : [...data];
        }
      });

      !isGroupItem && result.unshift(data);

      setTasks(result);
    }
  };

  const removeTask = (data) => {
    const result = [...tasks];

    result.forEach((task, index) => {
      if (data.id === task.id) {
        result.splice(index, 1);
      } else {
        if (task.hasOwnProperty("children")) {
          task["children"] = task.children.filter(
            (item) => item.id !== data.id
          );
        }
      }
    });

    setTasks(result);
  };

  const handleRealtimeUpdateName = (data) => updateTask(data, "name");

  const handleRealtimeAssignee = (data) => {
    const isFiltered = checkTaskWithFilter(data, dataFilter);
    const tasksContain = isTasksContain(tasks, data.id);

    if (isFiltered) {
      tasksContain ? updateTask(data, "assignee") : addTask(data);
    } else {
      if (tasksContain) {
        removeTask(data);
      }
    }
  };

  const handleRealtimeChecklist = (data) => {
    updateTask(data, "total_checklist");
  };

  const handleRealtimeComment = (data, operation) => {
    const copyArr = [...tasks];

    copyArr.forEach((item) => {
      if (item.id === data.task.id) {
        operation === "REMOVE"
          ? (item.total_comment -= 1)
          : (item.total_comment += 1);
      } else {
        if (item.hasOwnProperty("children")) {
          item.children.forEach((itemChildren) => {
            if (itemChildren.id === data.task.id) {
              operation === "REMOVE"
                ? (itemChildren.total_comment -= 1)
                : (itemChildren.total_comment += 1);
            }
          });
        }
      }
    });

    setTasks(copyArr);
  };

  const isTasksContain = (tasks, id) => {
    let isContain = false;
    const arr = [...tasks];

    arr.forEach((item) => {
      if (item.id === id) {
        isContain = true;
      } else {
        if (item.hasOwnProperty("children")) {
          item.children.forEach((itemChildren) => {
            if (itemChildren.id === id) {
              isContain = true;
            }
          });
        }
      }
    });

    return isContain;
  };

  const checkTaskWithFilter = (data, filterData) => {
    const timezoneCompany = company_info?.timezone;
    let taskStatus = data.status_id;
    let taskDueDate = data.due_date;
    let { keyword } = filterData;
    let checkFilterStatus = false;
    let checkFilterState = false;
    let checkFilterPriority = false;
    let checkFilterAssignee = false;
    let checkFilterToDate = false;
    let checkFilterFromDate = false;
    let taskDueDateTime = taskDueDate
      ? moment.tz(+taskDueDate, timezoneCompany).unix()
      : 0;

    let timeRightNow = moment.tz(moment(), timezoneCompany).unix();
    if (keyword && data.name.search(keyword) < 0) {
      return false;
    }
    if (filterData != null) {
      if (filterData["statuses[]"]?.length) {
        filterData["statuses[]"].forEach((item) => {
          if (taskStatus === item) {
            checkFilterStatus = true;
          }
        });
      } else {
        if (taskStatus !== statusDone && taskStatus !== statusCancel) {
          checkFilterStatus = true;
        }
      }

      if (filterData["state"]?.length) {
        filterData["state"].forEach((item) => {
          if (
            item === "overdue" &&
            timeRightNow >= taskDueDate &&
            taskDueDate != null &&
            taskStatus !== statusDone
          ) {
            checkFilterState = true;
          }
          if (item === "no_due_date" && taskDueDate == null) {
            checkFilterState = true;
          }
        });
      } else {
        checkFilterState = true;
      }

      if (filterData["priorities[]"]?.length) {
        let priorityId = data?.priority_id;
        if (priorityId) {
          checkFilterPriority = filterData["priorities[]"]?.includes(
            priorityId
          );
        }
      } else {
        checkFilterPriority = true;
      }

      if (filterData["assignees[]"]?.length) {
        let assigneeId = data?.assignee;
        if (assigneeId) {
          checkFilterAssignee = filterData["assignees[]"]?.includes(assigneeId);
        }
      } else {
        checkFilterAssignee = true;
      }

      if (filterData["to_time"]) {
        let toDateTime = moment
          .tz(filterData["to_time"], timezoneCompany)
          .unix();

        if (
          taskDueDateTime !== 0 &&
          parseInt(toDateTime) >= parseInt(taskDueDateTime)
        ) {
          checkFilterToDate = true;
        }
      } else {
        checkFilterToDate = true;
      }

      if (filterData["from_time"]) {
        let fromDateTime = moment
          .tz(filterData["from_time"], timezoneCompany)
          .unix();

        if (
          taskDueDateTime !== 0 &&
          parseInt(fromDateTime) <= parseInt(taskDueDateTime)
        ) {
          checkFilterFromDate = true;
        }
      } else {
        checkFilterFromDate = true;
      }

      return (
        checkFilterStatus &&
        checkFilterPriority &&
        checkFilterState &&
        checkFilterToDate &&
        checkFilterFromDate &&
        checkFilterAssignee
      );
    } else {
      if (taskStatus !== statusDone && taskStatus !== statusCancel) {
        return true;
      }
      return false;
    }
  };

  const handleUndo = (id, project_id, status_id) => {
    removeToast(id);
    setTasks(refTasks.current);
    setTotalTask((total) => total + 1);
    setCanRemove(true);
    handleApi(formData(id, project_id, status_id), true);
  };

  const formData = (id, project_id, status_id) => {
    const formData = new FormData();
    formData.append("task_id", id);
    formData.append("project_id", project_id);
    formData.append("status_id", status_id);
    return formData;
  };

  const handleComplete = (id, project_id, status_id) => {
    refTasks.current = tasks;
    removeAllToast();

    addToast({
      id,
      content: t("done_task_success"),
      textAgree: t("undo"),
      placement: "right-bottom",
      onAgree: () => handleUndo(id, project_id, status_id),
      timeout: 5000,
    });

    setTasks((tasks) => tasks.filter((task) => task.id !== id));
    setTotalTask((total) => total - 1);
    handleApi(formData(id, project_id, statusDone));
  };

  const handleDeleteGroup = (id, is_my_group) => {
    deleteWithToken(TASK_GROUP_DELETE, { id, is_my_group }).then((res) => {
      if (res.meta.code === CODE_SUCCESS) {
        setTasks((tasks) => tasks.filter((task) => task.id !== id));
      } else {
        addToast({
          id: "cant_delete_empty_group",
          content: res.meta.message,
          status: "error",
          placement: "right-top",
          timeout: 2000,
        });
      }
    });
  };

  const handleApi = async (formData, isUndo) => {
    try {
      await putFormData(UPDATE_TASK_STATUS, formData);
    } catch (error) {
      console.error(error);
    }
  };

  const handleSetView = (e, value) => {
    e && e.preventDefault();
    setView(value);
    history.push(`/project-detail/${value}/${id}`);
  };

  const setFeatured = (featured) => {
    const { name, setting_info } = locationState;
    const data = {
      project_id: id,
      featured,
    };

    const dataFeatured = Object.keys(locationState).length
      ? {
          id,
          name,
          setting_info,
          total_task_done:
            locationState?.total_task_done || projectDetail?.total_task_done,
          total_task: locationState?.total_task || projectDetail?.total_task,
          featured,
        }
      : {
          setting_info: { ...projectDetail },
          id,
          name: projectDetail.name,
          total_task_done:
            locationState?.total_task_done || projectDetail?.total_task_done,
          total_task: locationState?.total_task || projectDetail?.total_task,
          featured,
        };
    putWithToken(PROJECT_PIN, data).then(() => {
      dispatch(projectFeaturedAdd({ pin: featured, data: dataFeatured }));
    });
  };

  const handleAddTask = (e) => {
    e && e.preventDefault();

    switch (view) {
      case "calendar":
        refAddModal.current.openAddTaskCalendar();
        break;
      default:
        refAddModal.current.openAddTask();
        break;
    }
  };

  const handleAddGroup = (e) => {
    if (e) e.preventDefault();
    refAddModal.current.openAddGroup();
  };

  const defaultFilter = (data) => {
    setLoading(true);
    fetchWithToken(PROJECT_LIST_DETAIL, {
      ...data,
      fields,
      project_id: id,
    })
      .then((res) => {
        const sortedList = ConvertListTask(res.data.items || []);
        const count =
          filter(res.data.items, (item) => item.TYPE === "task" && item)
            .length || 0;
        setTasks(sortedList);
        setTotalTask(count || 0);
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  };

  const handleFilter = (data) => {
    setDataFilter(data);
    defaultFilter(data);
  };

  const handleAddTotal = (key, operation) => {
    operation === "MINUS"
      ? setTotalTask((totalTask) => totalTask - 1)
      : setTotalTask((totalTask) => totalTask + 1);
  };

  const handleSetTask = (res) => {
    setTasks((tasks) => [res, ...tasks]);
  };

  const taskData = () => {
    if (tasks && view !== "board") return ConvertListTask(tasks);
    if (tasks && view === "board") return ConvertTasksBoard(tasks);
  };

  const handleSubmitAddTask = (formData, startDate) => {
    setLoadingCreate(true);

    postFormData(CREATE_TASK, formData).then((res) => {
      if (res.meta.code === CODE_SUCCESS) {
        addToast({
          id: "add_task_successfully",
          content: t("add_task_successfully"),
          placement: "right-bottom",
          timeout: 3000,
        });
      } else {
        console.error(res.meta.message);
      }
      setLoadingCreate(false);
      refAddModal.current.closeAddTask();
    });
  };

  const handleSubmitAddGroup = (res, boardView) => {
    removeToast("add-section");
    setTasks((listTask) => {
      addToast({
        id: "add-section",
        content: t("add_task_group_successfully"),
      });
      if (boardView) return [...listTask, res];

      return [res, ...listTask];
    });
  };

  const className = (view) => {
    if (view === "column" || view === "board")
      return "white-box px-0 pt-0 pb-8 relative mt-4";
    if (view === "calendar" || view === "list")
      return "white-box p-0 relative mt-4";
    return "white-box p-0";
  };

  const handleFilterDrive = (filter) => {
    reftDrive.current.handleFilter(filter);
  };

  return (
    <>
      <AddModal
        ref={refAddModal}
        projectId={locationState?.id || projectDetail?.id}
        callback={handleSetTask}
        handleSubmitTask={handleSubmitAddTask}
        view={view}
        loading={loadingCreate}
      />
      <TopBar
        setFeatured={setFeatured}
        handleSetView={handleSetView}
        view={view}
        handleAddTask={handleAddTask}
        handleAddGroup={handleAddGroup}
        totalTask={totalTask}
        loading={loading}
        statuses={statuses}
        projectId={id}
        total_task_done={
          locationState?.total_task_done || projectDetail?.total_task_done
        }
        total_task={locationState?.total_task || projectDetail?.total_tasks}
        name={locationState?.name || projectDetail?.name}
        fromLink={locationState?.fromLink}
        featured={locationState?.featured || projectDetail?.featured}
        start_date={locationState?.start_date || projectDetail?.start_date}
        end_date={locationState?.end_date || projectDetail?.end_date}
        description={projectDetail?.description}
        members={members}
        handleFilterDrive={handleFilterDrive}
      />
      <div className={className(view)}>
        <TopFilter
          dataPanel={dataPanel}
          onAddTask={handleAddTask}
          onAddGroup={handleAddGroup}
          onFilter={handleFilter}
          sortOptions={TaskSortOptions}
          dataGet={[
            { key: "statuses[]", type: "array" },
            { key: "state", type: "array" },
            { key: "assignees[]", type: "array" },
            { key: "priorities[]", type: "array" },
            { key: "from_time", type: "string" },
            { key: "to_time", type: "string" },
          ]}
          isHidden={!["list", "column", "board"].includes(view)}
        />

        {view === "list" && (
          <DetailList
            projectId={id}
            loading={loading}
            tasks={tasks}
            onFilter={handleFilter}
            handleAddTotal={handleAddTotal}
            handleComplete={handleComplete}
            handleDeleteGroup={handleDeleteGroup}
          />
        )}

        {view === "column" && (
          <DetailColumn
            projectId={id}
            loading={loading}
            tasks={tasks}
            handleAddTotal={handleAddTotal}
            handleComplete={handleComplete}
            handleDeleteGroup={handleDeleteGroup}
          />
        )}

        {view === "board" && (
          <DetailBoard
            projectId={id}
            loadingTask={loading}
            tasks={taskData}
            handleAddTotal={handleAddTotal}
            handleComplete={handleComplete}
            handleSubmitAddGroup={handleSubmitAddGroup}
            handleDeleteGroup={handleDeleteGroup}
          />
        )}

        {view === "calendar" && (
          <DetailCalendar
            projectId={id}
            members={members || []}
            loading={loading}
            onFilter={handleFilter}
            onAddTask={handleAddTask}
            onAddGroup={handleAddGroup}
            handleComplete={handleComplete}
            handleDeleteGroup={handleDeleteGroup}
          />
        )}

        {view === "conversation" && <Conversations projectId={id} />}

        {view === "milestone" && <MileStone projectId={id} />}

        {view === "drive" && (
          <ProjectDrive
            ref={reftDrive}
            projectId={id}
            folderId={folderId}
            typeFile={typeFile}
            view={view}
          />
        )}
      </div>
    </>
  );
}

export default ProjectDetail;
