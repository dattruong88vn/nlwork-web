import React from "react";
import ListTask from "app/modules/task/components/ListTask";

function DetailColumn({ tasks, ...restProps }) {
  return (
    <ListTask
      listTask={tasks}
      panelClass="px-4"
      defaultView="column"
      isProject
      {...restProps}
    />
  );
}

export default DetailColumn;
