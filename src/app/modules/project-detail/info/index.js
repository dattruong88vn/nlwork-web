import React from "react";
import moment from "moment-timezone";
import find from "lodash/find";
import each from "lodash/each";
import PropTypes from "prop-types";
import ReactApexChart from "react-apexcharts";
import { useTranslation } from "react-i18next";
import MwModal from "app/components/modal";
import { useSelector } from "react-redux";
import MwMembers from "app/components/members";
import { ProgressCalculate } from "core/utils/ProgressCalculate";
import ShowMoreText from "app/components/showmore";
import { fetchWithToken } from "core/utils/APIUtils";
import { PROJECT_SUMMARY } from "app/const/Api";
import ListLoading from "app/components/loading/ListLoading";

const ProjectInfo = React.forwardRef(
  (
    {
      progress,
      name,
      startDate,
      endDate,
      statuses,
      totalTasks,
      projectId,
      colorFinished,
      visible,
      fromLink,
      members: membersProps,
      description: descriptionProps,
    },
    ref
  ) => {
    const { t } = useTranslation();
    const [userList, setUserList] = React.useState([]);
    const [members, setMembers] = React.useState([]);
    const [administrator, setAdministrator] = React.useState();
    const { timezone } = useSelector(
      (state) => state.user.userInfo.company_info
    );
    const listProject = useSelector(
      (state) => state.company.settings.list_project
    );
    const [statusList, setStatusList] = React.useState(statuses || []);
    const [loading, setLoading] = React.useState(false);
    const [description, setDescription] = React.useState(false);
    const [series, setSeries] = React.useState([]);
    const [labels, setLabels] = React.useState([]);
    const [date, setDate] = React.useState();
    const [totalLegend, setTotalLegend] = React.useState(totalTasks);
    const [colorLabels, setColorLabels] = React.useState([]);
    const [colorSeries, setColorSeries] = React.useState([]);

    const refModalInfo = React.useRef(null);

    React.useEffect(() => {
      membersProps && setMembers(membersProps);
      descriptionProps && setDescription(descriptionProps);
      const data = { end_date: endDate, start_date: startDate };
      setDate(data);
    }, [membersProps, descriptionProps, endDate, startDate]);

    React.useEffect(() => {
      if (fromLink) {
        visible && getStatuses();
      } else {
        handleDataChart(statuses);
        setStatusList(statuses);
      }

      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [statuses, visible, projectId]);

    React.useEffect(() => {
      setTotalLegend(totalTasks);
    }, [totalTasks]);

    React.useEffect(() => {
      const administrators = [];
      const users = [];
      const projectRoles = find(listProject, ["id", projectId]).roles;
      const roleManager = find(projectRoles, ["value", "project.manager"]).id;

      if (members.length) {
        each(members, (member) => {
          member.role_id === roleManager
            ? administrators.push(member)
            : users.push(member);
        });
      }

      setUserList(users);
      setAdministrator(administrators);
    }, [listProject, members, projectId]);

    React.useImperativeHandle(ref, () => ({ showModal }));

    const handleDataChart = (data) => {
      const colorSeries = [];
      const colorLabels = [];
      const series = [];
      const labels = [];
      each(data, function (status) {
        colorSeries.push(status.status_color);
        series.push(ProgressCalculate(status.total_tasks, totalTasks));
        labels.push(status.status_name);
        colorLabels.push(status.status_color);
      });

      setSeries(series);
      setLabels(labels);
      setColorSeries(colorSeries);
      setColorLabels(colorLabels);
    };

    const getStatuses = async () => {
      setLoading(true);
      try {
        const result = await fetchWithToken(PROJECT_SUMMARY, {
          project_id: projectId,
        });
        setStatusList(result.data.statuses);
        handleDataChart(result.data.statuses);
        setLoading(false);
        setTotalLegend(result.data.project_info.total_tasks);
        setMembers(result.data.members);
        setDescription(result.data.project_info.description);
        setDate({
          end_date: result.data.project_info.end_date,
          start_date: result.data.project_info.start_date,
        });
      } catch (error) {
        setLoading(false);
        console.error(error);
      }
    };

    const showModal = () => {
      refModalInfo.current.showModal();
    };

    const convertDate = (date) => {
      if (!date) return "N/A";

      return moment.tz(date * 1000, timezone).format("DD-MM-YYYY");
    };

    return (
      <MwModal
        ref={refModalInfo}
        id="project-info"
        wrapperClass="modal modal-full d-flex justify-end align-center"
        title={t("project_information")}
        contentClass="modal-content p-0"
        headClass="modal-head d-flex justify-space-between bg-white"
        bodyClass="modal-body p-0 scroll"
        backTitle={false}
      >
        <div className="px-6 px-sp-3 pt-3 pb-5 border-bottom-grey-6">
          <span className="dp-block fs-20 fw-bold">{name}</span>
          <div className="mt-3 border-bottom-sp-grey-6 pb-sp-3 d-flex align-center justify-space-between">
            <div className={`progress-bar --green per-${progress}`}>
              <span
                data-tooltip={`${t("complete")} ${progress}%`}
                style={{ background: colorFinished }}
              />
            </div>
            <span className="dp-pc-hide fs-12 grey-7">{progress}%</span>
          </div>
          <div className="conts-desc mt-5">
            {loading ? (
              <ListLoading />
            ) : (
              <>
                {!!administrator?.length && (
                  <div className="d-flex align-center">
                    <span className="dp-inline-block grey-7 fs-12 fw-500">
                      {t("administrator")}:
                    </span>
                    <MwMembers
                      members={administrator || []}
                      circleClass="mr-1 w-30"
                      customClass="mr-1 w-30"
                      limit={5}
                    />
                  </div>
                )}
                {!!userList.length && (
                  <div className="d-flex align-center mt-2">
                    <span className="dp-inline-block grey-7 fs-12 fw-500">
                      {t("member")}:
                    </span>
                    <MwMembers
                      members={userList || []}
                      circleClass="mr-1 w-30"
                      customClass="mr-1 w-30"
                      limit={5}
                    />
                  </div>
                )}
                {(date?.start_date || date?.end_date) && (
                  <div className="d-flex align-center mt-2">
                    <span className="dp-inline-block grey-7 fs-12 fw-500">
                      {t("time")}:
                    </span>
                    <div className="d-flex align-center">
                      <div className="d-flex align-center">
                        <span className="circle-cal mr-1 mr-sp-1 dp-inline-block border-circle bg-grey-2 text-center">
                          <i className="far fa-calendar" />
                        </span>
                        {convertDate(date?.start_date)}
                      </div>
                      <span className="mx-4 hyphen">–</span>
                      <div className="d-flex align-center">
                        <span className="circle-cal mr-1 mr-sp-1 dp-inline-block border-circle bg-grey-2 text-center">
                          <i className="far fa-calendar" />
                        </span>
                        {convertDate(date?.end_date)}
                      </div>
                    </div>
                  </div>
                )}

                {description && (
                  <>
                    <div className="mt-2">
                      <span className="dp-inline-block grey-7 fs-12 fw-500">
                        {t("description")}
                      </span>
                    </div>
                    <ShowMoreText
                      lines={3}
                      moreText={t("see_more")}
                      lessText={t("see_less")}
                      anchorClass=""
                      className="mt-1"
                    >
                      <span className="description">{description}</span>
                    </ShowMoreText>
                  </>
                )}
              </>
            )}
          </div>
        </div>
        <div className="px-6 py-5 p-sp-0 ">
          {loading ? (
            <ListLoading />
          ) : (
            <>
              <span className="fw-bold black-2 fs-15 dp-pc-hide border-bottom-sp-grey-6 p-sp-3 dp-block">
                {t("project_detail")}
              </span>
              <div className="d-flex justify-space-between p-sp-4 progress-status-2">
                <div className="details">
                  <span className="fw-bold black-2 fs-15 dp-sp-hide">
                    {t("project_detail")}
                  </span>
                  <div className="work-status mt-4 mt-sp-0">
                    {!!statusList?.length &&
                      statusList.map((status, index) => {
                        return (
                          <div
                            key={index}
                            className="d-flex align-center justify-space-between"
                          >
                            <div className="d-flex align-center">
                              <span
                                className="border-circle dp-inline-block mr-3"
                                style={{ background: status.status_color }}
                              />
                              {status.status_name}
                            </div>
                            <span className="grey-7 fw-bold">
                              {ProgressCalculate(
                                status.total_tasks,
                                totalTasks
                              )}
                              %
                            </span>
                          </div>
                        );
                      })}
                  </div>
                </div>
                {!!colorSeries?.length && (
                  <div className="progress-work-2">
                    <div className="progress-work-2__labels">
                      <h2 className="progress-work-2__labels--total">
                        {`${totalLegend}`}
                      </h2>
                      <span className="progress-work-2__labels--name">
                        {t("task")}
                      </span>
                    </div>
                    <ReactApexChart
                      options={{
                        dataLabels: {
                          enabled: false,
                        },
                        legend: {
                          show: false,
                        },
                        fill: {
                          colors: colorSeries,
                        },
                        markers: {
                          colors: colorSeries,
                        },
                        labels,
                        colors: colorLabels,
                      }}
                      series={series}
                      type="donut"
                      width="100%"
                      height="200"
                    />
                  </div>
                )}
              </div>
            </>
          )}
        </div>
      </MwModal>
    );
  }
);

ProjectInfo.propTypes = {
  progress: PropTypes.number,
  description: PropTypes.string,
  name: PropTypes.string,
  startDate: PropTypes.string,
  endDate: PropTypes.string,
  members: PropTypes.array,
  statuses: PropTypes.array,
  totalTasks: PropTypes.number,
  projectId: PropTypes.string,
};

export default ProjectInfo;
