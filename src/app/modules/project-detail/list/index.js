import React from "react";
import ListTask from "app/modules/task/components/ListTask";

function DetailList({ tasks, projectId, loading, onFilter, ...restProps }) {
  return (
    <>
      <ListTask
        listTask={tasks}
        projectId={projectId}
        isProject
        loading={loading}
        defaultView="list"
        {...restProps}
      />
    </>
  );
}

export default React.memo(DetailList);
