import React from "react";
import { userLoginRequest } from "core/redux/actions/authAction";
import { Redirect } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import Header from "./components/header";
import SignInForm from "./components/formSignin";
import ForgotPassword from "./components/forgotPassword";
import "./styles/index.scss";
import { KEY_TOKEN } from "app/const/App";
import { useSiteTitle } from "core/hooks/useSiteTitle";
import { validateEmail } from "core/utils";
import { useTranslation } from "react-i18next";
import { postWithToken } from "core/utils/APIUtils";
import { CODE_SUCCESS, FORGOT_PASSWORD } from "app/const/Api";
import useToast from "core/hooks/useToast";

function SignIn() {
  useSiteTitle("sign_in");
  const { t } = useTranslation();
  const { addToast } = useToast();
  const [loadingForgot, setLoadingForgot] = React.useState(false);
  const [errorsForgot, setErrorsForgot] = React.useState("");
  const [errorsLogin, setErrorsLogin] = React.useState({});
  const refForm = React.useRef(null);
  const refFormForgot = React.useRef(null);
  const dispatch = useDispatch();
  const token = localStorage.getItem(KEY_TOKEN);
  const { loading, error, forgotSuccess } = useSelector((state) => state.auth);
  const [visibleModal, setVisibleModal] = React.useState(false);

  React.useEffect(() => {
    return () => setErrorsForgot("");
  }, [visibleModal]);

  const openModal = (e) => {
    e.preventDefault();
    setVisibleModal(true);
  };

  const closeModal = () => {
    setVisibleModal(false);
  };

  React.useEffect(() => {
    closeModal();
  }, [forgotSuccess]);

  const handleSubmitLogin = (e) => {
    e && e.preventDefault();
    setErrorsLogin(null);
    const msgErrors = {};

    const { email, password } = refForm.current;

    if (!email.value) {
      msgErrors["email"] = email.dataset.helper;
    }

    if (password.value.length < 6) {
      msgErrors["password"] = password.dataset.helper;
    } else {
      if (password.value.trim().length < 1) {
        msgErrors["password"] = t("loginPage:incorrect_email_password");
        msgErrors["email"] = t("loginPage:incorrect_email_password");
      }
    }

    if (!validateEmail(email.value)) {
      msgErrors["email"] = t("loginPage:wrong_format_email");
    }

    if (Object.keys(msgErrors).length) return setErrorsLogin(msgErrors);

    dispatch(
      userLoginRequest({ email: email.value, password: password.value })
    );
  };

  const submitForgotPassword = (e) => {
    e && e.preventDefault();
    setErrorsForgot("");
    const email = refFormForgot.current["email"];
    if (!email.value) return setErrorsForgot(email.dataset.helper);
    if (!validateEmail(email.value)) {
      return setErrorsForgot(t("loginPage:wrong_format_email"));
    }

    setLoadingForgot(true);
    postWithToken(FORGOT_PASSWORD, { email: email.value })
      .then((res) => {
        if (res.meta.code === CODE_SUCCESS) {
          addToast({
            id: "forgot_password",
            content: t("pls_check_your_email"),
            placement: "right-bottom",
            timeout: 3000,
          });
          setVisibleModal(false);
        } else {
          setErrorsForgot(res.meta.message);
        }
        setLoadingForgot(false);
      })
      .catch((err) => console.error(err));
  };

  return token ? (
    <Redirect to="/" />
  ) : (
    <div className="login-page">
      <Header />
      <form className="login-wrap" ref={refForm}>
        <SignInForm
          forgotSuccess={forgotSuccess}
          loading={loading}
          openModal={openModal}
          errors={errorsLogin || error}
          handleSubmit={handleSubmitLogin}
        />
      </form>
      <Header isFooter className="text-center show-767 mt-50" />
      <div className="copyright hide-767">
        © 2020 MeWork. All rights reserved.
      </div>
      <form ref={refFormForgot}>
        <ForgotPassword
          loading={loadingForgot}
          errors={errorsForgot}
          closeModal={closeModal}
          visible={visibleModal}
          handleSubmit={submitForgotPassword}
        />
      </form>
    </div>
  );
}

export default SignIn;
