import React from "react";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import MwInput from "app/components/input";
import MwButton from "app/components/button";

function ForgotPassword({
  visible,
  loading,
  errors,
  closeModal,
  handleSubmit,
}) {
  const { t } = useTranslation(["common", "loginPage"]);
  return visible ? (
    <div className="modal mb-modal open">
      <div className="modal-wrap">
        <div className="modal-content">
          <div className="maxw-420 mg-auto">
            <button onClick={closeModal} className="btn-close" type="button">
              <i className="fal fa-times"></i>
              <i className="far fa-arrow-left"></i>
            </button>
            <div className="intro">
              <h3 className="title-1">{t("you_forgot_password")}</h3>
              <p>{t("forgot_password_fill")}</p>
            </div>
            <div className="inp-wrap">
              <MwInput
                autoFocus
                required
                error={errors || ""}
                type="email"
                name="email"
                customLabel={{ className: "lb" }}
                label={t("email")}
                placeholder={t("enter_your_email")}
                data-helper={t("loginPage:pls_enter_email")}
              />
            </div>
            <div className="text-right mt-20">
              <MwButton
                loading={loading}
                className="btn btn-2 minw-160 btn-flex login"
                title={t("send")}
                onClick={handleSubmit}
                preventEvent
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  ) : null;
}

ForgotPassword.propTypes = {
  visible: PropTypes.bool,
  loading: PropTypes.bool,
  errors: PropTypes.string,
  closeModal: PropTypes.func,
};

ForgotPassword.defaultProps = {
  visible: false,
  loading: false,
  errors: null,
  closeModal: () => {},
};

export default ForgotPassword;
