import React from "react";
import { useTranslation } from "react-i18next";
import "./styles/index.scss";
import { Link } from "react-router-dom";

function HeaderLogin({ isFooter, ...rest }) {
  const { t } = useTranslation(["loginPage", "common"]);
  return (
    <div className="note hide-767" {...rest}>
      {t("dont_have_account")}
      <Link to="/auth/signup" className={!isFooter ? "btn btn-1 minw-115" : ""}>
        {" "}
        {t("common:register")}
      </Link>
    </div>
  );
}

export default React.memo(HeaderLogin);
