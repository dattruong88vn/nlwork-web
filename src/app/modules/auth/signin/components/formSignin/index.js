import React from "react";
import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";
import MwInput from "app/components/input";
import MwButton from "app/components/button";
import logo from "assets/images/logo.svg";
import "./styles/index.scss";

function SignInForm({
  openModal,
  forgotSuccess,
  loading,
  errors,
  handleSubmit,
}) {
  const { t } = useTranslation(["common", "loginPage"]);

  return (
    <div>
      <div className="login-form mg-auto">
        <div className="intro mb-30 text-center">
          <div className="logo mg-auto">
            <img src={logo} alt="MeWork logo" />
          </div>
          <h1 className="title-1">{t("login")}</h1>
          <p>{t("loginPage:fill_all_info")}</p>
        </div>
        <MwInput
          name="email"
          tabIndex="1"
          type="email"
          required
          label={t("email")}
          customLabel={{ className: "lb" }}
          error={errors?.email}
          placeholder={t("enter_your_email")}
          data-helper={t("loginPage:pls_enter_email")}
        />
        <MwInput
          required
          name="password"
          tabIndex="2"
          customLabel={{ className: "lb" }}
          label={t("password")}
          type="password"
          error={errors?.password}
          labelRight={
            <a href="/" onClick={openModal} className="forgot hide-767">
              {t("forgot_password")}
            </a>
          }
          placeholder={t("enter_your_password")}
          data-helper={t("loginPage:password_limit")}
        />
        <a href="/" onClick={openModal} className="mt-10 show-767">
          {t("forgot_password")}
        </a>
        {forgotSuccess && (
          <div className="msg-success msg-icon mt-30">
            <i className="far fa-check ico"></i> {t("forgot_password_has_send")}
          </div>
        )}
        <MwButton
          tabIndex="3"
          type="submit"
          loading={loading}
          className="btn btn-2 btn-flex btn-w100 mt-30 login"
          title={t("login")}
          onClick={handleSubmit}
          preventEvent
        />
      </div>
    </div>
  );
}

SignInForm.propTypes = {
  errors: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
  forgotSuccess: PropTypes.bool,
};

SignInForm.defaultProps = {
  forgotSuccess: false,
};

export default SignInForm;
