import React from "react";
import { useTranslation } from "react-i18next";
import { useLocation, Link } from "react-router-dom";
import { getParameterByName } from "core/utils";
import FormForgot from "./components/formForgot";
import logo from "assets/images/logo.svg";
import { useSiteTitle } from "core/hooks/useSiteTitle";
import { KEY_TOKEN } from "app/const/App";
import { postWithToken } from "core/utils/APIUtils";
import { CODE_SUCCESS, RESET_PASSWORD } from "app/const/Api";

function ResetPassword() {
  const { t } = useTranslation(["common", "pageTitle"]);
  localStorage.removeItem(KEY_TOKEN);
  useSiteTitle(t("pageTitle:reset_password"));
  const [loading, setLoading] = React.useState(false);
  const [resetError, setResetError] = React.useState(false);
  const [resetSuccess, setResetSuccess] = React.useState(false);
  const [errors, setErrors] = React.useState({});

  const { search } = useLocation();
  const token = getParameterByName("token", search);
  const email = getParameterByName("email", search);
  const refForm = React.useRef(null);

  const handleSubmit = (e) => {
    e && e.preventDefault();
    const { confirm, new_password } = refForm.current;
    const msgError = {};
    setErrors({});

    for (let index = 0; index < refForm.current.length; index++) {
      const { type, name, value, required, dataset } = refForm.current[index];
      if ((type === "text" || "password" || "email") && !value && required) {
        msgError[name] = dataset.helper;
      }
    }

    if (confirm.value.length < 6) {
      msgError["confirm"] = confirm.dataset.helper;
    }

    if (new_password.value.length < 6) {
      msgError["new_password"] = confirm.dataset.helper;
    }

    if (confirm.value !== new_password.value) {
      msgError["confirm"] = t("password_not_match");
    }

    if (Object.keys(msgError).length) return setErrors(msgError);

    setLoading(true);
    postWithToken(RESET_PASSWORD, {
      email,
      token,
      confirm: confirm.value,
      new_password: new_password.value,
    })
      .then((res) => {
        res.meta.code === CODE_SUCCESS
          ? setResetSuccess(true)
          : setResetError(true);
        setLoading(false);
      })
      .catch((err) => console.error(err));
  };

  const ErrorComponent = () => {
    return (
      <div className="login-wrap">
        <div className="login-form mg-auto">
          <div className="intro mb-30 text-center">
            <div className="logo mg-auto">
              <img src={logo} alt="MeWork logo" />
            </div>
            <h1 className="title-1">{t("activate_account")}</h1>
            <p>{t("reset_password_failure")}</p>
            <Link to="/auth/signup" className="btn btn-2 btn-block mt-30">
              {t("go_back")}
            </Link>
          </div>
        </div>
      </div>
    );
  };

  const Form = React.useCallback(({ success, loading, errors }) => {
    return (
      <form ref={refForm} className="login-wrap">
        {success ? (
          <div className="login-form mg-auto">
            <div className="intro mb-30 text-center">
              <div className="logo mg-auto">
                <img src={logo} alt="MeWork" />
              </div>
              <h1 className="title-1">{t("activate_account")}</h1>
              <p>{t("reset_password_success")}</p>
              <Link to="/auth/login" className="btn btn-2 btn-block mt-30">
                {t("login_now")}
              </Link>
            </div>
          </div>
        ) : (
          <FormForgot
            loading={loading}
            errors={errors}
            handleSubmit={handleSubmit}
          />
        )}
      </form>
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="active-page">
      {resetError ? (
        <ErrorComponent />
      ) : (
        <Form success={resetSuccess} loading={loading} errors={errors} />
      )}
      <div className="copyright hide-767">
        © 2020 MeWork. All rights reserved.
      </div>
    </div>
  );
}

export default ResetPassword;
