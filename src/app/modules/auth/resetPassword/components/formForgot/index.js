import React from "react";
import { useTranslation } from "react-i18next";
import MwInput from "app/components/input";
import MwButton from "app/components/button";
import logo from "assets/images/logo.svg";

function FormForgot({ errors, loading, handleSubmit }) {
  const { t } = useTranslation(["loginPage"]);
  return (
    <div>
      <div className="login-form mg-auto">
        <div className="intro mb-30 text-center">
          <div className="logo mg-auto">
            <img src={logo} alt="MeWork" />
          </div>
          <h1 className="title-1">{t("reset_password")}</h1>
          <p>{t("reset_password_fill")}</p>
        </div>
        <MwInput
          required
          name="new_password"
          label={t("new_password")}
          type="password"
          placeholder={t("enter_your_new_password")}
          data-helper={t("password_limit")}
          error={errors?.new_password}
        />
        <MwInput
          required
          name="confirm"
          label={t("re_password")}
          type="password"
          placeholder={t("enter_your_re_password")}
          data-helper={t("password_limit")}
          error={errors?.confirm}
        />
        <MwButton
          type="submit"
          loading={loading}
          className="btn btn-2 btn-flex btn-w100 mt-30"
          title={t("reset_password")}
          onClick={handleSubmit}
          preventEvent
        />
      </div>
    </div>
  );
}

export default FormForgot;
