import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import MwButton from "app/components/button";
import MwInput from "app/components/input";
import logo from "assets/images/logo.svg";
import "./styles/index.scss";

function FormSignUp({
  errors,
  loading,
  registerSuccess,
  success,
  handleSubmit,
}) {
  const { t } = useTranslation(["common", "loginPage"]);

  return (
    <div className="inner mg-auto">
      <div className="show-767 mb-logo mg-auto">
        <img src={logo} alt="MeWork" />
      </div>
      <div className="text-center">
        <h1 className="title-1">{t("sign_up_account")}</h1>
        <p>{t("sign_up_account_fill")}</p>
      </div>
      <div className="row sp-row-15">
        <div className="col-575-50 col">
          <MwInput
            label={t("first_name")}
            name="first_name"
            required
            customLabel={{ className: "lb" }}
            placeholder={t("enter_your_first_name")}
            data-helper={t("loginPage:pls_enter_first_name")}
            error={errors?.first_name}
          />
        </div>
        <div className="col-575-50 col">
          <MwInput
            label={t("last_name")}
            required
            name="last_name"
            customLabel={{ className: "lb" }}
            placeholder={t("enter_your_last_name")}
            error={errors?.last_name}
            data-helper={t("loginPage:pls_enter_last_name")}
          />
        </div>
      </div>
      <MwInput
        label={t("email")}
        name="email"
        type="email"
        success={success?.email}
        error={errors?.email}
        customLabel={{ className: "lb mt-15" }}
        placeholder={t("enter_your_email")}
        data-helper={t("loginPage:pls_enter_email")}
        required
      />
      <MwInput
        label={t("password")}
        required
        type="password"
        name="password"
        error={errors?.password}
        customLabel={{ className: "lb" }}
        placeholder={t("enter_your_password")}
        data-helper={t("loginPage:password_limit")}
      />
      <MwInput
        label={t("phone")}
        type="phone"
        name="phone"
        error={errors?.phone}
        customLabel={{ className: "lb" }}
        placeholder={t("enter_your_phone")}
      />
      <MwInput
        label={t("your_company_name")}
        name="company_name"
        customLabel={{ className: "lb" }}
        placeholder={t("enter_your_company_name")}
        error={errors?.company_name}
      />
      <div className="checkbox mt-30">
        <input type="checkbox" id="accept" name="accept" />
        <label htmlFor="accept">
          {t("i_read_and_agree")}
          <Link to="/">{t("term")} </Link> &amp;
          <Link to="/"> {t("conditions")}</Link>
        </label>
      </div>
      {errors?.agreePolicy && (
        <div className="msg-err msg-icon mt-30">
          {t("pls_check_agree_policy")}
        </div>
      )}
      {registerSuccess && (
        <div className="msg-success msg-icon mt-30">
          <i className="far fa-check ico"></i> {t("register_user_success")}
        </div>
      )}
      <MwButton
        type="submit"
        title={t("register")}
        loading={loading}
        className="btn btn-2 btn-flex w-100 mt-30"
        onClick={handleSubmit}
        preventEvent
      />
      <div className="mt-50 text-center show-767">
        {t("have_account")} <Link to="/auth/login">{t("login")}</Link>
      </div>
      <div className="copyright hide-767">
        © 2020 MeWork. All rights reserved.
      </div>
    </div>
  );
}

FormSignUp.propTypes = {
  errors: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  loading: PropTypes.bool,
  registerSuccess: PropTypes.bool,
  success: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
};

FormSignUp.defaultProps = {
  errors: null,
  loading: false,
  registerSuccess: false,
  success: false,
};

export default FormSignUp;
