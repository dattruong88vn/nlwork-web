import React from "react";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { phoneValidate } from "core/utils/PhoneUtils";
import { postWithToken } from "core/utils/APIUtils";
import { useSiteTitle } from "core/hooks/useSiteTitle";
import { validateEmail } from "core/utils";
import FormSignUp from "./components/formSignUp";
import { CODE_SUCCESS, REGISTER, RESEND_ACTIVATE } from "app/const/Api";
import logoWhite from "assets/images/logo-white.svg";
import detailTaskImg from "assets/images/signup/details-task.jpg";
import "./styles/index.scss";

function SignUpPage() {
  const { t } = useTranslation(["pageTitle", "common", "loginPage"]);
  useSiteTitle(t("sign_up"));
  const refForm = React.useRef(null);
  const [loading, setLoading] = React.useState(false);
  const [errors, setErrors] = React.useState(null);
  const [email, setEmail] = React.useState();
  const [success, setSuccess] = React.useState(null);
  const [registerSuccess, setRegisterSuccess] = React.useState(false);
  const [resendSuccess, setResendSuccess] = React.useState(false);

  const isOnlySpace = (value) => {
    return !value.replace(/\s/g, "").length;
  };

  /**
   * handle form submit
   * @param {event} e
   */
  const handleSubmit = (e) => {
    e && e.preventDefault();
    const msgError = {};
    setErrors({});

    const {
      first_name,
      last_name,
      email,
      password,
      phone,
      company_name,
      accept,
    } = refForm.current;

    for (let index = 0; index < refForm.current.length; index++) {
      const { type, name, value, required, dataset } = refForm.current[index];
      if ((type === "text" || "password" || "email") && !value && required) {
        msgError[name] = dataset.helper;
      }
    }

    if (isOnlySpace(first_name.value)) {
      msgError["first_name"] = first_name.dataset.helper;
    }

    if (isOnlySpace(last_name.value)) {
      msgError["last_name"] = last_name.dataset.helper;
    }

    if (first_name.value.length > 100) {
      msgError["first_name"] = t("loginPage:limit_100");
    }

    if (last_name.value.length > 100) {
      msgError["last_name"] = t("loginPage:limit_100");
    }

    if (password.value.length < 6) {
      msgError["password"] = t("loginPage:password_limit");
    }

    if (email.value && !validateEmail(email.value)) {
      msgError["email"] = t("loginPage:wrong_format_email");
    }

    if (company_name.value.length > 64) {
      msgError["company_name"] = t("loginPage:limit_64");
    }

    if (company_name.value.length && company_name.value.length < 2) {
      msgError["company_name"] = t("loginPage:company_limit_2");
    }

    if (Object.keys(msgError).length) return setErrors(msgError);
    if (!accept.checked) return setErrors({ agreePolicy: true });

    if (phone.value !== "" && !phoneValidate(phone.value)) {
      !errors && setSuccess({ email: true });
      return setErrors({ phone: t("common:pls_enter_correct_phone") });
    }

    setEmail(email.value);

    setLoading(true);
    postWithToken(REGISTER, {
      first_name: first_name.value,
      last_name: last_name.value,
      email: email.value,
      password: password.value,
      phone: phone.value,
      company_name: company_name.value,
    })
      .then((res) => {
        res.meta.code === CODE_SUCCESS
          ? setRegisterSuccess(true)
          : setErrors({ email: res.meta.message });
        setLoading(false);
      })
      .catch((err) => console.error(err));
  };

  /**
   * Handle user request resend email active
   * @param {event} e
   */
  const resendHandle = (e) => {
    e.preventDefault();
    postWithToken(RESEND_ACTIVATE, { email })
      .then((res) => {
        res.meta.code === CODE_SUCCESS && setResendSuccess(true);
      })
      .catch((err) => console.error(err));
  };

  const SuccessComponent = ({ onClick }) => {
    return (
      <div className="text-center">
        <h3 className="title-1">{t("common:register_success")}</h3>
        <p>{t("common:register_success_msg")}</p>
        <p>
          {t("common:dont_have_code_mail")}
          <Link onClick={onClick} to="/">
            {t("common:resend_email")}
          </Link>
          {resendSuccess && (
            <div className="msg-success msg-icon mt-30">
              <i className="far fa-check ico" />
              {t("loginPage:resend_email_has_send")} {email}
            </div>
          )}
        </p>
      </div>
    );
  };

  return (
    <div className="signup-page">
      <div className="row">
        <div className="col-991-57 col-767-50 slider-intro hide-767">
          <img src={logoWhite} className="logo" alt="MeWork" />
          <div className="item">
            <div className="inner text-center">
              <img className="image" src={detailTaskImg} alt="detail task" />
              <h3>Task View Details</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin
                condimentum auctor eros, eget dignissim neque blandit at. In sit
                amet molestie arcu. Praesent porttitor fringilla justo, quis
                posuere diam sodales quis. Mauris luctus ac justo vitae maximus.
              </p>
            </div>
          </div>
        </div>
        <div className="col-991-43 col-767-50 content">
          <div className="note hide-767">
            {t("common:have_account")}
            <Link className="btn btn-1 minw-115" to="/auth/login">
              {t("common:login")}
            </Link>
          </div>
          <form ref={refForm} className="signup-form">
            {registerSuccess ? (
              <SuccessComponent onClick={resendHandle} />
            ) : (
              <FormSignUp
                registerSuccess={registerSuccess}
                loading={loading}
                errors={errors}
                success={success}
                handleSubmit={handleSubmit}
              />
            )}
          </form>
        </div>
      </div>
    </div>
  );
}

export default SignUpPage;
