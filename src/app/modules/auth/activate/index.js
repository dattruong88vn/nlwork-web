import React from "react";
import { useTranslation } from "react-i18next";
import { getParameterByName } from "core/utils";
import { useLocation } from "react-router-dom";
import logo from "assets/images/logo.svg";
import { useSiteTitle } from "core/hooks/useSiteTitle";
import { Link } from "react-router-dom";
import MwLoading from "app/components/loading";
import "./styles/index.scss";
import { postWithToken } from "core/utils/APIUtils";
import { CODE_SUCCESS, REGISTER_ACTIVATE } from "app/const/Api";
import { KEY_TOKEN } from "app/const/App";

function ActivateUser() {
  const { t } = useTranslation(["common", "pageTitle"]);
  useSiteTitle(t("pageTitle:activate"));
  localStorage.removeItem(KEY_TOKEN);
  const { search } = useLocation();
  const [loading, setLoading] = React.useState(true);
  const [success, setSuccess] = React.useState(false);

  const pathRedirect = success ? "/auth/login" : "/auth/signup";

  React.useEffect(() => {
    const token = getParameterByName("token", search);
    postWithToken(REGISTER_ACTIVATE, { token })
      .then((res) => {
        if (res.meta.code === CODE_SUCCESS) {
          setSuccess(true);
        }
        setLoading(false);
      })
      .catch((err) => {
        console.error(err);
        setLoading(false);
      });
  }, [search]);

  return loading ? (
    <MwLoading />
  ) : (
    <div className="active-page">
      <div className="login-wrap">
        <div className="login-form mg-auto">
          <div className="intro mb-30 text-center">
            <div className="logo mg-auto">
              <img src={logo} alt="MeWork logo" />
            </div>
            <h1 className="title-1">{t("activate_account")}</h1>
            <p>
              {success
                ? t("activate_account_success")
                : t("activate_account_failure")}
            </p>
            <Link to={pathRedirect} className="btn btn-2 btn-block mt-30">
              {success ? t("login_now") : t("register")}
            </Link>
          </div>
        </div>
      </div>
      <div className="copyright hide-767">
        © 2020 MeWork. All rights reserved.
      </div>
    </div>
  );
}

export default ActivateUser;
