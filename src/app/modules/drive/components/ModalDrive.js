import React, {
  forwardRef,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import MwModal from "app/components/modal";
import MwButton from "app/components/button";
import MwInput from "app/components/input";
import { postFormData, putFormData } from "core/utils/APIUtils";
import { DRIVE_ADD_FOLDER, DRIVE_RENAME } from "app/const/Api";

const ModalAddFolder = forwardRef(
  ({ handleSubmitAddFolder, handleSubmitRename, projectId, groupId }, ref) => {
    const { t } = useTranslation();
    const [value, setValue] = useState("");
    const [isRename, setIsRename] = useState(false);
    const [idRename, setIdRename] = useState();
    const [defaultName, setDefaultName] = useState();
    const [titleModal, setTitleModal] = useState();
    const [typeRename, setTypeRename] = useState();
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");
    const refModalAddFolder = useRef(null);

    useImperativeHandle(ref, () => ({
      showModal,
    }));

    const handleChange = (e) => {
      e && e.preventDefault();
      setValue(e.target.value);
    };

    const handleSubmit = async () => {
      setLoading(true);
      setError(null);
      const formData = new FormData();

      formData.append("name", value);
      formData.append("project_id", projectId);
      isRename && formData.append("id", idRename);
      groupId && formData.append("group_id", groupId);

      if (value.trim()) {
        try {
          const url = isRename ? DRIVE_RENAME : DRIVE_ADD_FOLDER;
          const result = isRename
            ? await putFormData(url, formData)
            : await postFormData(url, formData);

          isRename
            ? handleSubmitRename(result.data)
            : handleSubmitAddFolder(result.data);

          setLoading(false);
          closeModal();
        } catch (error) {
          closeModal();
          console.log(error);
        }
      } else {
        setError(t("don_let_this_field_blank"));
        setLoading(false);
      }
    };

    const showModal = (data) => {
      if (data) {
        const { name, id, isRename, typeRename } = data;
        const title = typeRename === "folder" ? "rename_folder" : "rename_file";
        setTitleModal(title);
        setDefaultName(name);
        setIdRename(id);
        setTypeRename(typeRename);
        setIsRename(isRename);
      } else {
        setTitleModal("");
        setDefaultName(null);
        setIdRename(null);
        setTypeRename(null);
        setIsRename(false);
      }
      refModalAddFolder.current.showModal();
    };

    const closeModal = () => {
      setLoading(false);
      refModalAddFolder.current.hideModal();
    };

    const FooterModal = () => {
      return (
        <>
          <MwButton
            title={t("cancel")}
            onClick={closeModal}
            className="btn btn-4 w-87 fs-13 fw-normal"
            style={{ background: "transparent" }}
          />
          <MwButton
            title={isRename ? t("rename") : t("create")}
            loading={loading}
            onClick={handleSubmit}
            className="btn btn-2 w-87 fs-14 fw-bold center-2"
          />
        </>
      );
    };

    const MobileRightTitle = () => {
      return (
        <div className="form">
          <div className="d-flex btn-modal-dr">
            <MwButton
              title={isRename ? t("rename") : t("create")}
              loading={loading}
              onClick={handleSubmit}
              className="btn btn-2 w-87 fs-14 fw-bold center-2"
            />
          </div>
        </div>
      );
    };

    return (
      <MwModal
        id="add-section"
        ref={refModalAddFolder}
        title={titleModal ? t(titleModal) : t("create_folder")}
        footer={<FooterModal />}
        rightTitleMobile={<MobileRightTitle />}
        footerClass="d-flex justify-end px-4 pb-4 dp-sp-hide"
      >
        <div className="form-group mb-5">
          <MwInput
            label={typeRename === "file" ? t("name_file") : t("name_folder")}
            name="name"
            error={error}
            autoFocus
            placeholder={
              typeRename === "file" ? t("enter_name_file") : t("name_folder")
            }
            customLabel={{ className: "fs-12 mb-1" }}
            defaultValue={defaultName}
            onChange={handleChange}
          />
        </div>
      </MwModal>
    );
  }
);

ModalAddFolder.propTypes = {
  handleSubmitAddFolder: PropTypes.func,
  handleSubmitRename: PropTypes.func,
  projectId: PropTypes.string,
  groupId: PropTypes.string,
};

export default ModalAddFolder;
