import React from "react";
import PropTypes from "prop-types";
import MwCheckbox from "app/components/checkbox";
import { useTranslation } from "react-i18next";
import useClickOutside from "core/hooks/useClickOutside";
import MwButton from "app/components/button";

const listFilter = [
  {
    name: "folder",
    title: "folder",
  },
  {
    name: "pdf",
    title: "pdf",
  },
  {
    name: "image",
    title: "image",
  },
  {
    name: "document",
    title: "drive_document",
  },
];

function BoxSearchDrivePc({ hidePc, handleFilter }) {
  const { t } = useTranslation();
  const [ref, expand, setExpand] = useClickOutside(false);
  const [value, setValue] = React.useState("");
  const refFormFilter = React.useRef(null);
  const refDataFilter = React.useRef({});

  const handleExpand = (e) => {
    e && e.preventDefault();
    setExpand(!expand);
  };

  const handleSubmitFilter = (e) => {
    e && e.preventDefault();
    const data = {};
    const mime = [];

    for (let index = 0; index < refFormFilter.current.length; index++) {
      const element = refFormFilter.current[index];
      if (element.type === "checkbox" && element.checked) {
        mime.push(element.name);
      }
    }

    data["mime[]"] = mime;
    handleFilter && handleFilter({ ...data, keyword: value });
    refDataFilter.current = data;
    setExpand(false);
  };

  const handleChange = (e) => {
    e && e.preventDefault();
    setValue(e.target.value);
  };

  return (
    <div className={`box-search ${hidePc ? "dp-sp-hide" : "dp-pc-hide"}`}>
      <div className="search bg-white p-sp-2">
        <div className="drive-search has-arrow d-flex align-center">
          <form
            ref={refFormFilter}
            className="d-flex align-center flex-sp-1-0 focus"
          >
            <i className="far fa-search" />
            <input
              type="text"
              placeholder={t("find_file_in_drive")}
              className="txt-input border-grey-6"
              onChange={handleChange}
            />

            <span className="search-close dp-hide" style={{ display: "block" }}>
              <i className="fal fa-times" />
            </span>

            <div className="dropdown search-dropdown">
              <a
                href="/"
                onClick={handleExpand}
                className={`dropbtn d-flex align-center justify-center ${
                  expand && "active"
                }`}
              >
                <i className="fal fa-angle-down fs-16 fw-500 grey-1" />
                <i className="fal fa-angle-up fs-16 fw-500 grey-1" />
              </a>
              <div ref={ref} className={`dropdown-content ${expand && "show"}`}>
                {expand && (
                  <>
                    <ul className="nav hover-2 px-0 py-1 border-0">
                      {listFilter.map((item, index) => (
                        <li className="nav-item nav-link pl-3 py-2" key={index}>
                          <MwCheckbox
                            isHorizontal
                            label={t(item.title)}
                            name={item.name}
                            customLabel={{
                              className: "fw-normal",
                            }}
                            id={`drive-check-pc-${index}`}
                            defaultValue={refDataFilter?.current[
                              "mime[]"
                            ]?.includes(item.name)}
                          />
                        </li>
                      ))}
                    </ul>
                    <div className="d-flex justify-end align-center p-3">
                      <a
                        href="/"
                        onClick={handleExpand}
                        className="btn btn-4 btn-h-32 py-1 mr-2 mx-sp-1 fw-normal"
                      >
                        {t("cancel")}
                      </a>
                      <MwButton
                        title={t("save")}
                        className="btn btn-2 mx-sp-1 btn-h-32 w-87 py-1 submit"
                        onClick={handleSubmitFilter}
                      />
                    </div>
                  </>
                )}
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

BoxSearchDrivePc.propTypes = {
  hidePc: PropTypes.bool,
  handleFilter: PropTypes.func,
};

export default BoxSearchDrivePc;
