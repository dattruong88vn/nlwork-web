import React from "react";
import PropTypes from "prop-types";
import SortDrive from "./SortDrive";
import { DriveSortOptions } from "app/const/SortOptions";
import { useTranslation } from "react-i18next";
import useClickOutside from "core/hooks/useClickOutside";
import BoxSearchDrivePc from "app/modules/drive/components/BoxSearchDrivePc";
import MwInput from "app/components/input";

function FilterDrive({
  handleFilter,
  handleAddFolder,
  handleAddFile,
  handleSubmitAddFile,
}) {
  const { t } = useTranslation();
  const [refAddFolder, expanded, setExpanded] = useClickOutside(false);
  const [keyword, setKeyword] = React.useState("");
  const [filterValue, setFilterValue] = React.useState({});
  const [sortValue, setSortValue] = React.useState("");
  const refAddDropdown = React.useRef(null);
  const refHiddenInput = React.useRef(null);

  const handleSubmit = (filters) => {
    setFilterValue(filters);
    setKeyword(filters["keyword"]);
    handleFilter({ sort: sortValue, ...filters });
  };

  const handleSort = (value) => {
    setSortValue(value);
    handleFilter({ keyword, sort: value, ...filterValue });
  };

  const openModalAddFolder = (e) => {
    e && e.preventDefault();
    handleAddFolder && handleAddFolder();
  };

  const handleExpand = () => {
    setExpanded(!expanded);
  };

  const openModalGroup = () => {
    refHiddenInput.current.onClick();
  };

  const handleChange = (e) => {
    const files = e.target.files;
    const dataFiles = [];
    if (!files?.length) {
      return;
    }

    for (let index = 0; index < files.length; index++) {
      const element = files[index];
      dataFiles.push(element);
    }

    handleSubmitAddFile(dataFiles);
  };

  return (
    <div className="relative p-2 p-sp-3 d-flex justify-space-between align-center border-bottom-grey-6 bg-sp-grey-2 border-none">
      <div className="d-flex">
        <SortDrive
          sortValue={DriveSortOptions}
          handleSort={handleSort}
          activeSort={!!sortValue}
          wrapperClass="dp-sp-hide"
          isProject
        />
        <div className={`dropdown ${expanded && "overflow-unset"}`}>
          <div className="btn has-arrow btn-add-project btn-h-32 white fs-14 d-flex align-center justify-space-between">
            <div style={{ cursor: "pointer" }} onClick={openModalGroup}>
              <i className="fal fa-plus fs-16 mr-2" />
              {t("add_new")}
              <MwInput
                className="dp-hide"
                ref={refHiddenInput}
                onChange={handleChange}
                classError="left"
                type="file"
                multiple
              />
            </div>
            <span
              onClick={handleExpand}
              className={`d-flex align-center justify-center ${
                expanded && "active"
              }`}
              style={{ cursor: "pointer" }}
            >
              <i className="fal fa-angle-down" />
              <i className="fal fa-angle-up" />
            </span>
          </div>
          <div
            ref={refAddFolder}
            className={`dropdown-content ${expanded && "show"}`}
          >
            <ul ref={refAddDropdown} className="nav hover-2 px-0 py-2 border-0">
              <li className="nav-item">
                <a
                  href="/"
                  onClick={openModalAddFolder}
                  className="nav-link pl-3 py-2"
                >
                  {t("add_new_group")}
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <SortDrive
        wrapperClass="soft-drive dp-pc-hide"
        sortValue={DriveSortOptions}
        handleSort={handleSort}
        activeSort={!!sortValue}
      />
      <BoxSearchDrivePc hidePc handleFilter={handleSubmit} />
    </div>
  );
}

FilterDrive.propTypes = {
  handleFilter: PropTypes.func,
};

export default FilterDrive;
