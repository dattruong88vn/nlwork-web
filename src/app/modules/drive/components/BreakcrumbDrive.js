import React from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";

function BreakCrumbDrive({ projectId, parents = [] }) {
  const { t } = useTranslation();

  return (
    <div className="dp-sp-hide path d-flex align-center px-4 pt-3">
      <Link to={`/project-detail/drive/${projectId}`} className="grey-7">
        {t("drive")}
      </Link>
      {!!parents.length &&
        parents.map((item, index) => (
          <>
            <span className="grey-7 fs-20 mx-2">›</span>
            {index === parents.length - 1 ? (
              <span>{item.name}</span>
            ) : (
              <Link
                to={`/project-project-detail/${projectId}/${item.id}`}
                className="grey-7"
              >
                {item.name}
              </Link>
            )}
          </>
        ))}
    </div>
  );
}

BreakCrumbDrive.propTypes = {
  projectId: PropTypes.string,
  parents: PropTypes.array,
};

export default BreakCrumbDrive;
