import React from "react";
import PropTypes from "prop-types";
import ListLoading from "app/components/loading/ListLoading";
import ItemDrive from "./ItemDrive";
import EmptyList from "app/components/emptyList";
import { useTranslation } from "react-i18next";

function ListDrive({
  view,
  loading,
  projectId,
  drives,
  handleDelete,
  handleRename,
  handleMoving,
  handleOpenDetail,
}) {
  const { t } = useTranslation();

  return loading ? (
    <ListLoading />
  ) : (
    <div className="w-100">
      <div className="drive-list d-flex align-center justify-space-between fs-12 fw-bold grey-7 border-bottom-grey-6 dp-sp-hide">
        <div className="pl-4 py-2">
          {t("name")} <i className="fas fa-long-arrow-alt-down ml-1" />
        </div>
        <div className="pl-4 py-2">{t("creator")}</div>
        <div className="pl-4 py-2">{t("create_date")}</div>
        <div className="pl-4 py-2">{t("capacity")}</div>
        <div className="pr-4 py-2" />
      </div>
      {drives?.length ? (
        drives.map((drive, index) => (
          <ItemDrive
            key={index}
            handleDelete={handleDelete}
            handleRename={handleRename}
            handleMoving={handleMoving}
            handleOpenDetail={handleOpenDetail}
            projectId={projectId}
            view={view}
            {...drive}
          />
        ))
      ) : (
        <EmptyList />
      )}
    </div>
  );
}

ListDrive.propTypes = {
  view: PropTypes.string,
  loading: PropTypes.bool,
  projectId: PropTypes.string,
  drives: PropTypes.array,
  handleDelete: PropTypes.func,
  handleRename: PropTypes.func,
  handleMoving: PropTypes.func,
  handleOpenDetail: PropTypes.func,
};

export default ListDrive;
