import React from "react";
import moment from "moment-timezone";
import PropTypes from "prop-types";
import find from "lodash/find";
import MwDropDown from "app/components/dropdown";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import { formatBytes } from "core/utils";
import { FileMime } from "app/const/MimeType";
import { ReactComponent as PdfIcon } from "assets/images/pdf.svg";
import { ReactComponent as ExcelIcon } from "assets/images/exel.svg";
import { ReactComponent as DocsIcon } from "assets/images/docs.svg";
import { ReactComponent as FolderIcon } from "assets/images/drive.svg";
import { Link } from "react-router-dom";

function ItemDrive({
  id,
  handleDelete,
  handleRename,
  handleMoving,
  handleOpenDetail,
  name,
  created,
  created_by,
  size,
  mime,
  url,
  type,
  projectId,
  view,
}) {
  const { t } = useTranslation();
  const members = useSelector((state) => state.company.members);
  const [creator, setCreator] = React.useState(null);
  const [date, setDate] = React.useState(null);

  React.useEffect(() => {
    if (created_by) {
      const creatorFined = find(members, ["id", created_by]);
      setCreator(creatorFined);
    }
    created && setDate(moment.unix(+created).format("DD/MM/YYYY"));
  }, [created, created_by, members]);

  const onDelete = (e) => {
    e && e.preventDefault();
    handleDelete(id, mime === "folder");
  };

  const onRename = (e) => {
    e && e.preventDefault();
    handleRename &&
      handleRename({
        id,
        name,
        isRename: true,
        typeRename: mime === "folder" ? "folder" : "file",
      });
  };

  const onMove = (e) => {
    e && e.preventDefault();
    handleMoving && handleMoving(id);
  };

  const onCopy = (e) => {
    e && e.preventDefault();
    handleMoving && handleMoving(id, true);
  };

  const onOpenDetail = (e) => {
    if (mime !== "folder") {
      e && e.preventDefault();
      handleOpenDetail({
        type_id: id,
        url,
        name,
        created,
        created_by,
        mime,
        FileType,
        projectId,
      });
    }
  };

  const folderDropdown = [
    {
      name: "rename",
      icon: "fal fa-pen mr-2",
      onClick: onRename,
    },
    {
      name: "move_to_folder",
      icon: "fal fa-folder-upload mr-2",
      onClick: onMove,
    },
    {
      name: "delete",
      icon: "fal fa-trash-alt mr-2",
      onClick: onDelete,
    },
  ];

  const fileDropdown = [
    {
      name: "rename",
      icon: "fal fa-pen mr-2",
      onClick: onRename,
    },
    {
      name: "move_to_folder",
      icon: "fal fa-folder-upload mr-2",
      onClick: onMove,
    },
    {
      name: "copy",
      icon: "fal fa-copy mr-2",
      onClick: onCopy,
    },
    {
      name: "delete",
      icon: "fal fa-trash-alt mr-2",
      onClick: onDelete,
    },
  ];

  const ListOptionsDropdown = ({ mime }) => {
    const listDropdown = mime === "folder" ? folderDropdown : fileDropdown;
    return (
      <ul className="nav hover-2 px-0 py-2 border-0">
        {listDropdown.map((item, index) => (
          <li key={index} className="nav-item">
            <a href="/" onClick={item.onClick} className="nav-link px-3 py-2">
              <i className={item.icon} />
              {t(item.name)}
            </a>
          </li>
        ))}
      </ul>
    );
  };

  const FileType = ({ mime, size = null }) => {
    let type;

    for (const [key, value] of Object.entries(FileMime)) {
      if (value.includes(mime)) {
        type = key;
      }
    }

    switch (type) {
      case "image":
        return <img src={url} alt="" className={!size ? "w-24" : ""} />;
      case "pdf":
        return <PdfIcon {...size} />;
      case "excel":
        return <ExcelIcon {...size} />;
      case "word":
        return <DocsIcon {...size} />;
      case "folder":
        return <FolderIcon {...size} />;
      default:
        return <img src={url} alt="" className={!size ? "w-24" : ""} />;
    }
  };

  return (
    <div className="drive-list rows d-flex align-center justify-space-between border-bottom-grey-6 mx-sp-3">
      <div className="file-name d-flex align-center pl-4 py-3 pl-sp-0">
        <span className="folder" onClick={onOpenDetail}>
          <FileType mime={mime} />
        </span>
        <div className="text">
          <Link
            to={`/project-detail/${view}/${projectId}/${id}`}
            className="name fw-500 black-3 dp-block"
            onClick={onOpenDetail}
          >
            {name}
          </Link>
          <div className="dp-pc-hide d-flex align-center grey-7 fs-12 pt-sp-1">
            {creator && (
              <>
                {t("created_by")}
                <a
                  href="/"
                  onClick={(e) => e.preventDefault()}
                  className="ml-1 blue-1"
                >
                  {creator.first_name} {creator.last_name}
                </a>
                <span className="fs-11 mx-2">•</span>
                <span>{date}</span>
              </>
            )}
          </div>
        </div>
      </div>

      <div className="pl-4 py-3 dp-sp-hide">
        {creator && (
          <>
            {creator.first_name} {creator.last_name}
          </>
        )}
      </div>

      <div className="pl-4 py-3 dp-sp-hide">{date}</div>
      <div className="pl-4 py-3 dp-sp-hide">{!!size && formatBytes(size)}</div>
      <div className="pr-4 py-2 pr-sp-0 py-3 text-right">
        {type === 3 && (
          <MwDropDown activeBtnClass="grey-4 fas fa-ellipsis-v">
            <ListOptionsDropdown mime={mime} />
          </MwDropDown>
        )}
      </div>
    </div>
  );
}

ItemDrive.propTypes = {
  id: PropTypes.string,
  handleDelete: PropTypes.func,
  handleRename: PropTypes.func,
  handleMoving: PropTypes.func,
  handleOpenDetail: PropTypes.func,
  name: PropTypes.string,
  created: PropTypes.string,
  created_by: PropTypes.string,
  size: PropTypes.number,
  mime: PropTypes.string,
  url: PropTypes.string,
  type: PropTypes.number,
  projectId: PropTypes.string,
  view: PropTypes.string,
};

export default ItemDrive;
