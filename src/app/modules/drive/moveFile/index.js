import React from "react";
import _ from "lodash";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import MwModal from "app/components/modal";
import MwButton from "app/components/button";
import { fetchWithToken, putWithToken } from "core/utils/APIUtils";
import { DRIVE_COPY_FILE, DRIVE_LIST, DRIVE_MOVE } from "app/const/Api";
import SelectFolder from "./components/SelectFolder";
import "./styles/index.css";
import useToast from "core/hooks/useToast";
import { useParams } from "react-router-dom";

function findObject(arr, id) {
  let result;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].id === id) {
      result = arr[i];
      break;
    }
    if (arr[i].children) {
      result = findObject(arr[i].children, id);
      if (result) break;
    }
  }
  return result;
}

const MovingFileDrive = React.forwardRef(
  ({ defaultFolder, projectId, handleMove, handleCopy }, ref) => {
    const { folderId } = useParams();
    const { t } = useTranslation();
    const { addToast } = useToast();
    const [id, setId] = React.useState();
    const [isCopy, setIsCopy] = React.useState(false);
    const [idToMove, setIdToMove] = React.useState("root");
    const [listToBack, setListToBack] = React.useState(["root"]);
    const [loading, setLoading] = React.useState(false);
    const [folders, setFolders] = React.useState();
    const refModalMoving = React.useRef(null);
    const refStoreDrives = React.useRef(null);
    const refSelectFolder = React.useRef(null);

    const getDrive = async (id) => {
      setLoading(true);
      try {
        const data = {
          project_id: projectId,
          type_file: 3,
          parents: true,
        };

        const drive = await fetchWithToken(DRIVE_LIST, data);
        const folders = drive.data.items.filter(
          (item) => item.type === 3 && item.mime === "folder" && item.id !== id
        );
        folders.forEach((item) => (item["parent_id"] = "root"));

        const menu = [
          {
            id: "root",
            name: "root_menu",
            mime: "folder",
            children: [...folders],
          },
        ];

        setFolders(menu);
        refStoreDrives.current = menu;
        setLoading(false);
      } catch (err) {
        setLoading(false);
        console.log(err);
      }
    };

    const handleSetIdMove = (id) => {
      setIdToMove(id);
    };

    const setNew = (id, newData) => {
      const copyArr = [...folders];
      const folderNeedUpdate = findObject(copyArr);
      const folder = _.find(copyArr[0].children, folderNeedUpdate);
      folder["children"] = newData;
      setFolders([...copyArr]);
    };

    React.useImperativeHandle(ref, () => ({
      showModal,
      closeModal,
    }));

    const showModal = (id, isCopy = false) => {
      id && setId(id);
      id && getDrive(id);
      setListToBack(["root"]);
      setIsCopy(isCopy);
      refModalMoving.current.showModal();
    };

    const handleSetListToBack = (menu, isBack) => {
      if (isBack) {
        setListToBack((list) => list.slice(0, list.length - 1));
      } else {
        setListToBack((list) => [...list, menu]);
      }
    };

    const closeModal = () => {
      setListToBack(["root"]);
      setIdToMove("root");
      refModalMoving.current.hideModal();
    };

    const handleSubmit = (e, disabled) => {
      if (disabled) return;
      isCopy ? handleSubmitCopy() : handleSubmitMove();
    };

    const handleSubmitMove = async () => {
      const data = { id, to_folder_id: idToMove, project_id: projectId };
      try {
        await putWithToken(DRIVE_MOVE, data);
        closeModal();
        addToast({
          id: "moving_file",
          content: t("move_successfully"),
          placement: "right-bottom",
          timeout: 3000,
        });
        handleMove(id);
      } catch (error) {
        console.error(error);
      }
    };

    const handleSubmitCopy = async () => {
      const data = { id, to_folder_id: idToMove, project_id: projectId };
      try {
        await putWithToken(DRIVE_COPY_FILE, data);
        closeModal();
        addToast({
          id: "copy_file",
          content: t("copy_successfully"),
          placement: "right-bottom",
          timeout: 3000,
        });
        handleCopy(id, listToBack.length <= 1);
      } catch (error) {
        console.error(error);
      }
    };

    const handlePrevFolder = () => {
      refSelectFolder.current.handleBackFolder();
    };

    const FooterModal = ({ idToMove }) => {
      const disabled = idToMove === "root" && !folderId;

      return (
        <>
          <MwButton
            title={t("cancel")}
            onClick={closeModal}
            className="btn btn-4 w-87 fs-13 fw-normal"
            style={{ background: "transparent" }}
          />
          <MwButton
            title={!isCopy ? t("move") : t("paste")}
            onClick={(e) => handleSubmit(e, disabled)}
            className={`btn btn-2 w-87 fs-14 fw-bold center-2 ${
              disabled && "btn-disable"
            }`}
          />
        </>
      );
    };

    const MobileRightTitle = ({ idToMove, isCopy }) => {
      const disabled = idToMove === "root" && !folderId;
      return (
        <div className="form">
          <div className="d-flex btn-modal-dr">
            <MwButton
              title={!isCopy ? t("move") : t("paste")}
              onClick={(e) => handleSubmit(e, disabled)}
              className={`btn btn-2 w-87 fs-14 fw-bold center-2 ${
                disabled && "btn-disable"
              }`}
            />
          </div>
        </div>
      );
    };

    const TitleModal = ({ listToBack, isCopy }) => {
      return (
        <div className="moving-title">
          {listToBack.length > 1 && (
            <i
              onClick={handlePrevFolder}
              className="far fa-arrow-left fw-bold fs-16"
              style={{ cursor: "pointer" }}
            />
          )}
          {isCopy ? t("copy_to_folder") : t("move_to_folder")}
        </div>
      );
    };

    return (
      <MwModal
        id="moving-folder"
        ref={refModalMoving}
        title={<TitleModal listToBack={listToBack} isCopy={isCopy} />}
        footer={<FooterModal idToMove={idToMove} />}
        rightTitleMobile={
          <MobileRightTitle idToMove={idToMove} isCopy={isCopy} />
        }
        footerClass="d-flex justify-end px-4 pb-4 mt-4 dp-sp-hide"
      >
        <SelectFolder
          ref={refSelectFolder}
          rootMenu={folders}
          projectId={projectId}
          setNew={setNew}
          listToBack={listToBack}
          handleSetIdMove={handleSetIdMove}
          handleSetListToBack={handleSetListToBack}
          loading={loading}
        />
      </MwModal>
    );
  }
);

MovingFileDrive.propTypes = {
  defaultFolder: PropTypes.array,
  projectId: PropTypes.string,
  handleMove: PropTypes.func,
  handleCopy: PropTypes.func,
};

export default MovingFileDrive;
