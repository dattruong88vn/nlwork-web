import React from "react";
import { CSSTransition } from "react-transition-group";

function SubMenuDrive({
  children,
  id,
  activeMenu,
  projectId,
  setNew,
  dropdownItem: DropdownItem,
}) {
  const [items, setItems] = React.useState();

  React.useEffect(() => {
    setItems(children);
  }, [children]);

  return items?.length
    ? items.map((folder, index) => {
        return (
          <CSSTransition
            in={activeMenu === folder.parent_id}
            timeout={400}
            unmountOnExit
            classNames="menu-secondary"
          >
            <div className="menu">
              <DropdownItem key={index} goToMenu={folder.id}>
                {folder.name}
              </DropdownItem>
            </div>
          </CSSTransition>
        );
      })
    : null;
}

SubMenuDrive.propTypes = {};

export default SubMenuDrive;
