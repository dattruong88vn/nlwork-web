import React from "react";
import { CSSTransition } from "react-transition-group";
import { ReactComponent as FolderIcon } from "assets/images/drive.svg";
import SubMenuDrive from "./SubMenuDrive";
import { fetchWithToken } from "core/utils/APIUtils";
import { DRIVE_LIST } from "app/const/Api";
import MwButton from "app/components/button";

const SelectFolder = React.forwardRef(
  (
    {
      rootMenu,
      handleSetIdMove,
      projectId,
      setNew,
      loading,
      listToBack,
      handleSetListToBack,
    },
    ref
  ) => {
    const [loadingFetchMore, setLoadingFetchMore] = React.useState(false);
    const [listMenu, setListMenu] = React.useState(rootMenu);
    const [activeMenu, setActiveMenu] = React.useState("root");
    const refBackButton = React.useRef(null);

    React.useEffect(() => {
      setListMenu(rootMenu);
    }, [rootMenu, activeMenu]);

    React.useImperativeHandle(ref, () => ({
      handleBackFolder,
    }));

    const handleBackFolder = () => {
      handleGoToMenu(listToBack[listToBack.length - 2], true);
    };

    const getDrive = async (id) => {
      try {
        setLoadingFetchMore(true);
        const data = {
          project_id: projectId,
          folder_id: id,
          type_file: 3,
        };

        const drive = await fetchWithToken(DRIVE_LIST, data);
        const items = drive.data.items.filter((item) => item.mime === "folder");
        setNew(id, items);
        setLoadingFetchMore(false);
      } catch (err) {
        console.log(err);
        setLoadingFetchMore(false);
      }
    };

    const handleGoToMenu = (menu, isBack) => {
      refBackButton.current = activeMenu;
      setActiveMenu(menu);
      handleSetIdMove(menu);
      handleSetListToBack(menu, isBack);
      menu !== "root" && getDrive(menu);
    };

    function DropdownItem({ goToMenu, children }) {
      return (
        <div
          className="drive-list rows d-flex align-center justify-space-between border-bottom-grey-6 mx-sp-3"
          onClick={() => goToMenu && handleGoToMenu(goToMenu)}
        >
          <div
            className="file-name d-flex align-center pl-4 py-3 pl-sp-0 "
            style={{ width: "100%!important" }}
          >
            <span className="folder">
              <FolderIcon />
            </span>
            <div className="text">
              <a
                href="/"
                onClick={(e) => e.preventDefault()}
                className="name fw-500 black-3 dp-block"
              >
                {children}
              </a>
            </div>
          </div>
        </div>
      );
    }

    const ItemDropdown = ({ id, name, children }) => {
      return children?.length
        ? children.map((item, index) => (
            <>
              {activeMenu === item.parent_id ? (
                <DropdownItem key={index} goToMenu={item.id}>
                  {item.name}
                </DropdownItem>
              ) : null}

              <SubMenuDrive
                key={item.id}
                projectId={projectId}
                activeMenu={activeMenu}
                setNew={setNew}
                dropdownItem={DropdownItem}
                {...item}
              />
            </>
          ))
        : null;
    };

    return loading || loadingFetchMore ? (
      <MwButton className="btn loader-drive" loading={true} />
    ) : (
      <>
        <CSSTransition
          in={activeMenu === "root"}
          timeout={400}
          classNames="menu-primary"
        >
          {listMenu?.length && (
            <div className="menu">
              {listMenu.map((item, index) => (
                <ItemDropdown key={index} {...item} />
              ))}
            </div>
          )}
        </CSSTransition>
      </>
    );
  }
);

export default SelectFolder;
