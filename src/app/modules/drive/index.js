import React, {
  forwardRef,
  useState,
  useImperativeHandle,
  useRef,
  useEffect,
} from "react";
import findIndex from "lodash/findIndex";
import find from "lodash/find";
import PropTypes from "prop-types";
import {
  deleteWithToken,
  fetchWithToken,
  postFormData,
} from "core/utils/APIUtils";
import ListDrive from "./components/ListDrive";
import {
  DRIVE_LIST,
  DRIVE_DELETE_FILE,
  DRIVE_DELETE_FOLDER,
  DRIVE_ADD_FILE,
} from "app/const/Api";
import FilterDrive from "./components/FilterDrive";
import ModalDrive from "./components/ModalDrive";
import MovingFileDrive from "./moveFile";
import DetailDrive from "./detail";
import BreakCrumbDrive from "./components/BreakcrumbDrive";

const ProjectDrive = forwardRef(
  ({ projectId, folderId, typeFile, view }, ref) => {
    const [drives, setDrives] = useState([]);
    const [parents, setParents] = useState([]);
    const [loading, setLoading] = useState(true);
    const [hasOpenDetail, setHasOpenDetail] = useState(false);
    const refModalDrive = useRef(null);
    const refFilterDrive = useRef([]);
    const refModalMoving = useRef(null);
    const refDetailDrive = useRef(null);

    useImperativeHandle(ref, () => ({
      handleFilter,
    }));

    useEffect(() => {
      const getDrive = async () => {
        try {
          const dataFetch = {
            project_id: projectId,
            folder_id: folderId,
            parents: true,
          };

          const drive = await fetchWithToken(DRIVE_LIST, dataFetch);
          setDrives(drive.data.items);
          refFilterDrive.current = drive.data.items;
          setParents(drive.data.parents);
          setLoading(false);
        } catch (err) {
          setLoading(false);
          console.log(err);
        }
      };

      getDrive();
    }, [projectId, folderId, typeFile]);

    const handleSubmitAddFile = async (data) => {
      try {
        const formData = new FormData();
        data.forEach((element) => {
          formData.append("files[]", element);
        });
        formData.append("project_id", projectId);

        const fileAdded = await postFormData(DRIVE_ADD_FILE, formData);
        const indexAdd = findIndex(drives, (drive) => drive.mime !== "folder");
        const newData = [...drives];
        newData.splice(indexAdd, 0, ...fileAdded.data);
        setDrives(newData);
        refFilterDrive.current = newData;
      } catch (error) {
        console.log(error);
      }
    };

    const handleFilter = async (filters) => {
      try {
        const drive = await fetchWithToken(DRIVE_LIST, {
          project_id: projectId,
          ...filters,
        });
        setDrives([...drive.data.items]);
        refFilterDrive.current = [...drive.data.items];
      } catch (err) {
        console.error(err);
      }
    };

    const handleDelete = async (id, isFolder) => {
      try {
        const url = isFolder ? DRIVE_DELETE_FOLDER : DRIVE_DELETE_FILE;
        const filteredItem = drives.filter((drive) => drive.id !== id);

        await deleteWithToken(url, { id });
        setDrives(filteredItem);
        refDetailDrive.current.hideModal();
      } catch (error) {
        console.log(error);
      }
    };

    const handleMove = (id) => {
      const newData = drives.filter((item) => item.id !== id);
      setDrives(newData);
    };

    const handleCopy = (id, isRoot) => {
      if (isRoot) {
        const element = find(drives, ["id", id]);
        setDrives((drives) => [...drives, element]);
      }
    };

    const handleAddFolder = () => {
      refModalDrive.current.showModal();
    };

    const handleRename = (data) => {
      refModalDrive.current.showModal(data);
    };

    const handleMoving = (data, isCopy) => {
      refModalMoving.current.showModal(data, isCopy);
    };

    const handleSubmitAddFolder = (newDriveFolder) => {
      const indexAdd = findIndex(drives, (drive) => drive.type === 3);
      const newData = [...drives];
      newData.splice(indexAdd, 0, newDriveFolder);
      setDrives(newData);
    };

    const handleSubmitRename = (data) => {
      const arrCopied = [...drives];
      const index = findIndex(arrCopied, (item) => item.id === data.id);
      arrCopied[index] = data;
      setDrives(arrCopied);
      hasOpenDetail && refDetailDrive.current.handleRenameSuccess(data?.name);
    };

    const handleSearch = (value) => {
      const filteredList = refFilterDrive.current.filter((drive) => {
        const valueLower = value.toLowerCase();
        const driveLower = drive.name.toLowerCase();
        return driveLower.includes(valueLower);
      });
      setDrives(filteredList);
    };

    const handleOpenDetail = (data) => {
      setHasOpenDetail(true);
      refDetailDrive.current.showModal(data);
    };

    return (
      <>
        <ModalDrive
          ref={refModalDrive}
          handleSubmitAddFolder={handleSubmitAddFolder}
          handleSubmitRename={handleSubmitRename}
          projectId={projectId}
        />

        <MovingFileDrive
          ref={refModalMoving}
          defaultFolder={drives}
          folderId={folderId}
          projectId={projectId}
          handleMove={handleMove}
          handleCopy={handleCopy}
        />

        <DetailDrive
          ref={refDetailDrive}
          handleRename={handleRename}
          handleDelete={handleDelete}
          handleMoving={handleMoving}
          handleCopy={handleCopy}
        />

        <FilterDrive
          handleFilter={handleFilter}
          handleAddFolder={handleAddFolder}
          handleSubmitAddFile={handleSubmitAddFile}
          handleSearch={handleSearch}
        />
        <BreakCrumbDrive parents={parents} projectId={projectId} />
        <ListDrive
          projectId={projectId}
          loading={loading}
          drives={drives}
          handleRename={handleRename}
          handleDelete={handleDelete}
          handleMoving={handleMoving}
          handleOpenDetail={handleOpenDetail}
          view={view}
        />
      </>
    );
  }
);

ProjectDrive.propTypes = {
  projectId: PropTypes.string,
  folderId: PropTypes.string,
  typeFile: PropTypes.string,
  view: PropTypes.string,
};

export default ProjectDrive;
