import React from "react";
import PropTypes from "prop-types";
import "emoji-mart/css/emoji-mart.css";
import { Picker } from "emoji-mart";
import MwButton from "app/components/button";
import { useTranslation } from "react-i18next";

const BoxCommentDriveDetail = React.forwardRef(
  (
    {
      comment,
      handleChange,
      handleSubmit,
      isUpdate,
      handleClearUpdate,
      loading,
      wrapperClass,
      isMobile,
    },
    ref
  ) => {
    const { t } = useTranslation();
    const [visibleEmoji, setVisibleEmoji] = React.useState(false);

    const onSubmit = (e) => {
      e && e.preventDefault();
      handleSubmit && handleSubmit();
    };

    const onChange = (e) => {
      handleChange && handleChange(e);
    };

    const addEmoji = ({ native }) => {
      handleChange && handleChange(native, true);
    };

    const onOpenEmojiPanel = () => {
      setVisibleEmoji(!visibleEmoji);
    };

    return (
      <div className={wrapperClass}>
        {isMobile ? (
          <div className="pl-sp-4 relative">
            <textarea
              ref={ref}
              className="py-1 w-100"
              placeholder={t("write_comment")}
              value={comment}
              onChange={onChange}
            />

            <a
              href="/"
              onClick={(e) => {
                e.preventDefault();
                onOpenEmojiPanel();
              }}
              className="absolute at wrapper-emoji"
            >
              <i className="far fa-smile grey-7" />
              {visibleEmoji && <Picker onSelect={addEmoji} />}
            </a>

            <a
              href="/"
              onClick={onSubmit}
              className="absolute click-cmt bg-blue-1 white border-rad5 px-4 py-1"
            >
              {isUpdate ? t("update") : t("send")}
            </a>
          </div>
        ) : (
          <>
            <div className="text-area">
              <textarea
                ref={ref}
                className="border-rad5"
                placeholder={t("write_comment")}
                name="content"
                value={comment}
                onChange={onChange}
              />
              <span
                onClick={handleClearUpdate}
                className={`search-close ${isUpdate && "active"}`}
              >
                <i className="fal fa-times" />
              </span>
            </div>

            <a
              href="/"
              onClick={(e) => {
                e.preventDefault();
                onOpenEmojiPanel();
              }}
              className="absolute at wrapper-emoji"
            >
              <i className="far fa-smile grey-7" />
              {visibleEmoji && <Picker onSelect={addEmoji} />}
            </a>

            <MwButton
              title={isUpdate ? t("update") : t("send")}
              className="absolute click-cmt bg-blue-1 white border-rad5 px-4 py-1 center-2"
              onClick={onSubmit}
              loading={loading}
            />
          </>
        )}
      </div>
    );
  }
);

BoxCommentDriveDetail.propTypes = {
  comment: PropTypes.string,
  handleChange: PropTypes.func,
  handleSubmit: PropTypes.func,
  wrapperClass: PropTypes.string,
  isMobile: PropTypes.bool,
};

BoxCommentDriveDetail.defaultProps = {
  comment: "",
  handleChange: null,
  handleSubmit: null,
  wrapperClass: "box-comment border-top-grey-2 px-4 py-2 relative",
  isMobile: false,
};

export default BoxCommentDriveDetail;
