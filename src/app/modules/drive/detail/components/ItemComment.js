import React from "react";
import find from "lodash/find";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import useTimeAgo from "core/hooks/useTimeAgo";
import MwDropDown from "app/components/dropdown";
import { useTranslation } from "react-i18next";

function ItemComment({
  content,
  created_by,
  created,
  handleEdit,
  handleDelete,
  attachments = [],
  id,
  idUpdate,
}) {
  const { t } = useTranslation();
  const members = useSelector((state) => state.company.members);
  const idUser = useSelector((state) => state.user.userInfo.id);
  const user = find(members, ["id", created_by]);
  const isOwner = user.id === idUser;

  const timeAgo = useTimeAgo(created);

  const onEdit = (e) => {
    handleEdit && handleEdit(e, content, id);
  };

  const onDelete = (e) => {
    handleDelete && handleDelete(e, id);
  };

  const dropdownList = [
    {
      name: "edit",
      onClick: onEdit,
      icon: "far fa-pen mr-2",
    },
    {
      name: "delete",
      onClick: onDelete,
      icon: "fal fa-trash-alt mr-2",
      aClass: "red",
    },
  ];

  return (
    <div
      className={`box-chat-${
        isOwner ? "right" : "left"
      } d-flex align-top justify-${isOwner ? "end" : "start"} p-4 ${
        idUpdate === id && "editing"
      }`}
    >
      <span className="avt">
        {!isOwner && (
          <img src={user?.avatar} alt="" className="w-30 border-circle" />
        )}

        {isOwner && (
          <MwDropDown activeBtnClass="grey-4 fas fa-ellipsis-v">
            <ul className="nav hover-2 px-0 py-2 border-0">
              {dropdownList.map((item, index) => (
                <li key={index} className="nav-item">
                  <a
                    href="/"
                    onClick={item.onClick}
                    className={`nav-link px-3 py-2 ${item?.aClass}`}
                  >
                    <i className={item.icon} />
                    {t(item.name)}
                  </a>
                </li>
              ))}
            </ul>
          </MwDropDown>
        )}
      </span>
      <div className={`conts ${isOwner ? "d-flex justify-end" : ""}`}>
        {!isOwner && (
          <>
            <div className={`fw-bold ${isOwner ? "text-right" : ""}`}>
              {`${user?.first_name} ${user?.last_name}`}
              <span className="fs-12 fw-normal grey-7 ml-2">{timeAgo}</span>
            </div>
            <div className="message word-break bg-grey-8 p-2 mt-1 fs-14 border-rad5">
              {content}
              {!!attachments.length &&
                attachments.map((at) => (
                  <img key={at.id} src={at.url} alt="attachments" />
                ))}
            </div>
          </>
        )}
        {isOwner && (
          <div className="cont-child">
            <div className={`fw-bold ${isOwner ? "text-right" : ""}`}>
              <span className="fs-12 fw-normal grey-7 mr-2">{timeAgo}</span>
              {`${user?.first_name} ${user?.last_name}`}
            </div>
            <div className="message word-break bg-grey-8 p-2 mt-1 fs-14 border-rad5">
              {content}
              {!!attachments.length &&
                attachments.map((at) => (
                  <img key={at.id} src={at.url} alt="attachments" />
                ))}
            </div>
          </div>
        )}
      </div>
      <span className="avt text-right">
        {isOwner && (
          <img src={user?.avatar} alt="" className="w-30 border-circle" />
        )}
      </span>
    </div>
  );
}

ItemComment.propTypes = {
  content: PropTypes.string,
  created_by: PropTypes.string,
  created: PropTypes.string,
  handleEdit: PropTypes.func,
  handleDelete: PropTypes.func,
  attachments: PropTypes.array,
  id: PropTypes.string,
};

ItemComment.defaultProps = {
  handleEdit: null,
  handleDelete: null,
  id: null,
};

export default ItemComment;
