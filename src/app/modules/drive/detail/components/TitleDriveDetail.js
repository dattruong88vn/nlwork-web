import React from "react";
import find from "lodash/find";
import PropTypes from "prop-types";
import MwDropDown from "app/components/dropdown";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import useTimeAgo from "core/hooks/useTimeAgo";
import { FileMime } from "app/const/MimeType";

function TitleDriveDetail({
  id,
  name,
  hideModal,
  createdBy,
  created,
  url,
  handleOpenMobile,
  handleRename,
  handleMoving,
  handleDelete,
  handleCopy,
  mime,
}) {
  const { t } = useTranslation();
  const members = useSelector((state) => state.company.members);
  const user = find(members, ["id", createdBy]);
  const timeAgo = useTimeAgo(created);

  const dropdownOptions = [
    {
      name: "rename",
      icon: "fal fa-pen mr-2",
      onClick: (e) => {
        e.preventDefault();
        handleRename &&
          handleRename({
            id,
            name,
            isRename: true,
            typeRename: "file",
          });
      },
    },
    {
      name: "move_to_folder",
      icon: "fal fa-folder-upload mr-2",
      onClick: (e) => {
        e && e.preventDefault();
        handleMoving && handleMoving(id);
      },
    },
    {
      name: "copy",
      icon: "fal fa-copy mr-2",
      onClick: (e) => {
        e && e.preventDefault();
        handleMoving && handleMoving(id, true);
      },
    },
    {
      name: "delete",
      icon: "fal fa-trash-alt mr-2",
      aClass: "red",
      onClick: (e) => {
        e && e.preventDefault();
        handleDelete && handleDelete(id);
      },
    },
  ];

  const FileType = ({ mime }) => {
    let type;

    for (const [key, value] of Object.entries(FileMime)) {
      if (value.includes(mime)) {
        type = key;
      }
    }
    return type === "image";
  };

  return (
    <>
      <div className="d-flex align-center">
        <div className="btn-back" onClick={hideModal} title={t("back")}>
          <i className="far fa-arrow-left fs-17 fs-sp-19" />
        </div>
        <div className="fs-18 fw-500 dp-pc-hide ml-4">{name}</div>
      </div>
      <div className="fs-16 fw-500 dp-sp-hide">
        {name}
        <div className="grey-7 fs-12 fw-normal">
          {t("created_by")}{" "}
          <span className="blue-1">{`${user.first_name} ${user.last_name}`}</span>{" "}
          {timeAgo}
        </div>
      </div>
      <div className="d-flex align-end justify-end">
        {FileType({ mime }) && (
          <a
            href={url}
            target="_blank"
            rel="noopener noreferrer"
            className="mr-5 dp-sp-hide"
            title={t("open_in_new_tab")}
          >
            <i className="far fa-external-link-alt fs-14" />
          </a>
        )}

        <a
          href={window.URL.createObjectURL(new Blob([url]))}
          target="_blank"
          rel="noopener noreferrer"
          download={name}
          className="mr-5 dp-sp-hide"
          title={t("download")}
        >
          <i className="far fa-arrow-to-bottom fs-16" />
        </a>
        <a href="/" onClick={handleOpenMobile} className="mr-5 dp-pc-hide">
          <i className="far fa-comment-alt fs-16" />
        </a>
        <MwDropDown activeBtnClass="fw-300 fas fa-ellipsis-v titleDrive">
          <ul className="nav hover-2 px-0 py-2 border-0">
            {dropdownOptions.map((item, index) => (
              <li key={index} className="nav-item">
                <a
                  href="/"
                  onClick={item.onClick}
                  className={`nav-link px-1 py-2 ${item.aClass}`}
                >
                  <i className={item.icon} />
                  {t(item.name)}
                </a>
              </li>
            ))}
          </ul>
        </MwDropDown>
      </div>
    </>
  );
}

TitleDriveDetail.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  hideModal: PropTypes.func,
  createdBy: PropTypes.string,
  created: PropTypes.string,
  url: PropTypes.string,
  handleOpenMobile: PropTypes.func,
  handleRename: PropTypes.func,
  handleMoving: PropTypes.func,
  handleDelete: PropTypes.func,
  handleCopy: PropTypes.func,
};

export default TitleDriveDetail;
