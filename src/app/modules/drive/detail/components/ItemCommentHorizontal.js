import React from "react";
import find from "lodash/find";
import PropTypes from "prop-types";
import useTimeAgo from "core/hooks/useTimeAgo";
import { useTranslation } from "react-i18next";
import MwDropDown from "app/components/dropdown";
import { useSelector } from "react-redux";

function ItemCommentHorizontal({
  content,
  created_by,
  created,
  handleEdit,
  handleDelete,
  attachments = [],
  id,
}) {
  const { t } = useTranslation();
  const timeAgo = useTimeAgo(created);
  const members = useSelector((state) => state.company.members);
  const idUser = useSelector((state) => state.user.userInfo.id);
  const user = find(members, ["id", created_by]);
  const isOwner = user.id === idUser;

  const onEdit = (e) => {
    handleEdit && handleEdit(e, content, id);
  };

  const onDelete = (e) => {
    handleDelete && handleDelete(e, id);
  };

  const dropdownList = [
    {
      name: "edit",
      onClick: onEdit,
      icon: "far fa-pen mr-2",
    },
    {
      name: "delete",
      onClick: onDelete,
      icon: "fal fa-trash-alt mr-2",
      aClass: "red",
    },
  ];

  return (
    <div className="box d-flex align-top justify-space-between mt-4">
      <div className="conts d-flex align-top">
        <span className="avatar">
          <img src={user?.avatar} alt="" className="img-circle w-30" />
        </span>
        <div className="comment-info">
          <div>
            <span className="fs-13 fw-bold name">{`${user?.first_name} ${user?.last_name}`}</span>
            <span className="fs-12 grey-7 ml-3">{timeAgo}</span>
          </div>
          <div className="fs-13 mt-1 info-line">{content}</div>
        </div>
      </div>
      {isOwner && (
        <MwDropDown activeBtnClass="grey-4 fas fa-ellipsis-v">
          <ul className="nav hover-2 px-0 py-2 border-0">
            {dropdownList.map((item, index) => (
              <li key={index} className="nav-item">
                <a
                  href="/"
                  onClick={item.onClick}
                  className={`nav-link px-3 py-2 ${item?.aClass}`}
                >
                  <i className={item.icon} />
                  {t(item.name)}
                </a>
              </li>
            ))}
          </ul>
        </MwDropDown>
      )}
    </div>
  );
}

ItemCommentHorizontal.propTypes = {
  content: PropTypes.string,
  created_by: PropTypes.string,
  created: PropTypes.string,
  handleEdit: PropTypes.func,
  handleDelete: PropTypes.func,
  attachments: PropTypes.array,
  id: PropTypes.string,
};

export default ItemCommentHorizontal;
