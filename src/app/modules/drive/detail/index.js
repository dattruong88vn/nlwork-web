import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import findIndex from "lodash/findIndex";
import PropTypes from "prop-types";
import MwModal from "app/components/modal";
import TitleDriveDetail from "./components/TitleDriveDetail";
import BoxCommentDriveDetail from "./components/BoxCommentDriveDetail";
import {
  deleteWithToken,
  fetchWithToken,
  putFormData,
} from "core/utils/APIUtils";
import {
  COMMENT_LIST,
  DRIVE_ADD_COMMENT,
  COMMENT_DELETE,
  CODE_SUCCESS,
  COMMENT_UPDATE,
} from "app/const/Api";
import ItemComment from "./components/ItemComment";
import ListLoading from "app/components/loading/ListLoading";
import { postFormData } from "core/utils/APIUtils";
import EmptyList from "app/components/emptyList";
import { useTranslation } from "react-i18next";
import ItemCommentHorizontal from "./components/ItemCommentHorizontal";

const SIZE_FILE = {
  width: 150,
  height: 150,
};

const DetailDrive = forwardRef(
  ({ handleRename, handleMoving, handleDelete, handleCopy, id }, ref) => {
    const { t } = useTranslation();
    const [totalComment, setTotalComment] = useState(null);
    const [currentOffset, setCurrentOffset] = useState(0);
    const [visible, setVisible] = useState(false);
    const [isUpdate, setIsUpdate] = useState(false);
    const [comments, setComments] = useState(null);
    const [comment, setComment] = useState();
    const [dataDetail, setDataDetail] = useState(null);
    const [loading, setLoading] = useState(true);
    const [loadingAction, setLoadingAction] = useState(false);
    const [idUpdate, setIdUpdate] = useState(null);
    const refModalDetail = useRef(null);
    const refBoxComment = useRef(null);

    useEffect(() => {
      const getListComment = async () => {
        try {
          const comments = await fetchWithToken(COMMENT_LIST, {
            type_id: dataDetail?.type_id,
            type: 3,
            limit: 15,
            offset: 0,
            total: true,
          });
          setComments(
            comments.data.items.sort((a, b) => a.created - b.created)
          );
          setTotalComment(comments.data.total);
          setLoading(false);
        } catch (error) {
          console.log(error);
        }
      };

      dataDetail?.type_id && getListComment();

      return () => {
        setLoading(true);
        setComments([]);
        setCurrentOffset(0);
        setTotalComment(0);
      };
    }, [dataDetail]);

    useImperativeHandle(ref, () => ({
      showModal,
      hideModal,
      handleRenameSuccess,
    }));

    const handleFetchMore = async () => {
      try {
        const result = await fetchWithToken(COMMENT_LIST, {
          type_id: dataDetail?.type_id,
          type: 3,
          limit: 15,
          offset: currentOffset + 15,
          total: true,
        });
        setCurrentOffset((currentOffset) => currentOffset + 15);
        const newComments = result.data.items.sort(
          (a, b) => a.created - b.created
        );
        setComments([...newComments, ...comments]);
      } catch (error) {
        console.log(error);
      }
    };

    const handleOpenMobile = (e) => {
      e && e.preventDefault();
      setVisible(!visible);
    };

    const showModal = (data) => {
      data && setDataDetail(data);
      refModalDetail.current.showModal();
    };

    const hideModal = () => {
      setComments(null);
      refModalDetail.current.hideModal();
    };

    const handleAddEmoji = (emoji) => {
      setComment((comment) => `${comment} ${emoji}`);
    };

    const handleSubmit = (e) => {
      e && e.preventDefault();
      if (!comment) return;
      setLoadingAction(true);

      if (isUpdate) {
        const formData = new FormData();
        formData.append("id", idUpdate);
        formData.append("content", comment);

        putFormData(COMMENT_UPDATE, formData)
          .then((res) => {
            if (res.meta.code === CODE_SUCCESS) {
              const newArr = [...comments];
              const index = findIndex(newArr, ["id", idUpdate]);
              newArr[index]["content"] = comment;
              setComments(newArr);
            }

            setLoadingAction(false);
            setIsUpdate(false);
          })
          .catch((err) => {
            console.error("err", err);
            setLoadingAction(false);
          });
      } else {
        const { projectId, type_id, type = 3 } = dataDetail;

        const formData = new FormData();
        formData.append("project_id", projectId);
        formData.append("type_id", type_id);
        formData.append("content", comment);
        formData.append("type", type);

        postFormData(DRIVE_ADD_COMMENT, formData)
          .then((res) => {
            res.meta.code === CODE_SUCCESS &&
              setComments([...comments, res.data]);
            setLoadingAction(false);
          })
          .catch((err) => {
            console.error("err", err);
            setLoadingAction(false);
          });
      }

      setComment("");
    };

    const handleRenameSuccess = (name) => {
      setDataDetail((dataDetail) => ({ ...dataDetail, name }));
    };

    const handleChange = (e, isAddEmoji) => {
      !isAddEmoji && e && e.preventDefault();
      if (isAddEmoji) {
        setComment(`${comment || ""} ${e}`);
      } else {
        setComment(e.target.value);
      }
    };

    const handleDeleteComment = (e, id) => {
      e && e.preventDefault();

      deleteWithToken(COMMENT_DELETE, { id })
        .then((res) => {
          res.meta.code === CODE_SUCCESS &&
            setComments((comments) =>
              comments.filter((item) => item.id !== id)
            );
        })
        .catch((err) => console.error(err));
    };

    const handleEdit = (e, content, id) => {
      e && e.preventDefault();
      setIsUpdate(true);
      setComment(content);
      setIdUpdate(id);
      refBoxComment.current.focus();
    };

    const handleClearUpdate = () => {
      setComment("");
      setIdUpdate("");
      setIsUpdate(false);
    };

    return (
      <MwModal
        ref={refModalDetail}
        id="view-attachment"
        wrapperClass="modal modal-full"
        title={
          <TitleDriveDetail
            hideModal={hideModal}
            name={dataDetail?.name}
            createdBy={dataDetail?.created_by}
            created={dataDetail?.created}
            url={dataDetail?.url}
            mime={dataDetail?.mime}
            handleOpenMobile={handleOpenMobile}
            handleRename={handleRename}
            handleDelete={handleDelete}
            handleMoving={handleMoving}
            handleCopy={handleCopy}
            id={dataDetail?.type_id}
          />
        }
        contentClass="modal-content p-0 scroll"
        headClass=" d-flex align-center justify-space-between"
        bodyClass="modal-body p-0"
        backTitle={false}
        footerClass="p-0"
      >
        <div className="d-flex align-top">
          <div className="view-drive">
            <div className="attachment d-flex align-center justify-center p-4">
              {dataDetail?.FileType({
                mime: dataDetail?.mime,
                size: { ...SIZE_FILE },
              })}
            </div>
          </div>
          <div className="convers-drive dp-sp-hide">
            {comments?.length < totalComment && (
              <a
                href="/"
                onClick={(e) => {
                  e.preventDefault();
                  handleFetchMore();
                }}
                className="convers__more"
              >
                {t("see_more")}
              </a>
            )}
            <div
              className={`box-chat scroll ${
                comments?.length < totalComment && "full-height"
              }`}
            >
              {loading ? (
                <ListLoading />
              ) : comments?.length ? (
                comments.map((comment, index) => (
                  <ItemComment
                    key={index}
                    handleEdit={handleEdit}
                    handleDelete={handleDeleteComment}
                    idUpdate={idUpdate}
                    {...comment}
                  />
                ))
              ) : (
                <EmptyList />
              )}
            </div>
            <BoxCommentDriveDetail
              ref={refBoxComment}
              isUpdate={isUpdate}
              comment={comment}
              handleSubmit={handleSubmit}
              handleChange={handleChange}
              loading={loadingAction}
              handleClearUpdate={handleClearUpdate}
            />
          </div>
        </div>

        {/* Bottom display in mobile */}
        <div className="convers-drive dp-pc-hide add-job">
          <a
            href="/"
            onClick={handleOpenMobile}
            className="dp-block px-5 py-4 black-3 fs-15 fw-bold bg-white w-100"
            style={{ display: visible ? "none" : "block" }}
          >
            {t("comment")} {!!totalComment && `(${totalComment})`}
          </a>
          <div
            className={`add-job-field ${visible && "active"}`}
            style={{ display: visible ? "block" : "none" }}
          >
            <div className="mobile-convers-head">
              <div className="d-flex align-center justify-space-between">
                <span className="fs-15 fw-bold">
                  {t("comment")} {!!totalComment && `(${totalComment})`}
                </span>
                <a href="/" onClick={handleOpenMobile}>
                  <i className="far fa-times grey-1 fs-15" />
                </a>
              </div>
              {comments?.length < totalComment && (
                <a
                  href="/"
                  onClick={(e) => {
                    e.preventDefault();
                    handleFetchMore();
                  }}
                  className="convers__more"
                >
                  {t("see_more")}
                </a>
              )}
            </div>
            <div
              className={`convers-drive-scroll p-sp-3 bg-white ${
                comments?.length < totalComment && "full-height"
              }`}
            >
              {loading ? (
                <ListLoading />
              ) : comments?.length ? (
                comments.map((comment, index) => (
                  <ItemCommentHorizontal
                    key={index}
                    handleEdit={handleEdit}
                    handleDelete={handleDeleteComment}
                    {...comment}
                  />
                ))
              ) : (
                <EmptyList />
              )}
            </div>
            <BoxCommentDriveDetail
              ref={refBoxComment}
              wrapperClass="box-comment border-top-grey-9 w-100 bg-white"
              isUpdate={isUpdate}
              comment={comment}
              handleSubmit={handleSubmit}
              handleChange={handleChange}
              loading={loadingAction}
              handleClearUpdate={handleClearUpdate}
              handleAddEmoji={handleAddEmoji}
              isMobile
            />
          </div>
        </div>
      </MwModal>
    );
  }
);

DetailDrive.propTypes = {
  handleRename: PropTypes.func,
  handleMoving: PropTypes.func,
  handleDelete: PropTypes.func,
  handleCopy: PropTypes.func,
  id: PropTypes.string,
};

export default DetailDrive;
